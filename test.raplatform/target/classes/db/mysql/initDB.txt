create table if not exists user (
  id varchar(10) not null primary key,
  password varchar(50) not null,
  name varchar(50) not null,
  division int not null,
  last_change_passwd_time datetime,
  lock_time datetime,
  usercompany varchar(20) default '1'
) engine=innodb;

create table if not exists authorities (
  username varchar(10) not null,
  authority varchar(50) not null,
  foreign key (username) references user(id),
  unique (username, authority)
) engine=innodb;

create table if not exists countries (
  code varchar(2) not null primary key,
  name varchar(30) not null
) engine=innodb;

create table if not exists regions (
  country varchar(2) not null,
  region_code varchar(3) not null,
  name varchar(30) not null,
  unique (country, region_code)
) engine=innodb;

create table if not exists evaluation_category (
  id bigint(8) unsigned not null auto_increment primary key,
  kind int not null,
  max_point bigint(8) not null,
  weight double not null,
  index(kind)
) engine=innodb;

create table if not exists evaluation_category_name (
  id bigint(8) unsigned not null auto_increment primary key,
  category_id bigint(8) not null,
  name varchar(100) not null,
  locale varchar(5) not null,
  unique(category_id, locale)
) engine=innodb;

create table if not exists evaluation_content (
  id bigint(8) unsigned not null auto_increment primary key,
  category_id bigint(8) not null,
  index(category_id)
) engine=innodb;

create table if not exists evaluation_content_content (
  id bigint(8) unsigned not null auto_increment primary key,
  content_id bigint(8) not null,
  content varchar(100) not null,
  locale varchar(5) not null,
  unique(content_id, locale)
) engine=innodb;


create table if not exists evaluation_item (
  id bigint(8) unsigned not null auto_increment primary key,
  content_id bigint(8) not null,
  points varchar(256) not null,
  weight double not null,
  index(content_id)
) engine=innodb;

create table if not exists evaluation_item_evaluation_standard (
  id bigint(8) unsigned not null auto_increment primary key,
  item_id bigint(8) not null,
  content varchar(512) not null,
  locale varchar(5) not null,
  index(item_id)
) engine=innodb;

create table if not exists evaluation_item_grading_standard (
  id bigint(8) unsigned not null auto_increment primary key,
  item_id bigint(8) not null,
  content varchar(512) not null,
  locale varchar(5) not null,
  index(item_id)
) engine=innodb;

create table if not exists vendor_basic_info (
  id bigint(8) unsigned not null auto_increment primary key,
  tran_id varchar(36) not null,
  vendor_code varchar(10),
  vendor_type int,
  vendor_chinese_name varchar(100),
  vendor_english_name varchar(100),
  country varchar(2),
  region varchar(3),
  city   varchar(100),
  address varchar(200),
  tel varchar(30),
  postal_code varchar(20),
  person varchar(100),
  homepage varchar(100),
  fax varchar(50),
  business_licence_id varchar(50),
  business_licence_expiration_date date,
  legal_representative varchar(50),
  registration_capital varchar(50),
  registration_address varchar(100),
  registration_date date,
  state_tax_reg_id varchar(50),
  state_tax_reg_expiration_date date,
  local_tax_reg_id varchar(50),
  local_tax_reg_expiration_date date,
  cert_iso9000 boolean,
  cert_iso14000 boolean,
  cert_qs9000 boolean,
  cert_other varchar(200),
  listed_company boolean,
  reg_date_and_address varchar(50),
  cooperation_date_or_period varchar(50),
  have_transaction boolean,
  domestic_investment int,
  foreign_investment int,
  foreign_investment_other varchar(100),
  joint_venture_investment_ratio varchar(100),
  business_type int,
  production text,
  business_relation varchar(100),
  payment_term varchar(50),
  lead_time varchar(50),
  minimum_lot varchar(10),
  delivery_types integer,
  delivery_types_other_value VARCHAR(50),
  date varchar(10),
  number_of_direct_staffs bigint(8),
  number_of_indirect_staffs bigint(8),
  total_staffs bigint(8),
  special_statement text,
  company_code varchar(10),
  vendor_account_type varchar(50),
  vendor_group varchar(10),
  group_key varchar(50),
  recon_account varchar(10),
  payment_term_for_accounting int,
  chk_double_inv boolean,
  registered_date datetime not null,
  status int not null,
  number varchar(12),
  applicant varchar(100),
  applicant_name varchar(50),
  super_company varchar(10),
  freeze varchar(10),
  action_time datetime,
  index(vendor_chinese_name, vendor_english_name, vendor_code, tran_id, status)
) engine=innodb;

create table if not exists tmp_vendor_basic_info (
  id bigint(8) unsigned not null primary key,
  tran_id varchar(36) not null,
  vendor_code varchar(10),
  vendor_type int,
  vendor_chinese_name varchar(100),
  vendor_english_name varchar(100),
  country varchar(2),
  region varchar(3),
  city   varchar(100),
  address varchar(200),
  tel varchar(30),
  postal_code varchar(20),
  person varchar(100),
  homepage varchar(100),
  fax varchar(50),
  business_licence_id varchar(50),
  business_licence_expiration_date date,
  legal_representative varchar(50),
  registration_capital varchar(50),
  registration_address varchar(100),
  registration_date date,
  state_tax_reg_id varchar(50),
  state_tax_reg_expiration_date date,
  local_tax_reg_id varchar(50),
  local_tax_reg_expiration_date date,
  cert_iso9000 boolean,
  cert_iso14000 boolean,
  cert_qs9000 boolean,
  cert_other varchar(200),
  listed_company boolean,
  reg_date_and_address varchar(50),
  cooperation_date_or_period varchar(50),
  have_transaction boolean,
  domestic_investment int,
  foreign_investment int,
  foreign_investment_other varchar(100),
  joint_venture_investment_ratio varchar(100),
  business_type int,
  production text,
  business_relation varchar(100),  
  payment_term varchar(50),
  lead_time varchar(50),
  minimum_lot varchar(10),
  delivery_types integer,
  delivery_types_other_value VARCHAR(50),
  date varchar(10),
  number_of_direct_staffs bigint(8),
  number_of_indirect_staffs bigint(8),
  total_staffs bigint(8),
  special_statement text,
  company_code varchar(10),
  vendor_account_type varchar(50),
  vendor_group varchar(10),
  group_key varchar(50),
  recon_account varchar(10),
  payment_term_for_accounting int,
  chk_double_inv boolean,
  registered_date datetime not null,
  status int not null,
  super_company varchar(10),
  freeze varchar(10),
  action_time datetime,
  number varchar(12),
  applicant   varchar(10),
  applicant_name  varchar(50),
  index(vendor_chinese_name, vendor_english_name, vendor_code, tran_id, status)
) engine=innodb;


create table if not exists business_info_performance (
  id bigint(8) unsigned not null auto_increment primary key,
  vendor_id bigint(8) not null,
  year varchar(50),
  sales_amount varchar(50),
  profit varchar(50),
  profitability varchar(50),
  index(vendor_id)
) engine=innodb;

create table if not exists tmp_business_info_performance (
  id bigint(8) unsigned not null primary key,
  vendor_id bigint(8) not null,
  year varchar(50),
  sales_amount varchar(50),
  profit varchar(50),
  profitability varchar(50),
  index(vendor_id)
) engine=innodb;

create table if not exists attachment (
  id bigint(8) unsigned not null auto_increment primary key,
  tran_id varchar(36) not null,
  vendor_id bigint(8),
  name varchar(256) not null,
  content_type varchar(256) not null,
  size bigint(8) not null,
  data longblob not null,
  index(tran_id, vendor_id)
) engine=innodb;

create table if not exists tmp_attachment (
  id bigint(8) unsigned not null primary key,
  tran_id varchar(36) not null,
  vendor_id bigint(8),
  name varchar(256) not null,
  content_type varchar(256) not null,
  size bigint(8) not null,
  data longblob not null,
  index(tran_id, vendor_id)
) engine=innodb;

create table if not exists bank_account (
  id bigint(8) unsigned not null auto_increment primary key,
  vendor_id bigint(8) not null,
  vendor_sub_code varchar(20),
  province_and_region varchar(512),
  swift_code varchar(20),
  name varchar(100),
  account varchar(50),
  city int,
  currency varchar(3),
  type varchar(10),
  institution_no varchar(10),
  joint_code varchar(30),
  cnaps varchar(20),
  status int,
  index(vendor_id, status)
) engine=innodb;

create table if not exists tmp_bank_account (
  id bigint(8) unsigned not null primary key,
  vendor_id bigint(8) not null,
  vendor_sub_code varchar(20),
  province_and_region varchar(512),
  swift_code varchar(20),
  name varchar(100),
  account varchar(50),
  city int,
  currency varchar(3),
  type varchar(10),
  institution_no varchar(10),
  joint_code varchar(30),
  cnaps varchar(20),
  status int,
  index(vendor_id, status)
) engine=innodb;

create table if not exists evaluation (
  id bigint(8) unsigned not null auto_increment primary key,
  vendor_id bigint(8) not null,
  
  company_short_name varchar(50),
  product_name varchar(200),
  product_category varchar(200),
  transaction_condition int,
  service_category varchar(200),
  service_industry varchar(200),
  service_scope varchar(200),
  major_accounts varchar(200),
  volume_per_year varchar(200),
  num_of_biz_sites_nationwide varchar(200),
  forwarding_company varchar(200),
  info_fix_assets_nationwide varchar(200),
  num_of_empl_nationwide varchar(200),
  cooperation_condition text,
  exp_wz_sony varchar(200),
  selection_reason text,
  comment text,
  total_point varchar(11) not null,
  registered_date datetime not null,
  status int not null,
  business_type int not null,
  servicename varchar(50),
  service2category varchar(50),
  applicant varchar(50),
  date2 varchar(50),
  grand_ttl_weight_score int,
  grand_ttl_top_weight_score int,
  index(vendor_id, status, business_type)
) engine=innodb;

create table if not exists evaluation_points (
  id bigint(8) unsigned not null auto_increment primary key,
  evaluation_id bigint(8) not null,  
  point  double(11,0) not null,
  remark varchar(512),
  sequence int not null,
  index(evaluation_id)
) engine=innodb;

create table if not exists cnaps (
  no varchar(12) not null primary key,
  name varchar(128),
  index(name)
) engine=innodb;

create table if not exists banks (
  net_bank_joint_code varchar(12) not null primary key,
  name varchar(128),
  institution_no varchar(12) not null,
  joint_code varchar(12) not null,
  parent_institution_no varchar(12) not null,
  parent_joint_code varchar(12) not null,
  index(name, institution_no, joint_code)
) engine=innodb;

create table if not exists workflow_rule (
  id varchar(36) not null,
  vendor_id bigint(8) not null,
  eval_id bigint(8) not null,
  applicant varchar(10),
  stage integer,
  processor varchar(10),
  l1 varchar(10),
  l2 varchar(10),
  ceo varchar(10),
  hd varchar(10),
  fm varchar(10),
  dm varchar(10),
  application_date DATETIME,
  status int,
  registered_date datetime,
  modified_data_type int,
  index(vendor_id, eval_id, applicant)
) engine=innodb;

create table if not exists workflow_approval_result (
  id bigint(8) unsigned not null auto_increment primary key,
  workflow_rule_id varchar(36) not null,
  stage integer,
  user_id varchar(10), 
  comment text,
  result int,
  processed_date date,
  index(workflow_rule_id)
) engine=innodb;

create table if not exists modification_history (
  id bigint(8) unsigned not null auto_increment primary key,
  vendor_id bigint(8),
  bank_account_id bigint(8),
  classification int,
  registered_date datetime
) engine=innodb;

create table if not exists interface_file_history (
  id bigint(8) unsigned not null auto_increment primary key,
  data longblob not null,
  created_date datetime
) engine=innodb;

create table if not exists system_setting (
  id int primary key,
  re_eval_month int,
  approval_request_mail_subject text,
  approval_request_mail_template text,
  denied_mail_subject text,
  denied_mail_template text,
  reject_mail_subject text,
  reject_mail_template text,
  cancel_mail_subject text,
  cancel_mail_template text,
  application_finished_mail_subject text,
  application_finished_mail_template text,
  re_evaluation_request_mail_subject text,
  re_evaluation_request_mail_template text,
  password_notification_mail_subject text,
  password_notification_mail_template text,
  freeze_day varchar(10),
  freeze_applicant_valid_subject text,
  freeze_applicant_invalid_subject text,
  freeze_applicant_valid_template text,
  freeze_applicant_invalid_template text,
  certatt_subject text;
  certatt_template text;
) engine=innodb;

create table if not exists attachment_workflow (
  id bigint(8) unsigned not null auto_increment primary key,
  tran_id varchar(36) not null,
  vendor_id bigint(8),
  name varchar(256) not null,
  content_type varchar(256) not null,
  size bigint(8) not null,
  data longblob not null,
  index(tran_id, vendor_id)
) engine=innodb;

create table if not exists evaluation_item_item (
  id bigint(8) unsigned not null auto_increment primary key,
  item_id bigint(8) not null,
  points varchar(256) not null,
  weight double not null,
  index(item_id)
) engine=innodb;

CREATE TABLE if not exists `non_trade_evaluation_form` (
  `form_id` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `business_type` int(11) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `weight` double DEFAULT NULL,
  `max_point` bigint(8) DEFAULT NULL,
  `review_flag` tinyint(1) NOT NULL,
  `display_order` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`form_id`,`locale`,`business_type`,`review_flag`),
  KEY `display_order` (`display_order`)
) ENGINE=InnoDB AUTO_INCREMENT=386 DEFAULT CHARSET=utf8;


CREATE TABLE if not exists `non_trade_category` (
  `form_id` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `category_id` bigint(8) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `review_flag` tinyint(1) NOT NULL,
  `display_order` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`form_id`,`locale`,`category_id`,`review_flag`),
  KEY `display_order` (`display_order`),
  KEY `form_id` (`form_id`,`category_id`),
  CONSTRAINT `FK_Reference_9` FOREIGN KEY (`form_id`, `locale`) REFERENCES `non_trade_evaluation_form` (`form_id`, `locale`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;


CREATE TABLE if not exists `non_trade_sub_category` (
  `form_id` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `category_id` bigint(8) NOT NULL,
  `sub_category_id` bigint(8) NOT NULL,
  `content` varchar(1000) DEFAULT NULL,
  `review_flag` tinyint(1) NOT NULL,
  `display_order` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`form_id`,`locale`,`category_id`,`sub_category_id`,`review_flag`),
  KEY `display_order` (`display_order`),
  KEY `non_trade_sub_categ` (`form_id`,`locale`,`category_id`,`review_flag`),
  KEY `form_id` (`form_id`,`category_id`,`sub_category_id`),
  CONSTRAINT `non_trade_sub_categ` FOREIGN KEY (`form_id`, `locale`, `category_id`, `review_flag`) REFERENCES `non_trade_category` (`form_id`, `locale`, `category_id`, `review_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=646 DEFAULT CHARSET=utf8;



CREATE TABLE if not exists `non_trade_items` (
  `form_id` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `category_id` bigint(8) NOT NULL,
  `sub_category_id` bigint(8) NOT NULL,
  `items_id` bigint(8) NOT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `review_flag` tinyint(1) NOT NULL,
  `display_order` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`form_id`,`locale`,`category_id`,`sub_category_id`,`items_id`,`review_flag`),
  KEY `form_id` (`form_id`,`locale`,`category_id`,`sub_category_id`,`review_flag`),
  KEY `display_order` (`display_order`),
  CONSTRAINT `non_trade_items_ibfk_1` FOREIGN KEY (`form_id`, `locale`, `category_id`, `sub_category_id`, `review_flag`) REFERENCES `non_trade_sub_category` (`form_id`, `locale`, `category_id`, `sub_category_id`, `review_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=1366 DEFAULT CHARSET=utf8;



create table if not exists `non_trade_detail_items` (
  `form_id` int(11) NOT NULL,
  `locale` varchar(5) NOT NULL,
  `category_id` bigint(8) NOT NULL,
  `sub_category_id` bigint(8) NOT NULL,
  `items_id` bigint(8) NOT NULL,
  `detail_items_id` bigint(8) NOT NULL,
  `content` varchar(1000) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
  `must_check` tinyint(1) DEFAULT NULL,
  `review_flag` tinyint(1) NOT NULL,
  `display_order` bigint(11) NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`form_id`,`locale`,`category_id`,`sub_category_id`,`items_id`,`detail_items_id`,`review_flag`),
  KEY `form_id` (`form_id`,`locale`,`category_id`,`sub_category_id`,`items_id`,`review_flag`),
  KEY `display_order` (`display_order`),
  KEY `form_id_2` (`form_id`,`category_id`,`sub_category_id`,`items_id`,`detail_items_id`,`review_flag`),
  CONSTRAINT `non_trade_detail_items_ibfk_1` FOREIGN KEY (`form_id`, `locale`, `category_id`, `sub_category_id`, `items_id`, `review_flag`) REFERENCES `non_trade_items` (`form_id`, `locale`, `category_id`, `sub_category_id`, `items_id`, `review_flag`)
) ENGINE=InnoDB AUTO_INCREMENT=2728 DEFAULT CHARSET=utf8;


create table if not exists `non_trade_evaluation_detail` (
  `form_id` int(11) NOT NULL,
  `category_id` bigint(11) NOT NULL,
  `sub_category_id` bigint(11) NOT NULL,
  `items_id` bigint(11) NOT NULL,
  `detail_items_id` bigint(11) NOT NULL,
  `evaluation_id` bigint(8) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `yes_no` int(11) DEFAULT NULL,
  `score_yesno` int(11) DEFAULT NULL,
  `point` int(11) DEFAULT NULL,
  `score_point` int(11) DEFAULT NULL,
  `top_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`evaluation_id`,`form_id`,`category_id`,`sub_category_id`,`items_id`,`detail_items_id`),
  KEY `evaluation_id` (`evaluation_id`,`form_id`,`category_id`,`sub_category_id`,`items_id`,`detail_items_id`),
  KEY `non_trade_evaluation_detail_r` (`form_id`,`category_id`,`sub_category_id`,`items_id`,`detail_items_id`),
  CONSTRAINT `non_trade_evaluation_detail_ibfk_1` FOREIGN KEY (`form_id`, `category_id`, `sub_category_id`, `items_id`, `detail_items_id`) REFERENCES `non_trade_detail_items` (`form_id`, `category_id`, `sub_category_id`, `items_id`, `detail_items_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table if not exists `score_summary` (
  `form_id` int(11) NOT NULL,
  `category_id` bigint(8) NOT NULL,
  `sub_category_id` bigint(8) NOT NULL,
  `evaluation_id` bigint(8) NOT NULL,
  `items_count` int(11) DEFAULT NULL,
  `ttl_score` int(11) DEFAULT NULL,
  `ttl_top_score` int(11) DEFAULT NULL,
  `weight` float DEFAULT NULL,
  `normalized_weight` float DEFAULT NULL,
  `ttl_weight_score` int(11) DEFAULT NULL,
  `ttl_top_weight_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`form_id`,`category_id`,`sub_category_id`,`evaluation_id`),
  CONSTRAINT `score_summary_ibfk_1` FOREIGN KEY (`form_id`, `category_id`, `sub_category_id`) REFERENCES `non_trade_sub_category` (`form_id`, `category_id`, `sub_category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


create table if not exists user_costcenter (
  id varchar(10) not null,
  division int not null,
 primary key(id, division)
) engine=innodb;

CREATE TABLE if not exists `category_score_summary` (
  `form_id` int(11) NOT NULL,
  `category_id` bigint(11) NOT NULL,
  `evaluation_id` bigint(11) NOT NULL,
  `items_count` int(11) DEFAULT NULL,
  `ttl_score` int(11) DEFAULT NULL,
  `ttl_top_score` int(11) DEFAULT NULL,
  `ttl_weight_score` int(11) DEFAULT NULL,
  `ttl_top_weight_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`form_id`,`category_id`,`evaluation_id`),
  KEY `summary_category` (`form_id`,`category_id`,`evaluation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



CREATE TABLE if not exists `form_score_summary` (
  `form_id` int(11) NOT NULL,
  `evaluation_id` bigint(8) NOT NULL,
  `items_count` int(11) DEFAULT NULL,
  `ttl_score` int(11) DEFAULT NULL,
  `ttl_top_score` int(11) DEFAULT NULL,
  `ttl_weight_score` int(11) DEFAULT NULL,
  `ttl_top_weight_score` int(11) DEFAULT NULL,
  PRIMARY KEY (`form_id`,`evaluation_id`),
  CONSTRAINT `form_result` FOREIGN KEY (`form_id`) REFERENCES `non_trade_evaluation_form` (`form_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

create table if not exists certificates (
  id bigint(8) unsigned not null auto_increment primary key,
  userid varchar(10) not null,
  data longblob not null,
  date datetime,
  index(id, userid)
) engine=innodb;

create table if not exists user_company (
  id varchar(10),
  usercompany varchar(20) default 'SSCS',
  valid tinyint DEFAULT 1,
  PRIMARY KEY(id, usercompany)
) engine=innodb;
