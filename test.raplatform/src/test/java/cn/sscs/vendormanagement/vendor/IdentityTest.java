package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class IdentityTest {

	@Test
	public void testIdentity() {
		Identity id = new Identity();
		id.setVendorType(Identity.TYPE_EXTERNAL);
		id.setVendorCode("200100");
		id.setEnglishName("englishName");
		id.setChineseName("chineseName");
		
		assertEquals("vendorType", id.getVendorType());
		assertEquals("200100", id.getVendorCode());
		assertEquals("englishName", id.getEnglishName());
		assertEquals("chineseName", id.getChineseName());
		
		Map<String, Object> map = new HashMap<String, Object>();
		id.toMap(map);
		
		id = new Identity(map);

		assertEquals(Identity.TYPE_EXTERNAL, id.getVendorType());
		assertEquals("200100", id.getVendorCode());
		assertEquals("englishName", id.getEnglishName());
		assertEquals("chineseName", id.getChineseName());

	}

}
