package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class TradeConditionTest {

	@Test
	public void testTradeCondition() {
		TradeCondition td = new TradeCondition();
		td.setPaymentTerm("paymentTerm");
		td.setLeadTime("leadTime");
		td.setMinimumLot("minimumLot");
		td.setDeliveryType(1);
		
		assertEquals("paymentTerm", td.getPaymentTerm());
		assertEquals("leadTime", td.getLeadTime());
		assertEquals("minimumLot", td.getMinimumLot());
		assertEquals(1, td.getDeliveryType());
		
		Map<String, Object> map = new HashMap<String, Object>();
		td.toMap(map);
		
		td = new TradeCondition(map);
		
		assertEquals("paymentTerm", td.getPaymentTerm());
		assertEquals("leadTime", td.getLeadTime());
		assertEquals("minimumLot", td.getMinimumLot());
		assertEquals(1, td.getDeliveryType());
	}

}
