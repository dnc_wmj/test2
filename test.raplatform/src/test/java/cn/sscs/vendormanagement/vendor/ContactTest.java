package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class ContactTest {

	@Test
	public void testContact() {
		Contact contact = new Contact();
		contact.setRegion("region");
		contact.setCity("city");
		contact.setAddress("address");
		contact.setTel("tel");
		contact.setPostalCode("postalCode");
		contact.setPerson("person");
		contact.setHomepage("homepage");
		contact.setFax("fax");
		
		assertEquals("region", contact.getRegion());
		assertEquals("city", contact.getCity());
		assertEquals("address", contact.getAddress());
		assertEquals("tel", contact.getTel());
		assertEquals("postalCode", contact.getPostalCode());
		assertEquals("person", contact.getPerson());
		assertEquals("homepage", contact.getHomepage());
		assertEquals("fax", contact.getFax());
		
		Map<String, Object> map = new HashMap<String, Object>();
		contact.toMap(map);

		contact = new Contact(map);
		assertEquals("region", contact.getRegion());
		assertEquals("city", contact.getCity());
		assertEquals("address", contact.getAddress());
		assertEquals("tel", contact.getTel());
		assertEquals("postalCode", contact.getPostalCode());
		assertEquals("person", contact.getPerson());
		assertEquals("homepage", contact.getHomepage());
		assertEquals("fax", contact.getFax());
	}


}
