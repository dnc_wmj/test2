package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class BusinessInformationTest {

	@Test
	public void testBusinessInformation() {
		BusinessInformation bi = new BusinessInformation();
		bi.setNumberOfDirectStaffs(100L);
		bi.setNumberOfIndirectStaffs(200L);
		bi.setTotalStaffs(300L);
		bi.setYear(new String[]{"2010", "2009"});
		bi.setSalesAmount(new String[]{null, "200000"});
		bi.setProfit(new String[]{null, "300000"});
		
		
		assertEquals((Integer)100, bi.getNumberOfDirectStaffs());
		assertEquals((Integer)200, bi.getNumberOfIndirectStaffs());
		assertEquals((Integer)300, bi.getTotalStaffs());
		
		Map<String, Object> map = new HashMap<String, Object>();
		bi.toMap(map);
		
		bi = new BusinessInformation(map, null);
		assertEquals((Integer)100, bi.getNumberOfDirectStaffs());
		assertEquals((Integer)200, bi.getNumberOfIndirectStaffs());
		assertEquals((Integer)300, bi.getTotalStaffs());
		
	}

}
