package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.joda.time.DateTime;
import org.junit.Test;

public class CertificateTest {

	@Test
	public void testCertificate() {		
		Certificate cert = new Certificate();
		cert.setBizLicenceId("bizLicenceId");
		cert.setLegalRepresentative("legalRepresentative");
		cert.setRegCapital("regCapital");
		cert.setRegAddress("regAddress");
		cert.setRegDate(new DateTime(2010, 12, 10, 0, 0, 0, 0).toDate());
		cert.setBizLicenceExpirationDate(new DateTime(2010, 12, 10, 1, 0, 0, 0).toDate());
		cert.setStateTaxRegId("stateTaxRegId");
		cert.setStateTaxExpirationDate(new DateTime(2010, 12, 10, 2, 0, 0, 0).toDate());
		cert.setLocalTaxRegId("localTaxRegId");
		cert.setLocalTaxExpirationDate(new DateTime(2010, 12, 10, 3, 0, 0, 0).toDate());
		cert.setRegDateAndAddress("regDateAndAddress");
		cert.setCooperationDateOrPeriod("cooperationDateOrPeriod");
		cert.setListedCompany(true);
		cert.setHaveTransaction(true);
		
		assertEquals("bizLicenceId", cert.getBizLicenceId());
		assertEquals("legalRepresentative", cert.getLegalRepresentative());
		assertEquals("regCapital", cert.getRegCapital());
		assertEquals("regAddress", cert.getRegAddress());
		assertEquals(new DateTime(2010, 12, 10, 0, 0, 0, 0).toDate(), cert.getRegDate());
		assertEquals(new DateTime(2010, 12, 10, 1, 0, 0, 0).toDate(), cert.getBizLicenceExpirationDate());
		assertEquals("stateTaxRegId", cert.getStateTaxRegId());
		assertEquals(new DateTime(2010, 12, 10, 2, 0, 0, 0).toDate(), cert.getStateTaxExpirationDate());
		assertEquals("localTaxRegId", cert.getLocalTaxRegId());
		assertEquals(new DateTime(2010, 12, 10, 3, 0, 0, 0).toDate(), cert.getLocalTaxExpirationDate());
		assertEquals("regDateAndAddress", cert.getRegDateAndAddress());
		assertEquals("cooperationDateOrPeriod", cert.getCooperationDateOrPeriod());
		assertEquals(true, cert.getListedCompany());
		assertEquals(true, cert.getHaveTransaction());
		
		Map<String, Object> map = new HashMap<String, Object>();
		cert.toMap(map);
		
		cert = new Certificate(map);
		
		assertEquals("bizLicenceId", cert.getBizLicenceId());
		assertEquals("legalRepresentative", cert.getLegalRepresentative());
		assertEquals("regCapital", cert.getRegCapital());
		assertEquals("regAddress", cert.getRegAddress());
		assertEquals(new DateTime(2010, 12, 10, 0, 0, 0, 0).toDate(), cert.getRegDate());
		assertEquals(new DateTime(2010, 12, 10, 1, 0, 0, 0).toDate(), cert.getBizLicenceExpirationDate());
		assertEquals("stateTaxRegId", cert.getStateTaxRegId());
		assertEquals(new DateTime(2010, 12, 10, 2, 0, 0, 0).toDate(), cert.getStateTaxExpirationDate());
		assertEquals("localTaxRegId", cert.getLocalTaxRegId());
		assertEquals(new DateTime(2010, 12, 10, 3, 0, 0, 0).toDate(), cert.getLocalTaxExpirationDate());
		assertEquals("regDateAndAddress", cert.getRegDateAndAddress());
		assertEquals("cooperationDateOrPeriod", cert.getCooperationDateOrPeriod());
		assertEquals(true, cert.getListedCompany());
		assertEquals(true, cert.getHaveTransaction());
	}

}
