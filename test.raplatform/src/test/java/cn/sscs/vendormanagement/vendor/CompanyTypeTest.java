package cn.sscs.vendormanagement.vendor;

import static org.junit.Assert.*;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

public class CompanyTypeTest {

	@Test
	public void testCompanyType() {
		CompanyType ct = new CompanyType();
		ct.setDomesticInvestment(0);
		ct.setForeignInvestment(1);
		ct.setJointVentureInvestmentRatio("2");
		ct.setBusinessType(3);
		
		assertEquals(0, ct.getDomesticInvestment());
		assertEquals(1, ct.getForeignInvestment());
		assertEquals("2", ct.getJointVentureInvestmentRatio());
		assertEquals(3, ct.getBusinessType());
		
		Map<String, Object> map = new HashMap<String, Object>();
		ct.toMap(map);
		
		ct = new CompanyType(map);
		assertEquals(0, ct.getDomesticInvestment());
		assertEquals(1, ct.getForeignInvestment());
		assertEquals("2", ct.getJointVentureInvestmentRatio());
		assertEquals(3, ct.getBusinessType());
		
	}

	
}
