package cn.sscs.vendormanagement.master;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;

import cn.sscs.vendormanagement.data.CellConfiguration;
import cn.sscs.vendormanagement.data.ExcelParser;
import cn.sscs.vendormanagement.data.ExcelParser.Type;
import cn.sscs.vendormanagement.data.ExcelParserRowHandler;

public class Banks {

	public static void main(String[] argv) throws IOException {
		ExcelParser parser = new ExcelParser();
		parser.configure("网银机构号", "net_bank_org_no", Type.String, 20, true);
		parser.configure("银行名称", "name", Type.String, 150, false);
		parser.configure("联行号", "joint_code", Type.String, 150, false);
		parser.configure("机构号", "institution_no", Type.String, 150, false);
		parser.configure("上级行联行号", "parent_joint_code", Type.String, 150, false);
		parser.configure("上级行机构号", "parent_institution_no", Type.String, 150, false);
		
		
		final PrintWriter writer = new PrintWriter(new FileWriter("src/test/resources/bank.txt"));
		parser.parse(null, new FileInputStream("src/test/resources/bank.xls"), 
				new ExcelParserRowHandler(){

					@Override
					public Map<String, Object> handleRow(int num, Map<String, Object> row) {
						writer.println("INSERT IGNORE INTO banks VALUES (" +
								"'" + row.get("net_bank_org_no") + "','"
								+ row.get("name") + "','"
								+ row.get("institution_no") + "','"
								+ row.get("joint_code") + "','"
								+ row.get("parent_institution_no") + "','"
								+ row.get("parent_joint_code") + "');");
						return null;
					}

					@Override
					public Object handleNullCellValue(HSSFCell cell,
							CellConfiguration cellconf, int row) {
						return null;
					}

                    @Override
                    public boolean validationRow(int num,
                            Map<String, Object> row,
                            String usercompanyBySelected) {
                        // TODO Auto-generated method stub
                        return true;
                    }}
		, 0);
		
		writer.close();
	}
	
}
