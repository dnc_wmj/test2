package cn.sscs.vendormanagement.master;

import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;

import cn.sscs.vendormanagement.data.CellConfiguration;
import cn.sscs.vendormanagement.data.ExcelParser;
import cn.sscs.vendormanagement.data.ExcelParser.Type;
import cn.sscs.vendormanagement.data.ExcelParserRowHandler;

public class Cnaps1 {

	public static void main(String[] argv) throws IOException {
		ExcelParser parser = new ExcelParser();
		parser.configure("CNAPS号", "no", Type.String, 20, true);
		parser.configure("银行名称", "name", Type.String, 150, false);
		
		final PrintWriter writer = new PrintWriter(new FileWriter("src/test/resources/cnap1.txt"));
		parser.parse(null, new FileInputStream("src/test/resources/cnap1.xls"), 
				new ExcelParserRowHandler(){

					@Override
					public Map<String, Object> handleRow(int num, Map<String, Object> row) {
						writer.println("INSERT IGNORE INTO cnaps VALUES ('" + row.get("no") + "','" + row.get("name") + "');");
						return null;
					}

					@Override
					public Object handleNullCellValue(HSSFCell cell,
							CellConfiguration cellconf, int row) {
						return null;
					}

                    @Override
                    public boolean validationRow(int num,
                            Map<String, Object> row, String string) {
                        // TODO Auto-generated method stub
                        return true;
                    }}
		, 0);
		
		writer.close();
	}
	
}
