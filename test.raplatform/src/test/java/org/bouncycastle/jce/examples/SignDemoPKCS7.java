package org.bouncycastle.jce.examples;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.SignatureException;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import org.bouncycastle.jce.PKCS7SignedData;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.util.encoders.Base64;

public class SignDemoPKCS7 {
    private byte[] contentdata;
    private String keystore = "D:/cert/icbxcmp",
            keystore51 = "D:/epay/wspki/wsc-01.jks";
    private String ks_type = "JKS";
    private String pswd = "1qaz2wsx", sqclientpass = "lavida";
    private String prikeyname = "icbccmpapiprivate", sqclientpri = "wsc01";
    
    private byte[] SignData(String text) {
        Provider provider = new BouncyCastleProvider();
        Security.addProvider(provider);
        PKCS7SignedData pkcs7sd;
        contentdata = text.getBytes();
        byte signdata[] = (byte[]) null;
        char passphrase[] = pswd.toCharArray();
        
        KeyStore ks;
        try {
            ks = KeyStore.getInstance(ks_type);
            ks.load(new FileInputStream(keystore), passphrase);
            
            PrivateKey prikey = (PrivateKey) ks.getKey(prikeyname, passphrase);
            
            pkcs7sd = new PKCS7SignedData(prikey,
                    ks.getCertificateChain("icbccmpapiprivate"), "SHA1", "BC");
            pkcs7sd.update(contentdata, 0, contentdata.length);
            
            signdata = pkcs7sd.getEncoded();
            
            return signdata;
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (CertificateException e) {
            e.printStackTrace();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (SignatureException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (SecurityException e) {
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public byte[] createPKCS7(String text) {
        byte[] signedData = (byte[]) null;
        char passphrase[] = pswd.toCharArray();
        try {
            Provider provider = new BouncyCastleProvider();
            Security.addProvider(provider);
            
            KeyStore ks = KeyStore.getInstance(ks_type);
            ks.load(new FileInputStream(keystore), passphrase);
            Certificate cert = (Certificate) ks
                    .getCertificate("icbccmpapipublic");
            PrivateKey prikey = (PrivateKey) ks.getKey(prikeyname, passphrase);
            ArrayList certList = new ArrayList();
            Certificate[] certChain = ks
                    .getCertificateChain("icbccmpapiprivate");
            for (int i = 0; i < certChain.length; i++) {
                certList.add(certChain[i]);
            }
            X509Certificate cerx509 = (X509Certificate) cert;
//            CMSProcessable msgcontent = new CMSProcessableByteArray(
//                    text.getBytes("GB2312"));
//            
//            CertStore certs = CertStore.getInstance("Collection",
//                    new CollectionCertStoreParameters(certList), "BC");
//            
//            CMSSignedDataGenerator gen = new CMSSignedDataGenerator();
//            
//            gen.addSigner(prikey, cerx509, CMSSignedGenerator.DIGEST_SHA1);
//            gen.addCertificatesAndCRLs(certs);
//            // 1,eContentType,版本号;3,detached model; 4,provider ;5, stream
//            CMSSignedData signdata = gen.generate(msgcontent, true, "BC");
            // gen.generate("Y",msgcontent,true,"BC",false);
//            signedData = signdata.getContentInfo().getEncoded("DER");// 数据分割DER
            return signedData;
            // byte[] op = (byte[])msgcontent.getContent();
            // CMSSignedDataStreamGenerator genstream = new
            // CMSSignedDataStreamGenerator();
            // genstream.addSigner(prikey, cerx509,
            // CMSSignedDataStreamGenerator.DIGEST_SHA1, "BC");
            // genstream.addCertificatesAndCRLs(certs);
            // int buff = 16384;
            // byte[] buffer = new byte[buff];
            // int unitsize = 0;
            // long read = 0;
            //
            // byte [] hong = text.getBytes("GB2312");
            //
            // ByteArrayOutputStream bOut = new ByteArrayOutputStream();
            //
            // OutputStream dataout = genstream.open(bOut,true);
            //
            //
            // dataout.write(hong);
            // dataout.close();
            // byte temp[] = bOut.toByteArray();
            // bOut.close();
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    
    public static char byte2char(byte b) {
        return (char) b;
    }
    
    public String plain() {
        
        // System.out.println(trandate+"--"+trantime+"--"+pid);
        String textcontent = "signdata";
        // System.out.println(textcontent);
        return textcontent;
    }
    
    public String getBase64() {
        // String x = plain();
        String myname = "wanghan";
        byte[] temp1 = createPKCS7(myname);
        byte[] temp2 = Base64.encode(temp1);
        int count = 0;
        String str = "";
        for (int i = 0; i < temp2.length; i++) {
            str = str + byte2char(temp2[i]);
            System.out.print(byte2char(temp2[i]));
            count++;
            if (count % 64 == 0) {
                System.out.println();
            }
            
        }
        System.out.println("");
        System.out.println(str);
        
        return str;
    }
    
    public String getdate() {
        return trandate;
    }
    
    public String gettime() {
        return trantime;
    }
    
    public String getpid() {
        return pid;
    }
    
    public SignDemoPKCS7() {
        date = new Date();
        SimpleDateFormat simple = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat t = new SimpleDateFormat("HHmmss");
        trandate = simple.format(date.getTime());
        trantime = t.format(date.getTime()) + "000";
        Random i = new Random();
        pid = String.valueOf(Math.abs(i.nextInt(1000)));
        Security.addProvider(new org.bouncycastle.jce.provider.BouncyCastleProvider());
    }
    
    public Date date;
    public String trandate;
    public String trantime;
    public String pid;
    
    public static void main(String[] args) {
        // new SignDemoPKCS7().plain();
        
        new SignDemoPKCS7().getBase64();
    }
    
}
