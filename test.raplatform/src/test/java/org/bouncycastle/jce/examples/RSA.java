package org.bouncycastle.jce.examples;

import java.math.BigInteger;
import java.security.SecureRandom;

import org.bouncycastle.crypto.AsymmetricBlockCipher;
import org.bouncycastle.crypto.AsymmetricCipherKeyPair;
import org.bouncycastle.crypto.CipherParameters;
import org.bouncycastle.crypto.digests.SHA1Digest;
import org.bouncycastle.crypto.engines.RSAEngine;
import org.bouncycastle.crypto.generators.RSAKeyPairGenerator;
import org.bouncycastle.crypto.params.RSAKeyGenerationParameters;
import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;
import org.bouncycastle.crypto.signers.PSSSigner;

/**
 * @author ssLong
 * @problem 1.进行签名和验证的时 都是用SHA1进行计算摘要，和64位盐值，这两个盐值是否相同 可以使用一个对象进行签名，然后另一个对象进行验证吗
 *          2.很慢 生成公钥需要很长时间
 */
public class RSA {
    
    public RSAPrivateCrtKeyParameters RSAprivKey = null; // RSA的私钥
    
    public RSAKeyParameters RSApubKey = null; // RSA的公钥
    
    /**
     * 产生RSA公私钥对
     */
    
    public void generateRSAKeyPair() throws Exception {
        
        SecureRandom sr = new SecureRandom();
        
        BigInteger pubExp = new BigInteger("10001", 16);
        
        RSAKeyGenerationParameters RSAKeyGenPara = new RSAKeyGenerationParameters(
                pubExp, sr, 1024, 80);
        
        RSAKeyPairGenerator RSAKeyPairGen = new RSAKeyPairGenerator();
        
        RSAKeyPairGen.init(RSAKeyGenPara);
        
        AsymmetricCipherKeyPair KeyPair = RSAKeyPairGen.generateKeyPair();
        
        RSAprivKey = (RSAPrivateCrtKeyParameters) KeyPair.getPrivate(); // 生成私钥
        RSApubKey = (RSAKeyParameters) KeyPair.getPublic(); // 生成公钥
    } // generateRSAKeyPair end
    
    /*
     * 使用私钥 签名
     */
    // 对字节数组签名
    public synchronized byte[] RSASign(byte[] toSign,
            CipherParameters RSAprivKey) throws Exception {
        if (RSAprivKey == null) {
            // 私钥为空产生异常
            throw new Exception(
                    "privateKey is null so please Generate RSA Keys first");
        }
        
        SHA1Digest dig = new SHA1Digest();
        RSAEngine eng = new RSAEngine();
        PSSSigner signer = new PSSSigner(eng, dig, 64); // 参数：加密算法 摘要算法 加盐的长度
        
        signer.init(true, RSAprivKey); // true 为签名 false 验证
        
        signer.update(toSign, 0, toSign.length); // 签名的数据 偏移 长度
        
        return signer.generateSignature(); // 返回签名后的字符串
    }// RSASign end
    
    // 签名字符串
    public byte[] RSASign(String data, CipherParameters RSAprivKey)
            throws Exception {
        
        if (data == null || data.length() == 0) {
            // 空串
            // throw new Exception("you input null, it is not interesting!");
            return new byte[0];
        }
        
        return RSASign(data.getBytes(), RSAprivKey);
    } // RSASign end
    
    /**
     * 使用公钥验证签名
     */
    // 对字符数组进行验证
    public synchronized boolean RSAVerify(byte[] mesg, byte[] sig,
            CipherParameters RSApubKey) throws Exception {
        if (RSApubKey == null) {
            // 输入空公钥
            throw new Exception(
                    "publicKey is null,please generate RSA key first!");
        } else if (sig == null || sig.length == 0) {
            // 签名的消息摘要不能为空
            throw new Exception("the digest of signed message is null ");
        }
        
        SHA1Digest dig = new SHA1Digest(); // 同样使用SHA1计算摘要
        RSAEngine eng = new RSAEngine();
        PSSSigner signer = new PSSSigner(eng, dig, 64); // 进行 签名和验证的验证长度相同
                                                        // 但是是否是使用不同的盐
        
        signer.init(false, RSApubKey); // false 进行验证 使用公钥
        
        signer.update(mesg, 0, mesg.length);
        
        return signer.verifySignature(sig); // 对签名进行验证
    } // RSAVerify end
      // 对字符串进行验证
    
    public boolean RSAVerify(String data, byte[] sig, CipherParameters RSApubKey)
            throws Exception {
        if (data == null || data.length() == 0) {
            // 空串产生异常
            throw new Exception(
                    "the signed data is null,please input the right signed data ");
        }
        
        return RSAVerify(data.getBytes(), sig, RSApubKey);
    }// RSAVerify end
    
    /**
     * 使用公钥进行加密
     */
    // 对字符串 加密
    public byte[] RSAEncrypt(String plainText, CipherParameters RSApubKey)
            throws Exception {
        
        if (plainText == null || plainText.length() == 0) {
            throw new Exception("plainText is null");
        } else if (RSApubKey == null) {
            throw new Exception("publicKey is null");
        }
        byte[] rv = null;
        AsymmetricBlockCipher eng = new RSAEngine();
        
        eng.init(true, RSApubKey); // true 加密 公钥 false 解密
        
        byte[] ptBytes = plainText.getBytes();
        rv = eng.processBlock(ptBytes, 0, ptBytes.length);
        return rv;
        
    } // RSAEncrypt end
    
    /**
     * 使用私钥进行解密
     */
    public String RSADecrypt(byte[] cipherText, CipherParameters RSAprivKey)
            throws Exception {
        if (cipherText == null || cipherText.length == 0) {
            // 输入null
            return "";
        }
        byte[] rv = null;
        AsymmetricBlockCipher eng = new RSAEngine();
        eng.init(false, RSAprivKey); // 进行解密操作
        rv = eng.processBlock(cipherText, 0, cipherText.length);
        
        return new String(rv).trim();
        
    }// RSADecrypt end
    
} // 类结束
