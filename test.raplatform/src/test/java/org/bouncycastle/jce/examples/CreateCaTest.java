package org.bouncycastle.jce.examples;

import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.CertificateParsingException;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Vector;

import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERConstructedSequence;
import org.bouncycastle.asn1.DERInputStream;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.util.encoders.Base64;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

public class CreateCaTest {
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        createCa();
    }
    
    public static void createCa() {
        Security.addProvider(new BouncyCastleProvider());
        /**
         * 如何生成密钥对,以及如何将诸如公钥,私钥,证书等这一类安全对象在文件系统和内存之间来回转换.这些是准备开CA的基本功,
         */
        // ---------------------------------------------
        
        // 作为一个CA要有自己的一对公钥和私钥,我们先要生成这么一对.使用KeyPairGenerator对象就可以了,
        // 调用KeyPairGenerator.getInstance方法可以根据要生成的密钥类型来产生一个合适的实例,例如常用的RSA,DSA等.然后调
        // 用该对象的initialize方法和generateKeyPair方法就可以产生一个KeyPair对象了.然后调用KeyPair对象中的相应方法
        // 就可以获取生成的密钥对中的公钥和私钥了.
        String keyType = "RSA";
        KeyPairGenerator keyPairGen = null;
        try {
            keyPairGen = KeyPairGenerator.getInstance(keyType,"BC");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (NoSuchProviderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        keyPairGen.initialize(1024);
        KeyPair keypair = keyPairGen.genKeyPair();
        PrivateKey privateKey = keypair.getPrivate();
        PublicKey publicKey = keypair.getPublic();
        
        // 有了公钥和私钥对以后,下面的一个很现实问题就是如何把它们储存起来.通常我们要对这些安全对象,如公钥,私钥,证书等先进行编码.编码的目的是为了把结
        // 构复杂的安全对象变成字节流以便存储和读取,如DER编码.另外,通常还把DER编码后的字节流再次进行base64编码,以便使字节流中所有的字节都变
        // 成可打印的字节.
        // 在Java语言中,这些安全对象基本上都有getEncoded()方法.例如:
        byte[] keyBytes = privateKey.getEncoded();// 这样就把一个私钥进行了DER编码后的结果保存到一个byte数组中了.
        // 然后就可以把这个byte数组保存到任何介质中.如果有必要的话,可以使用BC Provider中的Base64编码解码器类进行编码,
        // 就像这样:
        byte data[] = Base64.encode(keyBytes);
        // 要从文件中读取私钥则应该这样:
        /*
        PKCS8EncodedKeySpec spec = new PKCS8EncodedKeySpec(data);
        KeyFactory kfac = null;
        try {
            kfac = KeyFactory.getInstance("RSA");
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        try {
            privateKey = kfac.generatePrivate(spec);
        } catch (InvalidKeySpecException e) {
            e.printStackTrace();
        }
        // 这里说明一下,对RSA私钥进行编码就会自动地按照PKCS8进行.
        // 因此读取的时候将包含编码的字节数组作为PKCS8EncodedKeySpec对象 的构造函数的参数就可以生成一个该类型的对象.
        // 然后创建一个密钥工厂对象就可以按照要求生成一个RSA私钥了.很显然这里的keyData应该是和上面的 keyBytes内容相同.
        // 为了提高系统的安全性,通常私钥在经过DER编码后,还会使用一个口令进行加密,然后再储存在文件系统中.在使用私钥的时候,如果没有正确的口令,是无法把私钥还原出来的.
        // 保存证书和保存私钥类似.Certificate对象中也有一个getEncoded的方法.
        // 这次就讲这些.大家应该可以把这些安全对象很熟练地从文件系统和内存之间来回地折腾了吧.这对以后实现CA是很重要的.
        */
        // 下次我会讲一下证书中表示主体的方法:DN.
        
        // ---------------------------------------------
        /**
         * CA的基本原理以及如何使用主体名称结构DN(Distinguish Name)来表示每一个证书中的主体.
         */
        // ---------------------------------------------
        // 一份证书就是一个权威机构对一个主体的身份的确认的证明.
        // 即一份证书表示一个权威机构确认了一个主体就是它自己,而不是其它的冒名顶替者.
        // 主体可以是一个 个人,也可以不是,
        // 例如,在需要安全服务的时候,需要为一台网站的服务器颁发证书,这种证书的主体就是一台服务器.
        // 签署证书的权威机构就叫做CA,该权威 机构本身也是一个主体.
        // 权威机构通过对包含有待认证的主体的一系列信息的待签名证书"(TBS,to be signed)进行数字签名来表示该机构对它的认可.
        // 一份包含了待认证的主体的相关信息的TBS再加上CA使用自己的私钥进行签名产生的字节流放在一起, 就构成了一份标准的X509证书.
        
        // 一个TBS中包含了以下这些主要信息:
        // 证书的版本,通常是3(X509v3)
        // 证书的序列号,RFC3280中规定,每个CA必须确保它颁发的每一份证书的序列号都是唯一的,并且序列号只能使用非负整数.
        // 签发者(CA)的主体名称,一个DN对象.
        // 证书的有效期,表示方法是两个时间值,表示了该证书从何时开始生效,何时开始作废.
        // 待认证的主体的主体名称,也是一个DN对象.
        // 待认证的主体的公钥,任何安全应用在确认完证书的有效性后,就可以开始使用该主体的公钥与之进行安全通信.
        // 如果是X509v3证书,即版本号是3的话,后面还有一个证书扩展信息字段,可以在证书里面添加一些其它的信息.
        
        // 表示主体的主体名称结构:
        // DN.这个结就构是一个属性的集合.每个属性有属性名称和属性值.它的作用就是用来表示"我是谁",也就是说,这个证书到底是谁颁发给谁的,这个证书对应的公钥是谁拥有的.
        // 通常使用一个字符串来表示DN结构,这种字符串说明了这种结构中的各个属性的类型和值:
        // C=CN;S=BeiJing;L=BeiJing;O=PKU;OU=ICST;CN=wolfenstein
        // 这里C是国家和地区代码,S和L都是地区代码,S相当于省或者州这样的级别,L相当于城市级别,O是组织机构名称,OU是次级组织机构名称,CN是主体的
        // 通用名(common name).
        // 在这里,C,S,L等等属性的类型都是相对固定的,例如C一般就是用来表示国家和地区代码,在DN结构中还可以添加一些其它类型的信息,一般
        // 也都是以"xxx=xxx"这样来表示的.
        
        // 下面我们来说明如何在Java语言中构造出一个主体名称对象.
        // BC Provider中使用X509Name对象来表示DN,构造一个X509Name的步骤大体如下:
        // 先构造两个vector对象,用来表示属性名称和属性值:
        Vector oids = new Vector();
        Vector attributes = new Vector();
        // 然后在oids这个用来表示属性名称的vector对象中将属性名称一个一个添加进去:
        oids.addElement(X509Name.C);//country code
        oids.addElement(X509Name.O);//organization
        oids.addElement(X509Name.OU);//organizational unit name
        oids.addElement(X509Name.ST);//state, or province name
        oids.addElement(X509Name.L);//locality name
        oids.addElement(X509Name.CN);//common name
        // X509Name对象里面有若干常量,例如上面的X509Name.C.还有X509Name.ST等等,都可以和上面举的例子对应起来.
        // 然后将属性值添加到attributes对象中:
        attributes.addElement("CN");
        attributes.addElement("SSCS");
        attributes.addElement("35570");
        attributes.addElement("SH");
        attributes.addElement("SH");
        attributes.addElement("313800");
        // 最后就可以构造出一个X509Name对象:
        X509Name subjectDN = new X509Name(oids, attributes);
        // 这样,我们的主体名称结构就确立起来了.
        // ---------------------------------------------
        
        /**
         * 如何用Java程序完成CA最重要的功能,签署证书.
         */
        // ---------------------------------------------
        // 要做CA,第一步要准备好自己的证书和私钥.私钥如何从文件里面读取出来前面已经讲过了.从文件系统中读出证书的代码如下:
        // 这里cerBIS是一个InputStream类型的对象.例如一个标准的X509v3格式的证书文件所形成的输入流.
        FileInputStream certBIS;
        X509Certificate caCert = null;
        try {
            certBIS = new FileInputStream("ca.crt");
            CertificateFactory certCF = CertificateFactory.getInstance("X.509");
            caCert = (X509Certificate) certCF.generateCertificate(certBIS);
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // 第二步就是从用户那里获取输入,然后构造主体名称结构DN,如何构造DN上次已经说过了,如何从用户那里获取输入,这个不在本文讨论范围之内.
        
        // 下一步就是获取用户的公钥,好和他所需要的证书对应起来.也有不少CA的做法就是在这里替用户现场生成一对密钥对,然后把公钥放到证书中签发给用户.这个应该看实际需要选择合适的方式.
        // 现在一切信息都已经准备好了,可以签发证书了,下面的代码说明了这个过程:
        // 构造一个证书生成器对象
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        // 从CA的证书中获取签发者的主体名称(DN)
        // 这里有一点小技巧,我们要把JCE中定义的
        // 用来表示DN的对象X500Principal 转化成在
        // BC Provider中的相应的对象X509Name
        // 先从CA的证书中读取出CA的DN进行DER编码
        DERInputStream dnStream = new DERInputStream(new ByteArrayInputStream(
                caCert.getSubjectX500Principal().getEncoded()));
        // 马上又从编码后的字节流中读取DER编码对象
        DERConstructedSequence dnSequence = null;
        try {
            dnSequence = (DERConstructedSequence) dnStream.readObject();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        // 利用读取出来的DER编码对象创建X509Name
        // 对象,并设置为证书生成器中的"签发者DN"
        certGen.setIssuerDN(new X509Name(dnSequence));
        // 设置好证书生成器中的"接收方DN"
        certGen.setSubjectDN(subjectDN);
        // 设置好一些扩展字段,包括签发者和
        // 接收者的公钥标识
        try {
            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
            // createSubjectKeyId(keyToCertify));
                    new SubjectKeyIdentifierStructure(publicKey));
        } catch (CertificateParsingException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
                createAuthorityKeyId(caCert.getPublicKey()));
        // 设置证书的有效期和序列号
        certGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60 * 60 * 24 * 30));//- 1000L * 60 * 60 * 24 * 30
        certGen.setNotAfter(new Date(System.currentTimeMillis() + 1000L * 60 * 60 * 24 * 30));
        BigInteger serialNumber = new BigInteger("4");
        certGen.setSerialNumber(serialNumber);
        // 设置签名算法,本例中使用MD5hash后RSA
        // 签名,并且设置好主体的公钥
        certGen.setSignatureAlgorithm("MD5withRSA");
        certGen.setPublicKey(publicKey);
        // certGen.setPublicKey(caCert.getPublicKey());
//        try {
//            certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
//                    new AuthorityKeyIdentifierStructure(caCert));
//            certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
//                    new SubjectKeyIdentifierStructure(publicKey));
//        } catch (CertificateParsingException e1) {
//            // TODO Auto-generated catch block
//            e1.printStackTrace();
//        }
        
        /**
         * 
         */
        FileInputStream ca_key_fis = null;
        try {
            ca_key_fis = new FileInputStream("ca.key");
        } catch (FileNotFoundException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        PasswordFinder pswd = new PasswordFinder() {
            @Override
            public char[] getPassword() {
                return "dodopipe".toCharArray();
            }
        };
        PEMReader preader = new PEMReader(new InputStreamReader(ca_key_fis), pswd);
        Object o = null;
        try {
            o = preader.readObject();
        } catch (IOException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        }
        KeyPair pair = null;
        if( o instanceof KeyPair) {
                pair = (KeyPair)o;
        }
        else
        {
                System.out.println("unknown");
        }
        // 如果以上一切都正常地话,就可以生成证书了
        X509Certificate cert = null;
        try {
            cert = certGen.generateX509Certificate(pair.getPrivate());
            cert.verify(caCert.getPublicKey());
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SecurityException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        // ---------------------------------------------
        // PKCS#10证书请求文件.它的作用就是可以使申请和签发相分离.
        // PKCS#10证书请求结构中的主要信息包含了被签发者(证书申请者)的主体名称(DN)和他的公钥.因此一个CA在获取到一个PKCS#10证书请求后,就可以从中获取到任何和签发证书有关的信息,然后用它自己的私钥签发证书.
        // 使用BC Provider在Java中构造一个证书请求格式的对象调用其构造函数即可,这个函数如下:
        // PKCS10CertificationRequest(java.lang.String signatureAlgorithm,
        // X509Name subject, java.security.PublicKey key, ASN1Set attributes,
        // java.security.PrivateKey signingKey)
        // 它的参数是自签名算法,证书申请者的DN,证书申请者的公钥,额外的属性集(就是要申请的证书的扩展信息),
        // 申请证书者的私钥.申请证书者的私钥仅仅是用来进行一下自签名,并不出现在证书请求中,
        // 需要自签名的目的是保证该公钥确实为申请者所有.
        // 调用该对象的getEncoded()方法可以将其进行DER编码,然后储存起来,该对象还有另一个构造函数:
        // PKCS10CertificationRequest(byte[] bytes)
        // 这个构造函数的作用就是直接从储存的DER编码中把这个对象还原出来.
        // 利用证书请求结构进行证书签发的代码如下,这里假设CSR是一个已经获取出来的PKCS10CertificationRequest结构:
        // PublicKey SubjectPublicKey = CSR.getPublicKey();
        // CertificationRequestInfo CSRInfo = CSR.getCertificationRequestInfo();
        // X509Name SubjectDN = CSRInfo.getSubject();
        // ASN1Set Attributes = CSRInfo.getAttributes();
        // 这样,申请者的主体DN,申请者的公钥,申请者希望在证书扩展信息中填写的属性都得到了,剩下的事情就和用户在现场输入时一样了,其它的信息一般是申请者
        // 不能决定的.另外证书请求格式中有一样信息没有明确给出来,那就是证书的有效期,这个应该单独询问用户,或者用其它的方法保存起来.
        
//        X509CertificateEntry certEntry = new X509CertificateEntry(CAcert);  
//        Pkcs12Store store = new Pkcs12StoreBuilder().Build();  
//        store.SetCertificateEntry("CA's Primary Certificate", certEntry);   //设置证书  
//        X509CertificateEntry[] chain = new X509CertificateEntry[1];  
//        chain[0] = certEntry;  
//        store.SetKeyEntry("CA's Primary Certificate", new AsymmetricKeyEntry(priKey), chain);   //设置私钥  
//        FileStream fout = File.Create("CA.pfx");      
//        store.Save(fout, passwd, new SecureRandom());   //保存  
//        fout.Close();  
//        Certificate[] chain = new Certificate[3];
//        chain[2] = createMasterCert(caPubKey, caPrivKey);
//        chain[1] = createIntermediateCert(intPubKey, caPrivKey, (X509Certificate)chain[2]);
//        chain[0] = createCert(pubKey, intPrivKey, intPubKey);
        
        //
        // add the friendly name for the private key
        //
        PKCS12BagAttributeCarrier   bagAttr = (PKCS12BagAttributeCarrier) privateKey;
        Certificate[] chain = new Certificate[1];
        chain[0] = cert;
        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise which certificate the private key
        // is associated with you should at least use the pkcs_9_localKeyId
        // OID and set it to the same as you do for the private key's
        // corresponding certificate.
        //
        bagAttr.setBagAttribute(
            PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
            new DERBMPString("313800"));
        try {
            bagAttr.setBagAttribute(
                PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
                new SubjectKeyIdentifierStructure(publicKey));
        } catch (CertificateParsingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        /*
        // 创建签名对象
        // 原文；签名数据和签名者证书/签名者公钥，都是BASE64编码。
        Signature oSign = null;
        try {
            oSign = Signature.getInstance("SHA1withRSA");
            oSign.initVerify(caCert);
            // 传入签名原文
            oSign.update(data);
           
            // 验证数字签名
            boolean verifyRet = oSign.verify(data);
        } catch (NoSuchAlgorithmException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (InvalidKeyException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (SignatureException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        */
        //
        // store the key and the certificate chain
        //
        KeyStore store = null;
        try {
            store = KeyStore.getInstance("PKCS12", "BC");
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchProviderException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        try {
            store.load(null, null);
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        //
        // if you haven't set the friendly name and local key id above
        // the name below will be the name of the key
        //
        try {
            store.setKeyEntry("313800", privateKey, "dodopipe".toCharArray(), chain);
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        // ByteArrayOutputStream out = new ByteArrayOutputStream();
        FileOutputStream fOut = null;
        try {
            fOut = new FileOutputStream("313800.p12");
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        char[] passwd = "dodopipe".toCharArray();
        try {
            store.store(fOut, passwd);
        } catch (KeyStoreException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (CertificateException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        
        try {
            fOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        
        
    }
    
    // 这里是上面用到的生成签发者公钥标识的函数:
    protected static AuthorityKeyIdentifier createAuthorityKeyId(PublicKey pubKey) {
        AuthorityKeyIdentifier authKeyId = null;
        try {
            ByteArrayInputStream bIn = new ByteArrayInputStream(
                    pubKey.getEncoded());
            SubjectPublicKeyInfo info = new SubjectPublicKeyInfo(
                    (DERConstructedSequence) new DERInputStream(bIn)
                            .readObject());
            authKeyId = new AuthorityKeyIdentifier(info);
        } catch (IOException e) {
            System.err.println("Error generating SubjectKeyIdentifier:  "
                    + e.toString());
            System.exit(1);
        }
        return authKeyId;
    }

//    public static X509Certificate sign(PKCS10CertificationRequest inputCSR, PrivateKey caPrivate, KeyPair pair)
//            throws InvalidKeyException, NoSuchAlgorithmException,
//            NoSuchProviderException, SignatureException, IOException,
//            CertificateException {   
//
//        AlgorithmIdentifier sigAlgId = new DefaultSignatureAlgorithmIdentifierFinder()
//                .find("SHA1withRSA");
//        AlgorithmIdentifier digAlgId = new DefaultDigestAlgorithmIdentifierFinder()
//                .find(sigAlgId);
//
//        AsymmetricKeyParameter foo = PrivateKeyFactory.createKey(caPrivate
//                .getEncoded());
//        SubjectPublicKeyInfo keyInfo = SubjectPublicKeyInfo.getInstance(pair
//                .getPublic().getEncoded());
//
//        PKCS10CertificationRequestHolder pk10Holder = new PKCS10CertificationRequestHolder(inputCSR);
//        X509v3CertificateBuilder myCertificateGenerator = new X509v3CertificateBuilder(
//                new X500Name("CN=issuer"), new BigInteger("1"), new Date(
//                        System.currentTimeMillis()), new Date(
//                        System.currentTimeMillis() + 30 * 365 * 24 * 60 * 60
//                                * 1000), pk10Holder.getSubject(), keyInfo);
//
//        ContentSigner sigGen = new BcRSAContentSignerBuilder(sigAlgId, digAlgId)
//                .build(foo);        
//
//        X509CertificateHolder holder = myCertificateGenerator.build(sigGen);
//        X509CertificateStructure eeX509CertificateStructure = holder
//                .toASN1Structure(); 
//
//        CertificateFactory cf = CertificateFactory.getInstance("X.509", "BC");
//
//        // Read Certificate
//        InputStream is1 = new ByteArrayInputStream(eeX509CertificateStructure.getEncoded());
//        X509Certificate theCert = (X509Certificate) cf.generateCertificate(is1);
//        is1.close();
//        return theCert;
//        //return null;
//    }
}
