package org.bouncycastle.jce.examples;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.Signature;
import java.security.spec.PKCS8EncodedKeySpec;

public class JCETest {
    
    /**
     * @param args
     */
    public static void main(String[] args) {
        PrivateKey syprivatekey = null;
        File syfile = new File("c:\\安全文件\\ \\非对称\\本人公私钥\\yhb.private");
        try {
            FileInputStream fis = new FileInputStream(syfile);
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            int thebyte = 0;
            while ((thebyte = fis.read()) != -1) {
                baos.write(thebyte);
            }
            fis.close();
            byte[] keybytes = baos.toByteArray();
            baos.close();
            PKCS8EncodedKeySpec keyspec = new PKCS8EncodedKeySpec(keybytes);
            KeyFactory keyfactory = KeyFactory.getInstance("RSA");
            syprivatekey = keyfactory.generatePrivate(keyspec);
        } catch (Exception e9) {
            System.out.print("error when read the rsa private key");
            System.exit(0);
        } // 从对话框中取得要签名的文件
        File file = new File("path");
        String filename = file.getName();
        // 首先将文件读为byte[]对象
        int len = (int) file.length();
        if (len > 100000000) {
            System.out.println("the file length is too long!");
            System.exit(0);
        }
        byte[] inbuf = new byte[len];
        try {
            FileInputStream instream = new FileInputStream(file);
            int inbytes = instream.available();
            // inbuf[]=new byte[inbytes];
            int bytesread = instream.read(inbuf, 0, inbytes);
            instream.close();
            // System.out.println(inbuf);
        } catch (Exception eq2) {
            System.out.println("error when change the file to byte[]");
            System.exit(0);
        }
        // 签名的具体过程
        try {
            // byte[] signaturebytes=new byte[150];
            Signature sig = Signature.getInstance("MD5WithRSA");
            sig.initSign(syprivatekey);
            sig.update(inbuf);
            byte[] signaturebytes = sig.sign();
            // 写入对象流中
            DataOutputStream outfile = new DataOutputStream(
                    new FileOutputStream("c:\\安全文件\\文件\\" + filename + ".yhb3"));
            outfile.writeInt(signaturebytes.length);
            outfile.write(signaturebytes);
            outfile.writeInt(len);
            outfile.write(inbuf);
            outfile.close();
        } catch (Exception eh3) {
            System.out.println("error when generate the outfile");
            System.exit(0);
        }
    }
    
}
