package org.bouncycastle.jce.examples;

import java.math.BigInteger;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateCrtKey;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;

import org.bouncycastle.crypto.params.RSAKeyParameters;
import org.bouncycastle.crypto.params.RSAPrivateCrtKeyParameters;

/**
 * 使用java生成KeyPair 然后将公私钥参数保存到文件中
 * 使用BC恢复java生成的RSA公私钥
 * 并且利用BC做测试
 * 
 * @author Admin
 */
public class testRSA {
    
    public static void main(String[] args) {
        
        try {
            // 生成密钥对
            KeyPairGenerator rsaKeyGen = null;
            KeyPair rsaKeyPair = null;
            System.out.println("Generating a pair of RSA key ... ");
            rsaKeyGen = KeyPairGenerator.getInstance("RSA");
            SecureRandom random = new SecureRandom();
            random.nextBytes(new byte[1]);
            rsaKeyGen.initialize(1024, new SecureRandom());
            rsaKeyPair = rsaKeyGen.genKeyPair();
            RSAPublicKey rsaPublic = (RSAPublicKey) rsaKeyPair.getPublic();
            RSAPrivateKey rsaPrivate = (RSAPrivateKey) rsaKeyPair.getPrivate();
            // 获得公钥的参数
            BigInteger mod = rsaPublic.getModulus();
            BigInteger pubExp = rsaPublic.getPublicExponent();
            // BC 公钥
            RSAKeyParameters pubParameters = new RSAKeyParameters(false, mod,
                    pubExp);
            // 获得私钥的参数
            RSAPrivateCrtKey prvKey = (RSAPrivateCrtKey) rsaPrivate;
            mod = prvKey.getModulus();
            pubExp = prvKey.getPublicExponent();
            BigInteger privExp = prvKey.getPrivateExponent();
            BigInteger pExp = prvKey.getPrimeExponentP();
            BigInteger qExp = prvKey.getPrimeExponentQ();
            BigInteger p = prvKey.getPrimeP();
            BigInteger q = prvKey.getPrimeQ();
            BigInteger crtCoef = prvKey.getCrtCoefficient();
            // BC 私钥
            RSAKeyParameters privParameters = new RSAPrivateCrtKeyParameters(
                    mod, pubExp, privExp, p, q, pExp, qExp, crtCoef);
            
            // 验证密钥是否 正确
            String strPlainText = "验证密钥是否正确，希望正常!";
            RSA rsa = new RSA();
            
            // 验证签名是否 正确
            boolean ret = rsa.RSAVerify(strPlainText.getBytes(),
                    rsa.RSASign(strPlainText, privParameters), pubParameters);
            
            if (ret) {
                System.out.println("签名验证通过");
            } else {
                System.out.println("签名失败");
            }
            // 加密验证
            String strEncryMessage = "验证密钥，加密消息";
            
            System.out.println("解密之后的信息:"
                    + rsa.RSADecrypt(
                            rsa.RSAEncrypt(strEncryMessage, pubParameters),
                            privParameters));
            
            System.out.println("解密之后的信息:"
                    + rsa.RSADecrypt(
                            rsa.RSAEncrypt(strEncryMessage, privParameters),
                            pubParameters));
        } catch (Exception exp) {
            
        }
    }
    
}
