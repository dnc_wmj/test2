package org.bouncycastle.jce.examples;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyStore;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.Date;
import java.util.Hashtable;
import java.util.Vector;

import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.BasicConstraints;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.jce.PrincipalUtil;
import org.bouncycastle.jce.X509Principal;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.x509.X509V1CertificateGenerator;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.AuthorityKeyIdentifierStructure;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;

public class CreatePKCS12Test {
    
    static char[] passwd = { 'h', 'e', 'l', 'l', 'o', ' ', 'w', 'o', 'r', 'l',
            'd' };
    
    static X509V1CertificateGenerator v1CertGen = new X509V1CertificateGenerator();
    static X509V3CertificateGenerator v3CertGen = new X509V3CertificateGenerator();
    
    /**
     * we generate the CA's certificate
     */
    public static Certificate createMasterCert(PublicKey pubKey,
            PrivateKey privKey) throws Exception {
        //
        // signers name
        //
        String issuer = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
        
        //
        // subjects name - the same as we are self signed.
        //
        String subject = "C=AU, O=The Legion of the Bouncy Castle, OU=Bouncy Primary Certificate";
        
        //
        // create the certificate - version 1
        //
        
        v1CertGen.setSerialNumber(BigInteger.valueOf(1));
        v1CertGen.setIssuerDN(new X509Principal(issuer));
        v1CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60
                * 60 * 24 * 30));
        v1CertGen.setNotAfter(new Date(System.currentTimeMillis()
                + (1000L * 60 * 60 * 24 * 30)));
        v1CertGen.setSubjectDN(new X509Principal(subject));
        v1CertGen.setPublicKey(pubKey);
        v1CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        
        X509Certificate cert = v1CertGen.generateX509Certificate(privKey);
        
        cert.checkValidity(new Date());
        
        cert.verify(pubKey);
        
        PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        
        //
        // this is actually optional - but if you want to have control
        // over setting the friendly name this is the way to do it...
        //
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                new DERBMPString("Bouncy Primary Certificate"));
        
        return cert;
    }
    
    /**
     * we generate an intermediate certificate signed by our CA
     */
    public static Certificate createIntermediateCert(PublicKey pubKey,
            PrivateKey caPrivKey, X509Certificate caCert) throws Exception {
        //
        // subject name table.
        //
        Hashtable attrs = new Hashtable();
        Vector order = new Vector();
        
        attrs.put(X509Principal.C, "AU");
        attrs.put(X509Principal.O, "The Legion of the Bouncy Castle");
        attrs.put(X509Principal.OU, "Bouncy Intermediate Certificate");
        attrs.put(X509Principal.EmailAddress,
                "feedback-crypto@bouncycastle.org");
        
        order.addElement(X509Principal.C);
        order.addElement(X509Principal.O);
        order.addElement(X509Principal.OU);
        order.addElement(X509Principal.EmailAddress);
        
        //
        // create the certificate - version 3
        //
        v3CertGen.reset();
        
        v3CertGen.setSerialNumber(BigInteger.valueOf(2));
        v3CertGen.setIssuerDN(PrincipalUtil.getSubjectX509Principal(caCert));
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60
                * 60 * 24 * 30));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis()
                + (1000L * 60 * 60 * 24 * 30)));
        v3CertGen.setSubjectDN(new X509Principal(order, attrs));
        v3CertGen.setPublicKey(pubKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        
        //
        // extensions
        //
        v3CertGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
                new SubjectKeyIdentifierStructure(pubKey));
        
        v3CertGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
                new AuthorityKeyIdentifierStructure(caCert));
        
        v3CertGen.addExtension(X509Extensions.BasicConstraints, true,
                new BasicConstraints(0));
        
        X509Certificate cert = v3CertGen.generateX509Certificate(caPrivKey);
        
        cert.checkValidity(new Date());
        
        cert.verify(caCert.getPublicKey());
        
        PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        
        //
        // this is actually optional - but if you want to have control
        // over setting the friendly name this is the way to do it...
        //
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                new DERBMPString("Bouncy Intermediate Certificate"));
        
        return cert;
    }
    
    /**
     * we generate a certificate signed by our CA's intermediate certficate
     */
    public static Certificate createCert(PublicKey pubKey,
            PrivateKey caPrivKey, PublicKey caPubKey) throws Exception {
        //
        // signers name table.
        //
        Hashtable sAttrs = new Hashtable();
        Vector sOrder = new Vector();
        
        sAttrs.put(X509Principal.C, "AU");
        sAttrs.put(X509Principal.O, "The Legion of the Bouncy Castle");
        sAttrs.put(X509Principal.OU, "Bouncy Intermediate Certificate");
        sAttrs.put(X509Principal.EmailAddress,
                "feedback-crypto@bouncycastle.org");
        
        sOrder.addElement(X509Principal.C);
        sOrder.addElement(X509Principal.O);
        sOrder.addElement(X509Principal.OU);
        sOrder.addElement(X509Principal.EmailAddress);
        
        //
        // subjects name table.
        //
        Hashtable attrs = new Hashtable();
        Vector order = new Vector();
        
        attrs.put(X509Principal.C, "AU");
        attrs.put(X509Principal.O, "The Legion of the Bouncy Castle");
        attrs.put(X509Principal.L, "Melbourne");
        attrs.put(X509Principal.CN, "Eric H. Echidna");
        attrs.put(X509Principal.EmailAddress,
                "feedback-crypto@bouncycastle.org");
        
        order.addElement(X509Principal.C);
        order.addElement(X509Principal.O);
        order.addElement(X509Principal.L);
        order.addElement(X509Principal.CN);
        order.addElement(X509Principal.EmailAddress);
        
        //
        // create the certificate - version 3
        //
        v3CertGen.reset();
        
        v3CertGen.setSerialNumber(BigInteger.valueOf(3));
        v3CertGen.setIssuerDN(new X509Principal(sOrder, sAttrs));
        v3CertGen.setNotBefore(new Date(System.currentTimeMillis() - 1000L * 60
                * 60 * 24 * 30));
        v3CertGen.setNotAfter(new Date(System.currentTimeMillis()
                + (1000L * 60 * 60 * 24 * 30)));
        v3CertGen.setSubjectDN(new X509Principal(order, attrs));
        v3CertGen.setPublicKey(pubKey);
        v3CertGen.setSignatureAlgorithm("SHA1WithRSAEncryption");
        
        //
        // add the extensions
        //
        v3CertGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
                new SubjectKeyIdentifierStructure(pubKey));
        
        v3CertGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
                new AuthorityKeyIdentifierStructure(caPubKey));
        
        X509Certificate cert = v3CertGen.generateX509Certificate(caPrivKey);
        
        cert.checkValidity(new Date());
        
        cert.verify(caPubKey);
        
        PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) cert;
        
        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise the associated private key this
        // you should at least use the pkcs_9_localKeyId OID and set it
        // to the same as you do for the private key's localKeyId.
        //
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                new DERBMPString("Eric's Key"));
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
                new SubjectKeyIdentifierStructure(pubKey));
        
        return cert;
    }
    
    /**
     * 1) 生成客户端CSR
     * openssl genrsa -des3 -out tomcat_client.key 1024
     * openssl req -new -key tomcat_client.key -out tomcat_client.csr -config
     * openssl.cnf
     * 2) 用CA私钥进行签名，也可以到权威机构申请CA签名
     * openssl ca -in tomcat_client.csr -out tomcat_client.crt -cert ca.crt
     * -keyfile ca.key -notext -config openssl.cnf
     * 3) 生成PKCS12格式证书
     * openssl pkcs12 -export -inkey tomcat_client.key -in tomcat_client.crt
     * -out tomcat_client.p12
     * 
     * @param args
     * @throws Exception
     */
    public static void main(
        String[]    args)
        throws Exception
    {
        Security.addProvider(new BouncyCastleProvider());
        Certificate[] chain = new Certificate[3];
        
        FileInputStream tomcat_client_crt_fis = new FileInputStream("tomcat_client.crt");
        FileInputStream tomcat_client_csr_fis = new FileInputStream("tomcat_client.csr");
        
        FileInputStream ca_key_fis = new FileInputStream("ca.key");
        FileInputStream tomcat_client_key_fis = new FileInputStream("tomcat_client.key");
        CertificateFactory cf;
        
        cf = CertificateFactory.getInstance("X509");
        Certificate tomcat_client_crt=(Certificate) cf.generateCertificate(tomcat_client_crt_fis);
        Certificate tomcat_client_csr=(Certificate) cf.generateCertificate(tomcat_client_csr_fis);
        //Certificate tomcat_client_key=(Certificate) cf.generateCertificate(tomcat_client_key_fis);
        PasswordFinder pswd = new PasswordFinder() {
            @Override
            public char[] getPassword() {
                return "dodopipe".toCharArray();
            }
        };
        PEMReader preader = new PEMReader(new InputStreamReader(ca_key_fis), pswd);
        Object o = preader.readObject();
        KeyPair pair = null;
        if( o instanceof KeyPair) {
                pair = (KeyPair)o;
        }
        else
        {
                System.out.println("unknown");
        }
        
        
        PasswordFinder pswd2 = new PasswordFinder() {
            @Override
            public char[] getPassword() {
                return "dodopipe".toCharArray();
            }
        };
        PEMReader preader2 = new PEMReader(new InputStreamReader(tomcat_client_key_fis), pswd2);
        Object o2 = preader.readObject();
        KeyPair pair2 = null;
        if( o2 instanceof KeyPair) {
                pair2 = (KeyPair)o2;
        }
        else
        {
                System.out.println("unknown");
        }
        //chain[2] = tomcat_client_key;
        //
        // set up the keys
        //
        KeyFactory          fact = KeyFactory.getInstance("RSA", "BC");
        PrivateKey          caPrivKey = pair.getPrivate();
        PublicKey           caPubKey = pair.getPublic();
        PrivateKey          intPrivKey = pair2.getPrivate();
        PublicKey           intPubKey = pair2.getPublic();
//        PrivateKey          privKey = fact.generatePrivate(privKeySpec);
//        PublicKey           pubKey = fact.generatePublic(pubKeySpec);
        
        Certificate[] chain2 = new Certificate[3];

        chain[2] = createMasterCert(caPubKey, caPrivKey);
        chain[1] = createIntermediateCert(intPubKey, caPrivKey, (X509Certificate)chain[2]);
//        chain[0] = createCert(pubKey, intPrivKey, intPubKey);
//        chain[1] = tomcat_client_csr;
//        chain[0] = tomcat_client_crt;

        //
     // add the friendly name for the private key
        //
//        PKCS12BagAttributeCarrier   bagAttr = (PKCS12BagAttributeCarrier)privKey;

        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise which certificate the private key
        // is associated with you should at least use the pkcs_9_localKeyId
        // OID and set it to the same as you do for the private key's
        // corresponding certificate.
        //
//        bagAttr.setBagAttribute(
//            PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
//            new DERBMPString("Eric's Key"));
//        bagAttr.setBagAttribute(
//            PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
//            new SubjectKeyIdentifierStructure(pubKey));
        // store the key and the certificate chain
        // server_keystore
        //
        KeyStore store = KeyStore.getInstance("PKCS12", "BC");

        store.load(null, null);

        //
        // if you haven't set the friendly name and local key id above
        // the name below will be the name of the key
        //
        byte[] password = {'d','o','d','o','p','i','p','e'};
        
//        store.setKeyEntry("Eric's Key", privKey, null, chain);

        FileOutputStream fOut = new FileOutputStream("id.p12");
        char[] password2 = {'d','o','d','o','p','i','p','e'};
        store.store(fOut, password2);
    }
}
