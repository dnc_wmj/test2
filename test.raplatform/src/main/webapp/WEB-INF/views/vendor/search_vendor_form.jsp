<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
	<title><fmt:message key="search.title"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />" charset="UTF-8"></script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="search.title"/></h1>
<p><fmt:message key="search.description"/></p>

<form:form id="form" modelAttribute="criteria" action="search" method="post">
<input type="hidden" name="todoAction" id="todoAction">
 <form:input style="width:300px;" path="text" />
 <input type="submit" value="<fmt:message key="button.search"/>" onclick="$('#todoAction').val('search')"/>&nbsp;&nbsp;&nbsp;
 <input type="submit" value="<fmt:message key="vendor.download.button"/>" onclick="$('#todoAction').val('download')"/><br>
 <form:radiobutton value="name" path="type"/><fmt:message key="search.company_name"/>
 <form:radiobutton value="code" path="type"/><fmt:message key="search.vendor_code"/>
 <form:radiobutton value="number" path="type"/><fmt:message key="search.vendor_number"/>
 <fmt:message key="basicinfo.form.company_type.category"/>
 <form:select path="category" >
   <form:option value="3"><fmt:message key="basicinfo.form.vendor_type.all"/></form:option>
   <form:option value="0"><fmt:message key="basicinfo.form.vendor_type.external"/></form:option>
   <form:option value="1"><fmt:message key="basicinfo.form.vendor_type.internal"/></form:option>
   <form:option value="2"><fmt:message key="basicinfo.form.vendor_type.other"/></form:option>
 </form:select>
 <fmt:message key="basicinfo.form.company_type.parent_company"/>
 <form:select path="superCompanyType" style="width:auto;">
   <c:if test="${!isSSCS}">
	   <c:forEach var="supercompany" items="${supercompany}">
	      <option value="${supercompany.value}">${supercompany.name}
	   </c:forEach>
   </c:if>
   <c:if test="${isSSCS}">
	   <form:option value="0"><fmt:message key="basicinfo.form.vendor_type.all"/></form:option>
	   <form:option value="1"><fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/></form:option>
	   <form:option value="2"><fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/></form:option>
	   <form:option value="3"><fmt:message key="basicinfo.form.company_type.parent_company.SSV"/></form:option>
	   <form:option value="4"><fmt:message key="basicinfo.form.company_type.parent_company.SEW"/></form:option>
	   <form:option value="5"><fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/></form:option>
	   <form:option value="6"><fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/></form:option>
	   <form:option value="7"><fmt:message key="basicinfo.form.company_type.parent_company.SEH"/></form:option>
   </c:if>
 </form:select>
</form:form>
<c:if test="${vendors != null}">
<hr>
<span style="font-size:0.8em;margin-left:20px;">
  <c:if test="${criteria.page != 0}">
    <c:url var="prev" value="/vendor/search">
      <c:param name="page" value="${(criteria.page - 1)}" />
    </c:url>
    <a href="#" onclick="$('#form').attr('action','<c:out value="${prev}"/>'); $('#todoAction').val('search'); $('#form').submit();">&lt;-&nbsp;</a>
  </c:if>
  <c:out value="${(criteria.page + 1)}" />/<c:out value="${(pages + 1)}"/>
  <c:if test="${criteria.page < pages}">
  <c:url var="next" value="/vendor/search">
    <c:param name="page" value="${(criteria.page + 1)}" />
  </c:url>
  <a href="#" onclick="$('#form').attr('action','<c:out value="${next}"/>'); $('#todoAction').val('search'); $('#form').submit();">&nbsp;-&gt;</a>
  </c:if>
</span>

<table class="default_table">
  <tr>
    <th><fmt:message key="search.company_name"/></th>
    <th><fmt:message key="search.vendor_code"/></th>  
    <th><fmt:message key="search.vendor_number"/></th>
    <c:if test="${showStatus}">
    <th><fmt:message key="search.status"/></th>
    </c:if>
    <th><fmt:message key="search.vendor_freeze"/></th> <!-- 郑同 2012年7月13日15:22:29 add -->
    <th>&nbsp;</th>
  </tr>
<c:forEach var="vendor" items="${vendors}">
  <tr>
    <td><c:out value="${vendor.name}" /></td>
    <td style="width:100px;text-align:center;"><c:out value="${vendor.vendorCode}" /></td>
    <td style="width:100px;text-align:center;"><c:out value="${vendor.vendorNumber}" /></td>
    
    <!-- only search status is registered. -->
    <c:if test="${showStatus}">
	    <td style="width:100px;text-align:center;">
	      <c:if test="${vendor.status == 5}">
	        <fmt:message key="search.status.5"/>
	      </c:if>
	      <c:if test="${vendor.status == 10}">
	        <fmt:message key="search.status.10"/>
	      </c:if>
		  <c:if test="${vendor.status == 20}">
	        <fmt:message key="search.status.20"/>
	      </c:if>
		  <c:if test="${vendor.status == 30}">
	        <fmt:message key="search.status.30"/>
	      </c:if>
		  <c:if test="${vendor.status == 40}">
	        <fmt:message key="search.status.40"/>
	      </c:if>	  
	    </td>
    </c:if>
    
    <!-- zhengtong 2012年7月13日15:24:49 add -->
    <td style="width:100px;text-align:center;">
      <c:if test="${vendor.freeze == '1'}">
        <fmt:message key="search.isFreeze"/>
      </c:if>
      <c:if test="${vendor.freeze != '1'}">
        <fmt:message key="search.notFreeze"/>
      </c:if> 
    </td>
    <!-- zhengtong add end -->
    
    <td style="width:80px;text-align:center;">
      <div class="button">
        <a href="<c:url value="/vendor/view/${vendor.id}" />"><fmt:message key="search.detail"/></a>
      </div>
    </td>
  </tr>
</c:forEach>
</table>
</c:if>
</div>
</div>