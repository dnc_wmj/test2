<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="search.title"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />" charset="UTF-8"></script>
<div class="mainContainer">
	<div class="basic_form contextContainer">
		<h1 class="alt"><fmt:message key="data.menu.vendor.download"/></h1>
		<p><fmt:message key="down.description"/></p>
		
		<form:form modelAttribute="criteria" action="#" method="post" id="form">
			<button type="submit">
				<fmt:message key="vendor.download.button" />
			</button>
			<c:if test="$(show)">
				<br>
				<br>
				<form:select path="category">
					<form:option value="0"><fmt:message key="vendor.download.all"/></form:option>
					<form:option value="1"><fmt:message key="vendor.download.freeze"/></form:option>
					<form:option value="2"><fmt:message key="vendor.download.valid"/></form:option>
				</form:select>
			</c:if>
		</form:form>
	</div>
</div>