<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="search.bank.title"/></title>
<base target="_self">
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript">
    $(function() {
        $('.chineseBank').live('click',function(){
            var obj = $(this).parents('tr').eq(0);
            var reObj = new Object();
            reObj.bankType='chineseBank';
            reObj.bankName = $(obj).find('.bankName').eq(0).html();
            reObj.bankJointCode = $(obj).find('.bankJointCode').eq(0).html();
            reObj.bankInstitutionNo = $(obj).find('.bankInstitutionNo').eq(0).html();
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/chrome\/([\d.]+)/) || ua.indexOf("safari") != -1){
                window.opener.ReturnValue= reObj;
            }
            window.returnValue = reObj;
            window.close();
        });
        
        $('.otherBank').live('click',function(){
            var obj = $(this).parents('tr').eq(0);
            var reObj = new Object();
            reObj.bankType='otherBank';
            reObj.cnapName = $(obj).find('.cnapName').eq(0).html();
            reObj.cnapNo = $(obj).find('.cnapNo').eq(0).html();
            var ua = navigator.userAgent.toLowerCase();
            if(ua.match(/chrome\/([\d.]+)/) || ua.indexOf("safari") != -1){
                window.opener.ReturnValue= reObj;
            }
            window.returnValue = reObj;
            window.close();
        });
    });
</script>
<div class="mainContainer">
<div class="basic_form contextContainer" style="width:600px;">
<h1 class="alt"><fmt:message key="search.bank.title"/></h1>
<p><fmt:message key="search.bank.description"/></p>

<form:form modelAttribute="criteria" action="#" method="post" id="form">
 <span style="font-size:0.8em"><fmt:message key="search.bank.name"/> : </span> 
 <form:input style="width:300px;" path="name" />
 <form:select path="type">
 　<form:option value="0"><fmt:message key="search.bank.no"/></form:option>
     <form:option value="1"><fmt:message key="search.bank.cnaps"/></form:option>
  </form:select>
 
 <input type="submit" value="<fmt:message key="button.search"/>"><br>
</form:form>

<c:if test="${bankMasters != null}">
<hr>
<span style="font-size:0.8em;margin-left:20px;">
  <c:if test="${criteria.page != 0}">
    <c:url var="prev" value="/vendor/search/bank">
      <c:param name="page" value="${(criteria.page - 1)}" />
    </c:url>
    <a href="#" onclick="$('#form').attr('action','<c:out value="${prev}"/>');$('#form').submit();">&lt;-&nbsp;</a>
  </c:if>
  <c:out value="${(criteria.page + 1)}" />/<c:out value="${(pages + 1)}"/>
  <c:if test="${criteria.page < pages}">
  <c:url var="next" value="/vendor/search/bank">
    <c:param name="page" value="${(criteria.page + 1)}" />
  </c:url>
  <a href="#" onclick="$('#form').attr('action','<c:out value="${next}"/>');$('#form').submit();">&nbsp;-&gt;</a>
  </c:if>
</span>
<table class="default_table" style="width:480px;">
  <tr>
    <th><fmt:message key="search.bank.choose"/></th>
    <th><fmt:message key="search.bank.name"/></th>  
    <th><fmt:message key="search.bank.bankNo"/></th>
    <th><fmt:message key="search.bank.orgNo"/></th>
    <th><fmt:message key="search.bank.netBankOrgNo"/></th>
  </tr>
<c:forEach var="bankMaster" items="${bankMasters}">
  <tr>
    <td><input type="button" class="chineseBank" value="<fmt:message key='search.bank.choose'/>"></td>
    <td class="bankName"><c:out value="${bankMaster.name}" /></td>
    <td class="bankJointCode"><c:out value="${bankMaster.jointCode}" /></td>
    <td class="bankInstitutionNo"><c:out value="${bankMaster.institutionNo}" /></td>
    <td class="bankNetBankJointCode"><c:out value="${bankMaster.netBankJointCode}" /></td>
  </tr>
</c:forEach>
</table>
</c:if>

<c:if test="${cnaps != null}">
<hr>
<span style="font-size:0.8em;margin-left:20px;">
  <c:if test="${criteria.page != 0}">
    <c:url var="prev" value="/vendor/search/bank">
      <c:param name="page" value="${(criteria.page - 1)}" />
    </c:url>
    <a href="#" onclick="$('#form').attr('action','<c:out value="${prev}"/>');$('#form').submit();">&lt;-&nbsp;</a>
  </c:if>
  <c:out value="${(criteria.page + 1)}" />/<c:out value="${(pages + 1)}"/>
  <c:if test="${criteria.page < pages}">
  <c:url var="next" value="/vendor/search/bank">
    <c:param name="page" value="${(criteria.page + 1)}" />
  </c:url>
  <a href="#" onclick="$('#form').attr('action','<c:out value="${next}"/>');$('#form').submit();">&nbsp;-&gt;</a>
  </c:if>
</span>
<table class="default_table" style="width:480px;">
  <tr>
    <th><fmt:message key="search.bank.choose"/></th>
    <th><fmt:message key="search.bank.name"/></th>  
    <th><fmt:message key="search.bank.cnaps"/></th>  
  </tr>
<c:forEach var="cnap" items="${cnaps}">
  <tr>
    <td><input type="button" class="otherBank" value="<fmt:message key='search.bank.choose'/>"></td>
    <td class="cnapName"><c:out value="${cnap.name}" /></td>
    <td class="cnapNo"><c:out value="${cnap.no}" /></td>
  </tr>
</c:forEach>
</table>
</c:if>
</div>
</div>