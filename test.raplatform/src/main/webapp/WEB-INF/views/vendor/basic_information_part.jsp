<%@ include file="/WEB-INF/views/includes.jsp" %>
<br>
<div class="button">
<c:if test="${editable}">
<c:if test="${workflow == null}">
	<c:if test="${tmpRuleId != null}">
		<a href="<c:url value="/vendor/updateform/application/basic/${vendor_id}/${eval_id}/${tmpRuleId}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<c:if test="${isReeval && isImportExterVendor && tttt}">
			<a href="<c:url value="/vendor/updateform/application/basic/${vendor_id}/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
		</c:if>	
		<c:if test="${!isReeval}">
			<a href="<c:url value="/vendor/updateform/application/basic/${vendor_id}/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
		</c:if>	
	</c:if>
</c:if>
<c:if test="${workflow != null}">

	<a href="<c:url value="/vendor/updateform/workflow/basic/${vendor_id}/${workflow.id}" />"><fmt:message key="evaluation.edit"/></a>

</c:if>
</c:if>
<c:if test="${workflow != null && isRoleHD}">
	
	<a href="<c:url value="/vendor/updateform/workflow/hd/basic/${vendor_id}/${workflow.id}" />"><fmt:message key="evaluation.edit"/></a>
	
</c:if>

</div>
<br>

<div class="index"><fmt:message key="basicinfo.form.name"/></div>
<table class="basic_form_table">
<tbody>
   <tr>
    <th><fmt:message key="basicinfo.form.company_type.parent_company"/> : <br></th>
    <td colspan="4">
      <c:if test="${basic_info.companyType.superCompanyType == 1}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 2}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 3}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SSV"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 4}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SEW"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 5}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 6}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/>
      </c:if>
      <c:if test="${basic_info.companyType.superCompanyType == 7}">
        <fmt:message key="basicinfo.form.company_type.parent_company.SEH"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.vendor_code"/> : <br></th>
    <td><c:out value="${basic_info.identity.vendorCode}" /></td>
    <th><fmt:message key="basicinfo.form.vendor_type"/> : <br></th>
    <td style="width:300px;">
      <c:if test="${basic_info.identity.vendorType == 0}" >
        <fmt:message key="basicinfo.form.vendor_type.external"/>
      </c:if>
      <c:if test="${basic_info.identity.vendorType == 1}" >
        <fmt:message key="basicinfo.form.vendor_type.internal"/>
      </c:if>
      <c:if test="${basic_info.identity.vendorType == 2}" >
        <fmt:message key="basicinfo.form.vendor_type.other"/>
      </c:if>
      
    </td>
  </tr>
  <tr>
    <th>
      <fmt:message key="basicinfo.form.chinese_name"/> : <br>
      <form:errors path="identity.chineseName" cssClass="errors"/>
    </th>
    <td><c:out value="${basic_info.identity.chineseName}" /></td>
    <th>
      <fmt:message key="basicinfo.form.english_name"/> : <br> 
      <form:errors path="identity.englishName" cssClass="errors"/>
    </th>
    <td><c:out value="${basic_info.identity.englishName}" /></td>
  </tr>
  <tr>
    <th>
      <fmt:message key="search.vendor_number"/> : <br>
      <form:errors path="identity.vendorNumber" cssClass="errors"/>
    </th>
    <td><c:out value="${basic_info.identity.vendorNumber}" /></td>
    <th>
    	<fmt:message key="search.vendor_freeze"/> : <br>
    </th>
    <td>
		  <c:if test="${freeze}">
       	  	<fmt:message key="search.isFreeze"/>
          </c:if>
	      <c:if test="${!freeze}">
	        <fmt:message key="search.notFreeze"/>
	      </c:if> 
	</td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.contact"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.country"/> : <br> 
    </th>
    <td><c:out value="${basic_info.contact.country}" /></td>
    <th><fmt:message key="basicinfo.form.contact.region"/> : <br></th>
    <td style="width:300px;"><c:out value="${basic_info.contact.region}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.city"/> : <br></th>
    <td><c:out value="${basic_info.contact.city}" /></td>
    <th><fmt:message key="basicinfo.form.contact.address"/> : <br></th>
    <td><c:out value="${basic_info.contact.address}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.tel"/> : <br>
    </th>
    <td><c:out value="${basic_info.contact.tel}" /></td>
    <th><fmt:message key="basicinfo.form.contact.postal_code"/> : <br></th>
    <td><c:out value="${basic_info.contact.postalCode}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.person"/> : <br></th>
    <td><c:out value="${basic_info.contact.person}" /></td>
    <th><fmt:message key="basicinfo.form.contact.homepage"/> : <br></th>
    <td><c:out value="${basic_info.contact.homepage}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.fax"/> : <br></th>
    <td><c:out value="${basic_info.contact.fax}" /></td>
    <th colspan="2">&nbsp;</th>
  </tr>

</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.certificate"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.business_licence_id"/> : <br></th>
    <td><c:out value="${basic_info.certificate.bizLicenceId}" /></td>
    <th><fmt:message key="basicinfo.form.certificate.legal_representative"/> : <br></th>
    <td style="width:300px;"><c:out value="${basic_info.certificate.legalRepresentative}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.registration_capital"/> : <br></th>
    <td><c:out value="${basic_info.certificate.regCapital}" /></td>
    <th><fmt:message key="basicinfo.form.certificate.registration_address"/> : <br></th>
    <td><c:out value="${basic_info.certificate.regAddress}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.registration_date"/> : <br></th>
    <td><c:out value="${basic_info.certificate.regDate}" /></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_biz"/> : <br></th>
    <td><c:out value="${basic_info.certificate.bizLicenceExpirationDate}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.state_tax_reg_id"/> : <br></th>
    <td><c:out value="${basic_info.certificate.stateTaxRegId}" /></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_state"/> : <br></th>
    <td><c:out value="${basic_info.certificate.stateTaxExpirationDate}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.local_tax_reg_id"/> : <br></th>
    <td><c:out value="${basic_info.certificate.localTaxRegId}" /></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_local"/> : <br></th>
    <td><c:out value="${basic_info.certificate.localTaxExpirationDate}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.certificate"/> : <br></th>
    <td style="padding:0;">
      <c:if test="${basic_info.certificate.certIso9000 == true}">
        ISO9000<br>
      </c:if>
      <c:if test="${basic_info.certificate.certIso14000 == true}">
        ISO14000<br>
      </c:if>
      <c:if test="${basic_info.certificate.certQs9000 == true}">
        QS9000
      </c:if>
	  <c:if test="${basic_info.certificate.certIso9000 == false && basic_info.certificate.certIso14000 == false && basic_info.certificate.certIso9000 == false}">
        &nbsp;
      </c:if>
    </td>      
    <th>Others : </th>
    <td><c:out value="${basic_info.certificate.certOther}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.listed_company"/> : <br></th>
    <td>
      <c:if test="${basic_info.certificate.listedCompany == true}">
        <fmt:message key="basicinfo.yes"/>
      </c:if>
      <c:if test="${basic_info.certificate.listedCompany == false}">
        <fmt:message key="basicinfo.no"/>
      </c:if>
    </td>
    <th><fmt:message key="basicinfo.form.certificate.reg_date_and_address"/> : <br></th>
    <td><c:out value="${basic_info.certificate.regDateAndAddress}" /></td>
  </tr>
 <tr>
    <th><fmt:message key="basicinfo.form.certificate.have_transaction"/> : <br></th>
    <td>
      <c:if test="${basic_info.certificate.haveTransaction == true}">
        <fmt:message key="basicinfo.yes"/>
      </c:if>
      <c:if test="${basic_info.certificate.haveTransaction == false}">
        <fmt:message key="basicinfo.no"/>
      </c:if>      
    </td>
    <th><fmt:message key="basicinfo.form.certificate.cooperation_date_period"/> : <br></th>
    <td><c:out value="${basic_info.certificate.cooperationDateOrPeriod}" /></td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.company_type"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.domestic_investment"/> : <br></th>
    <td colspan="3">
      <c:if test="${basic_info.companyType.foreignInvestment == 5}">
        <fmt:message key="basicinfo.form.company_type.domestic_investment.state"/>
      </c:if>
      <c:if test="${basic_info.companyType.foreignInvestment == 6}">
        <fmt:message key="basicinfo.form.company_type.domestic_investment.private"/>  
      </c:if>
      <c:if test="${basic_info.companyType.foreignInvestment == 7}">
        <fmt:message key="basicinfo.form.company_type.domestic_investment.joint"/>
      </c:if>
      &nbsp;      
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.foreign_investment"/> : <br></th>
    <td>
      <c:if test="${basic_info.companyType.foreignInvestment == 0}">
      	 <fmt:message key="basicinfo.form.company_type.foreign_investment.tw"/>
      </c:if>
      <c:if test="${basic_info.companyType.foreignInvestment == 1}">
      	 <fmt:message key="basicinfo.form.company_type.foreign_investment.hk"/>
      </c:if>
      <c:if test="${basic_info.companyType.foreignInvestment == 2}">
      	 <fmt:message key="basicinfo.form.company_type.foreign_investment.jp"/>
      </c:if>      
      <c:if test="${basic_info.companyType.foreignInvestment == 3}">
      	 <fmt:message key="basicinfo.form.company_type.foreign_investment.usa"/>
      </c:if>
      <c:if test="${basic_info.companyType.foreignInvestment == 4}">
      	 <c:out value="${basic_info.companyType.foreignInvestmentOther}"/>
      </c:if>
      &nbsp;
    </td>
    <th>&nbsp;</th>
    <td style="width:300px;">&nbsp;</td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.joint_venture"/> : <br></th>
    <td colspan="3">
      <table>
        <tr>
          <td style="border:none;font-size:12px;"><fmt:message key="basicinfo.form.company_type.joint_venture.investor_and_investment_ratio"/> : </td>
          <td style="border:none;"><c:out value="${basic_info.companyType.jointVentureInvestmentRatio}"/></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.business_type"/> : <br></th>
    <td colspan="4">
      <c:if test="${basic_info.companyType.businessType == 0}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 1}">
        <fmt:message key="basicinfo.form.company_type.business_type.trade"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 2}">
        <fmt:message key="basicinfo.form.company_type.business_type.common"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 3}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 4}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 5}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 6}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 7}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 8}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 9}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 15}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 14}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 10}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 11}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 12}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 13}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
      </c:if>
      <c:if test="${basic_info.companyType.businessType == 16}">
        <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.production"/> : <br></th>
    <td colspan="4"><pre><c:out value="${basic_info.companyType.production}"  /></pre></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.business_relation"/> : <br></th>
    <td colspan="4"><c:out value="${basic_info.companyType.businessRelation}"  /></td>
  </tr>  
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.trade_conditions"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.trade_conditions.payment_term"/> : <br></th>
    <td>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 3}">
        <fmt:message key="basicinfo.form.accounting.payment_term.3"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 4}">
        <fmt:message key="basicinfo.form.accounting.payment_term.4"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 5}">
        <fmt:message key="basicinfo.form.accounting.payment_term.5"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 6}">
        <fmt:message key="basicinfo.form.accounting.payment_term.6"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 7}">
        <fmt:message key="basicinfo.form.accounting.payment_term.7"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 8}">
        <fmt:message key="basicinfo.form.accounting.payment_term.8"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 9}">
        <fmt:message key="basicinfo.form.accounting.payment_term.9"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 10}">
        <fmt:message key="basicinfo.form.accounting.payment_term.10"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 11}">
        <fmt:message key="basicinfo.form.accounting.payment_term.11"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 12}">
        <fmt:message key="basicinfo.form.accounting.payment_term.12"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 13}">
        <fmt:message key="basicinfo.form.accounting.payment_term.13"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 14}">
        <fmt:message key="basicinfo.form.accounting.payment_term.14"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 15}">
        <fmt:message key="basicinfo.form.accounting.payment_term.15"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.paymentTerm == 16}">
        <fmt:message key="basicinfo.form.accounting.payment_term.16"/>
      </c:if>
    </td>
    <th><fmt:message key="basicinfo.form.trade_conditions.lead_time"/> : <br></th>
    <td style="width:300px;"><c:out value="${basic_info.tradeCondition.leadTime}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.trade_conditions.minimum_lot"/> : <br></th>
    <td><c:out value="${basic_info.tradeCondition.minimumLot}" /></td>
    <th><fmt:message key="basicinfo.form.trade_conditions.delivery_types"/> : <br></th>
    <td>
      <c:if test="${basic_info.tradeCondition.deliveryType == 0}" >
        <fmt:message key="basicinfo.form.trade_conditions.delivery_types.CIF"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.deliveryType == 1}" >
        <fmt:message key="basicinfo.form.trade_conditions.delivery_types.FOB"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.deliveryType == 2}" >
        <fmt:message key="basicinfo.form.trade_conditions.delivery_types.CFR"/>
      </c:if>
      <c:if test="${basic_info.tradeCondition.deliveryType == 3}" >
        <c:out value="${basic_info.tradeCondition.otherValue}" />
      </c:if>      
    </td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.business_information"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th style="white-space:nowrap;"><fmt:message key="basicinfo.form.business_information.number_of_staffs"/> : <br></th>
    <td>
      <table style="width:100%;">
        <tr>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.date"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.number_of_direct_staffs"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.number_of_indirect_staffs"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.total_staffs"/></td>
        </tr>
        <tr>
          <td style="border:none;"><c:out value="${basic_info.businessInformation.date}" /></td>
          <td style="border:none;"><c:out value="${basic_info.businessInformation.numberOfDirectStaffs}" /></td>
          <td style="border:none;"><c:out value="${basic_info.businessInformation.numberOfIndirectStaffs}" /></td>
          <td style="border:none;"><c:out value="${basic_info.businessInformation.totalStaffs}" /></td>          
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th style="vertical-align:middle;"><fmt:message key="basicinfo.form.business_information.performance"/> : <br></th>
    <td>
      <table style="width:100%;">
        <tr>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.year"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.sales_amount"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.profit"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.profitability"/></td>
        </tr>
        <c:forEach var="performance" items="${basic_info.businessInformation.staticPerformanceList}">
        <tr>
          <td><c:out value="${performance.year}" /></td>
          <td><c:out value="${performance.salesAmount}" /></td>
          <td><c:out value="${performance.profit}" /></td>
          <td><c:out value="${performance.profitability}" /></td>
        </tr>
        </c:forEach>
      </table>
    </td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.special_statement"/></div>
<table class="basic_form_table">
<tbody>
<tr>
  <td><pre><c:out value="${basic_info.specialStatement}" /></pre></td>
</tr>
</tbody>
</table>
<div class="index"><fmt:message key="basicinfo.form.bank_information"/></div>
<c:forEach var="account" items="${basic_info.bankInformation.staticBankAccounts}">
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.vendor_subcode"/> : <br></th>
    <td><c:out value="${account.vendorSubcode}" /></td>
    <th><fmt:message key="basicinfo.form.bank_information.province_and_region"/> : <br></th>
    <td style="width:300px;"><c:out value="${account.provinceAndRegion}"  /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.swift_code_no_desc"/> : <br></th>
    <td><c:out value="${account.swiftCode}" /></td>
    <th><fmt:message key="basicinfo.form.bank_information.name"/> : <br></th>
    <td><c:out value="${account.name}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.account"/> : <br></th>
    <td><c:out value="${account.account}" /></td>
    <th><fmt:message key="basicinfo.form.bank_information.city"/> : <br></th>
    <td>
      <c:if test="${account.city == 0}">
        <fmt:message key="basicinfo.form.bank_information.city.others"/>
      </c:if>
      <c:if test="${account.city == 1}">
        <fmt:message key="basicinfo.form.bank_information.city.shanghai"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.currency"/> : <br></th>
    <td>
    	<c:if test="${account.currency == 'RMB'}">CNY</c:if>
    	<c:if test="${account.currency != 'RMB'}"><c:out value="${account.currency}" /></c:if>
    </td>
    <th><fmt:message key="basicinfo.form.bank_information.type"/> : <br></th>
    <td>
      <c:if test="${account.type == 0}">
        <fmt:message key="basicinfo.form.bank_information.type.bank_of_china"/>
      </c:if>
      <c:if test="${account.type == 1}">
        <fmt:message key="basicinfo.form.bank_information.type.other"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.institution_no"/> : <br></th>
    <td><c:out value="${account.institutionNo}" /></td>
    <th><fmt:message key="basicinfo.form.bank_information.joint_code"/> : <br></th>
    <td><c:out value="${account.jointCode}" /></td>
  </tr>
  <tr>
    <th>CNAPS : </th>
    <td><c:out value="${account.cnaps}" /></td>
    <th colspan="2">&nbsp;</th>
  </tr>
</tbody>
</table>
</c:forEach>

<sec:authorize access="hasRole('ROLE_HD') or hasRole('ROLE_FM') or hasRole('ROLE_DM')">
<div class="index"><fmt:message key="basicinfo.form.accounting"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.company_code"/> : <br></th>
    <td>
       <c:if test="${basic_info.accounting.companyCode == 'SSH1'}">
         <fmt:message key="basicinfo.form.accounting.company_code.sscs"/>
       </c:if>
       <c:if test="${basic_info.accounting.companyCode == 'SSZ1'}">
         <fmt:message key="basicinfo.form.accounting.company_code.ss10"/>
       </c:if>
       <c:if test="${basic_info.accounting.companyCode == 'SSZ2'}">
         <fmt:message key="basicinfo.form.accounting.company_code.ss11"/>
       </c:if>
       <c:if test="${basic_info.accounting.companyCode == 'SSD1'}">
         <fmt:message key="basicinfo.form.accounting.company_code.ssd1"/>
       </c:if>
       <c:if test="${basic_info.accounting.companyCode == 'SGD1'}">
         <fmt:message key="basicinfo.form.accounting.company_code.sgd1"/>
       </c:if>
       <c:if test="${basic_info.accounting.companyCode == 'SJS1'}">
         <fmt:message key="basicinfo.form.accounting.company_code.sjs1"/>
       </c:if>
    </td>
    <th><fmt:message key="basicinfo.form.accounting.vendor_account_type"/> : <br></th>
    <td style="width:300px;">
      <c:if test="${basic_info.accounting.vendorAccountType == 'vendor'}">
        <fmt:message key="basicinfo.form.accounting.vendor_account_type.vendor"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorAccountType == 'CompanyCard'}">
        <fmt:message key="basicinfo.form.accounting.vendor_account_type.companyCard"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorAccountType == 'SalaryCard'}">
        <fmt:message key="basicinfo.form.accounting.vendor_account_type.salaryCard"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.vendor_group"/> : <br></th>
    <td>
      <c:if test="${basic_info.accounting.vendorGroup == 'Z001'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.z001"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZFGV'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zfgv"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLAD'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlad"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLOV'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlov"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLRA'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlra"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLRO'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlro"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLSV'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlsv"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZLVO'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zlvo"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZOTV'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zotv"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZSAP'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zsap"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZSCN'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zscn"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZSO1'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zso1"/>
      </c:if>
      <c:if test="${basic_info.accounting.vendorGroup == 'ZSON'}">
         <fmt:message key="basicinfo.form.accounting.vendor_group.zson"/>
      </c:if>            
    </td>
    <th><fmt:message key="basicinfo.form.accounting.group_key"/> : <br></th>
    <td><c:out value="${basic_info.accounting.groupKey}" /></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.recon_account"/> : <br></th>
    <td>
      <c:if test="${basic_info.accounting.reconAccount == '21001000'}">
        <fmt:message key="basicinfo.form.accounting.recon_account.21001000"/>
      </c:if>
      <c:if test="${basic_info.accounting.reconAccount == '21899005'}">
        <fmt:message key="basicinfo.form.accounting.recon_account.21899005"/>
      </c:if>
    </td>
    <th><fmt:message key="basicinfo.form.accounting.payment_term"/> : <br></th>
    <td>
<!-- 
      <c:if test="${basic_info.accounting.paymentTerm == 1}">
        <fmt:message key="basicinfo.form.accounting.payment_term.1"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 2}">
        <fmt:message key="basicinfo.form.accounting.payment_term.2"/>
      </c:if>
-->
      <c:if test="${basic_info.accounting.paymentTerm == 3}">
        <fmt:message key="basicinfo.form.accounting.payment_term.3"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 4}">
        <fmt:message key="basicinfo.form.accounting.payment_term.4"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 5}">
        <fmt:message key="basicinfo.form.accounting.payment_term.5"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 6}">
        <fmt:message key="basicinfo.form.accounting.payment_term.6"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 7}">
        <fmt:message key="basicinfo.form.accounting.payment_term.7"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 8}">
        <fmt:message key="basicinfo.form.accounting.payment_term.8"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 9}">
        <fmt:message key="basicinfo.form.accounting.payment_term.9"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 10}">
        <fmt:message key="basicinfo.form.accounting.payment_term.10"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 11}">
        <fmt:message key="basicinfo.form.accounting.payment_term.11"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 12}">
        <fmt:message key="basicinfo.form.accounting.payment_term.12"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 13}">
        <fmt:message key="basicinfo.form.accounting.payment_term.13"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 14}">
        <fmt:message key="basicinfo.form.accounting.payment_term.14"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 15}">
        <fmt:message key="basicinfo.form.accounting.payment_term.15"/>
      </c:if>
      <c:if test="${basic_info.accounting.paymentTerm == 16}">
        <fmt:message key="basicinfo.form.accounting.payment_term.16"/>
      </c:if>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.chk_double_inv"/> : <br></th>
    <td>
      <c:if test="${basic_info.accounting.chkDoubleInv == true}">
        <fmt:message key="basicinfo.yes"/>
      </c:if>
      <c:if test="${basic_info.accounting.chkDoubleInv == false}">
        <fmt:message key="basicinfo.no"/>
      </c:if>

    </td>
    <th colspan="2">&nbsp;</th>
  </tr>
</tbody>
</table>
</sec:authorize>
 