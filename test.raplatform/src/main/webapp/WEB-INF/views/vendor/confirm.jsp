<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="confirm"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>

<div class="basic_form">
<h1 class="alt"><fmt:message key="confirm"/> - <c:out value="${basic_info.identity.name}" /></h1>
<%@ include file="/WEB-INF/views/vendor/basic_information_part.jsp" %>

<spring:url value="/evaluation/submit_directly/${basic_info.vendorId}" var="formUrl"/>
<form:form action="${fn:escapeXml(formUrl)}"
	method="post">
<button type="submit" style="margin-left:20px;"><fmt:message key="button.submit"/></button>
</form:form>
</div>
</body>
</html>