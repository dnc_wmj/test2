<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="search.title"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />" charset="UTF-8"></script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="data.menu.maintainance"/></h1>
<p><fmt:message key="search.description"/></p>

<form:form modelAttribute="criteria" action="#" method="post" id="form">
 <form:input style="width:300px;" path="text" />
 <input type="submit" value="<fmt:message key="button.search"/>"><br>
 <form:radiobutton value="name" path="type"/><fmt:message key="search.company_name"/>
 <form:radiobutton value="code" path="type"/><fmt:message key="search.vendor_code"/>
 <form:radiobutton value="number" path="type"/><fmt:message key="search.vendor_number"/>
 <!-- zhengtong modif 2012年7月10日18:00:26  -->
 <fmt:message key="basicinfo.form.company_type.category"/>
 <form:select path="category">
   <form:option value="3"><fmt:message key="basicinfo.form.vendor_type.all"/></form:option>
   <form:option value="0"><fmt:message key="basicinfo.form.vendor_type.external"/></form:option>
   <form:option value="1"><fmt:message key="basicinfo.form.vendor_type.internal"/></form:option>
   <form:option value="2"><fmt:message key="basicinfo.form.vendor_type.other"/></form:option>
 </form:select>
  <fmt:message key="basicinfo.form.company_type.parent_company"/>
 <form:select path="superCompanyType" style="width:auto;">
   <form:option value="0"><fmt:message key="basicinfo.form.vendor_type.all"/></form:option>
	   <form:option value="1"><fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/></form:option>
	   <form:option value="2"><fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/></form:option>
	   <form:option value="3"><fmt:message key="basicinfo.form.company_type.parent_company.SSV"/></form:option>
	   <form:option value="4"><fmt:message key="basicinfo.form.company_type.parent_company.SEW"/></form:option>
	   <form:option value="5"><fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/></form:option>
	   <form:option value="6"><fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/></form:option>
	   <form:option value="7"><fmt:message key="basicinfo.form.company_type.parent_company.SEH"/></form:option>
 </form:select>
  <!-- zhengtong end 2012年7月10日18:00:26  -->
</form:form>

<c:if test="${vendors != null}">
<hr>
<span style="font-size:0.8em;margin-left:20px;">
  <c:if test="${criteria.page != 0}">
    <c:url var="prev" value="/data/vendor/search">
      <c:param name="page" value="${(criteria.page - 1)}" />
    </c:url>
    <a href="#" onclick="$('#form').attr('action','<c:out value="${prev}"/>');$('#form').submit();">&lt;-&nbsp;</a>
  </c:if>
  <c:out value="${(criteria.page + 1)}" />/<c:out value="${(pages + 1)}"/>
  <c:if test="${criteria.page < pages}">
  <c:url var="next" value="/data/vendor/search">
    <c:param name="page" value="${(criteria.page + 1)}" />
  </c:url>
  <a href="#" onclick="$('#form').attr('action','<c:out value="${next}"/>');$('#form').submit();">&nbsp;-&gt;</a>
  </c:if>
</span>

<%@ include file="/WEB-INF/views/vendor/vendor_info_table.jsp" %>

</c:if>

</div>