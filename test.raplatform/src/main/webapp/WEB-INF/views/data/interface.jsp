<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="data.interface.title"/></title>
<c:if test="${interfaceFileId != null}">
<META HTTP-EQUIV="Refresh" CONTENT="0;URL=<c:url value="/data/interface/download/${interfaceFileId}"/>">
</c:if>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
});
</script>

<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="data.interface.title"/></h1>
<p>&nbsp;</p>
<form:form action="#"  
	method="post">

<button type="submit"><fmt:message key="data.interface.download"/></button>
</form:form>
<br>
<div id="ui-tab">
  <ul>
    <li><a href="#fragment-1"><span><fmt:message key="data.interface.current"/></span></a></li>
    <li><a href="#fragment-2"><span><fmt:message key="data.interface.past"/></span></a></li>
  </ul>

  <div id="fragment-1">
    <table style="width:500px;" class="default_table">
      <tr>
        <th style="width:150px;"><fmt:message key="data.interface.vendor.id"/></th>
        <th style="width:150px;"><fmt:message key="data.interface.modification.type"/></th>
        <th style="width:150px;"><fmt:message key="data.interface.modification.date"/></th>
      </tr>
      <c:forEach var="dataHistory" items="${dataHistories}">
        <tr>
          <td><c:out value="${dataHistory.vendorCode}"/></td>
          <td>
            <c:if test="${dataHistory.classification == 10}">
              <fmt:message key="data.interface.modification.type.insert"/>
            </c:if>
		    <c:if test="${dataHistory.classification == 20}">
              <fmt:message key="data.interface.modification.type.update"/>
            </c:if>
		    <c:if test="${dataHistory.classification == 30}">
              <fmt:message key="data.interface.modification.type.delete"/>
            </c:if>           
		    <c:if test="${dataHistory.classification == 40}">
              <fmt:message key="data.interface.modification.type.delete.bank"/>
            </c:if>
            <c:if test="${dataHistory.classification == 50}">
              <fmt:message key="data.interface.modification.type.freeze"/>
            </c:if>           
		    <c:if test="${dataHistory.classification == 60}">
              <fmt:message key="data.interface.modification.type.unfreeze"/>
            </c:if>
          </td>
          <td><c:out value="${dataHistory.shanghaiRegisteredDate}"/></td>
        </tr>
      </c:forEach>
    </table>
    
  </div>  
  <div id="fragment-2">
    <table style="width:500px;" class="default_table">
      <tr>
        <th style="width:150px;"><fmt:message key="data.interface.past.created.date"/></th>
      </tr>
      <c:forEach var="fileHistory" items="${fileHistories}">
        <tr>
          <td><a href="<c:url value="/data/interface/download/${fileHistory.id}"/>"><c:out value="${fileHistory.shanghaiCreatedDate}"/></a></td>
        </tr>
      </c:forEach>
    </table>
  </div>

</div>
</div>
</div>

</body>
</html>