<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="data.change.password"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="data.change.password"/></h1>
<p></p>
<spring:url value="/data/change/password" var="formUrl"/>
<form:form modelAttribute="changePassword" action="${formUrl}" method="post">
<table class="basic_form_table">
 <tr>
   <th style="width:180px;border:none;"><fmt:message key="data.change.password.new"/> : <br>
      <form:errors path="newPassword" cssClass="errors"/>
   </th>  
   <td style="border:none;"><form:password style="width:300px;" path="newPassword" /></td>
 </tr>
 <tr>
   <th style="width:180px;border:none;"><fmt:message key="data.change.password.confirm"/> : <br>
      <form:errors path="newPassword" cssClass="errors"/></th>  
   <td style="border:none;"><form:password style="width:300px;" path="passwordConfirmation" /></td>
 </tr>
 <tr>
   <td style="border:none;">&nbsp;</td>
   <td style="border:none;"><button type="submit"><fmt:message key="data.change.password"/></button></td>
 </tr>
</table>

</form:form>
</div>
</div>
</body>
</html>