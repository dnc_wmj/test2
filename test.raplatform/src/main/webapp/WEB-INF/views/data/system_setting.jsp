<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="system.title"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
});
</script>

<div class="mainContainer">
<div class="basic_form contextContainer"  style="width:960px;">
<h1 class="alt"><fmt:message key="system.title"/></h1>
<p>&nbsp;</p>
<form:form action="#" 
	modelAttribute="setting" 
	method="post" >

<button type="submit"><fmt:message key="system.button.set"/></button>
<p></p>
  <fmt:message key="system.reeval.month"/> : 
  <form:select path="reEvalMonth">
  	<form:option value="12">12</form:option>
	<form:option value="11">11</form:option>
	<form:option value="10">10</form:option>
	<form:option value="9">9</form:option>
	<form:option value="8">8</form:option>
	<form:option value="7">7</form:option>
	<form:option value="6">6</form:option>
	<form:option value="5">5</form:option>
	<form:option value="4">4</form:option>
  </form:select>
  &nbsp;&nbsp;<form:input path="freezeDay" style ="width: 20px;"/><c:if test="${errorSpan}"><span class="error" style="color: red;">(*<fmt:message key="error.isnotNumber"/>)</span></c:if>
  <fmt:message key="system.freezeDay.comment"/>
<p></p>
<div id="ui-tab">
  <ul>
    <li><a href="#fragment-1"><span><fmt:message key="system.mail.approval.request"/></span></a></li>
    <li><a href="#fragment-2"><span><fmt:message key="system.mail.denied"/></span></a></li>
    <li><a href="#fragment-6"><span><fmt:message key="system.mail.reject"/></span></a></li>
    <li><a href="#fragment-7"><span><fmt:message key="system.mail.cancel"/></span></a></li>
    <li><a href="#fragment-3"><span><fmt:message key="system.mail.application.finished"/></span></a></li>
    <li><a href="#fragment-4"><span><fmt:message key="system.mail.reevaluation"/></span></a></li>
    <li><a href="#fragment-8"><span><fmt:message key="system.mail.freeze.user_valid"/></span></a></li>
    <li><a href="#fragment-9"><span><fmt:message key="system.mail.freeze.user_invalid"/></span></a></li>
    <li><a href="#fragment-5"><span><fmt:message key="system.mail.password"/></span></a></li>
    <li><a href="#fragment-10"><span><fmt:message key="system.mail.certatt"/></span></a></li>
  </ul>

  <div id="fragment-1">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="approvalRequestMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="approvalRequestMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
  <div id="fragment-2">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="deniedMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="deniedMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div> 
  <div id="fragment-6">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="rejectMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="rejectMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
  <div id="fragment-7">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="cancelMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="cancelMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div> 
  <div id="fragment-3">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="applicationFinishedMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="applicationFinishedMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>  
  <div id="fragment-4">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="reEvaluationRequestMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="reEvaluationRequestMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
  <div id="fragment-8">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="freezeValidMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="freezeValidMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
  <div id="fragment-9">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="freezeInvalidMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="freezeInvalidMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
  <div id="fragment-5">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="passwordNotificationMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="passwordNotificationMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>  
  <div id="fragment-10">
    <table class="basic_form_table">
      <tr>
        <td style="text-align:right;width:80px;"><fmt:message key="system.mail.subject"/> : </td>
        <td><form:input path="certattMailSubject"/></td>
      </tr>
      <tr>
        <td style="text-align:right;"><fmt:message key="system.mail.template"/> : </td>
        <td><form:textarea style="width:100%;height:30em;" path="certattMailTemplate"></form:textarea></td>
      </tr>
    </table>
  </div>
</div>
</form:form>
</div>
</div>
</body>
</html>