<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="confirm"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.upload-1.0.2.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
  <spring:hasBindErrors name="submit">
  $('#ui-tab > ul').tabs("select","fragment-2");
  </spring:hasBindErrors>
	
    $("#attachment").change(function() {
	   $(this).upload('<c:url value="/evaluation/attachment/${basic_info.tranId}" />', function(res) {
			if (res.error != undefined && res.error) {
				alert("Faild to upload.");
				return false;
			}
			if (res != undefined && res == 'maxUploadSizeFailure') {
				alert("The uploaded file exceeds the maximum limit.");
				return false;
			}
			$(res).insertAfter(this);
		}, 'html');
	});
    $("#costcenter").change(function(evObj) {
    	$this = $(this);
    	var costcenter = $this.val();
    	var supercompany = '${supercompany}';
      	//var urltol1 = '<c:url value="/evaluation/changeCostcenterL1/${supercompany}" />' +'/'+ costcenter;
     	//var urltol2 = '<c:url value="/evaluation/changeCostcenterL2/${supercompany}" />' +'/'+ costcenter;
      
    	$("#l1").load('<c:url value="/evaluation/changeCostcenterL1/" />' +'/'+ supercompany +'/'+ costcenter);
 	    $("#l2").load('<c:url value="/evaluation/changeCostcenterL2/" />' +'/'+ supercompany +'/'+ costcenter);
 	});
});
function delAttachment(attachment, aid, tid) {
	$.ajax({
		type: 'POST',
	    url: '<c:url value="/evaluation/attachment_workflow/delete"/>/' + aid +'/' + tid + '?_='+new Date(),
	    cache: false,
	    success : function(data){
	    	$("#" + $(attachment).attr('class')).remove();
	    }
	});
	$('#attachment').val("");
	return false;
} 

function changeMethod() {
	$('#submit').attr('method', 'get');
} 
</script>

<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="confirm"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>&nbsp;</p>
<c:if test="${basicInfoUpdate == null && basicInfoDelete == null && basicInfoFreeze == null && basicInfoUnfreeze == null}">
	<c:if test="${tmpRuleId != null}">
		<spring:url value="/evaluation/submit/${vendor_id}/${eval_id}/${tmpRuleId}" var="formUrl"/>
		<c:if test="${basic_info.identity.vendorType == 0 && eval_id == -1}" >
			<spring:url value="/evaluation/register/${vendor_id}/${tmpRuleId}" var="formUrl"/>
		</c:if>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<spring:url value="/evaluation/submit/${vendor_id}/${eval_id}" var="formUrl"/>
	</c:if>
</c:if>
<c:if test="${basicInfoDelete != null}">
<spring:url value="/vendor/delete/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoUpdate != null}">
<spring:url value="/vendor/edit/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoFreeze != null}">
<spring:url value="/vendor/freeze/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoUnfreeze != null}">
<spring:url value="/vendor/unfreeze/submit/${vendor_id}" var="formUrl"/>
</c:if>

<c:if test="${tmpRuleId != null}">
<spring:url value="/evaluation/retmpsave/${vendor_id}/${eval_id}/${tmpRuleId} " var="tempsvaeUrl"/>
</c:if>
<c:if test="${tmpRuleId == null}">
<spring:url value="/evaluation/tmpsave/${vendor_id}/${eval_id} " var="tempsvaeUrl"/>
</c:if>
<form:form action="${fn:escapeXml(formUrl)}"
	modelAttribute="submit" 
	method="post" cssClass="contextContainer" class="form">
	<input type="hidden" name="result" value="${evaluation.result}"/>
<c:if test="${evaluation.result == 0 || evaluation.result == 3}">
<table class="basic_form_table">
  <tr>
    <td style="text-align:left;">
      <span style="font-size:1.3em;color:green;">
        <fmt:message key="workflow.applicant"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l1"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l2"/> 
        <c:if test="${isSSCS}">
	        <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <c:if test="${evaluation.result == 0}">[CEO] <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" /></c:if>
	        <fmt:message key="workflow.hd"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <fmt:message key="workflow.fm"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <fmt:message key="workflow.dm"/>
        </c:if>
      </span>
    </td>
  </tr>
</table>
<table class="basic_form_table" style="border-top:none">    
<tbody>
  <tr>
    <th><fmt:message key="workflow.costcenter"/> : </th>
    <td>
      <form:select path="costcenter" multiple="false" id="costcenter" style="width:300px;">
        <c:forEach var="costcenter" items="${costcenter}">
          <option value="${costcenter}" <c:if test="${selectedDivision == costcenter}">selected="true"</c:if>>${costcenter}
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="workflow.l1"/> : </th>
    <td>
      <form:select path="l1" multiple="false" id="l1" style="width:300px;">
      	<option value="">
        <c:forEach var="l1" items="${l1s}">
          <option value="${l1.id}">${l1.name}(${l1.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="workflow.l2"/> : </th>
    <td style="<c:if test="${l2NullErrer}">background-color: red;</c:if>">
      <form:select path="l2" multiple="false" id="l2" style="width:300px;">
        <c:forEach var="l2" items="${l2s}">
          <option value="${l2.id}">${l2.name}(${l2.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <c:if test="${isSSCS}">
	  <c:if test="${evaluation.result == 0}">
		  <tr>
		    <th>CEO : </th>
		    <td>
		      <form:select path="ceo" multiple="false" style="width:300px;">
		        <c:forEach var="ceo" items="${ceos}">
		          <option value="${ceo.id}">${ceo.name}(${ceo.id})
		        </c:forEach>
		      </form:select>
		    </td>
		  </tr>  
	  </c:if>
	  <tr>
	    <th><fmt:message key="workflow.hd"/> : </th>
	    <td>
	      <form:select path="hd" multiple="false" style="width:300px;">
	        <c:forEach var="hd" items="${hds}">
	          <option value="${hd.id}">${hd.name}(${hd.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.fm"/> : </th>
	    <td>
	      <form:select path="fm" multiple="false" style="width:300px;">
	        <c:forEach var="fm" items="${fms}">
	          <option value="${fm.id}">${fm.name}(${fm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.dm"/> : </th>
	    <td>
	      <form:select path="dm" multiple="false" style="width:300px;">
	        <c:forEach var="dm" items="${dms}">
	          <option value="${dm.id}">${dm.name}(${dm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
</c:if>  
  <c:if test="${evaluation.totalPointForCompare >= 70}">
	  <tr>
	    <td style="border:none;">
	    	<c:if test="${tmpRuleId != null && basic_info.identity.vendorType == 0 && eval_id == -1}">
	    		<button type="submit" onclick="changeMethod()" style="margin-left:20px;"><fmt:message key="button.evaluation"/></button>
	    	</c:if>
	    	<c:if test="${!(tmpRuleId != null && basic_info.identity.vendorType == 0 && eval_id == -1)}">
	    		<button type="submit" style="margin-left:20px;"><fmt:message key="button.submit"/></button>
	    	</c:if>
	      
	    </td>
	    <c:if test="${tmpRuleId == null && basicInfoUpdate == null && basicInfoDelete == null && basicInfoFreeze == null && basicInfoUnfreeze == null}">
		    <td style="border:none;">
		      <button type="button" onclick="window.location='${fn:escapeXml(tempsvaeUrl)}'"style="margin-left:20px;"><fmt:message key="button.save"/></button>
		    </td>
	    </c:if>
	  </tr>
  </c:if>
</tbody>
</table>
</c:if> 
<table style="width:600px;" class="default_table">
<tr>
  <th style="width:100px;"><fmt:message key="basicinfo.form.attachment"/></th>
  <td>
  <c:forEach var="attachment" items="${attachments}">
    <a href="<c:url value="/vendor/attachment/download/${attachment.id}"/>"><c:out value="${attachment.name}"/></a><br>
  </c:forEach>
  </td>
</tr>
</table>

<div class="index"><span style="color:red;">* </span><fmt:message key="evaluation.attachment.title"/>
	<c:if test="${attachmentsIsRequired}"><span class="errors" style="font-size: 12px;"><fmt:message key="error.required"/></span></c:if>
</div>
<table class="basic_form_table"">
<tbody>
  <tr>
    <th style="text-align:left;">
    	<div style="margin-top:2px;">
    	<a href="<c:url value="/samples/VendorCompare.xls"/>"><fmt:message key="evaluation.attachment.template"/></a>
    	</div>
    	<input id="attachment" name="file" type="file" class="<c:if test='${attachmentsIsRequired}'>input_error</c:if>"/>
    	
      <c:forEach var="attachment2" items="${attachment_workflow}">
	  <div id="a-<c:out value="${attachment2.id}"/>">
        <table>
          <tr><td rowspan="4" style="border:none;">
            <a class="a-<c:out value="${attachment2.id}"/>" onClick="delAttachment(this, '<c:out value='${attachment2.id}'/>', '<c:out value='${attachment2.tranId}'/>');return false;"><img src="<c:url value="/images/action_delete.png"/>"/></a>
          </td></tr>
          <tr><td style="border:none;">Content Type : <c:out value="${attachment2.contentType}" /></td></tr>
          <tr><td style="border:none;">Name : <c:out value="${attachment2.name}" /><br></td></tr>
          <tr><td style="border:none;">Size : <c:out value="${attachment2.size}" /><br></td></tr>
        </table>
        <hr>
       </div>      
     </c:forEach>
    </th>
  </tr>
</tbody>
</table>

<hr>
<div id="ui-tab">
  <ul>
    <c:if test="${internal == false}">
    <li><a href="#fragment-1"><span><fmt:message key="evaluation.title"/></span></a></li>
    </c:if>
    
    <li><a href="#fragment-2"><span><fmt:message key="basicinfo.detail"/></span></a></li>
  </ul>

  <c:if test="${internal == false}">
  <div id="fragment-1">
  	<c:if test="${evaluation.businessType < 3}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
    </c:if>
    <c:if test="${evaluation.businessType >= 3 && evaluation.businessType <= 9}">
    	 <!-- 临时保存，旧非贸以后不能再有新供应商申请，所以不进行对应 -->
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part.jsp" %>
    </c:if>
  </div>  
  </c:if>
  <div id="fragment-2">
    <%@ include file="/WEB-INF/views/vendor/basic_information_part.jsp" %>
  </div>

</div>
</form:form>
</div>
</div>
</body>
</html>