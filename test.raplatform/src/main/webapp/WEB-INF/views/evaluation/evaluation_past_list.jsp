<%@ include file="/WEB-INF/views/includes.jsp" %>
<div class="basic_form" style="width:840px">
    <table style="width:380px;" class="default_table">
      <tr>
        <th style="width:50px;">&nbsp;</th>
        <th style="width:150px:"><fmt:message key="confirm.total_point"/></th>
        <th style="width:150px:"><fmt:message key="evaluation.date"/></th>
      </tr>
      <c:forEach var="eval" items="${pastEvaluations}">
        <tr>
          <td style="width:100px;">
            <div class="button">
              <a href="<c:url value="/evaluation/view/${eval.id}"/>"><fmt:message key="workflow.detail"/></a>
            </div>
          </td>  
          <td><c:out value="${eval.totalPoint}"/></td>
          <td><c:out value="${eval.shanghaiRegisteredDate}"/></td>
        </tr>
      </c:forEach>
    </table>
</div>
