<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="evaluation.title"/></title>
</head>
<body>
	<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
	<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
	<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
	<link rel="stylesheet" href="<c:url value="/styles/basic_non_trade_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
	<script type="text/javascript">
		$(document).ready(function(){
			if($('#tranCond2').get(0).checked == true)
				$(".setVendorCode").css('display','');
			
			$('.tranCond1').click(function(){
				$(".setVendorCode").css('display','none');
				$("#vendorCode").val("");
			});
			
			$('.tranCond2').click(function(){
				$(".setVendorCode").css('display','');
			});
		 	
			$('.btn_submit').click(function(){
				$('.errorScore').removeClass('errorScore');
			  	var validate = true;
			  	if($('#tranCond2').get(0).checked == true)
			  		if($('#vendorCode').val() == ''){
			  			$('#vendorCode').addClass('errorScore');
			  			validate = false;
			  		}
			  			
			  	if(validate){
			  		$('.form').submit();
			  	}else{
			  		return false;
			  	}
			  	
			});
		})
	</script>
	<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8" />
	<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8" />
	<div class="mainContainer">
		<div class="basic_form contextContainer">
			<h1 class="alt">
				<fmt:message key="evaluation.title" />
				-
				<c:out value="${identity.name}" />
			</h1>
			
			<c:if test="${tmpRuleId != null}">
				<c:if test="${!reeval}">
					<spring:url value="/evaluation/register/${vendor_id}/${tmpRuleId}" var="formUrl" />
				</c:if>
				
				<c:if test="${reeval}">
					<!-- 临时保存的一定不是再评估 -->
					<spring:url value="/evaluation/reeval/${vendor_id}/${tmpRuleId}" var="formUrl" />
				</c:if>
			</c:if>
			<c:if test="${tmpRuleId == null}">
				<c:if test="${!reeval}">
					<spring:url value="/evaluation/register/${vendor_id}" var="formUrl" />
				</c:if>
				<c:if test="${reeval}">
					<spring:url value="/evaluation/reeval/${vendor_id}" var="formUrl" />
				</c:if>
			</c:if>
			
			<form:form action="${fn:escapeXml(formUrl)}" modelAttribute="evaluation" method="post">
				<c:if test="${evaluation.businessType != 2}">
					<div class="index" style="color: green;">
						<fmt:message key="evaluation.title.1" />
					</div>
					<table class="basic_form_table">
						<tbody>
							<tr>
								<th><fmt:message key="evaluation.company_short_name" /> :</th>
								<td style="width: 390px;"><form:input path="shortName" /></td>
								<td style="color: green;"><fmt:message key="evaluation.company_short_name.description" /></td>
							</tr>

							<c:if test="${evaluation.businessType == 1}">
								<tr>
									<th><fmt:message key="evaluation.product_name" /> :</th>
									<td><form:input path="productName" /></td>
									<td style="color: green;"><fmt:message key="evaluation.product_name.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.product_category" /> :</th>
									<td><form:input path="productCategory" /></td>
									<td style="color: green;"><fmt:message key="evaluation.product_category.description" /></td>
								</tr>
							</c:if>

							<tr>
								<th><fmt:message key="evaluation.transaction_condition" /> :</th>
								<td>
									<table>
										<tr>
											<td style="border: none;"><form:radiobutton value="0" path="tranCond" class="tranCond1"/> 
												<fmt:message key="evaluation.transaction_condition.new" />
											</td>
											<td style="border: none;"><form:radiobutton value="1" path="tranCond" class="tranCond2"/> 
												<fmt:message key="evaluation.transaction_condition.existing" />
											</td>
											<td style="border:none;">
									            <div class="setVendorCode" style="display:none;">
									            	&nbsp;&nbsp;<span style="color:red;">* </span>
									            	<fmt:message key="basicinfo.form.vendor_code"/> : 
									            	<input type="text" name="vendorCode" style="width: 110px;" <c:if test="${reeval}">disabled="true"</c:if> MAXLENGTH="10" value="${evaluation.vendorCode}"/><form:errors path="vendorCode" cssClass="errors"/>
									            </div>
									       </td>
										</tr>
									</table>
								</td>
								<td style="color: green;"><fmt:message key="evaluation.transaction_condition.description" /></td>
							</tr>

							<c:if test="${evaluation.businessType == 0}">
								<tr>
									<th><fmt:message key="evaluation.service_category" /> :</th>
									<td><form:select path="serviceCategory">
											<form:option value="0">
												<fmt:message key="evaluation.service_category.aircargo" />
											</form:option>
											<form:option value="1">
												<fmt:message key="evaluation.service_category.seacargo" />
											</form:option>
											<form:option value="2">
												<fmt:message key="evaluation.service_category.warehouse" />
											</form:option>
											<form:option value="3">
												<fmt:message key="evaluation.service_category.importing.exporting" />
											</form:option>
											<form:option value="4">
												<fmt:message key="evaluation.service_category.track.transportation" />
											</form:option>
											<form:option value="5">
												<fmt:message key="evaluation.service_category.other" />
											</form:option>
										</form:select></td>
									<td style="color: green;"><fmt:message key="evaluation.service_category.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.service_industry" /> :</th>
									<td><form:input path="serviceIndustry" /></td>
									<td style="color: green;"><fmt:message key="evaluation.service_industry.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.service_scope" /> :</th>
									<td><form:input path="serviceScope" /></td>
									<td style="color: green;"><fmt:message key="evaluation.service_scope.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.major_accounts" /> :</th>
									<td><form:input path="majorAccounts" /></td>
									<td style="color: green;"><fmt:message key="evaluation.major_accounts.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.volume_per_year" /> :</th>
									<td><form:input path="volumePerYear" /></td>
									<td style="color: green;"><fmt:message key="evaluation.volume_per_year.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.num_of_biz_sites_nationwide" /> :</th>
									<td><form:input path="numOfBizSitesNationwide" /></td>
									<td style="color: green;"><fmt:message key="evaluation.num_of_biz_sites_nationwide.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.forwarding_company" /> :</th>
									<td><form:input path="forwardingCompany" /></td>
									<td style="color: green;"><fmt:message key="evaluation.forwarding_company.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.info_fix_assets_nationwide" /> :</th>
									<td><form:input path="infoFixAssetsNationwide" /></td>
									<td style="color: green;"><fmt:message key="evaluation.info_fix_assets_nationwide.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.num_of_empl_nationwide" /> :</th>
									<td><form:input path="numOfEmplNationwide" /></td>
									<td style="color: green;"><fmt:message key="evaluation.num_of_empl_nationwide.description" /></td>
								</tr>
								<tr>
									<th><fmt:message key="evaluation.cooperation_condition" /> :</th>
									<td><form:textarea path="cooperationCondition" style="font-size:12px;width:350px;height:3em;"></form:textarea></td>
									<td style="color: green;"><fmt:message key="evaluation.cooperation_condition.description" /></td>
								</tr>
							</c:if>

							<tr>
								<th><fmt:message key="evaluation.selection_reason" /> :</th>
								<td><form:textarea path="selectionReason" style="font-size:12px;width:390px;height:3em;"></form:textarea></td>
								<td style="color: green;"><fmt:message key="evaluation.selection_reason.description" /></td>
							</tr>
							<tr>
								<th><fmt:message key="evaluation.comment" /> :</th>
								<td><form:textarea path="comment" style="font-size:12px;width:390px;height:3em;"></form:textarea></td>
								<td style="color: green;"><fmt:message key="evaluation.comment.description" /></td>
							</tr>
							<tr>
								<th><fmt:message key="evaluation.applicant" /> :</th>
								<td><form:input path="applicant" /></td>
								<td style="color: green;"><fmt:message key="evaluation.applicant.description" /></td>
							</tr>
							<tr>
								<th><fmt:message key="evaluation.date2" /> :</th>
								<td><form:input path="date2" /></td>
								<td style="color: green;"><fmt:message key="evaluation.date2.description" /></td>
							</tr>
						</tbody>
					</table>

					<div class="index" style="color: green;">
						<fmt:message key="evaluation.title.2" />
					</div>
				</c:if>
				<%int index = 0; %>
				<c:forEach var="category" items="${evaluationCategories}" varStatus="ctgrStatus">
					<div class="index">
						<c:out value="${ctgrStatus.count}" />
						.
						<c:out value="${category.name}" />
						&lt;MAX
						<c:out value="${category.maxPoint}" />
						Points &gt;
					</div>
					<table class="default_table">
						<tbody>


							<!-- 2012-7-6 15:51:08 zhengtong modif -->
							<!-- 
								   <tr>
								    <th>Detail Content</th> 具体内容
								    <th>Evaluation Standards</th>评估标准
								    <th>Standards</th>打分标准
								    <th>Points</th>分数
								    
								    <th>Remark</th> 说明
								  </tr>
							-->
							<tr>
								<th><fmt:message key="evaluation.form.trade.head.Detail-Content" /></th>
								<th><fmt:message key="evaluation.form.trade.head.Evaluation-Standards" /></th>
								<th><fmt:message key="evaluation.form.trade.head.Standards" /></th>
								<th><fmt:message key="evaluation.form.trade.head.Points" /></th>

								<th><fmt:message key="evaluation.form.trade.head.Remark" /></th>
							</tr>
							<!-- 2012-7-6 15:51:08 zhengtong modif end  -->
							<c:forEach var="content" items="${category.evaluationContents}" varStatus="contentStatus">
								<c:forEach var="item" items="${content.evaluationItems}" varStatus="itemStatus">
									<tr>
										<c:if test="${itemStatus.count == 1}">
											<td style="width: 130px;" rowspan="<c:out value="${content.evaluationItemLength}"/>"><c:out
													value="${content.content}"></c:out></td>
										</c:if>
										<td style="width: 200px;"><c:out escapeXml="false" value="${item.evaluationStandard}"></c:out></td>

										<c:choose>
											<c:when test="${itemStatus.count == 1 && ctgrStatus.count == 2 && contentStatus.count == 1}">
												<c:choose>
													<c:when test="${content.evaluationItemLength == 2}">
														<td rowspan="2" />
													</c:when>
													<c:otherwise>
														<td rowspan="4" />
													</c:otherwise>
												</c:choose>
												<c:out escapeXml="false" value="${item.gradingStandard}"></c:out>
												</td>
											</c:when>
											<c:when test="${itemStatus.count == 2 && ctgrStatus.count == 2 && contentStatus.count == 1}">

											</c:when>
											<c:when test="${itemStatus.count == 3 && ctgrStatus.count == 2 && contentStatus.count == 1}">

											</c:when>
											<c:when test="${itemStatus.count == 1 && ctgrStatus.count == 2 && contentStatus.count == 2}">

											</c:when>
											<c:otherwise>
												<td><c:out escapeXml="false" value="${item.gradingStandard}"></c:out></td>
											</c:otherwise>
										</c:choose>
										<td style="width: 50px; text-align: center;"><form:select path="point" style="border:solid 1px #aacfe4;"
												multiple="false">
												<c:forEach var="point" items="${item.pointArray}">
													<%
											            boolean selected = false;
														int[] points = ((Evaluation)pageContext.findAttribute("evaluation")).getPoint();
														
											            if (points !=null 
											            		&& Integer.parseInt((String)pageContext.getAttribute("point")) == points[index]) {
											            	selected = true;
											            }
										      		%>
										      		<option value="${point}" <%=selected ? "selected" : "" %>>
										            ${point}
												</c:forEach>
											</form:select></td>
										<td style="width: 150px;">
											<% 
												String[] remarks = ((Evaluation)pageContext.findAttribute("evaluation")).getRemark();
												String remark = remarks == null ? "" : remarks[index];
												   
									            if (remark == null || "".equals(remark)) {
									        	   remark = "";
									            }
									         %>
											<textarea name="remark" style="font-size:12px;border:solid 1px #aacfe4;width:100%;height:3em;"><%= remark%></textarea>
										</td>
									</tr>
									<% index++; %>
								</c:forEach>
							</c:forEach>
						</tbody>
					</table>
				</c:forEach>

				<hr>

				<button type="submit" style="margin-left: 20px;" class = "btn_submit">
					<fmt:message key="button.submit" />
				</button>
			</form:form>
		</div>
	</div>
</body>
</html>