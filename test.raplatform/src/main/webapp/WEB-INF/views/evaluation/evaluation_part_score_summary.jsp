<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>

<br>
<c:if test="${editable}">
<div class="button">
<c:if test="${workflow == null && !isReeval}">
	<c:if test="${tmpRuleId != null}">
		<a href="<c:url value="/evaluation/tmpsave/updateform/${eval_id}/${tmpRuleId}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<a href="<c:url value="/evaluation/updateform/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
</c:if>
<c:if test="${workflow == null && isReeval}">
<a href="<c:url value="/evaluation/updateform/reeval/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
<c:if test="${workflow != null}">
<a href="<c:url value="/evaluation/updateform/${workflow.evalId}/${workflow.id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
</div>
<br>
</c:if>
	<!-- 
		evaluation.form.trade.head.selection=复查
		evaluation.form.trade.head.weight=权重
		evaluation.form.trade.head.scorepackage=类型
		evaluation.form.trade.head.grandtotal=总计
		evaluation.form.trade.head.scorepackage.info=根据业务内容选择企业类型。
		evaluation.form.trade.head.weight.type=此权重的业务类型为
		evaluation.form.trade.head.weight.type2=
		evaluation.form.trade.head.weight.type3=
		evaluation.form.trade.head.weight.type4=
		<fmt:message key="evaluation.form.trade.head.weight.type"/>
 	-->
  <div class="index" style="color:green;"></div>
	<table class="basic_non_trade_form_table">
	<tbody>
	  <tr>
	  	<td style="width: 100px;"></td>
	  	<td style="width: 100px;">
		  	<c:if test="${!isReeval}">
		  		<b><fmt:message key="evaluation.form.trade.head.selection"/></b>
		  	</c:if>	
		  	<c:if test="${isReeval}">
		  		<b><fmt:message key="evaluation.form.trade.head.selection2"/></b>
		  	</c:if>
	  	</td>
	  	<td style="width: 100px;"><b><fmt:message key="evaluation.form.trade.head.weight"/></b></td>
	  	<td></td>
	  </tr>
	  <tr>
	   	<td style="width: 100px;"></td>
	    <td><fmt:message key="evaluation.form.trade.head.scorepackage"/></td>
	    <td>
	    	<c:if test="${evaluation.type == 10}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 11}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 12}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 13}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 14}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 15}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 16}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
	      	</c:if>
	    </td>
	    <td>
	    	<b><fmt:message key="evaluation.form.trade.head.scorepackage.info"/></b>
	    </td>
	  </tr>
	  <c:forEach var="form" items="${nonTradeEvaluationForm}" varStatus="formStatus">
		<tr>
			<td style="width: 100px;"></td>
			<td><c:out escapeXml="false" value="${form.content}"></c:out></td>
			<td><c:out escapeXml="false" value="${form.showWeight}"></c:out></td>
			<td></td>
		</tr>
	  </c:forEach>
	  <tr>
	  	<td style="width: 100px;"></td>
	  	<td><b><fmt:message key="evaluation.form.trade.head.grandtotal"/></b></td>
	  	<td>100%</td>
	  	<td><b><fmt:message key="evaluation.form.trade.head.weight.type"/>  
			<c:if test="${evaluation.type == 10}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/><fmt:message key="evaluation.form.trade.head.weight.type2"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 11}">
	      	  <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/><fmt:message key="evaluation.form.trade.head.weight.type2"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 12}">
	      	 <fmt:message key="evaluation.form.trade.head.weight.type3"/><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 13}">
	      	 <fmt:message key="evaluation.form.trade.head.weight.type4"/><fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 14}">
	      	 <fmt:message key="evaluation.form.trade.head.weight.type3"/><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 15}">
	      	 <fmt:message key="evaluation.form.trade.head.weight.type3"/><fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
	      	</c:if>
	      	<c:if test="${evaluation.type == 16}">
	      	 <fmt:message key="evaluation.form.trade.head.weight.type4"/><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
	      	</c:if>
	      	</b>
	      </td>
	  </tr>
	</tbody>
	</table>
	
	<div class="index" style="color:green;"><fmt:message key="evaluation.title.2"/></div>
<table class="default_non_trade_table">
	<tbody>
	  <!-- 2012-7-6 15:51:08 zhengtong modif -->
	<!-- 
	  <tr> 
	  	<th></th>
	   	分类 
	         子	分类 
	         项目
	         分数总计
	         最高分数总计
	         权重
	         标准权重
	         权重分数总计
	         最高权重分数总计
	  </tr>
	 -->
	  <tr>
	  	<th></th>
	    <th><fmt:message key="evaluation.form.head.Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Sub-Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Items"/></th>
	    <th><fmt:message key="evaluation.form.head.ttlscore"/></th>
	    <th><fmt:message key="evaluation.form.head.ttltopscore"/></th>
	    <th><fmt:message key="evaluation.form.head.weight"/></th>
	    <th><fmt:message key="evaluation.form.head.normalizedweight"/></th>
	    <th><fmt:message key="evaluation.form.head.ttlweightscore"/></th>
	    <th><fmt:message key="evaluation.form.head.ttltopweightscore"/></th>
	  </tr>
	<c:forEach var="form" items="${nonTradeEvaluationForm}" varStatus="formStatus">
	  <c:forEach var="category" items="${form.categoryList}" varStatus="categoryStatus">
	  	<c:forEach var="subCategory" items="${category.subCategorysList}" varStatus="subCategoryStatus">
			<tr style="background-color: #FFFFBB;">
				<td style="width:90px;">
		          <c:out escapeXml="false" value="${form.content}"></c:out>
		        </td>
				<td style="width:215px;">
		          <c:out escapeXml="false" value="${category.content}"></c:out>
		        </td>
		        <td style="width:190px;">
			      <c:out escapeXml="false" value="${subCategory.content}"></c:out>
			    </td>
			    <td style="width:85px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.itemsCount}"></c:out>
			    </td>
			    <td style="width:70px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.ttlScore}"></c:out>
			    </td>
			    <td style="width:70px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.ttlTopScore}"></c:out>
			    </td>
			    <td style="width:70px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.showWeight}"></c:out>
			    </td>
			    <td style="width:90px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.showNormalizedWeight}"></c:out>
			    </td>
			    <td style="width:60px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.ttlWeightScore}"></c:out>
			    </td>
			    <td style="width:80px; text-align: center;">
			      <c:out escapeXml="false" value="${subCategory.ttlTopWeightScore}"></c:out>
			    </td>
			</tr>
	  	</c:forEach>
	  	<tr style="background-color: #FFFF33;">
			<td>
	          <c:out escapeXml="false" value="${form.content}"></c:out>
	        </td>
			<td colspan="2">
	          <c:out escapeXml="false" value="${category.content}"></c:out>
	        </td>
		    <td style="text-align: center;"> 
		      <c:out escapeXml="false" value="${category.itemsCount}"></c:out>
		    </td>
		    <td style="text-align: center;"> 
		      <c:out escapeXml="false" value="${category.ttlScore}"></c:out>
		    </td>
		    <td style="text-align: center;">
		      <c:out escapeXml="false" value="${category.ttlTopScore}"></c:out>
		    </td>
		    <td>
		    </td>
		    <td>
		    </td>
		    <td style="text-align: center;">
		      <c:out escapeXml="false" value="${category.ttlWeightScore}"></c:out>
		    </td>
		    <td style="text-align: center;">
		      <c:out escapeXml="false" value="${category.ttlTopWeightScore}"></c:out>
		    </td>
		</tr>
	  </c:forEach>
	  <tr style="background-color: #888800; color: #ffffff">
		<td colspan="3">
          <c:out escapeXml="false" value="${form.content}"></c:out> TTL
        </td>
	    <td style="text-align: center;">
	      <c:out escapeXml="false" value="${form.itemsCount}"></c:out>
	    </td>
	    <td style="text-align: center;">
	      <c:out escapeXml="false" value="${form.ttlScore}"></c:out>
	    </td>
	    <td style="text-align: center;">
	      <c:out escapeXml="false" value="${form.ttlTopScore}"></c:out>
	    </td>
	    <td>
	    </td>
	    <td>
	    </td>
	    <td style="text-align: center;">
	      <c:out escapeXml="false" value="${form.ttlWeightScore}"></c:out>
	    </td>
	    <td style="text-align: center;">
	      <c:out escapeXml="false" value="${form.ttlTopWeightScore}"></c:out>
	    </td>
	</tr>
	</c:forEach>
	</tbody>
</table>
<br>
<table style="float: right; BORDER-COLLAPSE: collapse; margin: 0 8px;" borderColor=#000000 cellSpacing=0 width=250 align=center bgColor=#ffffff border=1>
	<tr>
		<th>Grand TTL</th>
		<th><c:out escapeXml="false" value="${evaluation.grandScore}"></c:out></th>
		<th><c:out escapeXml="false" value="${evaluation.grandTopScore}"></c:out></th>
	</tr>
</table>