<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="confirm"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_non_trade_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.upload-1.0.2.js" />"></script>
<spring:url value="/vendor/temp/save/${vendor_id}/${eval_id}" var="form2Url"/>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
  <spring:hasBindErrors name="submit">
  $('#ui-tab > ul').tabs("select","fragment-1");
  </spring:hasBindErrors>
    $("#attachment").change(function() {
	   $(this).upload('<c:url value="/evaluation/attachment/${basic_info.tranId}" />', function(res) {
			if (res.error != undefined && res.error) {
				alert("Faild to upload.");
				return false;
			}
			if (res != undefined && res == 'maxUploadSizeFailure') {
				alert("The uploaded file exceeds the maximum limit.");
				return false;
			}
			$(res).insertAfter(this);
		}, 'html');
	});
    
	$('.btnSave').click(function(){
		var url = '${fn:escapeXml(form2Url)}';
		window.location=url;
	});
	
	$("#costcenter").change(function(evObj) {
    	$this = $(this);
    	var costcenter = $this.val();
    	var supercompany = '${supercompany}';
      	//var urltol1 = '<c:url value="/evaluation/changeCostcenterL1/${supercompany}" />' +'/'+ costcenter;
     	//var urltol2 = '<c:url value="/evaluation/changeCostcenterL2/${supercompany}" />' +'/'+ costcenter;
      
    	$("#l1").load('<c:url value="/evaluation/changeCostcenterL1/" />' +'/'+ supercompany +'/'+ costcenter);
 	    $("#l2").load('<c:url value="/evaluation/changeCostcenterL2/" />' +'/'+ supercompany +'/'+ costcenter);
 	});
});
function delAttachment(attachment, aid, tid) {
	$.ajax({
		type: 'POST',
	    url: '<c:url value="/evaluation/attachment_workflow/delete"/>/' + aid +'/' + tid + '?_='+new Date(),
	    cache: false,
	    success : function(data){
	    	$("#" + $(attachment).attr('class')).remove();
	    }
	});
	$('#attachment').val("");
	return false;
} 
</script>

<div class="mainContainer">
<div class="basic_non_trade_form contextContainer">
<h1 class="alt"><fmt:message key="confirm"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>&nbsp;</p>
<spring:url value="/evaluation/submit/reeval/${vendor_id}/${eval_id}" var="formUrl"/>
<form:form action="${fn:escapeXml(formUrl)}"
	modelAttribute="submit" 
	method="post">
	<input type="hidden" name="result" value="${evaluation.result}"/>
<table class="basic_non_trade_form_table">
<tbody>
  <tr>
    <td colspan="2" style="text-align:left;">
      <!-- 
      <span style="font-size:1.7em;color:red;"><fmt:message key="confirm.total_point"/> : <c:out value="${evaluation.totalPoint}" /> Points</span><br>
       -->
      <span style="font-size:1.3em;color:green;">
        <fmt:message key="workflow.applicant"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l1"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l2"/> 
        <c:if test="${isSSCS}">
	        <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <c:if test="${evaluation.result == 0}">[CEO] <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" /></c:if>
	        <fmt:message key="workflow.hd"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <fmt:message key="workflow.fm"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	        <fmt:message key="workflow.dm"/>
        </c:if>
      </span>
    </td>
  </tr>
</table>
<c:if test="${evaluation.result == 0 || evaluation.result == 3}">  
<table class="basic_non_trade_form_table" style="border-top:none">  
  <tr>
    <th><fmt:message key="workflow.costcenter"/> : </th>
    <td>
      <form:select path="costcenter" multiple="false" id="costcenter" style="width:300px;">
        <c:forEach var="costcenter" items="${costcenter}">
          <option value="${costcenter}" <c:if test="${selectedDivision == costcenter}">selected="true"</c:if>>${costcenter}
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="workflow.l1"/> : </th>
    <td>
      <form:select path="l1" multiple="false" id="l1" style="width:300px;">
      	<option value="">
        <c:forEach var="l1" items="${l1s}">
          <option value="${l1.id}">${l1.name}(${l1.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="workflow.l2"/> : </th>
    <td style="<c:if test="${l2NullErrer}">background-color: red;</c:if>">
      <form:select path="l2" multiple="false" id="l2" style="width:300px;">
        <c:forEach var="l2" items="${l2s}">
          <option value="${l2.id}">${l2.name}(${l2.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <c:if test="${isSSCS}">  
	  <c:if test="${evaluation.result == 0}">
		  <tr>
		    <th>CEO : </th>
		    <td>
		      <form:select path="ceo" multiple="false" style="width: 300px;">
		        <c:forEach var="ceo" items="${ceos}">
		          <option value="${ceo.id}">${ceo.name}(${ceo.id})
		        </c:forEach>
		      </form:select>
		    </td>
		  </tr>  
	  </c:if>
	  <tr>
	    <th><fmt:message key="workflow.hd"/> : </th>
	    <td>
	      <form:select path="hd" multiple="false" style="width: 300px;">
	        <c:forEach var="hd" items="${hds}">
	          <option value="${hd.id}">${hd.name}(${hd.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.fm"/> : </th>
	    <td>
	      <form:select path="fm" multiple="false" style="width: 300px;">
	        <c:forEach var="fm" items="${fms}">
	          <option value="${fm.id}">${fm.name}(${fm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.dm"/> : </th>
	    <td>
	      <form:select path="dm" multiple="false" style="width: 300px;">
	        <c:forEach var="dm" items="${dms}">
	          <option value="${dm.id}">${dm.name}(${dm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
 </c:if>  
 
  <tr>
    <td>
      	<button type="submit" style="margin-left:20px;"><fmt:message key="button.submit"/></button>
    </td>
	<td>
    </td>
  </tr>
</tbody>
</table>
</c:if> 
</form:form>
<table><tr>

</tr></table>
<div class="index"><span style="color:red;">* </span><fmt:message key="evaluation.attachment.title"/>
	<c:if test="${attachmentsIsRequired}"><span class="errors" style="font-size: 12px;"><fmt:message key="error.required"/></span></c:if>
</div>
<table class="basic_non_trade_form_table"">
<tbody>
  <tr>
    <th style="text-align:left;">
		<div style="margin-top:2px;">
    		<a href="<c:url value="/samples/VendorCompare.xls"/>"><fmt:message key="evaluation.attachment.template"/></a>
    	</div>
      <input id="attachment" name="file" type="file" class="<c:if test='${attachmentsIsRequired}'>input_error</c:if>"/>
      <c:forEach var="attachment2" items="${attachment_workflow}">
	  <div id="a-<c:out value="${attachment2.id}"/>">
        <table>
          <tr><td rowspan="4" style="border:none;">
            <a class="a-<c:out value="${attachment2.id}"/>" onClick="delAttachment(this, '<c:out value='${attachment2.id}'/>', '<c:out value='${attachment2.tranId}'/>');return false;"><img src="<c:url value="/images/action_delete.png"/>"/></a>
          </td></tr>
          <tr><td style="border:none;">Content Type : <c:out value="${attachment2.contentType}" /></td></tr>
          <tr><td style="border:none;">Name : <c:out value="${attachment2.name}" /><br></td></tr>
          <tr><td style="border:none;">Size : <c:out value="${attachment2.size}" /><br></td></tr>
        </table>
        <hr>
       </div>      
     </c:forEach>
    </th>
  </tr>
</tbody>
</table>
<hr>
	<div id="ui-tab">
	  <ul>
	    <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
	    <li><a href="#fragment-1"><span><fmt:message key="evaluation.score_summary"/></span></a></li>
	    </c:if>
	    <li><a href="#fragment-3"><span><fmt:message key="evaluation.title"/></span></a></li>
	    <li><a href="#fragment-2"><span><fmt:message key="basicinfo.detail"/></span></a></li>
	  </ul>
	
	  <div id="fragment-1">
	  	<c:if test="${evaluation.businessType < 3}">
	    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
	    </c:if>
	    <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
	    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part_score_summary.jsp" %>
	    </c:if>
	  </div>  
	  <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
		  <div id="fragment-3">
		    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part_second.jsp" %>
		  </div>  
	  </c:if>
	  <div id="fragment-2">
    	<%@ include file="/WEB-INF/views/vendor/basic_information_part_score_summary.jsp" %>
 	  </div>
	</div>
</div>
</div>
</body>
</html>