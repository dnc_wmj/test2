<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title>Data access failure</title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8" />
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8" />
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<h3 style="color:darkblue;"><b>
Data access failure
</b></h3>
<br>
<input type="button" value="<fmt:message key='btn.exception.show' />" class="btn1" onclick="$('.exceptiondetail').show(); $('.btn1').hide();$('.btn2').show();"/>
<input type="button" value="<fmt:message key='btn.exception.hide' />" class="btn2" onclick="$('.exceptiondetail').hide(); $('.btn1').show();$('.btn2').hide();" style="display: none;"/>
<br>
<hr>
<%
Exception ex = (Exception) request.getAttribute("exception");
%>
<div class="exceptiondetail" style="display: none;">
<h4>Data access failure: 
<% 
	if (ex instanceof org.springframework.dao.EmptyResultDataAccessException) {
		out.println("Data does not exist.<br>");
		ex.printStackTrace(new java.io.PrintWriter(out));
	} 
	else {
		out.print(ex.getMessage());
	}
%>
</h4>
</div>
<p></p>
</body>
</html>