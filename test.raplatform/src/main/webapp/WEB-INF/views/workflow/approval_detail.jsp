<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="workflow.detail"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
});
</script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="workflow.detail"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>&nbsp;</p>
<table class="basic_form_table">
<tbody>
<%@ include file="/WEB-INF/views/workflow/approval_table.jsp" %>  
<%@ include file="/WEB-INF/views/workflow/approval_contents.jsp" %>
  <spring:url value="/workflow/submit/${workflow.id}" var="formUrl"/>
  <form:form action="${fn:escapeXml(formUrl)}"
		method="post">
  <tr>
    <td style="border:none;"><fmt:message key="evaluation.comment"/> : </th>
  </tr>
  <tr>
    <td><textarea name="comment" style="font-size:12px;height:5em;"></textarea></td>
  </tr>
  <!-- || (workflow.stage == 20 && !isSSCS && basic_info.identity.vendorCode == null) -->
  <c:if test="${workflow.stage == 60 && 
  					(workflow.modifiedDataType == 10 || workflow.modifiedDataType == 20 || workflow.modifiedDataType == 30) 
  					&& basic_info.identity.vendorCode == null}">
  <tr>
   <td style="border:none;">&nbsp;</td>
  </tr>
  <tr>
    <td style="border:none;"><fmt:message key="basicinfo.form.vendor_code"/> : 
     <c:if test="${errorRequired}">
       <span class="errors"><fmt:message key="error.required"/></span>
     </c:if>
     <c:if test="${errorDuplicated}">
       <span class="errors"><fmt:message key="error.exists"/></span>
     </c:if> 
     <br><input style="width:200px;border:solid 1px #aacfe4;" type="text" name="vendorCode" value="<c:out value="${vendorCode}"/>"/></td>
  </tr>
    <tr>
   <td>&nbsp;</td>
  </tr>
  </c:if>
  <!-- 如果想显示不等于NULL的vendorCode 在这里写c:if就ok了-->
  
  <tr>
    <td style="border:none;">
        <input type="hidden" name="action" id="action">
        <button name="actionButton" value="approve" type="submit" style="margin-left:20px;" 
        	onclick="<c:if test="${ isRoleHD && groupKeyIsNull }">
        				alert('<fmt:message key="basicinfo.form.accounting.group_key.isnull"/>');
        				return false;
        			 </c:if>
        			 $('#action').val('approve')">
        
          <c:if test="${workflow.stage != 20}">
            <fmt:message key="button.approve"/>
          </c:if>
          <c:if test="${workflow.stage == 20 && evaluation == null}">
            <fmt:message key="button.approve"/>
          </c:if>
          <c:if test="${workflow.stage == 20 && evaluation.totalPointForCompare >= 90}">
            <fmt:message key="button.approve"/>
          </c:if>
          <c:if test="${workflow.stage == 20 && evaluation.totalPointForCompare <= 89 && isSSCS}">
            <fmt:message key="button.to_ceo"/>
          </c:if>
          <c:if test="${workflow.stage == 20 && evaluation.totalPointForCompare <= 89 && !isSSCS}">
            <fmt:message key="button.approve"/>
          </c:if>
         </button>
         
         <c:if test="${workflow.stage != 60 && workflow.stage != 40}">  
		 <button name="actionButton" value="deny" type="submit" style="margin-left:20px;" onclick="$('#action').val('deny')">
          <c:if test="${workflow.stage < 40}">
          	<fmt:message key="button.improve"/>
          </c:if>       
          <c:if test="${workflow.stage == 50}">
          	<fmt:message key="button.reject"/>
          </c:if>
        </button>
        </c:if>
        <c:if test="${workflow.stage == 40 && workflow.workflowType == 'full'}">
        <button name="actionButton" value="deny" type="submit" style="margin-left:20px;" onclick="$('#action').val('deny')">
          <fmt:message key="button.improve"/>
        </button>  
        </c:if>
        <c:if test="${workflow.stage != 60}">
        <button name="actionButton" value="cancel" type="submit" style="margin-left:20px;" onclick="$('#action').val('cancel')">
          <fmt:message key="button.refuse"/>
        </button>  
        </c:if>
        
    </td>
  
  </tr>
  </form:form>
</tbody>
</table>
<%@ include file="/WEB-INF/views/workflow/detail_header.jsp" %>
<br>

<div id="ui-tab">
  <ul>
    <c:if test="${hasEvaluation == true}">
      <li><a href="#fragment-1"><span><fmt:message key="evaluation.title"/></span></a></li>
    </c:if>
    <li><a href="#fragment-2"><span><fmt:message key="basicinfo.detail"/></span></a></li>
  </ul>

  <c:if test="${hasEvaluation == true}">
    <div id="fragment-1">
      <c:if test="${evaluation.businessType < 3}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
      </c:if>
      <c:if test="${evaluation.businessType >= 3 && evaluation.businessType <= 9}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part.jsp" %>
      </c:if>
    </div>
  </c:if>    
  <div id="fragment-2">
    <%@ include file="/WEB-INF/views/vendor/basic_information_part.jsp" %>
  </div>

</div>
</div>
</div>
</body>
</html>