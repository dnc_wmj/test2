<%@ include file="/WEB-INF/views/includes.jsp" %>
  <tr>
    <td style="border:none;">
    <div class="index" style="color:green;"><fmt:message key="workflow.application.history"/></div>
    <table class="default_table">
      <tr>
        <th>&nbsp;</th>
        <th style="width:550px;"><fmt:message key="workflow.application.history.comment"/></th>
        <th><fmt:message key="workflow.application.history.result"/></th>
        <th><fmt:message key="workflow.application.history.process_date"/></th>
      </tr>
      <c:forEach var="result" items="${workflow_results}">
      <tr>
      <c:if test="${result.stage != 0}">  
        <td style="text-align:center;white-space:nowrap;">
          <c:out value="${result.userName}" />
          (<c:out value="${result.userId}" />)
          [
          <c:if test="${result.stage == 0}">
            <fmt:message key="workflow.applicant"/>
          </c:if>
          <c:if test="${result.stage == 5}">
            <fmt:message key="workflow.applicant"/>
          </c:if>
          <c:if test="${result.stage == 10}">
            <fmt:message key="workflow.l1"/>
          </c:if>
          <c:if test="${result.stage == 20}">
            <fmt:message key="workflow.l2"/>
          </c:if>
          <c:if test="${result.stage == 30}">
            CEO
          </c:if>
          <c:if test="${result.stage == 40}">
            <fmt:message key="workflow.hd"/>
          </c:if>
          <c:if test="${result.stage == 50}">
            <fmt:message key="workflow.fm"/>
          </c:if>
          <c:if test="${result.stage == 60}">
            <fmt:message key="workflow.dm"/>
          </c:if>
          ]
        </td>
        <td>
          <pre><c:out value="${result.comment}" /></pre>
        </td>
        <td style="text-align:center;">
          <c:if test="${result.result == 1}">
            <fmt:message key="workflow.application.approve"/>
          </c:if>
          <c:if test="${result.result == 2}">
            <fmt:message key="workflow.application.deny"/>
          </c:if>
          <c:if test="${result.result == 3}">
            <fmt:message key="button.cancel"/>
          </c:if>
          <c:if test="${result.result == 4}">
            <fmt:message key="button.refuse"/>
          </c:if><br/>
        </td>
        <td style="text-align:center;">
          <fmt:formatDate value="${result.processedDate}" pattern="yyyy/MM/dd" />
        </td>
      </c:if>
      <c:if test="${result.stage == 0}">
        <td style="text-align:center;background-color:#eeeeee;">
          <c:out value="${result.userName}" />
          (<c:out value="${result.userId}" />)
        </td>
        <td style="background-color:#eeeeee;">
          <pre><c:out value="${result.comment}" /></pre>
        </td>
        <td style="text-align:center;;background-color:#eeeeee;">
          <fmt:message key="button.reapplication"/>
        </td>
        <td style="text-align:center;;background-color:#eeeeee;">
          <fmt:formatDate value="${result.processedDate}" pattern="yyyy/MM/dd" />
        </td>
      </c:if>  
      </tr>
      </c:forEach>
    </table>
    </td>
  </tr>
