<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="workflow.my_application"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/jquery-ui-1.8.8.custom.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.upload-1.0.2.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-ui-1.8.8.custom.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.ui.datepicker-zh-CN.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>

<script type="text/javascript">
$(function() {
  var id = new Date().getMilliseconds();
	
  $("#add_performance").click(function(){
	var uid = ++id;
    $("#biz_performance").before(
        '<tr id="p-' + uid + '">' +
           '<input type="hidden" value="-1" name="businessInformation.bizInfoId"/>' +
           '<td style="border:none;"><a id="p-' + uid + '" href="#" onClick="delPeformance(this);return false;"><img width="10px" height="10px" src="<c:url value="/images/action_delete.png"/>"/></a></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.year"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.salesAmount"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.profit"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.profitability"></td>' +
        '</tr>'
    );
    return false;
  });

  $(".date").datepicker({
	changeMonth: true,
	changeYear: true,
	yearRange: "-40:+20"
  });
  
  //$(".searchType").change(function(){
  //	switch($(this).val()){
  //		case '2':$(".userValue").hide().val("%%");break;
  //		default: $(".userValue").val("").show();
  //	}
  //});
  //onload
  //if($(".searchType").val() == '2'){
  //	$(".userValue").hide().val("%%");
  //}
});

</script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="workflow.my_application"/></h1>
<p><fmt:message key="workflow.my_application.description"/></p>
<spring:url value="/workflow/myapplications/search" var="formUrl"/>

<form:form action="${fn:escapeXml(formUrl)}" modelAttribute="myapplicationsSearchDetail" method="post" >
	<table  style="text-align: center; font-size: 13px;">
		<tr>
			<td style="text-align: right;"><fmt:message key="workflow.myapplications.date"/></td>
			<td><form:input path="startDate" cssClass="date" style="width: 155px;"/></td>
			<td>-</td>
			<td><form:input path="endDate" cssClass="date" style="width: 155px;"/></td>
		</tr>
		<c:if test="${searchAll}">
		<tr>
			<td style="text-align: right;">
			<form:select class="searchType" path="userType">
		        <form:option value="0" selected="selected"><fmt:message key="workflow.myapplications.user.id"/></form:option>
		        <form:option value="1"><fmt:message key="workflow.myapplications.user.name"/></form:option>
		        <!-- 
		        <form:option value="2"><fmt:message key="search.all.user"/></form:option>
		         -->
		    </form:select><fmt:message key="workflow.myapplications.user"/>
      		</td>
			<td><form:input class="userValue" path="userValue" style="width: 155px;"/></td>
			<td></td>
			<td></td>
		</tr>
		</c:if>
		<tr>
			<td style="text-align: right;"><fmt:message key="workflow.myapplications.number"/></td>
			<td><form:input path="number" style="width: 155px;"/></td>
			<td></td>
			<td></td>
		</tr>
		<tr>
			<td style="text-align: right;"><fmt:message key="workflow.myapplications.status"/></td>
			<td><form:radiobutton value="4" path="type"/><fmt:message key="workflow.myapplications.status.type4"/></td>
			<td><form:radiobutton value="0" path="type"/><fmt:message key="workflow.myapplications.status.type1"/></td>
			<td>
				<form:radiobutton value="2" path="type"/><fmt:message key="workflow.myapplications.status.type3"/>
			</td>
			<td style="text-align: left;">
				<form:radiobutton value="1" path="type"/><fmt:message key="workflow.myapplications.status.type2"/>
			</td>
		</tr>
		<tr>
			<td><button type="submit"><fmt:message key="workflow.myapplications.search.button"/></button></td>
			<td></td>
			<td></td>
			<td></td>
		</tr>
	</table>
<hr>
</form:form>
<table class="default_table">
  <tr>
    <th><fmt:message key="workflow.vendor_name"/></th>
    <th><fmt:message key="workflow.type"/></th>
    <th><fmt:message key="workflow.stage"/></th>
    <th><fmt:message key="workflow.application_date"/></th>
    <th>&nbsp;</th>
  </tr>
<c:forEach var="application" items="${applications}">
  <tr>
    <td><c:out value="${application.name}" /></td>
    <td>
      <c:if test="${application.modificationType == 10}">
        <fmt:message key="workflow.type.new"/>
      </c:if>
      <c:if test="${application.modificationType == 20}">
        <fmt:message key="workflow.type.edit"/>
      </c:if>      
      <c:if test="${application.modificationType == 30}">
        <fmt:message key="workflow.type.reeval"/>
      </c:if>
      <c:if test="${application.modificationType == 40}">
        <fmt:message key="workflow.type.delete"/>
      </c:if>
      <c:if test="${application.modificationType == 50}">
        <fmt:message key="workflow.myapplications.status.type3"/>
      </c:if>
      <c:if test="${application.modificationType == 60}">
        <fmt:message key="workflow.type.freeze"/>
      </c:if>
      <c:if test="${application.modificationType == 70}">
        <fmt:message key="workflow.type.unfreeze"/>
      </c:if>
      <c:if test="${application.status == 5}">
        <fmt:message key="search.status.5"/>
      </c:if>
      <c:if test="${application.status == 20}">
        <fmt:message key="search.status.20"/>
      </c:if>
      <c:if test="${application.status == 30}">
        <fmt:message key="search.status.30"/>
      </c:if>
    </td>
    <td style="width:150px;">
     	<fmt:message key="${application.stageMessageKey}"/>
    </td>
    <td style="width:100px;text-align:center;">
    	<!-- <fmt:formatDate value="${application.applicationDate}" pattern="yyyy/MM/dd" /> -->
    	<c:out value="${application.shanghaiApplicationDate}" />
    </td>
    <td style="width:80px;text-align:center;">
      <div class="button">
      	<c:if test="${application.status == 4}">
      		<c:if test="${application.evalId != -1}">
      			<a href="<c:url value="/evaluation/tmpsave/confirm/${application.vendorId}/${application.evalId}/${application.id}" />"><fmt:message key="workflow.detail"/></a>
      		</c:if>
      		<c:if test="${application.evalId == -1}">
      			<a href="<c:url value="/vendor/tmpsave/noeval/confirm/${application.vendorId}/${application.id}" />"><fmt:message key="workflow.detail"/></a>
      		</c:if>
      	</c:if>
      	<c:if test="${application.status == 1 || application.status == 3}">
      		<a href="<c:url value="/workflow/application/${application.id}" />"><fmt:message key="workflow.detail"/></a>
      	</c:if>
      	<c:if test="${application.status == 2}">
      		<a href="<c:url value="/workflow/application/${application.id}" />"><fmt:message key="workflow.detail"/></a>
      	</c:if>
        <c:if test="${application.status == 5 || application.status == 20 || application.status == 30}">
      		<a href="<c:url value="/vendor/view/${application.vendorId}" />"><fmt:message key="workflow.detail"/></a>
      	</c:if>
      </div>
    </td>
  </tr>
</c:forEach>
</table>
</div>
</div>
</body>
</html>