<%@ include file="/WEB-INF/views/includes.jsp" %>
<table style="width:600px;" class="default_table">
<tr>
  <th style="width:100px;"><fmt:message key="workflow.applicant"/></th>
  <td><c:out value="${applicant.name}"/>(<c:out value="${applicant.username}"/>)</td>
</tr>
<tr>
  <th style="width: 100px;"><fmt:message key="basicinfo.form.attachment"/></th>
  <td>
  <c:forEach var="attachment" items="${attachments}">
    <a href="<c:url value="/vendor/attachment/download/${attachment.id}"/>"><c:out value="${attachment.name}"/></a><br>
  </c:forEach>
  </td>
</tr>
<c:if test="${attachments_workflow!=null}">
	<tr>
	  <th style="width: 100px;"><fmt:message key="workflow.application.attachment"/></th>
	  <td>
	  <c:forEach var="attachment" items="${attachments_workflow}">
	    <a href="<c:url value="/evaluation/attachment/download/${attachment.id}"/>"><c:out value="${attachment.name}"/></a><br>
	  </c:forEach>
	  </td>
	</tr>
</c:if>
</table>