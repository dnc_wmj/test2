<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="workflow.my_approval"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>

<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="workflow.my_approval"/></h1>
<p><fmt:message key="workflow.my_approval.description"/></p>

<table class="default_table">
  <tr>
    <th><fmt:message key="workflow.vendor_name"/></th>
    <th><fmt:message key="workflow.type"/></th>
    <th><fmt:message key="workflow.application_date"/></th>
    <th>&nbsp;</th>
  </tr>
<c:forEach var="approval" items="${approvals}">
  <tr>
    <td><c:out value="${approval.name}" /></td>
    <td>
      <c:if test="${approval.modificationType == 10}">
        <fmt:message key="workflow.type.new"/>
      </c:if>
      <c:if test="${approval.modificationType == 20}">
        <fmt:message key="workflow.type.edit"/>
      </c:if>
      <c:if test="${approval.modificationType == 30}">
        <fmt:message key="workflow.type.reeval"/>
      </c:if>
      <c:if test="${approval.modificationType == 40}">
        <fmt:message key="workflow.type.delete"/>
      </c:if>
	  <c:if test="${approval.modificationType == 60}">
        <fmt:message key="workflow.type.freeze"/>
      </c:if>
      <c:if test="${approval.modificationType == 70}">
        <fmt:message key="workflow.type.unfreeze"/>
      </c:if>
    </td>
    <td style="width:100px;text-align:center;"><c:out value="${approval.shanghaiApplicationDate}" /></td>
    <td style="width:80px;text-align:center;">
      <div class="button">
        <a href="<c:url value="/workflow/approval/${approval.id}" />"><fmt:message key="workflow.detail"/></a>
      </div>
    </td>
  </tr>
</c:forEach>
</table>
</div>
</div>
</body>
</html>