<%@ include file="/WEB-INF/views/includes.jsp" %>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<title>
		<decorator:title />
	</title>
	<link rel="stylesheet" href="<c:url value="/styles/base.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
</head>
<body>

<div id="body">
  <decorator:body/>
</div>

</body>
</html>