<link rel="stylesheet" href="<c:url value="/styles/menu.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<div class="menu">
  <a href="<c:url value="/workflow/myapplications" />"><fmt:message key="menu.myapplications"/></a>
  <a href="<c:url value="/workflow/myapprovals" />"><fmt:message key="menu.myapprovals"/></a>
  <a href="<c:url value="/vendor/register/basic" />"><fmt:message key="menu.registration"/></a>
  <a href="<c:url value="/vendor/search" />"><fmt:message key="menu.search"/></a>
  <a href="<c:url value="/data/change/password" />"><fmt:message key="data.change.password"/></a>
  <sec:authorize access="hasRole('ROLE_HD')">
    <a href="<c:url value="/data/menu" />"><fmt:message key="menu.mastermaintainance"/></a>
  </sec:authorize>
  <sec:authorize access="hasRole('ROLE_HD')">
    <a href="<c:url value="/data/system/setting" />"><fmt:message key="menu.system.setting"/></a>
  </sec:authorize>
  <% if (Utils.getUser().isCanDownCert()) { 	%>
	<a href="<c:url value="/data/down/cert" />" style ="background-color: fireBrick; border-color: fireBrick;"><fmt:message key="data.down.cert" /></a>
  <% } %>
</div>
