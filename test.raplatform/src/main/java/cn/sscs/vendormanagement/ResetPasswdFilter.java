package cn.sscs.vendormanagement;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
/**
 * filter all url for checking user's password last change time is in the 3 months 
 * 
 * @author mingjian
 *
 */
public class ResetPasswdFilter implements Filter {
	//public static int  RESETPASSWDTIME = 3;
	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse rsp = (HttpServletResponse) response;
		String url = req.getRequestURI().toString();
		/**
		 * http://localhost:8080/vndmanagement/signin;jsessionid=C8ACD46714BBADE159E12810C77B1BE1
		 */
		url = url.indexOf(";") != -1 ? url.split(";")[0] : url;
		/**
		 * 通过这个判断后，说明登陆成功了，在访问网站。
		 * 
		 * "/signin" 
		 * "/signout"
		 * "/": 			http://localhost:8080/vndmanagement/ 
		 * ".css": 			/vndmanagement/styles/base.css
		 *		   			/vndmanagement/styles/basic_form.css 
		 *		  			/vndmanagement/styles/signin.css 
		 * ".js":  			/vndmanagement/scripts/lib/jquery-1.4.4.min.js 
		 * "/password":		/vndmanagement/workflow/data/change/data/change/password
		 */
		if (!(url.endsWith("/signin") 
				|| url.endsWith("/signinprocess") 
				|| url.endsWith("/signout") 
//				|| url.endsWith(".css") 
				|| url.endsWith("/") 
				|| url.endsWith(".com") 
//				|| url.endsWith(".js") 
				|| url.endsWith("/password")
				|| url.endsWith("=true")
				|| url.indexOf("/styles/") != -1
				|| url.indexOf("/scripts/") != -1
				|| url.indexOf("/images/") != -1
				|| url.indexOf("/samples/") != -1)) {
			VndMgmntUser user = Utils.getUser();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext());
			DataSource dataSource = (DataSource) webApplicationContext.getBean("dataSource");
			SystemBean systemBean = (SystemBean) webApplicationContext.getBean("systemBean");
			int resetPswdDay = systemBean.getResetPswdDay();
			SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
			String sql = "SELECT "+
								"IF(COUNT(*)>0,1,0) "+
						 "FROM "+
								"user "+
						 "WHERE "+
								"DATE_FORMAT(DATE_ADD(last_change_passwd_time, INTERVAL ? DAY),'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d') AND "+
								"id = ?";
			boolean isRequireChangePw = Integer.parseInt(template.queryForObject(sql, Integer.class, resetPswdDay, user.getUsername()).toString()) == 1;
			// 
			//String sql_update_error_times = "update user set error_times = 0 where id = ?";
			//template.update(sql_update_error_times, user.getUsername());
			
			if(isRequireChangePw){
				rsp.sendRedirect("/vndmanagement/data/change/password");
			}else{
				chain.doFilter(request, response);
			}
		}else{
			chain.doFilter(request, response);
		}
		
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

}
