package cn.sscs.vendormanagement;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class MyLoginUrlAuthenticationFilter  implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		String url = req.getRequestURI().toString();
		if(!(url.endsWith("/signin") 
				|| url.endsWith("/signinprocess") 
				|| url.endsWith("/signout") 
				|| url.endsWith("/") 
				|| url.endsWith(".com") 
				|| url.endsWith("/password")
				|| url.endsWith("=true")
				|| url.indexOf("/styles/") != -1
				|| url.indexOf("/scripts/") != -1
				|| url.indexOf("/images/") != -1
				|| url.indexOf("/samples/") != -1)){
			VndMgmntUser user = Utils.getUser();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext());
			DataSource dataSource = (DataSource) webApplicationContext.getBean("dataSource");
			SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
			String sql = "update user set error_times = 0 where id = ?";
			template.update(sql, user.getUsername());
		}
		chain.doFilter(request, response);
	}

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
}
