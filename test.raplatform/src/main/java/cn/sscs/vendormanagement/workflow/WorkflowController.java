package cn.sscs.vendormanagement.workflow;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.sscs.vendormanagement.ExtendedJdbcUserDetailsManager;
import cn.sscs.vendormanagement.UnknownActionTypeException;
import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.data.IDataService;
import cn.sscs.vendormanagement.data.ModificationHistory;
import cn.sscs.vendormanagement.data.SystemSetting;
import cn.sscs.vendormanagement.evaluation.Evaluation;
import cn.sscs.vendormanagement.evaluation.IEvaluationService;
import cn.sscs.vendormanagement.vendor.IVendorService;
import cn.sscs.vendormanagement.vendor.VendorBasicInfo;

@Controller
@Transactional
@RequestMapping(value = "/workflow")
public class WorkflowController {

	public static int BUTTON_SET_DIVISION_HEAD = 0;

	@Autowired
	private IVendorService vendorService = null;

	@Autowired
	private IEvaluationService evaluationService = null;

	@Autowired
	private IWorkflowService workflowService = null;

	@Autowired
	private IDataService dataService = null;

	@Autowired
	ExtendedJdbcUserDetailsManager userService = null;

	@InitBinder  
	public void InitBinder(WebDataBinder dataBinder) {  
	    dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {  
	        public void setAsText(String value) {  
	            try {  
	                setValue(new SimpleDateFormat("yyyy/MM/dd").parse(value));  
	            } catch(ParseException e) {  
	                setValue(null);  
	            }
	        }  
	        public String getAsText() {
	        	return (getValue() == null) ? null : new SimpleDateFormat("yyyy/MM/dd").format((Date) getValue());
	        }          
	  
	    });  
	}
	
	@RequestMapping(value = "/myapplications", method = RequestMethod.GET)
	public String getMyApplications(Model model) {
		model.addAttribute("applications", workflowService
				.findMyApplications(Utils.getUser().getUsername()));
//		if (Utils.isRoleHD() || Utils.isRoleFM() || Utils.isRoleDM()) {
			model.addAttribute("searchAll", true);
//		}else{
//			model.addAttribute("searchAll", false);
//		}

		MyapplicationsSearchDetail myapplicationsSearchDetail = new MyapplicationsSearchDetail();
		model.addAttribute(myapplicationsSearchDetail);
		return "workflow/my_application";
	}
	@RequestMapping(value = "/myapplications/search", method = RequestMethod.POST)
	public String getMyApplicationsSearch(Model model,
			@Valid MyapplicationsSearchDetail myapplicationsSearchDetail) {
		model.addAttribute("applications", workflowService
				.findMyOldApplications(Utils.getUser().getUsername(), myapplicationsSearchDetail));
//		if (Utils.isRoleHD() || Utils.isRoleFM() || Utils.isRoleDM()) {
			model.addAttribute("searchAll", true);
//		}else{
//			model.addAttribute("searchAll", false);
//		}
		return "workflow/my_application";
	}
	@RequestMapping(value = "/myapprovals", method = RequestMethod.GET)
	public String getMyApproves(Model model) {
		model.addAttribute("approvals",
				workflowService.findMyApprovals(Utils.getUser().getUsername()));

		return "workflow/my_approval";
	}

	@RequestMapping(value = "/application/{workflowId}", method = RequestMethod.GET)
	public String getApplication(@PathVariable("workflowId") String workflowId,
			Model model) {

		WorkflowRule workflowRule = workflowService.getWorkflowRule(workflowId);
		List<String> userNames = dataService.findWorkflowUsers(
				workflowRule.getL1(), workflowRule.getL2(),
				workflowRule.getCeo(), workflowRule.getHd(),
				workflowRule.getFm(), workflowRule.getDm(),
				workflowRule.getApplicant());
//		VendorBasicInfo vendorBasicInfo = vendorService
//				.findVendorBasicInfoByStatus(workflowRule.getVendorId(),
//						VendorBasicInfo.STATUS_IN_APPLICATION);
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfo(workflowRule.getVendorId());
		
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		
		vendorBasicInfo.getContact().setRegion(
				vendorService
						.getRegionName(vendorBasicInfo.getContact()
								.getCountry(), vendorBasicInfo.getContact()
								.getRegion()));

		vendorBasicInfo.getContact().setCountry(
				vendorService.getCountryName(vendorBasicInfo.getContact()
						.getCountry()));

		if (workflowRule.getStatus() == WorkflowRule.STATUS_SUSPEND) {
			model.addAttribute("editable", true);
		}

		List<WorkflowApprovalResult> workflowResults = workflowService
				.findWorkflowApprovalResults(workflowId);
		boolean freezeFalg = false;
		if(vendorBasicInfo.getFreeze() != null && "1".equals(vendorBasicInfo.getFreeze())){
		    freezeFalg = true;
		}
		model.addAttribute("freeze", freezeFalg);
		model.addAttribute("userNames", userNames);
		model.addAttribute("workflow", workflowRule);
		model.addAttribute("workflow_results", workflowResults);
		model.addAttribute("workflow_results_for_seal",
				makeSealData(workflowResults));
		model.addAttribute("vendor_id", workflowRule.getVendorId());
		model.addAttribute("basic_info", vendorBasicInfo);
		
		model.addAttribute("applicant", userService.loadUserByUsername(workflowRule.getApplicant()));
		model.addAttribute("attachments", vendorService.findAttachment(workflowRule.getVendorId()));
		model.addAttribute("attachments_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		
		boolean hasEvaluation = evaluationService
				.hasEvaluationById(workflowRule.getEvalId());
		model.addAttribute("hasEvaluation", hasEvaluation);

		if (hasEvaluation) {
			Evaluation eval = null;
			if(workflowRule.getStatus() != 2){
				eval = evaluationService.findEvaluationById(
						workflowRule.getEvalId(), Evaluation.STATUS_IN_APPLICATION);
			}else{
				eval = evaluationService.getEvaluationById(workflowRule.getEvalId());
			}
			model.addAttribute("evaluation", eval);
			if(Utils.isNonTrade(eval.getBusinessType())){
				// 再评估flag
				int flag = workflowRule.getModifiedDataType() == 30 ? 1 : 0;
				model.addAttribute("nonTradeEvaluationForm", 
						evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), flag, eval.getId()));	
				return "workflow/application_detail_non_trade";
			}else{
				model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			}
		}

		return "workflow/application_detail";
	}

	@RequestMapping(value = "/approval/{workflowId}", method = RequestMethod.GET)
	public String getApproval(@PathVariable("workflowId") String workflowId,
			Model model) {

		WorkflowRule workflowRule = workflowService.getWorkflowRule(workflowId,
				Utils.getUser().getUsername());
		List<String> userNames = dataService.findWorkflowUsers(
				workflowRule.getL1(), workflowRule.getL2(),
				workflowRule.getCeo(), workflowRule.getHd(),
				workflowRule.getFm(), workflowRule.getDm(),
				workflowRule.getApplicant());
		VendorBasicInfo vendorBasicInfo = vendorService
				.findVendorBasicInfoByStatus(workflowRule.getVendorId(),
						VendorBasicInfo.STATUS_IN_APPLICATION);

		vendorBasicInfo.getContact().setRegion(
				vendorService
						.getRegionName(vendorBasicInfo.getContact()
								.getCountry(), vendorBasicInfo.getContact()
								.getRegion()));

		vendorBasicInfo.getContact().setCountry(
				vendorService.getCountryName(vendorBasicInfo.getContact()
						.getCountry()));

		if (Utils.isRoleHD()) {
			model.addAttribute("isRoleHD", true);
			// check groupkey
			if(vendorBasicInfo.getAccounting().getGroupKey() == null 
					|| "".equals(vendorBasicInfo.getAccounting().getGroupKey())){
				model.addAttribute("groupKeyIsNull", true);
			}
		}
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		
		List<WorkflowApprovalResult> workflowResults = workflowService
				.findWorkflowApprovalResults(workflowId);

		model.addAttribute("workflow", workflowRule);
		model.addAttribute("userNames", userNames);
		model.addAttribute("workflow_results", workflowResults);
		model.addAttribute("workflow_results_for_seal",
				makeSealData(workflowResults));
		model.addAttribute("vendor_id", workflowRule.getVendorId());
		model.addAttribute("basic_info", vendorBasicInfo);
		model.addAttribute("applicant",
				userService.loadUserByUsername(workflowRule.getApplicant()));
		model.addAttribute("attachments",
				vendorService.findAttachment(workflowRule.getVendorId()));
		model.addAttribute("attachments_workflow", vendorService
				.findAttachmentWorkflow(vendorBasicInfo.getTranId()));

		boolean hasEvaluation = evaluationService
				.hasEvaluationById(workflowRule.getEvalId());
		model.addAttribute("hasEvaluation", hasEvaluation);
		
		boolean freezeFalg = false;
        if(vendorBasicInfo.getFreeze() != null && "1".equals(vendorBasicInfo.getFreeze())){
            freezeFalg = true;
        }
        model.addAttribute("freeze", freezeFalg);
        
		if (hasEvaluation) {
			Evaluation eval = evaluationService.findEvaluationById(
					workflowRule.getEvalId(), Evaluation.STATUS_IN_APPLICATION);
			model.addAttribute("evaluation", eval);
			if(Utils.isNonTrade(eval.getBusinessType())){
				// 再评估flag
				int flag = workflowRule.getModifiedDataType() == 30 ? 1 : 0;
				model.addAttribute("nonTradeEvaluationForm", 
						evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), flag, eval.getId()));	
				return "workflow/approval_detail_non_trade";
			}else{
				model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			}
		}

		return "workflow/approval_detail";
	}

	private Collection<WorkflowApprovalResult> makeSealData(
			List<WorkflowApprovalResult> results) {
		Map<String, WorkflowApprovalResult> distincts = new LinkedHashMap<String, WorkflowApprovalResult>();
		for (WorkflowApprovalResult result : results) {
			if (result.getStage() <= WorkflowRule.STAGE_HD
					&& result.getResult() == WorkflowRule.RESULT_DENIED) {
				distincts.clear();
			} else {
				distincts.put(result.getUserId(), result);
			}
		}

		return distincts.values();
	}

	/**
	 * 
	 * @Title:       submit 
	 * @Description: ( 审核流提交  ) 
	 * @param        @param workflowId
	 * @param        @param action
	 * @param        @param comment
	 * @param        @param vendorCode
	 * @param        @param model
	 * @param        @param request
	 * @param        @return     
	 * @return       String     
	 * @throws UnknownActionTypeException 
	 * @throws 
	 * @User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
	@RequestMapping(value = "/submit/{workflowId}", method = RequestMethod.POST)
	public String submit(@PathVariable("workflowId") String workflowId,
			@RequestParam(required = true) String action,
			@RequestParam(required = false) String comment,
			@RequestParam(required = false) String vendorCode, Model model,
			HttpServletRequest request) throws UnknownActionTypeException {
		comment = comment == null ? "" : comment;
		WorkflowRule rule = workflowService.getWorkflowRule(workflowId, Utils
				.getUser().getUsername());
		VendorBasicInfo vbi = vendorService.findVendorBasicInfoByStatus(rule.getVendorId(),VendorBasicInfo.STATUS_IN_APPLICATION);
		
		int result = -1;
		String currentProcessor = rule.getProcessor();
		// approve
		if ("approve".equals(action)) {
			result = WorkflowRule.RESULT_APPROVED;
		// deny
		} else if ("deny".equals(action)) {
			result = WorkflowRule.RESULT_DENIED;
		// cancel
		} else if ("cancel".equals(action)) {
			result = WorkflowRule.RESULT_CANCEL;
			// 
			rejectWorkflow(workflowId, model, comment);
			// send email
			Map<String, String> templateModel = Utils.newMailTemplateModel(request);
			templateModel.put("wfid", rule.getId());
			String number = vendorService.getVendorNumber(rule.getVendorId());
			templateModel.put("number", number == null ? "" : number);
			templateModel.put("comment", comment);
			SystemSetting setting = dataService.getSystemSetting();
			dataService.sendMail(rule.getApplicant(),
					setting.getRejectMailSubject(),
					setting.getRejectMailTemplate(), templateModel, null);
			
			return "redirect:/workflow/myapprovals";
		}else{
		    throw new UnknownActionTypeException("Unknown action: "+action);
		}
		String superCompanyType = vbi.getCompanyType().getSuperCompanyType();
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(superCompanyType);
		
		if ((rule.getStage() == WorkflowRule.STAGE_DM 
//					L2 fill vendor code check				
//					|| (rule.getStage() == WorkflowRule.STAGE_L2 
//						&& !parentCompanyIsSSCS 
//						&& result != WorkflowRule.RESULT_DENIED)
						)
				
				&& (rule.getModifiedDataType() == WorkflowRule.MODIFIED_DATA_TYPE_NEW
						|| rule.getModifiedDataType() == WorkflowRule.MODIFIED_DATA_TYPE_EDIT 
						|| rule.getModifiedDataType() == WorkflowRule.MODIFIED_DATA_TYPE_REEVAL)
				&& vbi.getIdentity().getVendorCode() == null) {
			
			if (vendorCode == null || "".equals(vendorCode)) {// vendor code is not allow null
				model.addAttribute("errorRequired", true);
				return getApproval(workflowId, model);
			} else if (vendorService.exists(vendorCode, superCompanyType)) { /*
                                                                              *  vendor code is not allow same 
                                                                              *  (20121008 add condition) in the common super company
                                                                              */
			                            
				model.addAttribute("errorDuplicated", true);
				model.addAttribute("vendorCode", vendorCode);
				return getApproval(workflowId, model);
			} else {// insert
				vendorService.updateVendorCode(rule.getVendorId(), vendorCode);
			}
		}

		WorkflowApprovalResult war = new WorkflowApprovalResult();
		war.setStage(rule.getStage());
		war.setUserId(currentProcessor);
		war.setResult(result);
		war.setComment(comment);
		war.setWorkflowRuleId(rule.getId());
		war.setProcessedDate(new Date());

		workflowService.storeWorkflowApprovalResult(war);

		if (result == WorkflowRule.RESULT_APPROVED) {
			rule.process(result);

			if (evaluationService.hasEvaluationById(rule.getEvalId())) {
				Evaluation evaluation = evaluationService.findEvaluationById(
						rule.getEvalId(), Evaluation.STATUS_IN_APPLICATION);
				// new BigDecimal(evaluation.getTotalPoint()).compareTo(new
				// BigDecimal(90)) > -1
				if (rule.getStage() == WorkflowRule.STAGE_CEO
						&& evaluation.getTotalPointForCompare() >= 90) {
					rule.process(-1);
				}

			} else {
				if (rule.getStage() == WorkflowRule.STAGE_CEO) {
					rule.process(-1);
				}
			}
		} else {
			if (rule.getStage() != WorkflowRule.STAGE_FM) {
				rule.setStatus(WorkflowRule.STATUS_SUSPEND);
				rule.setProcessor(rule.getApplicant());
				rule.setStage(WorkflowRule.STAGE_APPLICANT);
			} else {
				rule.setProcessor(rule.getHd());
				rule.setStage(WorkflowRule.STAGE_HD);
			}

		}
		//when vendor's parent company is not SSCS, its workflow only had L1 and L2 
		if (rule.isFinished() || (!parentCompanyIsSSCS && rule.getStage() > WorkflowRule.STAGE_L2)) {
			// had created No. when this workflow submit or temporary save.
//			// 採番
//			String number = "VND-";
//			String numberTemp = ("00000000" + String.valueOf(rule.getVendorId()));
//			number += numberTemp.substring(numberTemp.length() - 8);
//			vendorService.updateVendorNumber(rule.getVendorId(), number);
			switch(rule.getModifiedDataType()){
				case WorkflowRule.MODIFIED_DATA_TYPE_NEW://新供应商申请
					
					vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
					evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, rule.getEvalId());
//					if(parentCompanyIsSSCS){
					ModificationHistory history = new ModificationHistory(
							rule.getVendorId(), -1,
							ModificationHistory.CLASS_INSERT);

					dataService.storeModificationHistory(history);
//					}
					break;
				case WorkflowRule.MODIFIED_DATA_TYPE_EDIT://基本信息编辑
					if (evaluationService.hasLatestEvaluationByVendorId(rule.getVendorId())) {
						
						Evaluation eval = evaluationService.findLatestEvaluationByVendorId(rule.getVendorId());
						switch (eval.getResult()) {
							case Evaluation.RESULT_IMPROVEMENT:
								vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING, rule.getVendorId());
								break;
							case Evaluation.RESULT_CLOSE:
								vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, rule.getVendorId());
								break;
							case Evaluation.RESULT_START_WORKFLOW:
							case Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO:
								vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
								break;
						}
					} else {
						vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
					}
					
					vendorService.deleteTmp(rule.getVendorId());
					//vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
//					if(parentCompanyIsSSCS){
					ModificationHistory editHistory = new ModificationHistory(
							rule.getVendorId(), -1,
							ModificationHistory.CLASS_UPDATE);

					dataService.storeModificationHistory(editHistory);
//					}
					break;
				case WorkflowRule.MODIFIED_DATA_TYPE_DELETE:// 删除
					vendorService.updateStatus(VendorBasicInfo.STATUS_DELETED, rule.getVendorId());
//					if(parentCompanyIsSSCS){
					ModificationHistory deleteHistory = new ModificationHistory(
							rule.getVendorId(), -1,
							ModificationHistory.CLASS_DELETE);

					dataService.storeModificationHistory(deleteHistory);
//					}
					break;
				case WorkflowRule.MODIFIED_DATA_TYPE_FREEZE:// 冻结
					vendorService.updateForFreeze(rule.getVendorId());
					vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
					evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, rule.getEvalId());
//					if(parentCompanyIsSSCS){
					ModificationHistory freezeHistory = new ModificationHistory(
							rule.getVendorId(), -1,
							ModificationHistory.CLASS_FREEZE);
					
					dataService.storeModificationHistory(freezeHistory);
//					}
					break;
				case WorkflowRule.MODIFIED_DATA_TYPE_REEVAL:// 再评估
					evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, rule.getEvalId());

					Evaluation eval = evaluationService
							.findLatestEvaluationByVendorId(rule.getVendorId());
					switch (eval.getResult()) {
						case Evaluation.RESULT_IMPROVEMENT:
							vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING,
									rule.getVendorId());
							break;
						case Evaluation.RESULT_CLOSE:
							vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE,
									rule.getVendorId());
							break;
						case Evaluation.RESULT_START_WORKFLOW:
						case Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO:
							vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED,
									rule.getVendorId());
							break;
					}
					/**
					 * 冻结需要记录history
					 */
					if(VendorBasicInfo.ISFREEZE.equals(vbi.getFreeze())){
						vendorService.updateForUnfreeze(rule.getVendorId());
//						if(parentCompanyIsSSCS){
						ModificationHistory unfreezeHistory = new ModificationHistory(
								rule.getVendorId(), -1,
								ModificationHistory.CLASS_UNFREEZE);
						dataService.storeModificationHistory(unfreezeHistory);
//						}
					}
					
					break;
				case WorkflowRule.MODIFIED_DATA_TYPE_UNFREEZE://解冻
					evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, rule.getEvalId());
					
					vendorService.updateForUnfreeze(rule.getVendorId());
//					if(parentCompanyIsSSCS){
					ModificationHistory unfreezeHistory2 = new ModificationHistory(
							rule.getVendorId(), -1,
							ModificationHistory.CLASS_UNFREEZE);
					
					dataService.storeModificationHistory(unfreezeHistory2);
//					}
					
					boolean hasEvaluation = evaluationService.hasLatestEvaluationByVendorId(rule.getVendorId());
					/**
					 * 有评估说明是正常做出来的外部公司，
					 * 需要按照评估的分数进行更新供应商的状态。
					 * （可以参考我给你的workflow图片）
					 */
					if(hasEvaluation){
						Evaluation lastEval = evaluationService
								.findLatestEvaluationByVendorId(rule.getVendorId());
						switch (lastEval.getResult()) {
							case Evaluation.RESULT_IMPROVEMENT:
								vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING,
										rule.getVendorId());
								break;
							case Evaluation.RESULT_CLOSE:
								vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE,
										rule.getVendorId());
								break;
							case Evaluation.RESULT_START_WORKFLOW:
							case Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO:
								vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED,
										rule.getVendorId());
								break;
						}
					}else{
						/**
						 * 内部供应商或导入的供应商，解冻后直接更新为登记。
						 */
						vendorService.updateStatus(VendorBasicInfo.STATUS_REGISTERED, rule.getVendorId());
					}
					
					break;
			}
			// when vendor's super company is not SSCS, show the workflow's process in my_applications
			if((!parentCompanyIsSSCS && rule.getStage() > WorkflowRule.STAGE_L2)){
				rule.setStage(70);// show null in my_application
			}
			rule.setStatus(WorkflowRule.STATUS_CLOSED);
			vendorService.updateApplicant(rule.getApplicant(), rule.getVendorId());
		}

		workflowService.updateWorkflowRule(rule);

		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", rule.getId());
		String number = vendorService.getVendorNumber(rule.getVendorId());
		templateModel.put("number", number == null ? "" : number);
		SystemSetting setting = dataService.getSystemSetting();
		templateModel.put("comment", comment);
		// rule normal finish or unSSCS L2 stage done
		if (rule.isFinished() || (!parentCompanyIsSSCS && rule.getStage() > WorkflowRule.STAGE_L2)) {
			templateModel.put("vid", "" + rule.getVendorId());
				dataService.sendMail(rule.getApplicant(),
							setting.getApplicationFinishedMailSubject(),
							setting.getApplicationFinishedMailTemplate(),
							templateModel, null);
		} else {
			if (result == WorkflowRule.RESULT_APPROVED) {
				dataService.sendMail(rule.getProcessor(),
								setting.getApprovalRequestMailSubject(),
								setting.getApprovalRequestMailTemplate(),
								templateModel, null);

			} else if (result == WorkflowRule.RESULT_DENIED) {
				dataService.sendMail(rule.getProcessor(),
								setting.getDeniedMailSubject(),
								setting.getDeniedMailTemplate(), templateModel, null);

			} 

		}
		// return "redirect:/workflow/myapprovals";
		model.addAttribute("approvals",
				workflowService.findMyApprovals(Utils.getUser().getUsername()));

		return "workflow/my_approval";

	}
	
	private List<String> getApprovalIds(WorkflowRule rule) {
		List<String> ids = new ArrayList<String>();
		if(rule.getL1() != null && !"".equals(rule.getL1())){
			ids.add(rule.getL1());
		}
		if(rule.getL2() != null && !"".equals(rule.getL2())){
			ids.add(rule.getL2());
		}
		if(rule.getCeo() != null && !"".equals(rule.getCeo())){
			ids.add(rule.getCeo());
		}
		if(rule.getDm() != null && !"".equals(rule.getDm())){
			ids.add(rule.getDm());
		}
		if(rule.getFm() != null && !"".equals(rule.getFm())){
			ids.add(rule.getFm());
		}
		if(rule.getHd() != null && !"".equals(rule.getHd())){
			ids.add(rule.getHd());
		}
		return ids;
	}
	
	/**
	 * 改善后，点击再申请
	 * 
	 * @param workflowId
	 * @param comment
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/resume/{workflowId}", method = RequestMethod.POST)
	public String resumeWorkflow(@PathVariable("workflowId") String workflowId,
			@RequestParam(required = false) String comment, Model model,
			HttpServletRequest request) {
		/**
		 * Remove args Utils.getUser().getUsername()
		 * Cause:
		 * 1) You can get a unique data only from UUUID in table witch named workflow_rule.
		 * 2) When this application is re-application by others(employee), you cann't get the workflow rule from DB by used login user name.
		 */
//		WorkflowRule rule = workflowService.getWorkflowRule(workflowId, Utils
//				.getUser().getUsername());
		comment = comment == null ? "" : comment;
		WorkflowRule rule = workflowService.getWorkflowRule(workflowId);
		rule.setProcessor(rule.getL1() == null || "".equals(rule.getL1()) ? rule.getL2() : rule.getL1());
		rule.setStage(rule.getL1() == null || "".equals(rule.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
		rule.setStatus(WorkflowRule.STATUS_RUNNING);
		// need update applicant, when other employee re-application
		rule.setApplicant(Utils.getUser().getUsername());
		WorkflowApprovalResult result = new WorkflowApprovalResult();

		result.setWorkflowRuleId(workflowId);
		result.setStage(0);
		result.setUserId(rule.getApplicant());
		result.setProcessedDate(new Date());
		result.setComment(comment);

		workflowService.storeWorkflowApprovalResult(result);

		workflowService.resumeUpdateWorkflowRule(rule);

		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", rule.getId());
		String number = vendorService.getVendorNumber(rule.getVendorId());
		templateModel.put("number", number == null ? "" : number);
		templateModel.put("comment", comment);
		SystemSetting setting = dataService.getSystemSetting();
		dataService.sendMail(rule.getProcessor(),
				setting.getApprovalRequestMailSubject(),
				setting.getApprovalRequestMailTemplate(), templateModel, null);

		return "redirect:/workflow/myapplications";
	}
	/**
	 * 
	 * @Title:       cancelWorkflow 
	 * @Description: ( 申请者提出取消操作  ) 
	 * @param        @param workflowId
	 * @param        @param model
	 * @param        @param comment
	 * @param        @param request
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * DataTime:     Jul 9, 2012
	 */
	@RequestMapping(value = "/cancel/{workflowId}", method = RequestMethod.POST)
	public String cancelWorkflow(@PathVariable("workflowId") String workflowId,
			Model model, @RequestParam(required = false) String comment,
			HttpServletRequest request) {
		comment = comment==null ? "" : comment;
		WorkflowRule rule = workflowService.getWorkflowRule(workflowId, Utils
				.getUser().getUsername());

		switch (rule.getModifiedDataType()) {
		case WorkflowRule.MODIFIED_DATA_TYPE_NEW:
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLYING,
					rule.getVendorId());
			break;
		case WorkflowRule.MODIFIED_DATA_TYPE_EDIT:
			vendorService.restoreVendorInfo(rule.getVendorId());
			break;
		case WorkflowRule.MODIFIED_DATA_TYPE_DELETE:
		case WorkflowRule.MODIFIED_DATA_TYPE_FREEZE:
		case WorkflowRule.MODIFIED_DATA_TYPE_UNFREEZE:
		case WorkflowRule.MODIFIED_DATA_TYPE_REEVAL:
			int vendorStatus = -1;

			if (evaluationService.hasLatestEvaluationByVendorId(rule
					.getVendorId())) {
				Evaluation eval = evaluationService
						.findLatestEvaluationByVendorId(rule.getVendorId());

				switch (eval.getResult()) {
				case Evaluation.RESULT_CLOSE:
					vendorStatus = VendorBasicInfo.STATUS_CLOSE;
					break;
				case Evaluation.RESULT_IMPROVEMENT:
					vendorStatus = VendorBasicInfo.STATUS_IMPROVING;
					break;
				case Evaluation.RESULT_START_WORKFLOW:
				case Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO:
					vendorStatus = VendorBasicInfo.STATUS_REGISTERED;
					break;

				}
			} else {
				vendorStatus = VendorBasicInfo.STATUS_REGISTERED;
			}
			vendorService.updateStatus(vendorStatus, rule.getVendorId());
			break;
		}
		WorkflowApprovalResult war = new WorkflowApprovalResult();
		
		war.setStage(WorkflowApprovalResult.STAGE_CANCEL);//WorkflowRule.CANCEL_END
		// everyone can cancel the workflow, so UserId should be login user's.
		//war.setUserId(rule.getApplicant());
		war.setUserId(Utils.getUser().getUsername());
		war.setResult(WorkflowRule.RESULT_CANCEL);
		war.setComment(comment);
		war.setWorkflowRuleId(rule.getId());
		war.setProcessedDate(new Date());

		workflowService.storeWorkflowApprovalResult(war);
		
		rule.setStatus(WorkflowRule.STATUS_CLOSED);
		// show process words in my applications
		rule.setStage(WorkflowRule.CANCEL_END);
		workflowService.updateWorkflowRule(rule);
		
		//发送取消邮件
		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", rule.getId());
		String number = vendorService.getVendorNumber(rule.getVendorId());
		templateModel.put("number", number == null ? "" : number);
		
		templateModel.put("comment", comment);
		SystemSetting setting = dataService.getSystemSetting();

		// send email to applicant
		dataService.sendMail(rule.getApplicant(),
				setting.getCancelMailSubject(),
				setting.getCancelMailTemplate(), templateModel, null);
		
		// send email to all approval users.
		List<String> ids = getApprovalIds(rule);
		
		for(String id :  ids){
			dataService.sendMail(id,
					setting.getCancelMailSubject(),
					setting.getCancelMailTemplate(), templateModel, null);
		}
		return "redirect:/workflow/myapplications";
	}
	/**
	 * reject
	 * 
	 * @param workflowId
	 * @param model
	 * @param comment
	 * @return
	 */
	public String rejectWorkflow(String workflowId, Model model,
			@RequestParam(required = false) String comment) {
		comment = comment == null ? "" : comment;
		WorkflowRule rule = workflowService.getWorkflowRule(workflowId, Utils
				.getUser().getUsername());

		switch (rule.getModifiedDataType()) {
		case WorkflowRule.MODIFIED_DATA_TYPE_NEW:
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLYING,
					rule.getVendorId());
			break;
		case WorkflowRule.MODIFIED_DATA_TYPE_EDIT:
			vendorService.restoreVendorInfo(rule.getVendorId());
			break;
		case WorkflowRule.MODIFIED_DATA_TYPE_DELETE:
		case WorkflowRule.MODIFIED_DATA_TYPE_FREEZE:
		case WorkflowRule.MODIFIED_DATA_TYPE_UNFREEZE:
		case WorkflowRule.MODIFIED_DATA_TYPE_REEVAL:
			int vendorStatus = -1;

			if (evaluationService.hasLatestEvaluationByVendorId(rule
					.getVendorId())) {
				Evaluation eval = evaluationService
						.findLatestEvaluationByVendorId(rule.getVendorId());

				switch (eval.getResult()) {
				case Evaluation.RESULT_CLOSE:
					vendorStatus = VendorBasicInfo.STATUS_CLOSE;
					break;
				case Evaluation.RESULT_IMPROVEMENT:
					vendorStatus = VendorBasicInfo.STATUS_IMPROVING;
					break;
				case Evaluation.RESULT_START_WORKFLOW:
				case Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO:
					vendorStatus = VendorBasicInfo.STATUS_REGISTERED;
					break;

				}
			} else {
				vendorStatus = VendorBasicInfo.STATUS_REGISTERED;
			}
			vendorService.updateStatus(vendorStatus, rule.getVendorId());
			break;
		}
		WorkflowApprovalResult war = new WorkflowApprovalResult();
		
		war.setStage(rule.getStage());
		war.setUserId(rule.getProcessor());
		war.setResult(WorkflowRule.RESULT_REJECT);
		war.setComment(comment);
		war.setWorkflowRuleId(rule.getId());
		war.setProcessedDate(new Date());

		workflowService.storeWorkflowApprovalResult(war);
		
		rule.setStatus(WorkflowRule.STATUS_CLOSED);
		// show process words in my applications
		rule.setStage(WorkflowRule.REJECT_END);
		workflowService.updateWorkflowRule(rule);
		
		return "redirect:/workflow/myapplications";
	}
}
