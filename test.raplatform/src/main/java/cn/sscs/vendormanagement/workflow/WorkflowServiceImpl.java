package cn.sscs.vendormanagement.workflow;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Service;

@Service("workflowService")
public class WorkflowServiceImpl implements IWorkflowService {

	private SimpleJdbcInsert insertWorkflowRule = null;
	
	private SimpleJdbcInsert insertWorkflowApproval = null;
	
	private SimpleJdbcTemplate jdbcTemplate = null;
	
	@Autowired
	public void init(DataSource dataSource) {
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
		
		insertWorkflowRule = new SimpleJdbcInsert(dataSource).withTableName("workflow_rule");
		insertWorkflowApproval = new SimpleJdbcInsert(dataSource).withTableName("workflow_approval_result");
		
	}

	@Override
	public void storeWorkflowRule(WorkflowRule rule) {
		insertWorkflowRule.execute(rule.toMap());
	}

	@Override
	public void storeWorkflowApprovalResult(WorkflowApprovalResult approval) {
		insertWorkflowApproval.execute(approval.toMap());
	}

	
	@Override
	public WorkflowRule getWorkflowRule(String id, String processor) {
		return jdbcTemplate.queryForObject(
				"SELECT * FROM workflow_rule WHERE id = ? AND (processor = ? OR applicant = ?)",
				new RowMapper<WorkflowRule>(){

					@Override
					public WorkflowRule mapRow(ResultSet result, int row)
							throws SQLException {
						
						WorkflowRule rule = new WorkflowRule();
						rule.setId(result.getString("id"));
						rule.setVendorId(result.getLong("vendor_id"));
						rule.setEvalId(result.getLong("eval_id"));
						rule.setApplicant(result.getString("applicant"));
						rule.setProcessor(result.getString("processor"));
						rule.setStage(result.getInt("stage"));
						rule.setL1(result.getString("l1"));
						rule.setL2(result.getString("l2"));
						rule.setCeo(result.getString("ceo"));
						rule.setHd(result.getString("hd"));
						rule.setFm(result.getString("fm"));
						rule.setDm(result.getString("dm"));
						rule.setApplicationDate(result.getDate("application_date"));
						rule.setStatus(result.getInt("status"));
						rule.setModifiedDataType(result.getInt("modified_data_type"));
						
						return rule;
					}},
				id, processor, processor);
	}
	@Override
	public WorkflowRule getWorkflowRule(String id) {
		return jdbcTemplate.queryForObject(
				"SELECT * FROM workflow_rule WHERE id = ? ",
				new RowMapper<WorkflowRule>(){

					@Override
					public WorkflowRule mapRow(ResultSet result, int row)
							throws SQLException {
						
						WorkflowRule rule = new WorkflowRule();
						rule.setId(result.getString("id"));
						rule.setVendorId(result.getLong("vendor_id"));
						rule.setEvalId(result.getLong("eval_id"));
						rule.setApplicant(result.getString("applicant"));
						rule.setProcessor(result.getString("processor"));
						rule.setStage(result.getInt("stage"));
						rule.setL1(result.getString("l1"));
						rule.setL2(result.getString("l2"));
						rule.setCeo(result.getString("ceo"));
						rule.setHd(result.getString("hd"));
						rule.setFm(result.getString("fm"));
						rule.setDm(result.getString("dm"));
						rule.setApplicationDate(result.getDate("application_date"));
						rule.setStatus(result.getInt("status"));
						rule.setModifiedDataType(result.getInt("modified_data_type"));
						
						return rule;
					}},
				id);
	}	
	@Override
	public void updateWorkflowRule(WorkflowRule workflow) {
		jdbcTemplate.update(
				"UPDATE workflow_rule SET " +
				"  stage= :stage," +
				"  processor = :processor," +
				"  status = :status," +
				"  eval_id = :evalId " +
				"WHERE id = :id ", 
				new BeanPropertySqlParameterSource(workflow));
	}
	
	@Override
	public void resumeUpdateWorkflowRule(WorkflowRule workflow) {
		jdbcTemplate.update(
				"UPDATE workflow_rule SET " +
				"  stage=:stage," +
				"  processor = :processor," +
				"  applicant = :applicant," +
				"  status = :status " +
				"WHERE id = :id ", 
				new BeanPropertySqlParameterSource(workflow));
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<Application> findMyApplications(String id) {
//		String sql = 
//				"SELECT " +
//					" vbi.vendor_chinese_name as chineseName, "+
//					" vbi.vendor_english_name as englishName, "+
//					" vbi.vendor_code as vendorCode, "+
//					" w.stage as stage, "+
//					" w.application_date as applicationDate, "+
//					" w.id as id, "+ 
//					" w.status as status, "+ 
//					" w.vendor_id as vendorId, "+ 
//					" w.eval_id as evalId, "+ 
//					" w.modified_data_type as modificationType "+
//				"FROM vendor_basic_info vbi, workflow_rule w "+ 
//				"WHERE vbi.id = w.vendor_id and w.applicant = ? AND (w.status = 1 OR w.status = 2 OR w.status = 3 OR w.status = 4) "+
//				"UNION ALL "+
//				"SELECT " +
//					 "vbi.vendor_chinese_name as chineseName, "+ 
//					 "vbi.vendor_english_name as englishName, "+ 
//					 "vbi.vendor_code as vendorCode, "+ 
//					 "NULL as stage, "+
//					 "vbi.registered_date as applicationDate, "+ 
//					 "NULL as id, "+ 
//					 "vbi.status as status, "+ 
//					 "NULL as vendorId, "+ 
//					 "NULL as evalId, "+ 
//					 "NULL as modificationType "+
//				"FROM vendor_basic_info vbi "+
//				"WHERE vbi.status IN(5, 20, 30) "+
//				"ORDER BY applicationDate DESC";
		return jdbcTemplate.query(
				"SELECT vbi.vendor_chinese_name as chineseName, " +
				" vbi.vendor_english_name as englishName, " +
				" vbi.vendor_code as vendorCode, " +
				" w.stage as stage," +
				" w.application_date as applicationDate, " +
				" w.id as id, " +
				" w.status as status, " +
				" w.vendor_id as vendorId, "+
				" w.eval_id as evalId, "+
				" w.modified_data_type as modificationType " +
				"FROM vendor_basic_info vbi, workflow_rule w " +
				"WHERE vbi.id = w.vendor_id " +
				"  AND w.applicant = ?" +
				"  AND (w.status = 1 OR w.status = 2 OR w.status = 3 OR w.status = 4) " +
				"ORDER BY w.registered_date DESC", 
				//sql,
				ParameterizedBeanPropertyRowMapper.newInstance(Application.class),
				id);
	}
	@SuppressWarnings("deprecation")
	@Override
	public List<Application> findMyOldApplications(String id, MyapplicationsSearchDetail myapplicationsSearchDetail) {
		List<String> usercompanyList = getUsercompanyById(id);
		String usercompanyStr = "and vbi.super_company in (";
		boolean firstTime = true;
		for(String usercompany : usercompanyList){
		    if("1".equals(usercompany)){
		        usercompanyStr = "";
		        break;
		    }
		    if(!firstTime){
		        usercompanyStr += ", ";
		        firstTime = false;
		    }
		    usercompanyStr += usercompany ;
		}
		if(!"".equals(usercompanyStr)){
		    usercompanyStr += ") ";
		}
	    String typeStr = "";
		String unionAll = "";// 供应商编辑中、改善和关闭
		switch (myapplicationsSearchDetail.getType()){
			case 1 : typeStr = " AND (w.status = 2) ";//结束
			 		 break;
			case 0 : typeStr = " AND (w.status = 1 OR w.status = 3) ";//申请中
					 break;
			case 2 : typeStr = " AND (w.status = 4) ";//临时保存
			 		 break;
			case 4 : typeStr = " AND (w.status = 1 OR w.status = 2 OR w.status = 3 OR w.status = 4) ";//全部
					 unionAll = " UNION ALL "+
								" SELECT " +
									 "vbi.vendor_chinese_name as chineseName, "+ 
									 "vbi.vendor_english_name as englishName, "+ 
									 "vbi.vendor_code as vendorCode, "+ 
									 "-1 as stage, "+
									 "date('19700101') as applicationDate, "+//to_date() 也可以吧
									 "0 as id, "+ 
									 "vbi.status as status, "+ 
									 "vbi.id as vendorId, "+ 
									 "'' as evalId, "+ 
									 "'' as modificationType "+
								"FROM vendor_basic_info vbi "+
								"WHERE vbi.status IN(5, 20, 30) " + usercompanyStr;
			 		 break;
			default: typeStr = " AND (w.status = 1 OR w.status = 3) ";
		}
		String numberStr = "";
		if(!"".equals(myapplicationsSearchDetail.getNumber().trim())){
			numberStr = " AND vbi.number = '"+myapplicationsSearchDetail.getNumber().trim()+"' ";
			unionAll = "";
		}
			
		String startDate = "";
		if(myapplicationsSearchDetail.getStartDate() != null){
			java.sql.Date sqlDate = new java.sql.Date(myapplicationsSearchDetail.getStartDate().getTime());
			startDate = " and application_date >= '" + sqlDate.toString() + "'";
			unionAll = "";
		}
		String endDate = "";
		if(myapplicationsSearchDetail.getEndDate() != null){
			java.sql.Date sqlDate = new java.sql.Date(myapplicationsSearchDetail.getEndDate().getTime());
			endDate = " and application_date <= '" + sqlDate.toString() + "'";
			unionAll = "";
		}
		
		if(myapplicationsSearchDetail.getUserType() == 0){
			String applicantStr = "  ";
			if(!"".equals(myapplicationsSearchDetail.getUserValue().trim())){
				applicantStr = "  AND w.applicant = '" + myapplicationsSearchDetail.getUserValue().trim() + "'";
				unionAll = "";
			}
			return jdbcTemplate.query(
					"SELECT vbi.vendor_chinese_name as chineseName, " +
					" vbi.vendor_english_name as englishName, " +
					" vbi.vendor_code as vendorCode, " +
					" w.stage as stage," +
					" w.application_date as applicationDate, " +
					" w.id as id," +
					" w.status as status, " +
					" w.vendor_id as vendorId, "+
					" w.eval_id as evalId, "+
					" w.modified_data_type as modificationType " +
					"FROM vendor_basic_info vbi, workflow_rule w " +
					"WHERE vbi.id = w.vendor_id " +
					applicantStr+
					typeStr+
					numberStr+
					startDate +
					endDate +
					usercompanyStr +
					unionAll +
					" ORDER BY applicationDate DESC", 
					ParameterizedBeanPropertyRowMapper.newInstance(Application.class)
					);
		}else{
			String applicantStr = " ";
			if(!"".equals(myapplicationsSearchDetail.getUserValue().trim())){
				applicantStr = "  AND u.name like '%"+myapplicationsSearchDetail.getUserValue().trim()+"%'";
				unionAll = "";
			}
			return jdbcTemplate.query(
					"SELECT vbi.vendor_chinese_name as chineseName, " +
					" vbi.vendor_english_name as englishName, " +
					" vbi.vendor_code as vendorCode, " +
					" w.stage as stage," +
					" w.application_date as applicationDate, " +
					" w.id as id," +
					" w.status as status, " +
					" w.vendor_id as vendorId, "+
					" w.eval_id as evalId, "+
					" w.modified_data_type as modificationType " +
					"FROM vendor_basic_info vbi, workflow_rule w, user u " +
					"WHERE vbi.id = w.vendor_id " +
					"and w.applicant = u.id "+
					applicantStr+
					typeStr+
					numberStr+
					startDate +
					endDate +
					usercompanyStr +
					unionAll +
					" ORDER BY applicationDate DESC", 
					ParameterizedBeanPropertyRowMapper.newInstance(Application.class)
					);
		}
		
	}
	
	@SuppressWarnings("deprecation")
	@Override
    public List<String> getUsercompanyById(String id) {
	    ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>() {
            @Override
            public String mapRow(ResultSet rs, int rowNum)
                    throws SQLException {
                return rs.getString("usercompany");
            }
        };
	    return jdbcTemplate.query(
                "SELECT ui.usercompany "+
                "FROM  user_info ui, user u  "+
                "WHERE  "+
                        "u.id = ui.id AND   "+
                        "ui.valid = 1 AND   "+
                        "u.id = ?",
                mapper, id);
    }

    @SuppressWarnings("deprecation")
	@Override
	public List<Approval> findMyApprovals(String id) {
		return jdbcTemplate.query(
				"SELECT vbi.vendor_chinese_name as chineseName, " +
				" vbi.vendor_english_name as englishName, " +
				" vbi.vendor_code as vendorCode, " +
				" w.stage as stage," +
				" w.application_date as applicationDate, " +
				" w.id as id," +
				" w.modified_data_type as modificationType " +
				"FROM vendor_basic_info vbi, workflow_rule w " +
				"WHERE vbi.id = w.vendor_id " +
				"  AND w.processor = ?" +
				"  AND w.status = 1 " +
				"ORDER BY w.registered_date DESC",
				ParameterizedBeanPropertyRowMapper.newInstance(Approval.class),
				id);
	}
	
	
	@SuppressWarnings("deprecation")
	@Override
	public List<WorkflowApprovalResult> findWorkflowApprovalResults(String id) {
		return jdbcTemplate.query(
				"SELECT war.id, " +
				" war.stage," +
				" war.user_id as userId," +
				" war.comment," +
				" war.result," +
				" war.processed_date as processedDate," +
				" u.name as userName " +
				"FROM  workflow_approval_result war, user u " +
				"WHERE war.user_id = u.id " +
				"  AND war.workflow_rule_id = ? " +
				"ORDER BY id",
				ParameterizedBeanPropertyRowMapper.newInstance(WorkflowApprovalResult.class),
				id);
		
	}

	@Override
	public void updateStatus(String id, int statusTmpsaveEnd) {
		jdbcTemplate.update(
				"UPDATE workflow_rule SET " +
				"  status = ? " +
				"WHERE id = ? ", 
				statusTmpsaveEnd, id);
	}
}
