package cn.sscs.vendormanagement.workflow;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author kazzy
 *
 */
public class WorkflowApprovalResult {
	/**
	 * When applicant cancel the workflow.
	 */
	public static final int STAGE_CANCEL = 5;
	
	private long id = -1;
	
	private String workflowRuleId = null;
	
	private int stage = -1;
	
	private String userId = null;
	
	private String userName = null;
	
	private String comment = null;
	
	private int result = -1;

	private Date processedDate = null;
		
	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the workflowRuleId
	 */
	public String getWorkflowRuleId() {
		return workflowRuleId;
	}

	/**
	 * @param workflowRuleId the workflowRuleId to set
	 */
	public void setWorkflowRuleId(String workflowRuleId) {
		this.workflowRuleId = workflowRuleId;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId
	 */
	public void setUserId(String userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}
	
	/**
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getStage() {
		return stage;
	}
	
	/**
	 * 
	 * @param stage
	 */
	public void setStage(int stage) {
		this.stage = stage;
	}
	
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		if ("".equals(comment)) {
			comment = null;
		}
		this.comment = comment;
	}

	/**
	 * @return the result
	 */
	public int getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(int result) {
		this.result = result;
	}

	/**
	 * 
	 * @return
	 */
	public Date getProcessedDate() {
		return processedDate;
	}
	
	/**
	 * 
	 * @param processedDate
	 */
	public void setProcessedDate(Date processedDate) {
		this.processedDate = processedDate;
	}
	
	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("workflow_rule_id",workflowRuleId);
		map.put("user_id", userId);
		map.put("stage", stage);
		map.put("comment", comment);
		map.put("result", result);
		map.put("processed_date", processedDate);
		
		return map;
	}

	
}
