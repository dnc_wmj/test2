package cn.sscs.vendormanagement.workflow;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import cn.sscs.vendormanagement.Utils;

public class MyapplicationsSearchDetail {
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date startDate = null;
	
	@DateTimeFormat(pattern = "yyyy/MM/dd")
	private Date endDate = null;

	private String number = "";

	private int type = 4;
	
	private String userValue = Utils.getUser().getUsername();
	
	private int userType = 0;
	
	public MyapplicationsSearchDetail(){
		
	}
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public void setUserValue(String userValue) {
		this.userValue = userValue;
	}
	public String getUserValue() {
		return userValue;
	}
	public void setUserType(int userType) {
		this.userType = userType;
	}
	public int getUserType() {
		return userType;
	}

}
