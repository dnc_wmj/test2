package cn.sscs.vendormanagement.workflow;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class WorkflowRule {
	
	public static final int STAGE_APPLICANT = 0;

	public static final int STAGE_L1 = 10;
	
	public static final int STAGE_L2 = 20;
	
	public static final int STAGE_CEO = 30;
	
	public static final int STAGE_HD = 40;
	
	public static final int STAGE_FM = 50;
	
	public static final int STAGE_DM = 60;
	
	/**
	 * add a 
	 */
	public static final int NORMAL_END = 70;

	public static final int CANCEL_END = 80;

	public static final int REJECT_END = 90;
	
	public static final int RESULT_APPROVED = 1;
	
	public static final int RESULT_DENIED = 2;
	
	public static final int RESULT_CANCEL = 3;
	
	public static final int RESULT_REJECT = 4;
	
	public static final int STATUS_RUNNING = 1;
	
	public static final int STATUS_CLOSED = 2;
	
	public static final int STATUS_SUSPEND = 3;
	
	public static final int STATUS_TMPSAVE = 4;

	public static final int MODIFIED_DATA_TYPE_NEW = 10;
	
	public static final int MODIFIED_DATA_TYPE_EDIT = 20;
	
	public static final int MODIFIED_DATA_TYPE_REEVAL = 30;

	public static final int MODIFIED_DATA_TYPE_DELETE = 40;

	public static final int MODIFIED_DATA_TYPE_TMPSAVE = 50;
	
	public static final int MODIFIED_DATA_TYPE_FREEZE = 60;
	
	public static final int MODIFIED_DATA_TYPE_UNFREEZE = 70;

	public static final int STATUS_TMPSAVE_END = 5;

	

	
	
	private String id = null;
	
	private long vendorId = -1;
	
	private long evalId = -1;
	
	private String applicant = null;
	
	private int stage = -1;
	
	private String processor = null;
	
	private String l1 = null;
	
	private String l2 = null;
	
	private String ceo = null;
	
	private String hd = null;
	
	private String fm = null;
	
	private String dm = null;
	
	private Date applicationDate = null;
		
	private int status = -1;
	
	private int modifiedDataType = MODIFIED_DATA_TYPE_NEW;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the vendorId
	 */
	public long getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getEvalId() {
		return evalId;
	}
	
	/**
	 * 
	 * @param evalId
	 */
	public void setEvalId(long evalId) {
		this.evalId = evalId;
	}
	
	
	/**
	 * @return the applicant
	 */
	public String getApplicant() {
		return applicant;
	}

	/**
	 * @param applicant the applicant to set
	 */
	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	/**
	 * @return the stage
	 */
	public int getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(int stage) {
		this.stage = stage;
	}

	/**
	 * @return the processor
	 */
	public String getProcessor() {
		return processor;
	}

	/**
	 * @param processor the processor to set
	 */
	public void setProcessor(String processor) {
		this.processor = processor;
	}

	/**
	 * @return the l1
	 */
	public String getL1() {
		return l1;
	}

	/**
	 * @param l1 the l1 to set
	 */
	public void setL1(String l1) {
		this.l1 = l1;
	}

	/**
	 * @return the l2
	 */
	public String getL2() {
		return l2;
	}

	/**
	 * @param l2 the l2 to set
	 */
	public void setL2(String l2) {
		this.l2 = l2;
	}

	/**
	 * 
	 * @return
	 */
	public String getCeo() {
		return ceo;
	}
	
	/**
	 * 
	 * @param ceo
	 */
	public void setCeo(String ceo) {
		this.ceo = ceo;
	}
	
	/**
	 * @return the hd
	 */
	public String getHd() {
		return hd;
	}

	/**
	 * @param hd the hd to set
	 */
	public void setHd(String hd) {
		this.hd = hd;
	}

	/**
	 * @return the fm
	 */
	public String getFm() {
		return fm;
	}

	/**
	 * @param fm the fm to set
	 */
	public void setFm(String fm) {
		this.fm = fm;
	}

	/**
	 * @return the dm
	 */
	public String getDm() {
		return dm;
	}

	/**
	 * @param dm the dm to set
	 */
	public void setDm(String dm) {
		this.dm = dm;
	}

	/**
	 * @return the applicationDate
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}

	/**
	 * @param applicationDate the applicationDate to set
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}

	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public int getModifiedDataType() {
		return modifiedDataType;
	}
	
	/**
	 * 
	 * @param modifiedDataType
	 */
	public void setModifiedDataType(int modifiedDataType) {
		this.modifiedDataType = modifiedDataType;
	}
	
	/**
	 * @return the stageL1
	 */
	public static int getStageL1() {
		return STAGE_L1;
	}

	/**
	 * @return the stageL2
	 */
	public static int getStageL2() {
		return STAGE_L2;
	}

	/**
	 * @return the stageCeo
	 */
	public static int getStageCeo() {
		return STAGE_CEO;
	}

	/**
	 * @return the stageHd
	 */
	public static int getStageHd() {
		return STAGE_HD;
	}

	/**
	 * @return the stageFm
	 */
	public static int getStageFm() {
		return STAGE_FM;
	}

	/**
	 * @return the stageDm
	 */
	public static int getStageDm() {
		return STAGE_DM;
	}

	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> rowMap = new HashMap<String, Object>();
		
		rowMap.put("id", id);
		rowMap.put("vendor_id", vendorId);
		rowMap.put("eval_id", evalId);
		rowMap.put("applicant", applicant);
		rowMap.put("stage", stage);
		rowMap.put("processor", processor);
		rowMap.put("l1", l1);
		rowMap.put("l2", l2);
		rowMap.put("ceo", ceo);
		rowMap.put("hd", hd);
		rowMap.put("fm", fm);
		rowMap.put("dm", dm);
		rowMap.put("application_date", applicationDate);
		rowMap.put("status", status);
		rowMap.put("registered_date", new Date());
		rowMap.put("modified_data_type", modifiedDataType);
		
		return rowMap;
	}

	/**
	 * 
	 */
	void process(int result) {
		
		if (result == RESULT_DENIED) {
			if (stage == STAGE_FM) {
				setStage(STAGE_HD);
				setProcessor(getHd());				
			}
			return;
		}
		
		stage += 10;
		
		String processor = null;
		switch(stage) {
		case STAGE_L2:
			processor = getL2();
			break;
		case STAGE_CEO:
			processor = getCeo();
			break;
		case STAGE_HD:
			processor = getHd();
			break;
		case STAGE_FM:
			processor = getFm();
			break;
		case STAGE_DM:
			processor = getDm();
			break;
		}
		
		setProcessor(processor);
	}

	/**
	 * 
	 * @return
	 */
	boolean isFinished() {
		return stage > STAGE_DM;
	}
	
	public String getWorkflowType() {
		if (l1 == null && l2 == null) {
			return "partial";
		}
		return "full";
	}
}
