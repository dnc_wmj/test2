package cn.sscs.vendormanagement.workflow;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.context.i18n.LocaleContextHolder;

/**
 * 
 * @author kazzy
 *
 */
public class Application {

	private String id = null;
	
	private String vendorCode = null;
	
	private String chineseName = null;
	
	private String englishName = null;
	
	private int stage = -1;

	private Date applicationDate = null;
	
	private int modificationType = -1;
	
	private Date registeredDate = null;
	
	private String status = null;
	
	private String vendorId = null;
	
	private String evalId = null;
	
	/**
	 * 
	 * @return
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * @param vendorCode the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	/**
	 * @return the chineseName
	 */
	public String getChineseName() {
		return chineseName;
	}

	/**
	 * @param chineseName the chineseName to set
	 */
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	/**
	 * @return the englishName
	 */
	public String getEnglishName() {
		return englishName;
	}

	/**
	 * @param englishName the englishName to set
	 */
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	/**
	 * @return the stage
	 */
	public int getStage() {
		return stage;
	}

	/**
	 * @param stage the stage to set
	 */
	public void setStage(int stage) {
		this.stage = stage;
	}

	/**
	 * 
	 * @return
	 */
	public Date getApplicationDate() {
		return applicationDate;
	}
	
	/**
	 * 
	 * @param applicationDate
	 */
	public void setApplicationDate(Date applicationDate) {
		this.applicationDate = applicationDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getModificationType() {
		return modificationType;
	}
	
	/**
	 * 
	 * @param modificationType
	 */
	public void setModificationType(int modificationType) {
		this.modificationType = modificationType;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getStageMessageKey() {
		String label = null;
		switch (stage) {
		case WorkflowRule.STAGE_APPLICANT:
			label = "workflow.applicant";
			break;			
		case WorkflowRule.STAGE_L1:
			label = "workflow.l1";
			break;
		case WorkflowRule.STAGE_L2:
			label = "workflow.l2";
			break;
		case WorkflowRule.STAGE_CEO:
			label = "workflow.ceo";
			break;
		case WorkflowRule.STAGE_HD:
			label = "workflow.hd";
			break;
		case WorkflowRule.STAGE_FM:
			label = "workflow.fm";
			break;
		case WorkflowRule.STAGE_DM:
			label = "workflow.dm";
			break;
		case WorkflowRule.NORMAL_END:
			label = "workflow.normal_end";
			break;
		case WorkflowRule.CANCEL_END:
			label = "workflow.cancel_end";
			break;
		case WorkflowRule.REJECT_END:
			label = "workflow.reject_end";
			break;
		default:
			label = "workflow.empty";
		}
		
		return label;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		Locale locale = LocaleContextHolder.getLocale();
		if ("zh".equals(locale.getLanguage())) {
			return chineseName != null && !chineseName.equals("") ? chineseName : englishName;
		} else if ("en".equals(locale.getLanguage())){
			return englishName != null && !englishName.equals("") ? englishName : chineseName;
		} else {
			return chineseName != null ? chineseName : englishName;
		}
	}
	public String getShanghaiApplicationDate(){
		//又不是精确到小时，不需要设置时区
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		return "1970/01/01".equals(sdf.format(applicationDate)) ? "" : sdf.format(applicationDate);
	}

	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public Date getRegisteredDate() {
		return registeredDate;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getStatus() {
		return status;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setEvalId(String evalId) {
		this.evalId = evalId;
	}

	public String getEvalId() {
		return evalId;
	}
	
}
