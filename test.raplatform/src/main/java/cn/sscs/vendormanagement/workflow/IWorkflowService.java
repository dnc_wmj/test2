package cn.sscs.vendormanagement.workflow;

import java.util.List;

/**
 * 
 * @author kazzy
 *
 */
public interface IWorkflowService {

	/**
	 * 
	 * @param workflow
	 */
	void storeWorkflowRule(WorkflowRule workflow);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<Application> findMyApplications(String id);

	/**
	 * 
	 * @param id
	 * @param processor
	 * @return
	 */
	WorkflowRule getWorkflowRule(String id, String processor);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<Approval> findMyApprovals(String id);

	/**
	 * 
	 * @param workflow
	 */
	void updateWorkflowRule(WorkflowRule workflow);

	/**
	 * 
	 * @param approval
	 */
	void storeWorkflowApprovalResult(WorkflowApprovalResult approval);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<WorkflowApprovalResult> findWorkflowApprovalResults(String id);

	Object findMyOldApplications(String username,
			MyapplicationsSearchDetail myapplicationsSearchDetail);

	WorkflowRule getWorkflowRule(String id);

	void updateStatus(String id, int statusTmpsaveEnd);

	void resumeUpdateWorkflowRule(WorkflowRule workflow);

    List<String> getUsercompanyById(String id);
}
