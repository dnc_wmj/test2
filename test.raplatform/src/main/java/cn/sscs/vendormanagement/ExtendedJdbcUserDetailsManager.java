package cn.sscs.vendormanagement;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.context.ApplicationContextException;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.MappingSqlQuery;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.provisioning.JdbcUserDetailsManager;

/**
 * @author kazzy
 */
public class ExtendedJdbcUserDetailsManager extends JdbcUserDetailsManager {
    
    /**
     * @author kazzy
     */
    private class UsersByUsernameMapping extends MappingSqlQuery<UserDetails> {
        
        protected UsersByUsernameMapping(DataSource ds) {
            super(ds, getUsersByUsernameQuery());
            declareParameter(new SqlParameter(Types.VARCHAR));
            compile();
        }
        
        @Override
        protected UserDetails mapRow(ResultSet result, int rownum)
                throws SQLException {
            String id = result.getString("id");
            String password = result.getString("password");
            String name = result.getString("name");
            String email = result.getString("email");
            int division = result.getInt("division");
            boolean canDownCert = result.getBoolean("canDownCert");
            String userCompany = result.getString("usercompany");
            
            return new VndMgmntUser(id, password, name, email, division, canDownCert, userCompany, 
                    true, true, true, true, new ArrayList<GrantedAuthority>());
        }
        
    }
    
    private MappingSqlQuery<UserDetails> usersByUsernameMapping = null;
    
    @Override
    protected void initDao() throws ApplicationContextException {
        super.initDao();
        this.usersByUsernameMapping = new UsersByUsernameMapping(
                getDataSource());
    }
    
    @Override
    protected UserDetails createUserDetails(String username,
            UserDetails userFromUserQuery,
            List<GrantedAuthority> combinedAuthorities) {
        
        return new VndMgmntUser(userFromUserQuery.getUsername(),
                userFromUserQuery.getPassword(),
                ((VndMgmntUser) userFromUserQuery).getName(),
                ((VndMgmntUser) userFromUserQuery).getEmail(),
                ((VndMgmntUser) userFromUserQuery).getDivision(),
                ((VndMgmntUser) userFromUserQuery).isCanDownCert(),
                ((VndMgmntUser) userFromUserQuery).getUserCompany(),
                userFromUserQuery.isEnabled(), true, true, true,
                combinedAuthorities);
    }
    
    @Override
    protected List<UserDetails> loadUsersByUsername(String username) {
        // TODO username进行check 与证书的common name是否相同， 不同3次，把证书的username锁定喽
        // 放在成功跳转里check(class: AfterLoginSuccessHandler)
        return usersByUsernameMapping.execute(username);
    }
}
