package cn.sscs.vendormanagement;

import java.io.IOException;
import java.security.cert.X509Certificate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.sscs.vendormanagement.certificate.X500PrincipalHelper;

@Controller
@RequestMapping(value = "/signin")
public class SigninController {
    private static Log logger = LogFactory.getLog(SigninController.class);
    
    private static final String ATTR_CER = "javax.servlet.request.X509Certificate";
    
    // private static final String CONTENT_TYPE = "text/plain;charset=UTF-8";
    // private static final String DEFAULT_ENCODING = "UTF-8";
    // private static final String SCHEME_HTTPS = "https";
    
    @RequestMapping(method = RequestMethod.GET)
    public String signin(final Model model, final HttpServletRequest request,
            final HttpServletResponse response) throws IOException {
        if (logger.isDebugEnabled()) {
            logger.debug("start SigninController.signin.");
        }
        // -----------------------------------------------------------------
        // - 得到证书
        //
        // 客户端（浏览器）
        final X509Certificate[] certs = (X509Certificate[]) request
                .getAttribute(ATTR_CER);
        final boolean hasPermission = isHasPermission(certs, model);
        
        // ------------------------------------------------------------------
        String returnPath;
        if (hasPermission) {
            returnPath = "signin";
        } else {
            returnPath = "denyAccess";
        }
        
        return returnPath;
    }
    
    /**
     * <p>
     * 校验证书是否过期
     * </p>
     * 
     * @param certificate
     * @return
     * @throws Exception
     */
    private boolean verifyCertificate(final X509Certificate certificate) {
        if (logger.isDebugEnabled()) {
            logger.debug("start SigninController.verifyCertificate.");
        }
        boolean valid = true;
        try {
            certificate.checkValidity();
        } catch (Exception e) {
            // throw e;
            if (logger.isDebugEnabled()) {
                logger.debug("SigninController.verifyCertificate catch exception");
                logger.debug(e.toString());
                // e.printStackTrace();
            }
            valid = false;
        }
        return valid;
    }
    
    private boolean isHasPermission(final X509Certificate[] certs,
            final Model model) throws IOException {
        
        boolean hasPermission = true;
        if (certs == null) {
            if (logger.isDebugEnabled()) {
                logger.debug("start SigninController.isHasPermission: certs is null.");
            }
            hasPermission = false;
        } else {
            for (X509Certificate certificate : certs) {
                if (!verifyCertificate(certificate)) {
                    hasPermission = false;
                    model.addAttribute("message", "CaOutofDate");
                } else {
                    // get common name
                    final X500PrincipalHelper x500Principal = new X500PrincipalHelper(certificate.getSubjectX500Principal());
                    model.addAttribute("username", x500Principal.getCN());
                }
            }
        }
        return hasPermission;
    }
}
