package cn.sscs.vendormanagement;

public class GeneralException extends RuntimeException {

	private static final long serialVersionUID = 6582685590954740784L;

	private String msgKey = null;
	
	public GeneralException() {
	}
	
	public GeneralException(String msgKey) {
		this.msgKey = msgKey;
	}
	
	public String getMsgKey() {
		return msgKey;
	}
}
