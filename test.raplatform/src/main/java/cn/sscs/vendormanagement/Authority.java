package cn.sscs.vendormanagement;

import java.io.Serializable;

public class Authority implements Serializable{
    
    /**
     * 
     */
    private static final long serialVersionUID = -2298805743974210282L;

    /**
     * user id
     */
    private String username = null;
    
    /**
     * user authority
     */
    private String authority = null;
    
    /**
     * user division
     */
    private int division = 0;
    
    /**
     * user super company
     */
    private String usercompany = null;
    
    public String getUsername() {
        return username;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }
    
    public String getAuthority() {
        return authority;
    }
    
    public void setAuthority(String authority) {
        this.authority = authority;
    }
    
    public int getDivision() {
        return division;
    }
    
    public void setDivision(int division) {
        this.division = division;
    }
    
    public String getUsercompany() {
        return usercompany;
    }
    
    public void setUsercompany(String usercompany) {
        this.usercompany = usercompany;
    }
}
