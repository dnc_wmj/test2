package cn.sscs.vendormanagement;

public class SuperCompany {
    
    public final static int SSCS = 1;
    public final static String SSCS_NAME = "SSCS";
    public final static int SSGE = 2;
    public final static String SSGE_NAME = "SSGE";
    public final static int SSV = 3;
    public final static String SSV_NAME = "SSV";
    public final static int SEW = 4;
    public final static String SEW_NAME = "SEW";
    public final static int SDPW = 5;
    public final static String SDPW_NAME = "SDPW";
    public final static int SPDH = 6;
    public final static String SPDH_NAME = "SPDH";
    public final static int SEH = 7;
    public final static String SEH_NAME = "SEH";
    
    private String name = null;
    private int value = 1;
    
    public SuperCompany() {
    }
    
    public SuperCompany(String val) {
        this.name = getName(val);
        this.value = getValue(val);
    }
    
    public String getName() {
        return name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public int getValue() {
        return value;
    }
    
    public int getValue(String val) {
        return Integer.parseInt(val);
    }
    
    public void setValue(int value) {
        this.value = value;
    }
    
    public String getName(String val) {
        int v = Integer.parseInt(val);
        return getName(v);
    }
    
    public String getName(int v) {
        switch (v) {
            case SSCS:
                return SSCS_NAME;
            case SSGE:
                return SSGE_NAME;
            case SSV:
                return SSV_NAME;
            case SEW:
                return SEW_NAME;
            case SDPW:
                return SDPW_NAME;
            case SPDH:
                return SPDH_NAME;
            case SEH:
                return SEH_NAME;
        }
        return null;
    }
}
