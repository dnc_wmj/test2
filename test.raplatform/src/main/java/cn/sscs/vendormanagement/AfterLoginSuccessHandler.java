package cn.sscs.vendormanagement;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AfterLoginSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {
	
	private final static String	TRY_MAX_COUNT	= "_TRY_MAX_COUNT";
	private final static String INITED = "_INITED";
	private String				defaultTargetUrl;
	
	@SuppressWarnings("deprecation")
    @Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws ServletException, IOException {
	    // 只需要进来一次就够了
	    HttpSession session = request.getSession();
	    String username = authentication.getName();
	    if(session.getAttribute(username + INITED) == null){
	        WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(((HttpServletRequest) request)
                            .getSession().getServletContext());
            DataSource dataSource = (DataSource) webApplicationContext
                    .getBean("dataSource");
            SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
	        VndMgmntUser user = Utils.getUser();
	        // init UserInfos
	        if(user.getUserInfos() == null || user.getUserInfos().size() == 0){
	            List<UserInfo> userInfos = template.query(
	                    "SELECT * " +
	                    "FROM user_info ui "+
	                    "WHERE " +
	                    "ui.id = ?",
	                    ParameterizedBeanPropertyRowMapper.newInstance(UserInfo.class),
	                    user.getId());
	            for (UserInfo userInfo : userInfos) {
	                List<Authority> authorities = template.query(
	                        "SELECT * " +
	                        "FROM authorities a "+
	                        "WHERE " +
	                            "a.username = ? AND " +
	                            "a.usercompany = ?",
	                        ParameterizedBeanPropertyRowMapper.newInstance(Authority.class),
	                        userInfo.getId(), userInfo.getUserCompany());
	                userInfo.setAuthorityList(authorities);
	            }
	            user.setUserInfos(userInfos);
	        }
	        // init certificate
	        if(user.getCertificate() == null){
	            
	        }
	        session.setAttribute(username + INITED, "true");
            session.removeAttribute(username + TRY_MAX_COUNT);
	    }
		
		super.setDefaultTargetUrl(defaultTargetUrl);
		super.onAuthenticationSuccess(request, response, authentication);
		
	}
	
	public void setDefaultTargetUrl(String defaultTargetUrl) {
		this.defaultTargetUrl = defaultTargetUrl;
	}
}
