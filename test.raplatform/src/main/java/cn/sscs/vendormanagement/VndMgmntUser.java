package cn.sscs.vendormanagement;

import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import cn.sscs.vendormanagement.certificate.Certificate;

/**
 * 
 * @author kazzy
 *
 */
public class VndMgmntUser extends User {

	private static final long serialVersionUID = -7207706145586487247L;
	/**
	 * 现有DB存的id列为id， 方便开发 使id=username
	 */
	private String id = null;
	
	private String name = null;
	
	private String email = null;
	
	private boolean canDownCert = false;
	
	private int division = 0;
	        
	private String userCompany = "";
	
	private Certificate certificate = null;
	
	private List<UserInfo> userInfos = null;
	
	public VndMgmntUser(String username, String password, 
	        String name, String email, int division,
	        boolean canDownCert, String userCompany, boolean enabled,
			boolean accountNonExpired, boolean credentialsNonExpired,
			boolean accountNonLocked, Collection<GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired,
				accountNonLocked, authorities);
		this.id = username;
		this.name = name;
		this.email = email;
		this.division = division;
		this.canDownCert = canDownCert;
		this.setUserCompany(userCompany);
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getEmail() {
		return email;
	}
	
    public boolean isCanDownCert() {
        return canDownCert;
    }

    public void setCanDownCert(boolean canDownCert) {
        this.canDownCert = canDownCert;
    }

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }

    public int getUserInfosLength(){
        return userInfos.size();
    }
//    public int getUserCompanyLength(){
//        return userInfos.size();
//    }
}
