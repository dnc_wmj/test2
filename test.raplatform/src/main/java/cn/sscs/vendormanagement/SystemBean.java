package cn.sscs.vendormanagement;

public class SystemBean {
	
	/**
	 * 重置密码时间
	 */
	private int resetPswdDay;
	
	/**
	 * 错误登陆次数上限
	 */
	private int upperLimitErrorLoginTimes;
	
	public int getResetPswdDay() {
		return resetPswdDay;
	}

	public void setResetPswdDay(int resetPswdDay) {
		this.resetPswdDay = resetPswdDay;
	}

	public int getUpperLimitErrorLoginTimes() {
		return upperLimitErrorLoginTimes;
	}

	public void setUpperLimitErrorLoginTimes(int upperLimitErrorLoginTimes) {
		this.upperLimitErrorLoginTimes = upperLimitErrorLoginTimes;
	}
}
