package cn.sscs.vendormanagement.evaluation;

public class NonTradeEvaluationDetail {
	private int formId;
	private int categoryId;
	private int subCategoryId;
	private int itemsId;
	private int detailItemsId;
	private long evaluationId;
	private int reviewId;
	private int priority;
	private int yesNo;
	private String scoreYesno;
	private int point;
	private String scorePoint;
	private int topPoint;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public int getItemsId() {
		return itemsId;
	}

	public void setItemsId(int itemsId) {
		this.itemsId = itemsId;
	}

	public long getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(long evaluationId) {
		this.evaluationId = evaluationId;
	}

	public int getReviewId() {
		return reviewId;
	}

	public void setReviewId(int reviewId) {
		this.reviewId = reviewId;
	}

	public int getDetailItemsId() {
		return detailItemsId;
	}

	public void setDetailItemsId(int detailItemsId) {
		this.detailItemsId = detailItemsId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getYesNo() {
		return yesNo;
	}

	public void setYesNo(int yesNo) {
		this.yesNo = yesNo;
	}

	public String getScoreYesno() {
		return scoreYesno;
	}

	public void setScoreYesno(String scoreYesno) {
		this.scoreYesno = scoreYesno;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getScorePoint() {
		return scorePoint;
	}

	public void setScorePoint(String scorePoint) {
		this.scorePoint = scorePoint;
	}

	public int getTopPoint() {
		return topPoint;
	}

	public void setTopPoint(int topPoint) {
		this.topPoint = topPoint;
	}
}
