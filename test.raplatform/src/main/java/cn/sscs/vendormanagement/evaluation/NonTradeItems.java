package cn.sscs.vendormanagement.evaluation;

import java.util.List;

public class NonTradeItems {
	private int formId;
	private String content;
	private int categoryId;
	private int subCategoryId;
	private int itemsId;
	private List<NonTradeDetailItems> detailItemsList;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public int getItemsId() {
		return itemsId;
	}

	public void setItemsId(int itemsId) {
		this.itemsId = itemsId;
	}

	public List<NonTradeDetailItems> getDetailItemsList() {
		return detailItemsList;
	}

	public void setDetailItemsList(List<NonTradeDetailItems> detailItemsList) {
		this.detailItemsList = detailItemsList;
	}

	public int getNonTradeItemsLength() {
		return detailItemsList.size();
	}
}
