package cn.sscs.vendormanagement.evaluation;

import java.util.HashMap;
import java.util.Map;

public class FormScoreSummary {
	private int formId;
	private long evaluationId;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private int ttlWeightScore;
	private int ttlTopWeightScore;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public long getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(long evaluationId) {
		this.evaluationId = evaluationId;
	}

	public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}

	public int getTtlScore() {
		return ttlScore;
	}

	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}

	public int getTtlTopScore() {
		return ttlTopScore;
	}

	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}

	public int getTtlWeightScore() {
		return ttlWeightScore;
	}

	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}

	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}

	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}

	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("form_id", formId);
		map.put("evaluation_id", evaluationId);
		map.put("items_count", itemsCount);
		map.put("ttl_score", ttlScore);
		map.put("ttl_top_score", ttlTopScore);
		map.put("ttl_weight_score", ttlWeightScore);
		map.put("ttl_top_weight_score", ttlTopWeightScore);
		return map;
	}
}
