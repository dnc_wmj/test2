package cn.sscs.vendormanagement.evaluation;

public class EvaluationItemItem {

	private long id = -1;
	
	private String gradingStandard = null;

	private double weight = -1;

	private String points = null;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getGradingStandard() {
		return gradingStandard;
	}

	public void setGradingStandard(String gradingStandard) {
		this.gradingStandard = gradingStandard;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public String getPoints() {
		return points;
	}

	public void setPoints(String points) {
		this.points = points;
	}
	
	public String[] getPointArray() {
		return points.split(";");
	}
}
