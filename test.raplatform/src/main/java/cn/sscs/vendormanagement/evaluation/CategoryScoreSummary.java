package cn.sscs.vendormanagement.evaluation;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CategoryScoreSummary {
	private int formId;
	private int categoryId;
	private long evaluationId;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private int ttlWeightScore;
	private int ttlTopWeightScore;
	private List<ScoreSummary> scoreSummaryList = null;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public long getEvaluationId() {
		return evaluationId;
	}

	public void setEvaluationId(long evaluationId) {
		this.evaluationId = evaluationId;
	}

	public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}

	public int getTtlScore() {
		return ttlScore;
	}

	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}

	public int getTtlTopScore() {
		return ttlTopScore;
	}

	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}

	public int getTtlWeightScore() {
		return ttlWeightScore;
	}

	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}

	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}

	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}

	public void setScoreSummaryList(List<ScoreSummary> scoreSummaryList) {
		this.scoreSummaryList = scoreSummaryList;
	}

	public List<ScoreSummary> getScoreSummaryList() {
		return scoreSummaryList;
	}

	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("form_id", formId);
		map.put("category_id", categoryId);
		map.put("evaluation_id", evaluationId);
		map.put("items_count", itemsCount);
		map.put("ttl_score", ttlScore);
		map.put("ttl_top_score", ttlTopScore);
		map.put("ttl_weight_score", ttlWeightScore);
		map.put("ttl_top_weight_score", ttlTopWeightScore);
		return map;
	}
}
