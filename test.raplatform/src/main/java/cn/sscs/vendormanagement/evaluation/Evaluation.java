package cn.sscs.vendormanagement.evaluation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

/**
 * 
 * @author kazzy
 * 
 */
public class Evaluation {

	public static final int RESULT_START_WORKFLOW = 0;

	public static final int RESULT_IMPROVEMENT = 1;

	public static final int RESULT_CLOSE = 2;

	public static final int RESULT_START_WORKFLOW_WITHOUT_CEO = 3;

	public static final int STATUS_IN_APPLYING = 0; //提出

	public static final int STATUS_IN_APPLICATION = 10; //申请中 

	public static final int STATUS_IS_EFFECTIVE = 20; // 有效 
	
	public static final int STATUS_IS_TEMP_SAVE = 30; // 一时保存
	
	public static final int TYPE_NON_TRADE = 0;

	public static final int TYPE_TRADE = 1;

	public static final int STATUS_TMPSAVE = 30;

    public static final int STATUS_IN_DELETE = 40;

	private long id = -1;

	private long vendorId = -1;

	private String shortName = null;

	private String productName = null;

	private String productCategory = null;

	private int tranCond = 0;

	private String serviceCategory = null;

	private String serviceIndustry = null;

	private String serviceScope = null;

	private String majorAccounts = null;

	private String volumePerYear = null;

	private String numOfBizSitesNationwide = null;

	private String forwardingCompany = null;

	private String infoFixAssetsNationwide = null;

	private String numOfEmplNationwide = null;

	private String cooperationCondition = null;

	private String selectionReason = null;

	private String comment = null;

	private String totalPoint = "";

	private int[] point = null;
	
	private String[] remark = null;

	private Date registeredDate = null;

	private int status = STATUS_IN_APPLYING;

	private int type = -1;

	private String serviceName = null;

	private String service2Category = null;

	private String applicant = null;

	private String date2 = null;

	private int totalPointForCompare = -1;
	
	// non trade start
	private int[] formId;

	private int[] categoryId;

	private int[] subCategoryId;

	private int[] itemsId;

	private int[] detailItemsId;

	private int[] mustCheck;
	
	private boolean[] mResult;
	
	private String[] priority;

	private String[] yesNo;

	private String[] scoreYesno;

	private String[] non_point;

	private int scorePoint;

	private int topPoint = -1;
	
	private float[] weight = null;
	
	private int grandScore = 0;

	private int grandTopScore = 0;
	
	private boolean afterValidte = false;
	//non trade end
	
	private String vendorCode = "";
	
	public float[] getWeight() {
		return weight;
	}

	public void setWeight(float[] weight) {
		this.weight = weight;
	}
	public String[] getScoreYesno() {
		return scoreYesno;
	}

	public void setScoreYesno(String[] scoreYesno) {
		this.scoreYesno = scoreYesno;
	}

	public int getScorePoint() {
		return scorePoint;
	}

	public void setScorePoint(int scorePoint) {
		this.scorePoint = scorePoint;
	}

	public int getTopPoint() {
		return topPoint;
	}

	public void setTopPoint(int topPoint) {
		this.topPoint = topPoint;
	}

	public int getGrandScore() {
		return grandScore;
	}

	public void setGrandScore(int grandScore) {
		this.grandScore = grandScore;
	}

	public int getGrandTopScore() {
		return grandTopScore;
	}

	public void setGrandTopScore(int grandTopScore) {
		this.grandTopScore = grandTopScore;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public String[] getPriority() {
		return priority;
	}

	public void setPriority(String[] priority) {
		this.priority = priority;
	}

	public String[] getYesNo() {
		return yesNo;
	}

	public void setYesNo(String[] yesNo) {
		this.yesNo = yesNo;
	}

	public String[] getNon_point() {
		return non_point;
	}

	public void setNon_point(String[] non_point) {
		this.non_point = non_point;
	}

	public void setTotalPointForCompare(int totalPointForCompare) {
		this.totalPointForCompare = totalPointForCompare;
	}

	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public long getVendorId() {
		return vendorId;
	}

	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName
	 *            the shortName to set
	 */
	public void setShortName(String shortName) {
		if ("".equals(shortName)) {
			return;
		}
		this.shortName = shortName;
	}

	/**
	 * @return the productName
	 */
	public String getProductName() {
		return productName;
	}

	/**
	 * @param productName
	 *            the productName to set
	 */
	public void setProductName(String productName) {
		if ("".equals(productName)) {
			return;
		}

		this.productName = productName;
	}

	/**
	 * @return the productCategory
	 */
	public String getProductCategory() {
		return productCategory;
	}

	/**
	 * @param productCategory
	 *            the productCategory to set
	 */
	public void setProductCategory(String productCategory) {
		if ("".equals(productCategory)) {
			return;
		}

		this.productCategory = productCategory;
	}

	/**
	 * @return the tranCond
	 */
	public int getTranCond() {
		return tranCond;
	}

	/**
	 * @param tranCond
	 *            the tranCond to set
	 */
	public void setTranCond(int tranCond) {
		this.tranCond = tranCond;
	}

	/**
	 * @return the serviceCategory
	 */
	public String getServiceCategory() {
		return serviceCategory;
	}

	/**
	 * @param serviceCategory
	 *            the serviceCategory to set
	 */
	public void setServiceCategory(String serviceCategory) {
		if ("".equals(serviceCategory)) {
			return;
		}
		this.serviceCategory = serviceCategory;
	}

	/**
	 * @return the serviceIndustry
	 */
	public String getServiceIndustry() {
		return serviceIndustry;
	}

	/**
	 * @param serviceIndustry
	 *            the serviceIndustry to set
	 */
	public void setServiceIndustry(String serviceIndustry) {
		if ("".equals(serviceIndustry)) {
			return;
		}
		this.serviceIndustry = serviceIndustry;
	}

	/**
	 * @return the serviceScope
	 */
	public String getServiceScope() {
		return serviceScope;
	}

	/**
	 * @param serviceScope
	 *            the serviceScope to set
	 */
	public void setServiceScope(String serviceScope) {
		if ("".equals(serviceScope)) {
			return;
		}
		this.serviceScope = serviceScope;
	}

	/**
	 * @return the majorAccounts
	 */
	public String getMajorAccounts() {
		return majorAccounts;
	}

	/**
	 * @param majorAccounts
	 *            the majorAccounts to set
	 */
	public void setMajorAccounts(String majorAccounts) {
		if ("".equals(majorAccounts)) {
			return;
		}
		this.majorAccounts = majorAccounts;
	}

	/**
	 * @return the volumePerYear
	 */
	public String getVolumePerYear() {
		return volumePerYear;
	}

	/**
	 * @param volumePerYear
	 *            the volumePerYear to set
	 */
	public void setVolumePerYear(String volumePerYear) {
		if ("".equals(volumePerYear)) {
			return;
		}
		this.volumePerYear = volumePerYear;
	}

	/**
	 * @return the numOfBizSitesNationwide
	 */
	public String getNumOfBizSitesNationwide() {
		return numOfBizSitesNationwide;
	}

	/**
	 * @param numOfBizSitesNationwide
	 *            the numOfBizSitesNationwide to set
	 */
	public void setNumOfBizSitesNationwide(String numOfBizSitesNationwide) {
		if ("".equals(numOfBizSitesNationwide)) {
			return;
		}
		this.numOfBizSitesNationwide = numOfBizSitesNationwide;
	}

	/**
	 * @return the forwardingCompany
	 */
	public String getForwardingCompany() {
		return forwardingCompany;
	}

	/**
	 * @param forwardingCompany
	 *            the forwardingCompany to set
	 */
	public void setForwardingCompany(String forwardingCompany) {
		if ("".equals(forwardingCompany)) {
			return;
		}
		this.forwardingCompany = forwardingCompany;
	}

	/**
	 * @return the infoFixAssetsNationwide
	 */
	public String getInfoFixAssetsNationwide() {
		return infoFixAssetsNationwide;
	}

	/**
	 * @param infoFixAssetsNationwide
	 *            the infoFixAssetsNationwide to set
	 */
	public void setInfoFixAssetsNationwide(String infoFixAssetsNationwide) {
		if ("".equals(infoFixAssetsNationwide)) {
			return;
		}
		this.infoFixAssetsNationwide = infoFixAssetsNationwide;
	}

	/**
	 * @return the numOfEmplNationwide
	 */
	public String getNumOfEmplNationwide() {
		return numOfEmplNationwide;
	}

	/**
	 * @param numOfEmplNationwide
	 *            the numOfEmplNationwide to set
	 */
	public void setNumOfEmplNationwide(String numOfEmplNationwide) {
		if ("".equals(numOfEmplNationwide)) {
			return;
		}
		this.numOfEmplNationwide = numOfEmplNationwide;
	}

	/**
	 * @return the cooperationCondition
	 */
	public String getCooperationCondition() {
		return cooperationCondition;
	}

	/**
	 * @param cooperationCondition
	 *            the cooperationCondition to set
	 */
	public void setCooperationCondition(String cooperationCondition) {
		if ("".equals(selectionReason)) {
			return;
		}
		this.cooperationCondition = cooperationCondition;
	}

	/**
	 * @return the selectionReason
	 */
	public String getSelectionReason() {
		return selectionReason;
	}

	/**
	 * @param selectionReason
	 *            the selectionReason to set
	 */
	public void setSelectionReason(String selectionReason) {
		if ("".equals(selectionReason)) {
			return;
		}

		this.selectionReason = selectionReason;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment
	 *            the comment to set
	 */
	public void setComment(String comment) {
		if ("".equals(comment)) {
			return;
		}

		this.comment = comment;
	}

	/**
	 * 
	 * @return
	 */
	public String getTotalPoint() {
		return totalPoint;
	}

	/**
	 * 
	 * @param totalPoint
	 */
	public void setTotalPoint(String totalPoint) {
		if (totalPoint.indexOf(".") == -1) {
			this.totalPoint = totalPoint;
		} else {
			int i, len = totalPoint.length();
			for (i = 0; i < len; i++) {
				// 倒数第二位有值时，不舍0
				if (i == 0 && len >= 2 && totalPoint.charAt(len - 2) != '0') {
					break;
				}
				if (totalPoint.charAt(len - 1 - i) != '0') {
					if (totalPoint.charAt(len - i - 1) == '.') {
						totalPoint = totalPoint.substring(0, len - i - 1);
					}
					break;
				} else {
					totalPoint = totalPoint.substring(0, len - i - 1);
				}
			}
			this.totalPoint = totalPoint;
		}
		setTotalPointForCompare();
	}

	/**
	 * 
	 * @return
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * 
	 * @param status
	 */
	public void setStatus(int status) {
		this.status = status;
	}

	/**
	 * 
	 * @return
	 */
	public int getBusinessType() {
		return type;
	}

	/**
	 * 
	 * @param type
	 */
	public void setBusinessType(int type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public Date getRegisteredDate() {
		return registeredDate;
	}

	/**
	 * 
	 * @param registeredDate
	 */
	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	/**
	 * @return the point
	 */
	public int[] getPoint() {
		return point;
	}

	/**
	 * @param point
	 *            the point to set
	 */
	public void setPoint(int[] point) {
		this.point = point;
	}

	/**
	 * 
	 * @return
	 */
	public String[] getRemark() {
		return remark;
	}

	/**
	 * 
	 * @param remark
	 */
	public void setRemark(String[] remark) {
		this.remark = remark;
	}

	/**
	 * 
	 * @return
	 */
	public int getResult() {
		// new BigDecimal(totalPoint).compareTo(new BigDecimal(90)) > -1
		if (totalPointForCompare >= 90) {
			return RESULT_START_WORKFLOW_WITHOUT_CEO;
			// new BigDecimal(totalPoint).compareTo(new BigDecimal(70)) > -1
		} else if (totalPointForCompare >= 70) {
			return RESULT_START_WORKFLOW;
			// new BigDecimal(totalPoint).compareTo(new BigDecimal(50)) > -1
		} else if (totalPointForCompare >= 50) {
			return RESULT_IMPROVEMENT;
		}
		return RESULT_CLOSE;
	}

	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("vendor_id", vendorId);
		map.put("company_short_name", shortName);
		map.put("product_name", productName);
		map.put("product_category", productCategory);
		map.put("selection_reason", selectionReason);
		map.put("transaction_condition", tranCond);
		map.put("comment", comment);
		map.put("total_point", totalPoint);
		map.put("status", status);
		map.put("business_type", type);

		map.put("service_category", serviceCategory);
		map.put("service_industry", serviceIndustry);
		map.put("service_scope", serviceScope);
		map.put("major_accounts", majorAccounts);
		map.put("volume_per_year", volumePerYear);
		map.put("num_of_biz_sites_nationwide", numOfBizSitesNationwide);
		map.put("forwarding_company", forwardingCompany);
		map.put("info_fix_assets_nationwide", infoFixAssetsNationwide);
		map.put("num_of_empl_nationwide", numOfEmplNationwide);
		map.put("cooperation_condition", cooperationCondition);
		map.put("serviceName", serviceName);
		map.put("service2Category", service2Category);
		map.put("applicant", applicant);
		map.put("date2", date2);
		return map;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getService2Category() {
		return service2Category;
	}

	public void setService2Category(String service2Category) {
		this.service2Category = service2Category;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getDate2() {
		return date2;
	}

	public void setDate2(String date2) {
		this.date2 = date2;
	}

	public void setTotalPointForCompare() {
		if (totalPoint.indexOf(".") == -1) {
			this.totalPointForCompare = Integer.parseInt(totalPoint);
		} else {
			this.totalPointForCompare = Integer.parseInt(totalPoint
					.split("\\.")[0]);
		}
	}

	public int getTotalPointForCompare() {
		return totalPointForCompare;
	}

	public int[] getFormId() {
		return formId;
	}

	public void setFormId(int[] formId) {
		this.formId = formId;
	}

	public int[] getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int[] categoryId) {
		this.categoryId = categoryId;
	}

	public int[] getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int[] subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public int[] getItemsId() {
		return itemsId;
	}

	public void setItemsId(int[] itemsId) {
		this.itemsId = itemsId;
	}

	public int[] getDetailItemsId() {
		return detailItemsId;
	}

	public void setDetailItemsId(int[] detailItemsId) {
		this.detailItemsId = detailItemsId;
	}

	public void setMustCheck(int[] mustCheck) {
		this.mustCheck = mustCheck;
	}

	public int[] getMustCheck() {
		return mustCheck;
	}

	public void setmResult(boolean[] mResult) {
		this.mResult = mResult;
	}

	public boolean[] getmResult() {
		return mResult;
	}

	public void setAfterValidte(boolean afterValidte) {
		this.afterValidte = afterValidte;
	}

	public boolean isAfterValidte() {
		return afterValidte;
	}
	public String getShanghaiRegisteredDate(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		return sdf.format(registeredDate);
	}

	public String getVendorCode() {
		return vendorCode;
	}

	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
}
