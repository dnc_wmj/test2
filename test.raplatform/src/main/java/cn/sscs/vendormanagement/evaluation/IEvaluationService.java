package cn.sscs.vendormanagement.evaluation;

import java.util.List;
import java.util.Map;

import cn.sscs.vendormanagement.User;

public interface IEvaluationService {

	/**
	 * 
	 * @param kind
	 * @return
	 */
	List<EvaluationCategory> getEvaluationCategories(int kind);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	List<EvaluationHistory> getEvaluationHistories(long vendorId);

	/**
	 * 
	 * @param kind
	 * @param points
	 * @return
	 */
	String calculatePoint(int kind, int[] points);

	/**
	 * 
	 * @param evaluation
	 * @return
	 */
	long store(Evaluation evaluation);

	/**
	 * 
	 * @param division
	 * @return
	 */
	List<User> findL1Users(int division, String supercompany);

	/**
	 * 
	 * @param division
	 * @return
	 */
	List<User> findL2Users(int division, String supercompany);

	/**
	 * 
	 * @return
	 */
	List<User> findHelpDeskUsers();

	/**
	 * 
	 * @return
	 */
	List<User> findFinancialManagers();

	/**
	 * 
	 * @return
	 */
	List<User> findDataMaintainers();

	/**
	 * 
	 * @param id
	 * @param status
	 * @return
	 */
	Evaluation findEvaluationById(long id, int status);


	/**
	 * 
	 * @Title:       findEvaluationById 
	 * @Description: 多状态查询评估信息
	 * @param        @param id
	 * @param        @param status
	 * @param        @return     
	 * @return       Evaluation     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	Evaluation findEvaluationById(long id, List<Integer> status);
	/**
	 * 
	 * @param evalId
	 * @return
	 */
	boolean hasEvaluationById(long evalId);

	/**
	 * 
	 * @param evaluation
	 */
	void updateEvaluation(Evaluation evaluation);

	/**
	 * 
	 * @return
	 */
	List<User> findCeo();

	/**
	 * 
	 * @param status
	 * @param id
	 */
	void updateStatus(int status, long id);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	Evaluation findLatestEvaluationByVendorId(long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	boolean hasLatestEvaluationByVendorId(long vendorId);

	/**
	 * 
	 * @param oldVendorId
	 * @param newVendorId
	 */
	void updateVendorIdOfEvaluation(long oldVendorId, long newVendorId);

	/**
	 * 
	 * @param id
	 * @return Evaluation
	 */
	Evaluation findEvaluationById(long id);

	/**
	 * 
	 * @param attachment
	 * @return
	 */
	long storeAttachment(Map<String, Object> attachment);

	void deleteAttachmentData(long id, String tranId);

	// List<NonTradeEvaluationForm> getNonTradeEvaluationForm(int businessType,
	// int reviewFlag);

//	int calculateGrandScore(String[] nonPoint, String[] yesNo, String[] priority);

//	int calculateTopGrandScore(String[] nonPoint, String[] yesNo, String[] priority);

	void storeNonTradeEvaluationResultDetail(Evaluation evaluation);

	List<NonTradeEvaluationForm> getNonTradeEvaluationForm(int businessType,
			int i, long evaluationId);

	void updateNonTradeEvaluationResultDetail(Evaluation evaluation);

	List<EvaluationHistory> getNonTradeEvaluationHistories(long vendorId);

	Long findFirstEvalId(long vendorId, int statusIsEffective);

	Evaluation getEvaluationById(long id);

	String getLatestEval(long vendorId);
	
	/**
	 * 
	 * @Title:       hasEvals 
	 * @Description: (  获取供应商的有效评估信息数量  ) 
	 * @param        @param vendorId
	 * @param        @return     
	 * @return       int     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 12, 2012
	 */
	int hasEvals(long vendorId,int status);

    List<String> getCostcenterBySuperCompany(String supercompany);

}
