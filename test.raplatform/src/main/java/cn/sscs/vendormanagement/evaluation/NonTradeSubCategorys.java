package cn.sscs.vendormanagement.evaluation;

import java.math.BigDecimal;
import java.util.List;

public class NonTradeSubCategorys {
	private int formId;
	private String content;
	private int categoryId;
	private int subCategoryId;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private double weight;
	private double normalizedWeight;
	private int ttlWeightScore;
	private int ttlTopWeightScore;
	private List<NonTradeItems> nonTradeItems;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public List<NonTradeItems> getNonTradeItems() {
		return nonTradeItems;
	}

	public void setNonTradeItems(List<NonTradeItems> nonTradeItems) {
		this.nonTradeItems = nonTradeItems;
	}

	public int getSubCategorysLength() {
		int length = 0;
		for (NonTradeItems items : nonTradeItems) {
			length += items.getNonTradeItemsLength();
		}
		return length;
	}

	public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}

	public int getTtlScore() {
		return ttlScore;
	}

	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}

	public int getTtlTopScore() {
		return ttlTopScore;
	}

	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}

	public int getTtlWeightScore() {
		return ttlWeightScore;
	}

	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}

	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}

	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}

	public double getWeight() {
		return weight;
	}
	
	public String getShowWeight() {
		BigDecimal bd = new BigDecimal(weight * 100); 
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
		return bd.intValue() +"%";
	}
	
	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getNormalizedWeight() {
		return normalizedWeight;
	}
	
	public String getShowNormalizedWeight() {
		BigDecimal bd = new BigDecimal(normalizedWeight * 100); 
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
		return bd.intValue() +"%";
	}
	public void setNormalizedWeight(double normalizedWeight) {
		this.normalizedWeight = normalizedWeight;
	}
	
}
