package cn.sscs.vendormanagement.evaluation;

import java.util.HashMap;
import java.util.Map;

public class ScoreSummary {
	private int formId;
	private int categoryId;
	private int subCategoryId;
	private long evaluationId;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private double weight;
	private double normalizedWeight;
	private int ttlWeightScore;
	private int ttlTopWeightScore;
	private double basicWeight;
	
	public int getFormId() {
		return formId;
	}
	public void setFormId(int formId) {
		this.formId = formId;
	}
	public int getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}
	public int getSubCategoryId() {
		return subCategoryId;
	}
	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}
	public long getEvaluationId() {
		return evaluationId;
	}
	public void setEvaluationId(long evaluationId) {
		this.evaluationId = evaluationId;
	}
	public int getItemsCount() {
		return itemsCount;
	}
	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}
	public int getTtlScore() {
		return ttlScore;
	}
	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}
	public int getTtlTopScore() {
		return ttlTopScore;
	}
	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}
	public double getWeight() {
		return weight;
	}
	public void setWeight(double weight) {
		this.weight = weight;
	}
	public double getNormalizedWeight() {
		return normalizedWeight;
	}
	public void setNormalizedWeight(double normalizedWeight) {
		this.normalizedWeight = normalizedWeight;
	}
	public int getTtlWeightScore() {
		return ttlWeightScore;
	}
	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}
	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}
	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}
	public void setBasicWeight(double basicWeight) {
		this.basicWeight = basicWeight;
	}
	public double getBasicWeight() {
		return basicWeight;
	}
	public Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("form_id", formId);
		map.put("category_id", categoryId);
		map.put("sub_category_id", subCategoryId);
		map.put("evaluation_id", evaluationId);
		map.put("items_count", itemsCount);
		map.put("ttl_score", ttlScore);
		map.put("ttl_top_score", ttlTopScore);
		map.put("weight", weight);
		map.put("normalized_weight", normalizedWeight);
		map.put("ttl_weight_score", ttlWeightScore);
		map.put("ttl_top_weight_score", ttlTopWeightScore);
		return map;
	}
	
}
