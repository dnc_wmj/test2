package cn.sscs.vendormanagement.evaluation;

import java.util.List;

public class EvaluationItem {

	private long id = -1;
	
	private String evaluationStandard = null;
	
	private String gradingStandard = null;
	
	private double weight = -1;
	
	private String points = null;
	
	private List<EvaluationItemItem> evaluationItemItems = null;
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getEvaluationStandard() {
		return evaluationStandard;
	}
	
	/**
	 * 
	 * @param evaluationStandard
	 */
	public void setEvaluationStandard(String evaluationStandard) {
		this.evaluationStandard = evaluationStandard;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getGradingStandard() {
		return gradingStandard;
	}
	
	/**
	 * 
	 * @param gradingStandard
	 */
	public void setGradingStandard(String gradingStandard) {
		this.gradingStandard = gradingStandard;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWeight() {
		return weight;
	}
	
	/**
	 * 
	 * @param weight
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	/**
	 * 
	 * @param points
	 */
	public void setPoints(String points) {
		this.points = points;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPoints() {
		return points;
	}
	
	public String[] getPointArray() {
		return points.split(";");
	}

	public void setEvaluationItemItems(List<EvaluationItemItem> evaluationItemItems) {
		this.evaluationItemItems = evaluationItemItems;
	}

	public List<EvaluationItemItem> getEvaluationItemItems() {
		return evaluationItemItems;
	}
	public int getEvaluationItemItemLength() {
		return evaluationItemItems.size();
	}
}
