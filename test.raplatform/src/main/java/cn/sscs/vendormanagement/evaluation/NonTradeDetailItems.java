package cn.sscs.vendormanagement.evaluation;

public class NonTradeDetailItems {
	private int formId;
	private String content;
	private int categoryId;
	private int subCategoryId;
	private int itemsId;
	private int mustCheck;
	private int detailItemsId;
	
	//result
	private int priority;
	private int yesNo;
	private String scoreYesno;
	private int point;
	private String scorePoint;
	private int topPoint;
	
	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public int getSubCategoryId() {
		return subCategoryId;
	}

	public void setSubCategoryId(int subCategoryId) {
		this.subCategoryId = subCategoryId;
	}

	public int getItemsId() {
		return itemsId;
	}

	public void setItemsId(int itemsId) {
		this.itemsId = itemsId;
	}

	public void setMustCheck(int mustCheck) {
		this.mustCheck = mustCheck;
	}

	public int getMustCheck() {
		return mustCheck;
	}

	public void setDetailItemsId(int detailItemsId) {
		this.detailItemsId = detailItemsId;
	}

	public int getDetailItemsId() {
		return detailItemsId;
	}

	public int getPriority() {
		return priority;
	}

	public void setPriority(int priority) {
		this.priority = priority;
	}

	public int getYesNo() {
		return yesNo;
	}

	public void setYesNo(int yesNo) {
		this.yesNo = yesNo;
	}

	public String getScoreYesno() {
		return scoreYesno;
	}

	public void setScoreYesno(String scoreYesno) {
		this.scoreYesno = scoreYesno;
	}

	public int getPoint() {
		return point;
	}

	public void setPoint(int point) {
		this.point = point;
	}

	public String getScorePoint() {
		return scorePoint;
	}

	public void setScorePoint(String scorePoint) {
		this.scorePoint = scorePoint;
	}

	public int getTopPoint() {
		return topPoint;
	}

	public void setTopPoint(int topPoint) {
		this.topPoint = topPoint;
	}
	
}
