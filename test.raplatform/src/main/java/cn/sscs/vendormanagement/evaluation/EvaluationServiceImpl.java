package cn.sscs.vendormanagement.evaluation;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.sscs.vendormanagement.User;
import cn.sscs.vendormanagement.Utils;


/**
 * 
 * @author kazzy
 *
 */
@Service("evaluationService")
@Transactional
public class EvaluationServiceImpl implements IEvaluationService {

	private SimpleJdbcTemplate jdbcTemplate = null;
	
	private SimpleJdbcInsert insertEvaluation = null;
	
	private SimpleJdbcInsert insertEvaluationPoint = null;
	
	private SimpleJdbcInsert insertAttachment = null;
	
	@SuppressWarnings("unused")
	private SimpleJdbcInsert insertTmpAttachment = null;
	
	private SimpleJdbcInsert insertNonTradeEvaluationDetail = null;
	
	private SimpleJdbcInsert insertScoreSummary = null;
	
	private SimpleJdbcInsert insertCategoryScoreSummary = null;
	
	private SimpleJdbcInsert insertFormScoreSummary = null;
	
	/**
	 * 
	 * @param dataSource
	 */
	@Autowired
	public void init(DataSource dataSource) {
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
		
		insertEvaluation = new SimpleJdbcInsert(dataSource)
			.withTableName("evaluation")
			.usingGeneratedKeyColumns("id");
		
		insertNonTradeEvaluationDetail = new SimpleJdbcInsert(dataSource).withTableName("non_trade_evaluation_detail");
		
		insertEvaluationPoint = new SimpleJdbcInsert(dataSource).withTableName("evaluation_points");
		
		insertAttachment = new SimpleJdbcInsert(dataSource).withTableName("attachment_workflow").usingGeneratedKeyColumns("id");

		insertTmpAttachment = new SimpleJdbcInsert(dataSource).withTableName("tmp_attachment");
		
		insertScoreSummary = new SimpleJdbcInsert(dataSource).withTableName("score_summary");
		
		insertCategoryScoreSummary = new SimpleJdbcInsert(dataSource).withTableName("category_score_summary");
		
		insertFormScoreSummary = new SimpleJdbcInsert(dataSource).withTableName("form_score_summary");
	
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: getEvaluationCategories</p> 
	 * <p>Description: 贸易\共通    准备评估页面评估信息</p> 
	 * @param kind
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#getEvaluationCategories(int)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<EvaluationCategory> getEvaluationCategories(int kind) {
		List<EvaluationCategory> evaluationCategories = 
			jdbcTemplate.query(
				"SELECT ec.id, ecn.name, ec.max_point as maxPoint, weight " +
				"FROM evaluation_category ec, evaluation_category_name ecn " +
				"WHERE ec.id = ecn.category_id AND ec.kind = ? AND ecn.locale = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(EvaluationCategory.class),
				kind, LocaleContextHolder.getLocale().getLanguage());
		
		for (EvaluationCategory category : evaluationCategories) {
			category.setEvaluationContext(getEvaluationContents(category.getId(), kind));
		}
		
		return evaluationCategories;
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: getNonTradeEvaluationForm</p> 
	 * <p>Description: 非贸  准备评估页面评估信息  </p> 
	 * @param businessType
	 * @param reviewFlag
	 * @param evaluationId
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#getNonTradeEvaluationForm(int, int, long)
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<NonTradeEvaluationForm> getNonTradeEvaluationForm(int businessType, int reviewFlag, long evaluationId) {
		String local = LocaleContextHolder.getLocale().getLanguage();
		String sql;
		List<NonTradeEvaluationForm> nonTradeEvaluationForm;
		if(evaluationId > 0){
			sql =
			"SELECT " +
				"f.form_id as formId, " +
				"f.content, " +
				"f.weight, " +
				"s.items_count, " +
				"s.ttl_score, " +
				"s.ttl_top_score, " +
				"s.ttl_weight_score, " +
				"s.ttl_top_weight_score "+
			"FROM " +
				"non_trade_evaluation_form f, " +
				"form_score_summary s "+
			"WHERE " +
				"f.form_id = s.form_id  AND " +
				"f.business_type = ? AND " +
				"f.locale = ? AND " +
				"f.review_flag = ? AND " +
				"s.evaluation_id = ? "+
			"ORDER BY display_order";
			nonTradeEvaluationForm = jdbcTemplate.query(
					sql, 
					ParameterizedBeanPropertyRowMapper.newInstance(NonTradeEvaluationForm.class),
					businessType, local, reviewFlag, evaluationId);
		}else{
			sql =
				"SELECT " +
				"form_id as formId, " +
				"content, " +
				"weight " +
			"FROM " +
				"non_trade_evaluation_form " +
			"WHERE " +
				"business_type = ? AND " +
				"locale = ? AND " +
				"review_flag = ? " +
			"ORDER BY display_order";
			nonTradeEvaluationForm = jdbcTemplate.query(
					sql, 
					ParameterizedBeanPropertyRowMapper.newInstance(NonTradeEvaluationForm.class),
					businessType, local, reviewFlag);
		}
		for (NonTradeEvaluationForm form : nonTradeEvaluationForm) {
			form.setCategoryList(getNonTradeEvaluationCategorys(form.getFormId(), businessType, local, reviewFlag, evaluationId));
		}
			
		return nonTradeEvaluationForm;
	}
	/**
	 * 
	 * @param categoryId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<EvaluationContent> getEvaluationContents(long categoryId, int kind) {
		List<EvaluationContent> evaluationContents =
			jdbcTemplate.query(
				"SELECT ec.id as id, content " +
				"FROM evaluation_content ec, evaluation_content_content ecc " +
				"WHERE ec.id = ecc.content_id AND ec.category_id = ? AND ecc.locale = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(EvaluationContent.class),
				categoryId, LocaleContextHolder.getLocale().getLanguage());
	
		for (EvaluationContent evaluationContent : evaluationContents) {
			evaluationContent.setEvaluationItems(getEvaluationItems(evaluationContent.getId(), kind));
		}
		
		return evaluationContents;
	}
	
	@SuppressWarnings("deprecation")
	private List<NonTradeEvaluationCategory> getNonTradeEvaluationCategorys(int formId, int businessType, String local, int reviewFlag, long evaluationId){
		String sql;
		List<NonTradeEvaluationCategory> nonTradeEvaluationCategory;
		if(evaluationId > 0){
			sql = 
				"SELECT " +
					"c.form_id as formId, " +
					"c.content, " +
					"c.category_id as categoryId, " +
					"s.items_count as itemsCount, " +
					"s.ttl_score as ttlScore, " +
					"s.ttl_top_score as ttlTopScore, " +
					"s.ttl_weight_score as ttlWeightScore, " +
					"s.ttl_top_weight_score as ttlTopWeightScore " +
				"FROM " +
					"non_trade_category c, " +
					"category_score_summary s "+
				"WHERE " +
					"c.form_id = s.form_id AND " +
					"c.category_id = s.category_id AND " +
					"c.form_id = ? AND " +
					"c.locale = ?  AND " +
					"c.review_flag = ? AND " +
					"s.evaluation_id = ? " +
				"ORDER BY display_order";
			nonTradeEvaluationCategory = 
				jdbcTemplate.query(
					sql, 
					ParameterizedBeanPropertyRowMapper.newInstance(NonTradeEvaluationCategory.class),
					formId, local, reviewFlag, evaluationId);
		}else{
			sql = 
				"SELECT " +
					"form_id as formId, " +
					"content, category_id as categoryId " +
				"FROM " +
					"non_trade_category " +
				"WHERE " +
					"form_id = ? AND " +
					"locale = ?  AND " +
					"review_flag = ? "+
				"ORDER BY display_order";
			nonTradeEvaluationCategory = 
				jdbcTemplate.query(
					sql, 
					ParameterizedBeanPropertyRowMapper.newInstance(NonTradeEvaluationCategory.class),
					formId, local, reviewFlag);
		}
		for (NonTradeEvaluationCategory category : nonTradeEvaluationCategory) {
			category.setSubCategorysList(getNonTradeSubCategorys(formId, category.getCategoryId(), businessType, local, reviewFlag, evaluationId));
		}
		return nonTradeEvaluationCategory;
	}
	
	@SuppressWarnings("deprecation")
	private List<NonTradeSubCategorys> getNonTradeSubCategorys(int formId,
			int categoryId, int businessType, String local, int reviewFlag, long evaluationId) {
		String sql;
		List<NonTradeSubCategorys> nonTradeSubCategorys;
		if(evaluationId > 0){
			sql = 
				"SELECT " +
					"sc.form_id as formId, " +
					"sc.content, " +
					"sc.category_id as categoryId, " +
					"sc.sub_category_id as subCategoryId, " +
					"s.items_count as itemsCount, " +
					"s.ttl_score as ttlScore, " +
					"s.ttl_top_score as ttlTopScore, " +
					"s.weight, " +
					"s.normalized_weight as normalizedWeight, " +
					"s.ttl_weight_score as ttlWeightScore, " +
					"s.ttl_top_weight_score as ttlTopWeightScore "+ 
				"FROM " +
					"non_trade_sub_category sc, " +
					"score_summary s "+
				"WHERE " +
					"sc.form_id = s.form_id AND " +
					"sc.category_id = s.category_id AND " +
					"sc.sub_category_id = s.sub_category_id AND " +
					"sc.form_id = ? AND " +
					"sc.category_id = ? AND " +
					"sc.locale = ? AND " +
					"sc.review_flag = ? AND " +
					"s.evaluation_id =? "+
				"ORDER BY display_order";
			nonTradeSubCategorys =
				jdbcTemplate.query(
						sql, 
						ParameterizedBeanPropertyRowMapper.newInstance(NonTradeSubCategorys.class),
						formId, categoryId, local, reviewFlag, evaluationId);
		}else{
			sql = 
				"SELECT " +
					"form_id as formId, " +
					"content, " +
					"category_id as categoryId, " +
					"sub_category_id as subCategoryId " +
				"FROM " +
					"non_trade_sub_category " +
				"WHERE " +
					"form_id = ? AND " +
					"category_id = ? AND " +
					"locale = ? AND " +
					"review_flag = ? "+
				"ORDER BY display_order";
			nonTradeSubCategorys =
				jdbcTemplate.query(
						sql, 
						ParameterizedBeanPropertyRowMapper.newInstance(NonTradeSubCategorys.class),
						formId, categoryId, local, reviewFlag);
		}
		for (NonTradeSubCategorys subCategory : nonTradeSubCategorys) {
			subCategory.setNonTradeItems(getNonTradeItems(formId, subCategory.getSubCategoryId(), businessType, local, categoryId, reviewFlag, evaluationId));
		}
		return nonTradeSubCategorys;
	}
	@SuppressWarnings("deprecation")
	private List<NonTradeItems> getNonTradeItems(int formId, int subCategoryId,
			int businessType, String local, int categoryId, int reviewFlag, long evaluationId) {
		List<NonTradeItems> nonTradeItems =
			jdbcTemplate.query(
					"SELECT form_id as formId, content, category_id as categoryId, sub_category_id as subCategoryId, items_id as itemsId " +
					"FROM non_trade_items " +
					"WHERE form_id = ? AND category_id = ? AND locale = ? AND sub_category_id = ? AND review_flag = ? "+
					"ORDER BY display_order", 
					ParameterizedBeanPropertyRowMapper.newInstance(NonTradeItems.class),
					formId, categoryId, local, subCategoryId, reviewFlag);
		for (NonTradeItems item : nonTradeItems) {
			item.setDetailItemsList(getNonTradeDetailItems(formId, item.getSubCategoryId(), businessType, local, categoryId, item.getItemsId(), reviewFlag, evaluationId));
		}
		return nonTradeItems;
	}
	
	@SuppressWarnings("deprecation")
	private List<NonTradeDetailItems> getNonTradeDetailItems(int formId, int subCategoryId,
			int businessType, String local, int categoryId, int itemsId, int reviewFlag, long evaluationId) {
		if(evaluationId > 0){
			List<NonTradeDetailItems> nonTradeDetailItems =
				jdbcTemplate.query(
						"SELECT " +
							"i.form_id as formId, " +
							"i.content, " +
							"i.category_id as categoryId, " +
							"i.sub_category_id as subCategoryId, " +
							"i.items_id as itemsId, " +
							"i.detail_items_id as detailItemsId, " +
							"i.must_check as mustCheck, "+ 
						    "r.priority as priority, " +
						    "r.yes_no as yesNo, " +
						    "r.score_yesno as scoreYesno, " +
						    "r.point as point, " +
						    "r.score_point as scorePoint, " +
						    "r.top_score as topPoint "+
						"FROM " +
							"non_trade_detail_items i, " +
							"non_trade_evaluation_detail r "+
						"WHERE " +
						    "i.form_id = r.form_id and " +
						    "i.category_id = r.category_id and " +
						    "i.sub_category_id = r.sub_category_id and " +
						    "i.items_id = r.items_id and " +
						    "i.detail_items_id = r.detail_items_id  and " +
						    "i.form_id = ? AND " +
						    "i.category_id = ? AND " +
						    "i.locale = ? AND " +
						    "i.sub_category_id = ? AND " +
						    "i.items_id = ? AND " +
						    "i.review_flag = ? AND " +
						    "r.evaluation_id =? " +
						"ORDER BY display_order", 
						ParameterizedBeanPropertyRowMapper.newInstance(NonTradeDetailItems.class),
						formId, categoryId, local, subCategoryId, itemsId, reviewFlag, evaluationId);
			return nonTradeDetailItems;
		}else{
			List<NonTradeDetailItems> nonTradeDetailItems =
				jdbcTemplate.query(
						"SELECT " +
							"form_id as formId, " +
							"content, " +
							"category_id as categoryId, " +
							"sub_category_id as subCategoryId, " +
							"items_id as itemsId, " +
							"detail_items_id as detailItemsId, " +
							"must_check as mustCheck "+
						"FROM " +
							"non_trade_detail_items " +
						"WHERE " +
							"form_id = ? AND " +
							"category_id = ? AND " +
							"locale = ? AND " +
							"sub_category_id = ? AND " +
							"items_id = ? AND " +
							"review_flag = ? "+
						"ORDER BY display_order", 
						ParameterizedBeanPropertyRowMapper.newInstance(NonTradeDetailItems.class),
						formId, categoryId, local, subCategoryId, itemsId, reviewFlag);
			return nonTradeDetailItems;
		}
		
	}

	/**
	 * 
	 * @param contentId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<EvaluationItem> getEvaluationItems(long contentId, int kind) {	
		
		if(kind >=3 && kind <=9){
			List<EvaluationItem> evaluationContents =
				jdbcTemplate.query(
					"SELECT ei.id, eies.content as evaluationStandard " +
					"FROM evaluation_item ei, evaluation_item_evaluation_standard eies " +
					"WHERE ei.id = eies.item_id " +
					"  AND ei.content_id = ? " +
					"  AND eies.locale = ?", 
					ParameterizedBeanPropertyRowMapper.newInstance(EvaluationItem.class),
					contentId, 
					LocaleContextHolder.getLocale().getLanguage());
			for (EvaluationItem evaluationItem : evaluationContents) {
				evaluationItem.setEvaluationItemItems(getEvaluationItemItems(evaluationItem.getId()));
			}
			return evaluationContents;
		}else{
			List<EvaluationItem> evaluationContents =
				jdbcTemplate.query(
					"SELECT ei.id, eies.content as evaluationStandard," +
					"    eigs.content as gradingStandard," +
					"    points, weight " +
					"FROM evaluation_item ei, evaluation_item_evaluation_standard eies, " +
					"  evaluation_item_grading_standard eigs " +
					"WHERE ei.id = eies.item_id " +
					"  AND ei.id = eigs.item_id " +
					"  AND ei.content_id = ? " +
					"  AND eies.locale = ?" +
					"  AND eigs.locale = ?", 
					ParameterizedBeanPropertyRowMapper.newInstance(EvaluationItem.class),
					contentId, 
					LocaleContextHolder.getLocale().getLanguage(),
					LocaleContextHolder.getLocale().getLanguage());
		
			return evaluationContents;
		}
		
	}
	
	@SuppressWarnings("deprecation")
	private List<EvaluationItemItem> getEvaluationItemItems(long id) {
		List<EvaluationItemItem> evaluationItemItems =
			jdbcTemplate.query(
				"SELECT eii.id," +
				"    eigs.content as gradingStandard," +
				"    points, weight " +
				"FROM evaluation_item_item eii, " +
				"  evaluation_item_grading_standard eigs " +
				"WHERE eii.id = eigs.item_id " +
				"  AND eii.item_id = ? " +
				"  AND eigs.locale = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(EvaluationItemItem.class),
				id, 
				LocaleContextHolder.getLocale().getLanguage());
	
		return evaluationItemItems;
	}

	@Override
	public List<User> findL1Users(int division, String supercompany) {
		return findUsers(division, supercompany, "ROLE_L1");
	}
	
	@Override
	public List<User> findL2Users(int division, String supercompany) {
		return findUsers(division, supercompany, "ROLE_L2");
	}
	
	@Override
	public List<User> findCeo() {
		return findUsers("ROLE_CEO");
	}
	
	@Override
	public List<User> findHelpDeskUsers() {
		return findUsers("ROLE_HD");
	}
	
	@Override
	public List<User> findFinancialManagers() {
		return findUsers("ROLE_FM");
	}
	
	@Override
	public List<User> findDataMaintainers() {
		return findUsers("ROLE_DM");
	}
	
	@SuppressWarnings("deprecation")
	private List<User> findUsers(int division, String authority) {
		return jdbcTemplate.query(
				"SELECT u.id as id, u.name as name " +
				"FROM user u, authorities a " +
				"WHERE u.id = a.username " +
				"  AND u.division = ? " +
				"  AND a.authority = ? " +
				"  AND u.valid = 1 ", 
				ParameterizedBeanPropertyRowMapper.newInstance(User.class),
				division, authority);
	}
	/**
	 * Get role l2 list
	 * @param division
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<User> findL2s(int division, String usercompany) {
		return jdbcTemplate.query(
				"SELECT u.id as id, u.name as name "+
				"FROM user u, authorities a "+
				"WHERE u.id = a.username "+
				"AND (u.division = ? OR EXISTS(SELECT * from  user_costcenter c where c.id = u.id and c.division = ?)) "+
				"AND a.authority = 'ROLE_L2' "+
				"AND u.valid = 1 ",
				ParameterizedBeanPropertyRowMapper.newInstance(User.class),
				division, division);
	}
	/**
	 * Get role l1 list
	 * @param division
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<User> findUsers(int division, String usercompany, String authority) {
		return jdbcTemplate.query(
				"SELECT u.id as id, u.name as name " +
                "FROM authorities a, user_info ui, user u  " +
                "WHERE a.username = ui.id AND  " +
                    "u.id = ui.id AND  " +
                    "u.id = a.username AND " + 
                    "a.usercompany = ui.usercompany AND " +
                    "ui.valid = 1 AND  " +
                    "a.division = ? AND  " +
                    "a.authority = ? AND  " +
                    "a.usercompany = ?",
				ParameterizedBeanPropertyRowMapper.newInstance(User.class),
				division, authority, usercompany);
	}
	
	@SuppressWarnings("deprecation")
	private List<User> findUsers(String authority) {
		return jdbcTemplate.query(
				"SELECT u.id as id, u.name as name  "+
                "FROM authorities a, user_info ui, user u  "+
                "WHERE a.username = ui.id AND  "+
                    "u.id = ui.id AND  "+
                    "u.id = a.username AND  "+
    				"a.authority = ? AND "+
    				"ui.valid = 1 ",
				ParameterizedBeanPropertyRowMapper.newInstance(User.class),
				authority);
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<EvaluationHistory> getEvaluationHistories(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, " +
				"  total_point as totalPoint, " +
				"  registered_date as registeredDate " +
				"FROM evaluation " +
				"WHERE status = 20 AND vendor_id = ? AND registered_date != (" +
				"  SELECT MAX(registered_date) FROM evaluation WHERE vendor_id = ? AND status = 20 " +
				") " +
				"ORDER BY registered_date DESC", 
				ParameterizedBeanPropertyRowMapper.newInstance(EvaluationHistory.class), 
				vendorId, vendorId);
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<EvaluationHistory> getNonTradeEvaluationHistories(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, " +
				"  grand_ttl_weight_score as grandScore, " +
				"  grand_ttl_top_weight_score as grandTopScore, " +
				"  registered_date as registeredDate " +
				"FROM evaluation " +
				"WHERE status = 20 AND vendor_id = ? AND registered_date != (" +
				"  SELECT MAX(registered_date) FROM evaluation WHERE vendor_id = ? AND status = 20 " +
				") " +
				"ORDER BY registered_date DESC", 
				ParameterizedBeanPropertyRowMapper.newInstance(EvaluationHistory.class), 
				vendorId, vendorId);
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: calculatePoint</p> 
	 * <p>Description: 评估 from提交   -  point 核算</p> 
	 * @param kind
	 * @param points
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#calculatePoint(int, int[])
	 */
	@Override
	public String calculatePoint(int kind, int[] points) {
		BigDecimal result = new BigDecimal(0);
		int index = 0;
		for (EvaluationCategory category : getEvaluationCategories(kind)) {
			BigDecimal ttl = new BigDecimal(0);
			for (EvaluationContent content : category.getEvaluationContents()) {
				for (EvaluationItem item : content.getEvaluationItems()) {
					if(kind >=3 && kind <=9){
						for (EvaluationItemItem itemItem : item.getEvaluationItemItems()) {
							ttl = ttl.add(new BigDecimal(itemItem.getWeight()).multiply(new BigDecimal(points[index++])));
						}
					}else{
						ttl = ttl.add(new BigDecimal(item.getWeight()).multiply(new BigDecimal(points[index++])));
					}
					
				}
			}
			
			result = result.add(ttl.multiply(new BigDecimal(category.getWeight())));
		}
		if(kind == 2){
			//common
			return result.setScale(0, BigDecimal.ROUND_HALF_UP).toString();
		}else if(kind >=3 && kind <=9){
			//non-trade
			return result.multiply(new BigDecimal(20)).setScale(2, BigDecimal.ROUND_HALF_UP).toString();
		}else{
			return result.multiply(new BigDecimal(25)).setScale(0, BigDecimal.ROUND_HALF_UP).toString();
		}
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: store</p> 
	 * <p>Description: </p> 
	 * @param evaluation
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#store(cn.sscs.vendormanagement.evaluation.Evaluation)
	 */
	@Override
	public long store(Evaluation evaluation) {
		
		Map<String, Object> rowMap = evaluation.toMap();
		rowMap.put("registered_date", new Date());
		
		long id = insertEvaluation.executeAndReturnKey(rowMap).intValue();
		
		evaluation.setId(id);
		
		rowMap.clear();
		rowMap.put("evaluation_id", id);
		// new non trade 
		if(Utils.isNonTrade(evaluation.getBusinessType())){
			return id;
		}
		int[] points = evaluation.getPoint();
		String[] remarks = evaluation.getRemark();
	
		for (int i=0; i<points.length; i++) {
			rowMap.put("point", points[i]);
			rowMap.put("remark", remarks[i]);
			rowMap.put("sequence", i);
			
			insertEvaluationPoint.execute(rowMap);
		}
		
		return id;
	}
	
	@Override
	public void updateEvaluation(Evaluation evaluation) {
		jdbcTemplate.update(
				"UPDATE evaluation " +
				"SET " +
				"company_short_name =:shortName," +
				"product_name =:productName," +
				"product_category =:productCategory," +
				"transaction_condition =:tranCond," +
				"selection_reason =:selectionReason," +
				"comment =:comment," +
				"total_point =:totalPoint, " +
				
				"service_category =:serviceCategory, " +
				"service_industry =:serviceIndustry, " +
				"service_scope =:serviceScope, " +
				"major_accounts =:majorAccounts, " +
				"volume_per_year =:volumePerYear, " +
				"num_of_biz_sites_nationwide =:numOfBizSitesNationwide, " +
				"forwarding_company =:forwardingCompany, " +
				"info_fix_assets_nationwide =:infoFixAssetsNationwide, " +
				"num_of_empl_nationwide =:numOfEmplNationwide, " +
				"applicant =:applicant, " +
				"service2Category =:service2Category, " +
				"serviceName =:serviceName, " +
				"date2 =:date2 " +
				"WHERE id=:id",
				new BeanPropertySqlParameterSource(evaluation));

		int[] points = evaluation.getPoint();
		String[] remarks = evaluation.getRemark();
		
		for (int i=0; i<points.length; i++) {
			jdbcTemplate.update(
					"UPDATE evaluation_points " +
					"SET " +
					"point=?," +
					"remark=?" +
					"WHERE evaluation_id=?" +
					" AND sequence=?", 
					points[i], remarks[i], evaluation.getId(), i);
		}
	}
	
	@Override
	public void updateVendorIdOfEvaluation(long oldVendorId, long newVendorId) {
		jdbcTemplate.update("UPDATE evaluation set vendor_id = ? where vendor_id = ?", newVendorId, oldVendorId);
	}
	
	@Override
	public Evaluation findEvaluationById(long id, int status) {
		Evaluation evaluation = jdbcTemplate.queryForObject(
				"SELECT * " +
				"FROM evaluation " +
				"WHERE id = ? AND status=?", 
				new RowMapper<Evaluation>(){

					@Override
					public Evaluation mapRow(ResultSet result, int row)
							throws SQLException {
					    
						Evaluation evaluation = new Evaluation();
						evaluation.setId(result.getInt("id"));
						evaluation.setVendorId(result.getLong("vendor_id"));
						evaluation.setShortName(result.getString("company_short_name"));
						evaluation.setProductName(result.getString("product_name"));
						evaluation.setProductCategory(result.getString("product_category"));
						evaluation.setTranCond(result.getInt("transaction_condition"));
						evaluation.setSelectionReason(result.getString("selection_reason"));
						evaluation.setComment(result.getString("comment"));
						//evaluation.setTotalPoint(result.getString("total_point"));		
						evaluation.setStatus(result.getInt("status"));
						evaluation.setBusinessType(result.getInt("business_type"));
						if (Utils.isNonTrade(evaluation.getBusinessType())){
							// non-trade need CEO 
							evaluation.setTotalPointForCompare(75);
							evaluation.setGrandScore(result.getInt("grand_ttl_weight_score"));
							evaluation.setGrandTopScore(result.getInt("grand_ttl_top_weight_score"));
						}else{
							evaluation.setTotalPoint(result.getString("total_point"));
						}
						evaluation.setRegisteredDate(result.getDate("registered_date"));
						
						evaluation.setServiceCategory(result.getString("service_category"));
						evaluation.setServiceIndustry(result.getString("service_industry"));
						evaluation.setServiceScope(result.getString("service_scope"));
						evaluation.setMajorAccounts(result.getString("major_accounts"));
						evaluation.setVolumePerYear(result.getString("volume_per_year"));
						evaluation.setNumOfBizSitesNationwide(result.getString("num_of_biz_sites_nationwide"));
						evaluation.setForwardingCompany(result.getString("forwarding_company"));
						evaluation.setInfoFixAssetsNationwide(result.getString("info_fix_assets_nationwide"));
						evaluation.setNumOfEmplNationwide(result.getString("num_of_empl_nationwide"));
						evaluation.setCooperationCondition(result.getString("cooperation_condition"));
						evaluation.setServiceName(result.getString("serviceName"));
						evaluation.setService2Category(result.getString("service2Category"));
						evaluation.setDate2(result.getString("date2"));
						evaluation.setApplicant(result.getString("applicant"));
						return evaluation;
					}}, 
					id, status);
		// non-trade
		if (Utils.isNonTrade(evaluation.getBusinessType())){
			return evaluation;
		}
		List<Map<String, Object>> evalItems = jdbcTemplate.query(
				"SELECT point, remark " +
				"FROM evaluation_points " +
				"WHERE evaluation_id = ? " +
				"ORDER BY sequence", 
				new RowMapper<Map<String, Object>>(){

					@Override
					public Map<String, Object> mapRow(ResultSet result, int row)
							throws SQLException {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("point", result.getInt("point"));
						map.put("remark", result.getString("remark"));
						
						return map;
					}},
				evaluation.getId());
		
		int[] points = new int[evalItems.size()];
		String[] remarks = new String[evalItems.size()];
		for (int i=0; i<points.length; i++) {
			points[i] = (Integer) evalItems.get(i).get("point");
			remarks[i] = (String) evalItems.get(i).get("remark");
		}

		evaluation.setPoint(points);
		evaluation.setRemark(remarks);

		
		return evaluation;
	}
	
	/**
	 * (非 Javadoc) 
	 * <p>Title: findEvaluationById</p> 
	 * <p>Description: 查询多个状态的评估信息</p>
	 * @param id
	 * @param status
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#findEvaluationById(long, int)
	 */
	@Override
	public Evaluation findEvaluationById(long id, List<Integer> status) {
		
		String statusCondition = "";
		for (int i = 1 ; i <= status.size() ; i++ ){
			statusCondition =  statusCondition + status.get(i - 1) + (i == status.size() ? "":",")  ;
		}
		Evaluation evaluation = jdbcTemplate.queryForObject(
				"SELECT * " +
				"FROM evaluation " +
				"WHERE id = ? " +
				"AND status in ("+statusCondition+") ", 
				new RowMapper<Evaluation>(){

					@Override
					public Evaluation mapRow(ResultSet result, int row)
							throws SQLException {

						Evaluation evaluation = new Evaluation();
						evaluation.setId(result.getInt("id"));
						evaluation.setVendorId(result.getLong("vendor_id"));
						evaluation.setShortName(result.getString("company_short_name"));
						evaluation.setProductName(result.getString("product_name"));
						evaluation.setProductCategory(result.getString("product_category"));
						evaluation.setTranCond(result.getInt("transaction_condition"));
						evaluation.setSelectionReason(result.getString("selection_reason"));
						evaluation.setComment(result.getString("comment"));
						//evaluation.setTotalPoint(result.getString("total_point"));		
						evaluation.setStatus(result.getInt("status"));
						evaluation.setBusinessType(result.getInt("business_type"));
						if (Utils.isNonTrade(evaluation.getBusinessType())){
							// non-trade need CEO 
							evaluation.setTotalPointForCompare(75);
							evaluation.setGrandScore(result.getInt("grand_ttl_weight_score"));
							evaluation.setGrandTopScore(result.getInt("grand_ttl_top_weight_score"));
						}else{
							evaluation.setTotalPoint(result.getString("total_point"));
						}
						evaluation.setRegisteredDate(result.getDate("registered_date"));
						
						evaluation.setServiceCategory(result.getString("service_category"));
						evaluation.setServiceIndustry(result.getString("service_industry"));
						evaluation.setServiceScope(result.getString("service_scope"));
						evaluation.setMajorAccounts(result.getString("major_accounts"));
						evaluation.setVolumePerYear(result.getString("volume_per_year"));
						evaluation.setNumOfBizSitesNationwide(result.getString("num_of_biz_sites_nationwide"));
						evaluation.setForwardingCompany(result.getString("forwarding_company"));
						evaluation.setInfoFixAssetsNationwide(result.getString("info_fix_assets_nationwide"));
						evaluation.setNumOfEmplNationwide(result.getString("num_of_empl_nationwide"));
						evaluation.setCooperationCondition(result.getString("cooperation_condition"));
						evaluation.setServiceName(result.getString("serviceName"));
						evaluation.setService2Category(result.getString("service2Category"));
						evaluation.setDate2(result.getString("date2"));
						evaluation.setApplicant(result.getString("applicant"));
						return evaluation;
					}}, 
					id);
		// non-trade
		if (Utils.isNonTrade(evaluation.getBusinessType())){
			return evaluation;
		}
		List<Map<String, Object>> evalItems = jdbcTemplate.query(
				"SELECT point, remark " +
				"FROM evaluation_points " +
				"WHERE evaluation_id = ? " +
				"ORDER BY sequence", 
				new RowMapper<Map<String, Object>>(){

					@Override
					public Map<String, Object> mapRow(ResultSet result, int row)
							throws SQLException {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("point", result.getInt("point"));
						map.put("remark", result.getString("remark"));
						
						return map;
					}},
				evaluation.getId());
		
		int[] points = new int[evalItems.size()];
		String[] remarks = new String[evalItems.size()];
		for (int i=0; i<points.length; i++) {
			points[i] = (Integer) evalItems.get(i).get("point");
			remarks[i] = (String) evalItems.get(i).get("remark");
		}

		evaluation.setPoint(points);
		evaluation.setRemark(remarks);

		
		return evaluation;
	}
	
	@Override
	public Evaluation getEvaluationById(long id) {
		Evaluation evaluation = jdbcTemplate.queryForObject(
				"SELECT * " +
				"FROM evaluation " +
				"WHERE id = ?", 
				new RowMapper<Evaluation>(){

					@Override
					public Evaluation mapRow(ResultSet result, int row)
							throws SQLException {

						Evaluation evaluation = new Evaluation();
						evaluation.setId(result.getInt("id"));
						evaluation.setVendorId(result.getLong("vendor_id"));
						evaluation.setShortName(result.getString("company_short_name"));
						evaluation.setProductName(result.getString("product_name"));
						evaluation.setProductCategory(result.getString("product_category"));
						evaluation.setTranCond(result.getInt("transaction_condition"));
						evaluation.setSelectionReason(result.getString("selection_reason"));
						evaluation.setComment(result.getString("comment"));
						//evaluation.setTotalPoint(result.getString("total_point"));		
						evaluation.setStatus(result.getInt("status"));
						evaluation.setBusinessType(result.getInt("business_type"));
						if (Utils.isNonTrade(evaluation.getBusinessType())){
							// non-trade need CEO 
							evaluation.setTotalPointForCompare(75);
							evaluation.setGrandScore(result.getInt("grand_ttl_weight_score"));
							evaluation.setGrandTopScore(result.getInt("grand_ttl_top_weight_score"));
						}else{
							evaluation.setTotalPoint(result.getString("total_point"));
						}
						evaluation.setRegisteredDate(result.getDate("registered_date"));
						
						evaluation.setServiceCategory(result.getString("service_category"));
						evaluation.setServiceIndustry(result.getString("service_industry"));
						evaluation.setServiceScope(result.getString("service_scope"));
						evaluation.setMajorAccounts(result.getString("major_accounts"));
						evaluation.setVolumePerYear(result.getString("volume_per_year"));
						evaluation.setNumOfBizSitesNationwide(result.getString("num_of_biz_sites_nationwide"));
						evaluation.setForwardingCompany(result.getString("forwarding_company"));
						evaluation.setInfoFixAssetsNationwide(result.getString("info_fix_assets_nationwide"));
						evaluation.setNumOfEmplNationwide(result.getString("num_of_empl_nationwide"));
						evaluation.setCooperationCondition(result.getString("cooperation_condition"));
						evaluation.setServiceName(result.getString("serviceName"));
						evaluation.setService2Category(result.getString("service2Category"));
						evaluation.setDate2(result.getString("date2"));
						evaluation.setApplicant(result.getString("applicant"));
						return evaluation;
					}}, 
					id);
		// non-trade
		if (Utils.isNonTrade(evaluation.getBusinessType())){
			return evaluation;
		}
		List<Map<String, Object>> evalItems = jdbcTemplate.query(
				"SELECT point, remark " +
				"FROM evaluation_points " +
				"WHERE evaluation_id = ? " +
				"ORDER BY sequence", 
				new RowMapper<Map<String, Object>>(){

					@Override
					public Map<String, Object> mapRow(ResultSet result, int row)
							throws SQLException {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("point", result.getInt("point"));
						map.put("remark", result.getString("remark"));
						
						return map;
					}},
				evaluation.getId());
		
		int[] points = new int[evalItems.size()];
		String[] remarks = new String[evalItems.size()];
		for (int i=0; i<points.length; i++) {
			points[i] = (Integer) evalItems.get(i).get("point");
			remarks[i] = (String) evalItems.get(i).get("remark");
		}

		evaluation.setPoint(points);
		evaluation.setRemark(remarks);

		
		return evaluation;
	}

	@Override
	public Evaluation findLatestEvaluationByVendorId(long vendorId) {
		Evaluation evaluation = jdbcTemplate.queryForObject(
				"SELECT * " +
				"FROM evaluation " +
				"WHERE vendor_id = ? AND registered_date = (" +
				"  SELECT MAX(registered_date) FROM evaluation WHERE vendor_id = ? AND status = 20" +
				")", 
				new RowMapper<Evaluation>(){

					@Override
					public Evaluation mapRow(ResultSet result, int row)
							throws SQLException {

						Evaluation evaluation = new Evaluation();
						evaluation.setId(result.getInt("id"));
						evaluation.setVendorId(result.getLong("vendor_id"));
						evaluation.setShortName(result.getString("company_short_name"));
						evaluation.setProductName(result.getString("product_name"));
						evaluation.setProductCategory(result.getString("product_category"));
						evaluation.setTranCond(result.getInt("transaction_condition"));
						evaluation.setSelectionReason(result.getString("selection_reason"));
						evaluation.setComment(result.getString("comment"));
						//evaluation.setTotalPoint(result.getString("total_point"));		
						evaluation.setStatus(result.getInt("status"));
						evaluation.setBusinessType(result.getInt("business_type"));
						if (Utils.isNonTrade(evaluation.getBusinessType())){
							// non-trade need CEO 
							evaluation.setTotalPointForCompare(75);
							evaluation.setGrandScore(result.getInt("grand_ttl_weight_score"));
							evaluation.setGrandTopScore(result.getInt("grand_ttl_top_weight_score"));
						}else{
							evaluation.setTotalPoint(result.getString("total_point"));
						}
						evaluation.setRegisteredDate(result.getDate("registered_date"));
						
						evaluation.setServiceCategory(result.getString("service_category"));
						evaluation.setServiceIndustry(result.getString("service_industry"));
						evaluation.setServiceScope(result.getString("service_scope"));
						evaluation.setMajorAccounts(result.getString("major_accounts"));
						evaluation.setVolumePerYear(result.getString("volume_per_year"));
						evaluation.setNumOfBizSitesNationwide(result.getString("num_of_biz_sites_nationwide"));
						evaluation.setForwardingCompany(result.getString("forwarding_company"));
						evaluation.setInfoFixAssetsNationwide(result.getString("info_fix_assets_nationwide"));
						evaluation.setNumOfEmplNationwide(result.getString("num_of_empl_nationwide"));
						evaluation.setServiceName(result.getString("serviceName"));
						evaluation.setService2Category(result.getString("service2Category"));
						evaluation.setDate2(result.getString("date2"));
						evaluation.setApplicant(result.getString("applicant"));
						return evaluation;
					}}, 
					vendorId, vendorId);
		if (Utils.isNonTrade(evaluation.getBusinessType())){
			return evaluation;
		}
		List<Map<String, Object>> evalItems = jdbcTemplate.query(
				"SELECT point, remark " +
				"FROM evaluation_points " +
				"WHERE evaluation_id = ? " +
				"ORDER BY sequence", 
				new RowMapper<Map<String, Object>>(){

					@Override
					public Map<String, Object> mapRow(ResultSet result, int row)
							throws SQLException {
						Map<String, Object> map = new HashMap<String, Object>();
						map.put("point", result.getInt("point"));
						map.put("remark", result.getString("remark"));
						
						return map;
					}},
				evaluation.getId());
		
		int[] points = new int[evalItems.size()];
		String[] remarks = new String[evalItems.size()];
		for (int i=0; i<points.length; i++) {
			points[i] = (Integer) evalItems.get(i).get("point");
			remarks[i] = (String) evalItems.get(i).get("remark");
		}

		evaluation.setPoint(points);
		evaluation.setRemark(remarks);

		
		return evaluation;
	}
	
	
	@Override
	public boolean hasEvaluationById(long evalId) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) " +
				"FROM evaluation " +
				"WHERE id = ? ",
			 	evalId) == 1;
	}

	@Override
	public boolean hasLatestEvaluationByVendorId(long vendorId) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) " +
				"FROM evaluation " +
				"WHERE vendor_id = ? AND registered_date = (" +
				"  SELECT MAX(registered_date) FROM evaluation WHERE vendor_id = ? AND status = 20" +
				")", vendorId, vendorId) > 0;
	}

	@Override
	public void updateStatus(int status, long id) {
		jdbcTemplate.update(
				"UPDATE evaluation SET status = ? " +
				"WHERE id = ? ",
				status, id);
	}

	@Override
	public Evaluation findEvaluationById(long id) {
		Evaluation evaluation = jdbcTemplate.queryForObject(
				"SELECT total_point " +
				"FROM evaluation " +
				"WHERE id = ?", 
				new RowMapper<Evaluation>(){

					@Override
					public Evaluation mapRow(ResultSet result, int row)
							throws SQLException {

						Evaluation evaluation = new Evaluation();
						if(result.getString("total_point") == null || "".equals(result.getString("total_point"))){
							// non-trade
							 evaluation.setTotalPointForCompare(75);
						}else{
							evaluation.setTotalPoint(result.getString("total_point"));	
						}
						return evaluation;
					}}, 
					id);
		return evaluation;
	}
	
	@Override
	public long storeAttachment(Map<String, Object> attachment) {
		return insertAttachment.executeAndReturnKey(attachment).longValue();
	}
	
	@Override
	public void deleteAttachmentData(long id, String tranId) {
		jdbcTemplate.update(
				"DELETE FROM attachment_workflow WHERE id = ?", 
				id);
	}

	@Override
	public void storeNonTradeEvaluationResultDetail(Evaluation evaluation) {
		String[] nonPoint = evaluation.getNon_point();
		String[] yesNo = evaluation.getYesNo();
		String[] priority = evaluation.getPriority();
		int[] formId = evaluation.getFormId();
		int[] categoryId = evaluation.getCategoryId();
		int[] subCategoryId = evaluation.getSubCategoryId();
		int[] itemsId = evaluation.getItemsId();
		int[] detailItemsId = evaluation.getDetailItemsId();
		float[] weight = evaluation.getWeight();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("evaluation_id", evaluation.getId());
		
		//subCategory
		List<ScoreSummary> scoreSummaryList = new ArrayList<ScoreSummary>();
		int subCategory_items_count = 0;
		int subCategory_ttl_score = 0;
		int subCategory_ttl_top_sore = 0;
		double normalized_weight = 0;
		int subCategory_ttl_weight_score = 0;
		int subCategory_ttl_top_weight_sore = 0;
		
		//category
		List<CategoryScoreSummary> categoryScoreSummaryList = new ArrayList<CategoryScoreSummary>();
		int category_items_count = 0;
		int category_ttl_score = 0;
		int category_ttl_top_sore = 0;
		int category_ttl_weight_score = 0;
		int category_ttl_top_weight_sore = 0;
		
		//form
		int form_items_count = 0;
		int form_ttl_score = 0;
		int form_ttl_top_sore = 0;
		int form_ttl_weight_score = 0;
		int form_ttl_top_weight_sore = 0;

		// grand
		int grandTtlScore = 0;
		int grandTtlTopScore = 0;
		
		// temp
		int detailResult = 0;
		int temp_items_count = 0;
		int temp_subCategory_ttl_score = 0;
		double basic_weight = 0;
		double form_total_basic_weight = 0;
		double temp_grandTtlScore = 0;
		double temp_grandTtlTopScore = 0;
		for (int i=0; i < nonPoint.length; i++) {
			String topScore = "1".equals(priority[i]) ? "25" : ("2".equals(priority[i]) ? "15" : "5");
			String scoreYesno;
			if("2".equals(yesNo[i])){
				scoreYesno = null;
			}else{
				if("0".equals(yesNo[i])){
					if("1".equals(priority[i])){
						scoreYesno = "0";
					}else if("2".equals(priority[i])){
						scoreYesno = "3";
					}else{
						scoreYesno = "1";
					}
				}else{
					if("1".equals(priority[i])){
						scoreYesno = "25";
					}else if("2".equals(priority[i])){
						scoreYesno = "15";
					}else{
						scoreYesno = "5";
					}
				}
			}
			String scorePoint;
			if(nonPoint[i] == null || "0".equals(nonPoint[i])){
				scorePoint = null;
			}else{
				int base = "1".equals(priority[i]) ? 5 : ("2".equals(priority[i]) ? 3 : 1);
				detailResult = base * Integer.parseInt(nonPoint[i]);
				scorePoint = String.valueOf(detailResult);
			}
			map.put("form_id", formId[i]);
			map.put("category_id", categoryId[i]);
			map.put("sub_category_id", subCategoryId[i]);
			map.put("items_id", itemsId[i]);
			map.put("detail_items_id", detailItemsId[i]);
			map.put("priority", priority[i]);
			map.put("yes_no", yesNo[i]);
			map.put("score_yesno", scoreYesno);
			map.put("point", nonPoint[i]);
			map.put("score_point", scorePoint);
			map.put("top_score", topScore);
			insertNonTradeEvaluationDetail.execute(map);
			
			// subCategory
			if(scoreYesno == null && scorePoint == null){
				subCategory_ttl_top_sore += 0;
			}else{
				temp_items_count++;
				subCategory_ttl_top_sore += Integer.parseInt(topScore);
			}
			temp_subCategory_ttl_score +=  (scoreYesno == null ? 0 : Integer.parseInt(scoreYesno)) 
											+ (scorePoint==null ? 0 : Integer.parseInt(scorePoint));
			//评估的非最后一条用id判断是否为同一类别（sub-category）
			if((i < nonPoint.length - 2 && subCategoryId[i] != subCategoryId[i + 1]) 
					//评估的最后一条
					|| i == nonPoint.length - 1){
				subCategory_items_count = temp_items_count;
				
				subCategory_ttl_score = temp_subCategory_ttl_score;
				temp_subCategory_ttl_score = 0;
				double weight_sub_category = weight[i];
				if(temp_items_count ==0){
					basic_weight = 0;
					normalized_weight = 0;
				}else{
					basic_weight = Math.sqrt(temp_items_count);
					form_total_basic_weight += basic_weight;
					temp_items_count = 0;
				}
				
				ScoreSummary scoreSummary = new ScoreSummary();
				scoreSummary.setFormId(formId[i]);
				scoreSummary.setCategoryId(categoryId[i]);
				scoreSummary.setSubCategoryId(subCategoryId[i]);
				scoreSummary.setEvaluationId(evaluation.getId());
				scoreSummary.setItemsCount(subCategory_items_count);
				scoreSummary.setTtlScore(subCategory_ttl_score);
				scoreSummary.setTtlTopScore(subCategory_ttl_top_sore);
				scoreSummary.setWeight(weight_sub_category);
				scoreSummary.setBasicWeight(basic_weight);
				scoreSummaryList.add(scoreSummary);
				
				// category
				category_items_count += subCategory_items_count;
				subCategory_items_count = 0;
				category_ttl_score += subCategory_ttl_score;
				subCategory_ttl_score= 0;
				category_ttl_top_sore += subCategory_ttl_top_sore;
				subCategory_ttl_top_sore = 0;
				//评估的非最后一条用id判断是否为同一类别（category）
				if((i < nonPoint.length - 2 && categoryId[i] != categoryId[i + 1]) 
						|| i == nonPoint.length - 1){
					CategoryScoreSummary categoryScoreSummary = new CategoryScoreSummary();
					categoryScoreSummary.setFormId(formId[i]);
					categoryScoreSummary.setCategoryId(categoryId[i]);
					categoryScoreSummary.setEvaluationId(evaluation.getId());
					categoryScoreSummary.setItemsCount(category_items_count);
					categoryScoreSummary.setTtlScore(category_ttl_score);
					categoryScoreSummary.setTtlTopScore(category_ttl_top_sore);
					List<ScoreSummary> scoreSummaryList2 = new ArrayList<ScoreSummary>();
					scoreSummaryList2.addAll(scoreSummaryList);
					categoryScoreSummary.setScoreSummaryList(scoreSummaryList2);
					scoreSummaryList.clear();
					categoryScoreSummaryList.add(categoryScoreSummary);
					// form
					form_items_count += category_items_count;
					category_items_count = 0;
					form_ttl_score += category_ttl_score;
					category_ttl_score = 0;
					form_ttl_top_sore += category_ttl_top_sore;
					category_ttl_top_sore =0;
					//评估的非最后一条用id判断是否为同一类别（form）
					if((i < nonPoint.length - 2 && formId[i] != formId[i + 1]) 
							|| i == nonPoint.length - 1){
						double form_ttl_weight_score_tmp = 0;
						double form_ttl_top_weight_sore_tmp = 0;
						for(CategoryScoreSummary categoryScoreSummaryTmp : categoryScoreSummaryList){
							double category_ttl_weight_score_tmp = 0;
							double category_ttl_top_weight_sore_tmp = 0;
							for(ScoreSummary scoreSummaryTmp : categoryScoreSummaryTmp.getScoreSummaryList()){
								if(Double.compare(form_total_basic_weight, 0) <= 0){
									normalized_weight = 0;
								}else{
									normalized_weight = scoreSummaryTmp.getBasicWeight()/form_total_basic_weight * scoreSummaryTmp.getWeight();
								}
								scoreSummaryTmp.setNormalizedWeight(normalized_weight);
								
								double subCategory_ttl_weight_score_tmp = (scoreSummaryTmp.getTtlScore() * normalized_weight * 10);
								
								category_ttl_weight_score_tmp += subCategory_ttl_weight_score_tmp;
								
								BigDecimal bd = new BigDecimal(subCategory_ttl_weight_score_tmp); 
						        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
						        subCategory_ttl_weight_score = bd.intValue();
						        scoreSummaryTmp.setTtlWeightScore(subCategory_ttl_weight_score);
						        
						        double subCategory_ttl_top_weight_sore_tmp = (scoreSummaryTmp.getTtlTopScore() * normalized_weight * 10);
						        
						        category_ttl_top_weight_sore_tmp += subCategory_ttl_top_weight_sore_tmp;
						        
						        BigDecimal bd2 = new BigDecimal(subCategory_ttl_top_weight_sore_tmp); 
						        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
						        subCategory_ttl_top_weight_sore = bd2.intValue();
						        scoreSummaryTmp.setTtlTopWeightScore(subCategory_ttl_top_weight_sore);
						        
						        insertScoreSummary.execute(scoreSummaryTmp.toMap());
							}
							form_ttl_weight_score_tmp += category_ttl_weight_score_tmp;
							BigDecimal bd = new BigDecimal(category_ttl_weight_score_tmp); 
					        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
					        category_ttl_weight_score = bd.intValue();
							categoryScoreSummaryTmp.setTtlWeightScore(category_ttl_weight_score);
							
							form_ttl_top_weight_sore_tmp += category_ttl_top_weight_sore_tmp;
					        BigDecimal bd2 = new BigDecimal(category_ttl_top_weight_sore_tmp); 
					        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
					        category_ttl_top_weight_sore = bd2.intValue();
					        categoryScoreSummaryTmp.setTtlTopWeightScore(category_ttl_top_weight_sore);
					        
					        insertCategoryScoreSummary.execute(categoryScoreSummaryTmp.toMap());
						}
						BigDecimal bd = new BigDecimal(form_ttl_weight_score_tmp); 
				        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
				        form_ttl_weight_score = bd.intValue();
						
				        BigDecimal bd2 = new BigDecimal(form_ttl_top_weight_sore_tmp); 
				        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
				        form_ttl_top_weight_sore = bd2.intValue();
				        
						FormScoreSummary formScoreSummary = new FormScoreSummary();
						formScoreSummary.setFormId(formId[i]);
						formScoreSummary.setEvaluationId(evaluation.getId());
						formScoreSummary.setItemsCount(form_items_count);
						formScoreSummary.setTtlScore(form_ttl_score);
						formScoreSummary.setTtlTopScore(form_ttl_top_sore);
						formScoreSummary.setTtlWeightScore(form_ttl_weight_score);
						formScoreSummary.setTtlTopWeightScore(form_ttl_top_weight_sore);
						
						insertFormScoreSummary.execute(formScoreSummary.toMap());
						categoryScoreSummaryList.clear();
						// grand 
						temp_grandTtlScore += form_ttl_weight_score_tmp;
						temp_grandTtlTopScore += form_ttl_top_weight_sore_tmp;
						form_items_count = 0;
						form_ttl_score = 0;
						form_ttl_top_sore = 0;
						form_ttl_weight_score_tmp = 0;
						form_ttl_top_weight_sore_tmp = 0;
						form_total_basic_weight = 0;
					}
				}
			}
		}
		//grand
		BigDecimal bd = new BigDecimal(temp_grandTtlScore); 
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
        grandTtlScore = bd.intValue();
		
        BigDecimal bd2 = new BigDecimal(temp_grandTtlTopScore); 
        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
        grandTtlTopScore = bd2.intValue();
        
        evaluation.setGrandScore(grandTtlScore);
        evaluation.setGrandTopScore(grandTtlTopScore);
        
        jdbcTemplate.update(
				"UPDATE evaluation SET grand_ttl_weight_score = ? , grand_ttl_top_weight_score = ? " +
				"WHERE id = ? ",
				evaluation.getGrandScore(), evaluation.getGrandTopScore(), evaluation.getId());
	}

	@Override
	public void updateNonTradeEvaluationResultDetail(Evaluation evaluation) {

		String[] nonPoint = evaluation.getNon_point();
		String[] yesNo = evaluation.getYesNo();
		String[] priority = evaluation.getPriority();
		int[] formId = evaluation.getFormId();
		int[] categoryId = evaluation.getCategoryId();
		int[] subCategoryId = evaluation.getSubCategoryId();
		int[] itemsId = evaluation.getItemsId();
		int[] detailItemsId = evaluation.getDetailItemsId();
		float[] weight = evaluation.getWeight();
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("evaluation_id", evaluation.getId());
		
		//subCategory
		List<ScoreSummary> scoreSummaryList = new ArrayList<ScoreSummary>();
		int subCategory_items_count = 0;
		int subCategory_ttl_score = 0;
		int subCategory_ttl_top_sore = 0;
		double normalized_weight = 0;
		int subCategory_ttl_weight_score = 0;
		int subCategory_ttl_top_weight_sore = 0;
		
		//category
		List<CategoryScoreSummary> categoryScoreSummaryList = new ArrayList<CategoryScoreSummary>();
		int category_items_count = 0;
		int category_ttl_score = 0;
		int category_ttl_top_sore = 0;
		int category_ttl_weight_score = 0;
		int category_ttl_top_weight_sore = 0;
		
		//form
		int form_items_count = 0;
		int form_ttl_score = 0;
		int form_ttl_top_sore = 0;
		int form_ttl_weight_score = 0;
		int form_ttl_top_weight_sore = 0;

		// grand
		int grandTtlScore = 0;
		int grandTtlTopScore = 0;
		
		// temp
		int detailResult = 0;
		int temp_items_count = 0;
		int temp_subCategory_ttl_score = 0;
		double basic_weight = 0;
		double form_total_basic_weight = 0;
		double temp_grandTtlScore = 0;
		double temp_grandTtlTopScore = 0;
		for (int i=0; i < nonPoint.length; i++) {
			String topScore = "1".equals(priority[i]) ? "25" : ("2".equals(priority[i]) ? "15" : "5");
			String scoreYesno;
			if("2".equals(yesNo[i])){
				scoreYesno = null;
			}else{
				if("0".equals(yesNo[i])){
					if("1".equals(priority[i])){
						scoreYesno = "0";
					}else if("2".equals(priority[i])){
						scoreYesno = "3";
					}else{
						scoreYesno = "1";
					}
				}else{
					if("1".equals(priority[i])){
						scoreYesno = "25";
					}else if("2".equals(priority[i])){
						scoreYesno = "15";
					}else{
						scoreYesno = "5";
					}
				}
			}
			String scorePoint;
			if(nonPoint[i] == null || "0".equals(nonPoint[i])){
				scorePoint = null;
			}else{
				int base = "1".equals(priority[i]) ? 5 : ("2".equals(priority[i]) ? 3 : 1);
				detailResult = base * Integer.parseInt(nonPoint[i]);
				scorePoint = String.valueOf(detailResult);
			}
			map.put("form_id", formId[i]);
			map.put("category_id", categoryId[i]);
			map.put("sub_category_id", subCategoryId[i]);
			map.put("items_id", itemsId[i]);
			map.put("detail_items_id", detailItemsId[i]);
			map.put("priority", priority[i]);
			map.put("yes_no", yesNo[i]);
			map.put("score_yesno", scoreYesno);
			map.put("point", nonPoint[i]);
			map.put("score_point", scorePoint);
			map.put("top_score", topScore);
			jdbcTemplate.update(
					"UPDATE " +
						"non_trade_evaluation_detail " +
					"SET " +
						"priority=?, " +
						"yes_no=?, " +
						"score_yesno=?, " +
						"point=?, " +
						"score_point=?, " +
						"top_score=? " +
					"WHERE " +
						"form_id=? AND " +
						"category_id=? AND " +
						"sub_category_id=? AND " +
						"items_id=? AND " +
						"detail_items_id=? AND " +
						"evaluation_id=?", priority[i], yesNo[i], scoreYesno, nonPoint[i], scorePoint, topScore, formId[i], categoryId[i], subCategoryId[i], itemsId[i], detailItemsId[i], evaluation.getId());
			// subCategory
			if(scoreYesno == null && scorePoint == null){
				subCategory_ttl_top_sore += 0;
			}else{
				temp_items_count++;
				subCategory_ttl_top_sore += Integer.parseInt(topScore);
			}
			temp_subCategory_ttl_score +=  (scoreYesno==null?0:Integer.parseInt(scoreYesno)) + (scorePoint==null?0:Integer.parseInt(scorePoint));
			
			if((i < nonPoint.length - 2 && subCategoryId[i] != subCategoryId[i + 1]) || i == nonPoint.length - 1){
				subCategory_items_count = temp_items_count;
				subCategory_ttl_score = temp_subCategory_ttl_score;
				temp_subCategory_ttl_score = 0;
				double weight_sub_category = weight[i];
				if(temp_items_count ==0){
					basic_weight = 0;
					normalized_weight = 0;
				}else{
					basic_weight = Math.sqrt(temp_items_count);
					form_total_basic_weight += basic_weight;
					temp_items_count = 0;
				}
				ScoreSummary scoreSummary = new ScoreSummary();
				scoreSummary.setFormId(formId[i]);
				scoreSummary.setCategoryId(categoryId[i]);
				scoreSummary.setSubCategoryId(subCategoryId[i]);
				scoreSummary.setEvaluationId(evaluation.getId());
				scoreSummary.setItemsCount(subCategory_items_count);
				scoreSummary.setTtlScore(subCategory_ttl_score);
				scoreSummary.setTtlTopScore(subCategory_ttl_top_sore);
				scoreSummary.setWeight(weight_sub_category);
				scoreSummary.setBasicWeight(basic_weight);
				scoreSummaryList.add(scoreSummary);
				
				// category
				category_items_count += subCategory_items_count;
				subCategory_items_count = 0;
				category_ttl_score += subCategory_ttl_score;
				subCategory_ttl_score = 0;
				category_ttl_top_sore += subCategory_ttl_top_sore;
				subCategory_ttl_top_sore = 0;
				if((i < nonPoint.length - 2 && categoryId[i] != categoryId[i + 1]) || i == nonPoint.length - 1){
					CategoryScoreSummary categoryScoreSummary = new CategoryScoreSummary();
					categoryScoreSummary.setFormId(formId[i]);
					categoryScoreSummary.setCategoryId(categoryId[i]);
					categoryScoreSummary.setEvaluationId(evaluation.getId());
					categoryScoreSummary.setItemsCount(category_items_count);
					categoryScoreSummary.setTtlScore(category_ttl_score);
					categoryScoreSummary.setTtlTopScore(category_ttl_top_sore);
					List<ScoreSummary> scoreSummaryList2 = new ArrayList<ScoreSummary>();
					scoreSummaryList2.addAll(scoreSummaryList);
					categoryScoreSummary.setScoreSummaryList(scoreSummaryList2);
					scoreSummaryList.clear();
					categoryScoreSummaryList.add(categoryScoreSummary);
					// form
					form_items_count += category_items_count;
					category_items_count = 0;
					form_ttl_score += category_ttl_score;
					category_ttl_score = 0;
					form_ttl_top_sore += category_ttl_top_sore;
					category_ttl_top_sore =0;
					if((i < nonPoint.length - 2 && formId[i] != formId[i + 1]) || i == nonPoint.length - 1){
						double form_ttl_weight_score_tmp = 0;
						double form_ttl_top_weight_sore_tmp = 0;
						for(CategoryScoreSummary categoryScoreSummaryTmp : categoryScoreSummaryList){
							double category_ttl_weight_score_tmp = 0;
							double category_ttl_top_weight_sore_tmp = 0;
							for(ScoreSummary scoreSummaryTmp : categoryScoreSummaryTmp.getScoreSummaryList()){
								if(Double.compare(form_total_basic_weight, 0) <= 0){
									normalized_weight = 0;
								}else{
									normalized_weight = scoreSummaryTmp.getBasicWeight()/form_total_basic_weight * scoreSummaryTmp.getWeight();
								}
								scoreSummaryTmp.setNormalizedWeight(normalized_weight);
								
								double subCategory_ttl_weight_score_tmp = (scoreSummaryTmp.getTtlScore() * normalized_weight * 10);
								
								category_ttl_weight_score_tmp += subCategory_ttl_weight_score_tmp;
								
								BigDecimal bd = new BigDecimal(subCategory_ttl_weight_score_tmp); 
						        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
						        subCategory_ttl_weight_score = bd.intValue();
						        scoreSummaryTmp.setTtlWeightScore(subCategory_ttl_weight_score);
						        
						        double subCategory_ttl_top_weight_sore_tmp = (scoreSummaryTmp.getTtlTopScore() * normalized_weight * 10);
						        
						        category_ttl_top_weight_sore_tmp += subCategory_ttl_top_weight_sore_tmp;
						        
						        BigDecimal bd2 = new BigDecimal(subCategory_ttl_top_weight_sore_tmp); 
						        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
						        subCategory_ttl_top_weight_sore = bd2.intValue();
						        scoreSummaryTmp.setTtlTopWeightScore(subCategory_ttl_top_weight_sore);
						        
						        jdbcTemplate.update(
										"UPDATE " +
											"score_summary " +
										"SET " +
											"items_count=?, " +
											"ttl_score=?, " +
											"ttl_top_score=?, " +
											"weight=?, " +
											"normalized_weight=?, " +
											"ttl_weight_score=?, " +
											"ttl_top_weight_score=? " +
										"WHERE " +
											"form_id=? AND " +
											"category_id=? AND " +
											"sub_category_id=? AND " +
											"evaluation_id=?",
											scoreSummaryTmp.getItemsCount(),
											scoreSummaryTmp.getTtlScore(),
											scoreSummaryTmp.getTtlTopScore(),
											scoreSummaryTmp.getWeight(),
											scoreSummaryTmp.getNormalizedWeight(),
											scoreSummaryTmp.getTtlWeightScore(),
											scoreSummaryTmp.getTtlTopWeightScore(),
											scoreSummaryTmp.getFormId(),
											scoreSummaryTmp.getCategoryId(),
											scoreSummaryTmp.getSubCategoryId(),
											scoreSummaryTmp.getEvaluationId());
							}
							form_ttl_weight_score_tmp += category_ttl_weight_score_tmp;
							BigDecimal bd = new BigDecimal(category_ttl_weight_score_tmp); 
					        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
					        category_ttl_weight_score = bd.intValue();
							categoryScoreSummaryTmp.setTtlWeightScore(category_ttl_weight_score);
							
							form_ttl_top_weight_sore_tmp += category_ttl_top_weight_sore_tmp;
					        BigDecimal bd2 = new BigDecimal(category_ttl_top_weight_sore_tmp); 
					        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
					        category_ttl_top_weight_sore = bd2.intValue();
					        categoryScoreSummaryTmp.setTtlTopWeightScore(category_ttl_top_weight_sore);
					        
					        jdbcTemplate.update(
									"UPDATE " +
										"category_score_summary " +
									"SET " +
										"items_count=?, " +
										"ttl_score=?, " +
										"ttl_top_score=?, " +
										"ttl_weight_score=?, " +
										"ttl_top_weight_score=? " +
									"WHERE " +
										"form_id=? AND " +
										"category_id=? AND " +
										"evaluation_id=?",
										categoryScoreSummaryTmp.getItemsCount(),
										categoryScoreSummaryTmp.getTtlScore(),
										categoryScoreSummaryTmp.getTtlTopScore(),
										categoryScoreSummaryTmp.getTtlWeightScore(),
										categoryScoreSummaryTmp.getTtlTopWeightScore(),
										categoryScoreSummaryTmp.getFormId(),
										categoryScoreSummaryTmp.getCategoryId(),
										categoryScoreSummaryTmp.getEvaluationId());
						}
						BigDecimal bd = new BigDecimal(form_ttl_weight_score_tmp); 
				        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
				        form_ttl_weight_score = bd.intValue();
						
				        BigDecimal bd2 = new BigDecimal(form_ttl_top_weight_sore_tmp); 
				        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
				        form_ttl_top_weight_sore = bd2.intValue();
				        
						FormScoreSummary formScoreSummary = new FormScoreSummary();
						formScoreSummary.setFormId(formId[i]);
						formScoreSummary.setEvaluationId(evaluation.getId());
						formScoreSummary.setItemsCount(form_items_count);
						formScoreSummary.setTtlScore(form_ttl_score);
						formScoreSummary.setTtlTopScore(form_ttl_top_sore);
						formScoreSummary.setTtlWeightScore(form_ttl_weight_score);
						formScoreSummary.setTtlTopWeightScore(form_ttl_top_weight_sore);
						
						jdbcTemplate.update(
								"UPDATE " +
									"form_score_summary " +
								"SET " +
									"items_count=?, " +
									"ttl_score=?, " +
									"ttl_top_score=?, " +
									"ttl_weight_score=?, " +
									"ttl_top_weight_score=? " +
								"WHERE " +
									"form_id=? AND " +
									"evaluation_id=?",
									formScoreSummary.getItemsCount(),
									formScoreSummary.getTtlScore(),
									formScoreSummary.getTtlTopScore(),
									formScoreSummary.getTtlWeightScore(),
									formScoreSummary.getTtlTopWeightScore(),
									formScoreSummary.getFormId(),
									formScoreSummary.getEvaluationId());
						categoryScoreSummaryList.clear();
						// grand 
						temp_grandTtlScore += form_ttl_weight_score_tmp;
						temp_grandTtlTopScore += form_ttl_top_weight_sore_tmp;
						form_items_count = 0;
						form_ttl_score = 0;
						form_ttl_top_sore = 0;
						form_ttl_weight_score_tmp = 0;
						form_ttl_top_weight_sore_tmp = 0;
						form_total_basic_weight = 0;
					}
				}
			}
		}
		//grand
		BigDecimal bd = new BigDecimal(temp_grandTtlScore); 
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
        grandTtlScore = bd.intValue();
		
        BigDecimal bd2 = new BigDecimal(temp_grandTtlTopScore); 
        bd2 = bd2.setScale(0, BigDecimal.ROUND_HALF_UP); 
        grandTtlTopScore = bd2.intValue();
        
        evaluation.setGrandScore(grandTtlScore);
        evaluation.setGrandTopScore(grandTtlTopScore);
        
        jdbcTemplate.update(
				"UPDATE evaluation " +
				"SET " +
				"company_short_name =:shortName," +
				"product_name =:productName," +
				"product_category =:productCategory," +
				"transaction_condition =:tranCond," +
				"selection_reason =:selectionReason," +
				"comment =:comment," +
				//"total_point =:totalPoint, " +
				
				"service_category =:serviceCategory, " +
				"service_industry =:serviceIndustry, " +
				"service_scope =:serviceScope, " +
				"major_accounts =:majorAccounts, " +
				"volume_per_year =:volumePerYear, " +
				"num_of_biz_sites_nationwide =:numOfBizSitesNationwide, " +
				"forwarding_company =:forwardingCompany, " +
				"info_fix_assets_nationwide =:infoFixAssetsNationwide, " +
				"num_of_empl_nationwide =:numOfEmplNationwide, " +
				"applicant =:applicant, " +
				"service2Category =:service2Category, " +
				"serviceName =:serviceName, " +
				"date2 =:date2, " +
				"grand_ttl_weight_score =:grandScore, "+
				"grand_ttl_top_weight_score =:grandTopScore "+
				"WHERE id=:id",
				new BeanPropertySqlParameterSource(evaluation));
	}

	@Override
	public Long findFirstEvalId(long vendorId, int statusIsEffective) {
		List<Map<String, Object>> fidList = jdbcTemplate.queryForList(
					"SELECT id FROM evaluation WHERE vendor_id =? AND status = ? AND registered_date = (SELECT MIN(registered_date) FROM evaluation WHERE vendor_id =? AND status = ?)", 
					vendorId, statusIsEffective, vendorId, statusIsEffective);
		
		if(fidList != null && fidList.size() > 0){
			return Long.valueOf(fidList.get(0).get("id").toString());
		}else{
			return Long.valueOf(0);
		}

	}

	@Override
	public String getLatestEval(long vendorId) {
		return jdbcTemplate.queryForObject(
					"SELECT DATE_FORMAT(MAX(registered_date),'%Y-%m-%d') FROM evaluation WHERE vendor_id = ? AND status = 20", 
					String.class, 
					vendorId);
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: getLatestEval</p> 
	 * <p>Description: 获取供应商的有效评估信息数量</p> 
	 * @param vendorId
	 * @return 
	 * @see cn.sscs.vendormanagement.evaluation.IEvaluationService#getLatestEval(long)
	 */
	@Override
	public int hasEvals(long vendorId,int status) {
		return jdbcTemplate.queryForInt(
				" SELECT COUNT(*) " +
				" FROM  evaluation e " +
				" WHERE   e.vendor_id = ? "+
				" AND     e.status = ? ", // 有效的评估状态
				vendorId, 
				status);
	}
    @Override
    public List<String> getCostcenterBySuperCompany(String supercompany) {
        String sql = 
                "SELECT " +
                    "DISTINCT(a.division) " +
                "FROM " +
                    "authorities a, user_info ui " +
                "WHERE " +
                    "a.username = ui.id AND ui.valid = 1 AND a.usercompany = ? " +
                "ORDER BY a.division ASC";
            ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>(){
                @Override
                public String mapRow(ResultSet rs, int rowNum) throws SQLException {
                    String name = rs.getString("division");
                    return name;
                }
            };
            return jdbcTemplate.query(sql, mapper, supercompany);
    }
}
