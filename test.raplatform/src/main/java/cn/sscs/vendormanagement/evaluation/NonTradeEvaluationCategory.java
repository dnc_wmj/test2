package cn.sscs.vendormanagement.evaluation;

import java.util.List;

public class NonTradeEvaluationCategory {
	private int formId;
	private String content;
	private int categoryId;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private int ttlWeightScore;
	private int ttlTopWeightScore;
	private List<NonTradeSubCategorys> subCategorysList;

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public int getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(int categoryId) {
		this.categoryId = categoryId;
	}

	public void setSubCategorysList(List<NonTradeSubCategorys> subCategorysList) {
		this.subCategorysList = subCategorysList;
	}

	public List<NonTradeSubCategorys> getSubCategorysList() {
		return subCategorysList;
	}

	public int getCategoryLength() {
		int length = 0;
		for (NonTradeSubCategorys subCategorys : subCategorysList) {
			length += subCategorys.getSubCategorysLength();
		}
		return length;
	}

	public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}

	public int getTtlScore() {
		return ttlScore;
	}

	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}

	public int getTtlTopScore() {
		return ttlTopScore;
	}

	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}

	public int getTtlWeightScore() {
		return ttlWeightScore;
	}

	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}

	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}

	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}

}
