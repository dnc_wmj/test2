package cn.sscs.vendormanagement.evaluation;

import cn.sscs.vendormanagement.vendor.Identity;

public class Submit {

    private int costcenter = 0;
    
	private String l1 = null;
	
	private String l2 = null;
	
	private String ceo = null;
	
	private String hd = null;
	
	private String fm = null;
	
	private String dm = null;

	private int result = -1;
	
	private Identity identity = new Identity();
	
	/**
	 * @return the l1
	 */
	public String getL1() {
		return l1;
	}

	/**
	 * @param l1 the l1 to set
	 */
	public void setL1(String l1) {
		this.l1 = l1;
	}

	/**
	 * @return the l2
	 */
	public String getL2() {
		return l2;
	}

	/**
	 * @param l2 the l2 to set
	 */
	public void setL2(String l2) {
		this.l2 = l2;
	}

	/**
	 * 
	 * @return
	 */
	public String getCeo() {
		return ceo;
	}
	
	/**
	 * 
	 * @param ceo
	 */
	public void setCeo(String ceo) {
		this.ceo = ceo;
	}
	
	/**
	 * @return the hd
	 */
	public String getHd() {
		return hd;
	}

	/**
	 * @param hd the hd to set
	 */
	public void setHd(String hd) {
		this.hd = hd;
	}

	/**
	 * @return the fm
	 */
	public String getFm() {
		return fm;
	}

	/**
	 * @param fm the fm to set
	 */
	public void setFm(String fm) {
		this.fm = fm;
	}

	/**
	 * @return the dm
	 */
	public String getDm() {
		return dm;
	}

	/**
	 * @param dm the dm to set
	 */
	public void setDm(String dm) {
		this.dm = dm;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getResult() {
		return result;
	}
	
	/**
	 * 
	 * @param result
	 */
	public void setResult(int result) {
		this.result = result;
	}

	public Identity getIdentity() {
		return identity;
	}

	public void setIdentity(Identity identity) {
		this.identity = identity;
	}

    public int getCostcenter() {
        return costcenter;
    }

    public void setCostcenter(int costcenter) {
        this.costcenter = costcenter;
    }
}
