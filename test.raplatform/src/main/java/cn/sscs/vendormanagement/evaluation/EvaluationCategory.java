package cn.sscs.vendormanagement.evaluation;

import java.util.List;

public class EvaluationCategory {

	private long id = -1;
	
	private String name = null;
	
	private List<EvaluationContent> evaluationContents = null;
	
	private int maxPoint = -1;
	
	private double weight = -1;
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<EvaluationContent> getEvaluationContents() {
		return evaluationContents;
	}
	
	/**
	 * 
	 * @param evaluationContext
	 */
	public void setEvaluationContext(List<EvaluationContent> evaluationContext) {
		this.evaluationContents = evaluationContext;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getEvaluationItemLength() {
		int length = 0;
		for (EvaluationContent evaluationContent : evaluationContents) {
			length += evaluationContent.getEvaluationItemLength();
		}
		
		return length;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getMaxPoint() {
		return maxPoint;
	}
	
	/**
	 * 
	 * @param maxPoint
	 */
	public void setMaxPoint(int maxPoint) {
		this.maxPoint = maxPoint;
	}
	
	/**
	 * 
	 * @return
	 */
	public double getWeight() {
		return weight;
	}
	
	/**
	 * 
	 * @param weight
	 */
	public void setWeight(double weight) {
		this.weight = weight;
	}
}
