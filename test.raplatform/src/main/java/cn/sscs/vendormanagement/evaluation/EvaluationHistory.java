package cn.sscs.vendormanagement.evaluation;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class EvaluationHistory {
	
	private long id = -1;
	
	private String totalPoint = "";
	
	private int grandScore = 0;

	private int grandTopScore = 0;
	
	private Date registeredDate = null;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the totalPoint
	 */
	public String getTotalPoint() {
		return totalPoint;
	}

	/**
	 * @param totalPoint the totalPoint to set
	 */
	public void setTotalPoint(String totalPoint) {
		this.totalPoint = totalPoint;
	}
	
	/**
	 * @return the registeredDate
	 */
	public Date getRegisteredDate() {
		return registeredDate;
	}

	/**
	 * @param registeredDate the registeredDate to set
	 */
	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public int getGrandScore() {
		return grandScore;
	}

	public void setGrandScore(int grandScore) {
		this.grandScore = grandScore;
	}

	public int getGrandTopScore() {
		return grandTopScore;
	}

	public void setGrandTopScore(int grandTopScore) {
		this.grandTopScore = grandTopScore;
	}
	
	public String getShanghaiRegisteredDate(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		return sdf.format(registeredDate);
	}
	
}
