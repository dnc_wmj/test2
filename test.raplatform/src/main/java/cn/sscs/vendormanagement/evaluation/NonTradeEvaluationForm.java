package cn.sscs.vendormanagement.evaluation;

import java.math.BigDecimal;
import java.util.List;

public class NonTradeEvaluationForm {
	// private int displayOrder;
	private int formId;
	private int businessType;
	private String content;
	private int itemsCount;
	private int ttlScore;
	private int ttlTopScore;
	private int ttlWeightScore;
	private int ttlTopWeightScore;
	private List<NonTradeEvaluationCategory> categoryList;
	private double weight;

	// public int getDisplayOrder() {
	// return displayOrder;
	// }
	//
	// public void setDisplayOrder(int displayOrder) {
	// this.displayOrder = displayOrder;
	// }

	public int getFormId() {
		return formId;
	}

	public void setFormId(int formId) {
		this.formId = formId;
	}

	public int getBusinessType() {
		return businessType;
	}

	public void setBusinessType(int businessType) {
		this.businessType = businessType;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public List<NonTradeEvaluationCategory> getCategoryList() {
		return categoryList;
	}

	public void setCategoryList(List<NonTradeEvaluationCategory> categoryList) {
		this.categoryList = categoryList;
	}

	public int getFormLength() {
		int length = 0;
		for (NonTradeEvaluationCategory category : categoryList) {
			length += category.getCategoryLength();
		}
		return length;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public double getWeight() {
		return weight;
	}
	
	public String getShowWeight() {
		BigDecimal bd = new BigDecimal(weight * 100); 
        bd = bd.setScale(0, BigDecimal.ROUND_HALF_UP); 
		return bd.intValue() +"%";
	}
	
	public int getItemsCount() {
		return itemsCount;
	}

	public void setItemsCount(int itemsCount) {
		this.itemsCount = itemsCount;
	}

	public int getTtlScore() {
		return ttlScore;
	}

	public void setTtlScore(int ttlScore) {
		this.ttlScore = ttlScore;
	}

	public int getTtlTopScore() {
		return ttlTopScore;
	}

	public void setTtlTopScore(int ttlTopScore) {
		this.ttlTopScore = ttlTopScore;
	}

	public int getTtlWeightScore() {
		return ttlWeightScore;
	}

	public void setTtlWeightScore(int ttlWeightScore) {
		this.ttlWeightScore = ttlWeightScore;
	}

	public int getTtlTopWeightScore() {
		return ttlTopWeightScore;
	}

	public void setTtlTopWeightScore(int ttlTopWeightScore) {
		this.ttlTopWeightScore = ttlTopWeightScore;
	}
	
}
