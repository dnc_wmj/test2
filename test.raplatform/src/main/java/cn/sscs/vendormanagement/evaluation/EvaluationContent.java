package cn.sscs.vendormanagement.evaluation;

import java.util.List;

public class EvaluationContent {

	private long id = -1;
	
	private String content = null;
	
	private List<EvaluationItem> evaluationItems = null;
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getContent() {
		return content;
	}

	/**
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getEvaluationItemLength() {
		return evaluationItems.size();
	}
	public int getEvaluationNonItemLength() {
		int i = 0;
		for (EvaluationItem evaluationItem : evaluationItems){
			i += evaluationItem.getEvaluationItemItemLength();
		}
		return i;
	}
	/**
	 * 
	 * @return
	 */
	public List<EvaluationItem> getEvaluationItems() {
		return evaluationItems;
	}
	
	/**
	 * 
	 * @param evaluationItems
	 */
	public void setEvaluationItems(List<EvaluationItem> evaluationItems) {
		this.evaluationItems = evaluationItems;
	}
}
