package cn.sscs.vendormanagement.evaluation;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.sscs.vendormanagement.GeneralException;
import cn.sscs.vendormanagement.User;
import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.VndMgmntUser;
import cn.sscs.vendormanagement.data.IDataService;
import cn.sscs.vendormanagement.data.SystemSetting;
import cn.sscs.vendormanagement.vendor.Attachment;
import cn.sscs.vendormanagement.vendor.IVendorService;
import cn.sscs.vendormanagement.vendor.VendorBasicInfo;
import cn.sscs.vendormanagement.workflow.IWorkflowService;
import cn.sscs.vendormanagement.workflow.WorkflowRule;

@Controller
@Transactional
@RequestMapping(value = "/evaluation")
public class EvaluationController {
	public static int REEVAL_FLAG = 1;
	public static int NOT_REEVAL_FLAG = 0;
	@Autowired
	private IVendorService vendorService = null;

	@Autowired
	private IEvaluationService evaluationService = null;

	@Autowired
	private IWorkflowService workflowService = null;

	@Autowired
	private IDataService dataService = null;
	
	@InitBinder  
	public void InitBinder(WebDataBinder dataBinder) {  
	    dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {  
	        public void setAsText(String value) {  
	            try {  
	                setValue(new SimpleDateFormat("yyyy/MM/dd").parse(value));  
	            } catch(ParseException e) {  
	                setValue(null);  
	            }
	        }  
	        public String getAsText() {
	        	return (getValue() == null) ? null : new SimpleDateFormat("yyyy/MM/dd").format((Date) getValue());
	        }          
	  
	    });  
	}
	/**
	 * 
	 * 
	 * @param vendorId
	 * @param model
	 * @return
	 */
	private String getEvaluationFormUrl(long vendorId, Model model){
	 // kind businessType
        // 1 : trade 贸易
        // 2 : common 共通
        // old
        // 3 : non-trade()
        // 4 : non-trade()
        // 5 : non-trade()
        // 6 : non-trade()
        // 7 : non-trade()
        // 8 : non-trade()
        // 9 : non-trade()
        // new
        // 10 : non-trade()
        // 11 : non-trade()
        // 12 : non-trade()
        // 13 : non-trade()
        // 14 : non-trade()
        // 15 : non-trade()
        // 16 : non-trade()
        int businessType = vendorService.getBusiessType(vendorId);

        Evaluation evaluation = new Evaluation();
        evaluation.setVendorId(vendorId);
        evaluation.setBusinessType(businessType);

        evaluation.setApplicant(Utils.getUser().getName());
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        evaluation.setDate2(sdf.format(Calendar.getInstance().getTime()));

        model.addAttribute("vendor_id", vendorId);
        model.addAttribute("identity", vendorService.getIdentity(vendorId));

        model.addAttribute("evaluation", evaluation);

        // if(businessType >= 3 && businessType <= 9){
        // return "evaluation/evaluation_non_trade_form";
        // }else{
        // return "evaluation/evaluation_form";
        // }
        // old non trade
        if (businessType >= 3 && businessType <= 9) {
            model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
            return "evaluation/evaluation_non_trade_form";
            // non trade by 2012
        } else if (Utils.isNonTrade(businessType)) {
            long evaluationId = 0;
            model.addAttribute("nonTradeEvaluationForm", 
                    evaluationService.getNonTradeEvaluationForm(businessType, NOT_REEVAL_FLAG, evaluationId));
            return "evaluation/evaluation_non_trade_form_second";
        } else {
            model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
            return "evaluation/evaluation_form";
        }
	}
	/**
	 * 
	 * @Title:       getEvaluationForm 
	 * @Description:  ( 新供应商申请 根据不同businessType 跳转评估页面   ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 2, 2012
	 */
	@RequestMapping(value = "/register/{vendorId}", method = RequestMethod.GET)
	public String getEvaluationForm(@PathVariable("vendorId") long vendorId, Model model) {
		return getEvaluationFormUrl(vendorId, model);
	}

	/**
     * 
     * @Title:       getEvaluationFormTmp
     * @Description:  tmp
     * @param        @param vendorId
     * @param        @param model
     * @param        @return     
     * @return       String     
     * @throws 
     * User:         dnc
     * DataTime:     Jul 2, 2012
     */
    @RequestMapping(value = "/register/{vendorId}/{id}", method = RequestMethod.GET)
    public String getEvaluationFormFromTmp(@PathVariable("vendorId") long vendorId, 
            Model model, @PathVariable("id") String id) {
        model.addAttribute("tmpRuleId", id);
        return getEvaluationFormUrl(vendorId, model);
    }
    
	/**
	 * 
	 * @Title:       getReEvaluationForm 
	 * @Description:  ( 再评估跳转  ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
	@RequestMapping(value = "/reeval/{vendorId}", method = RequestMethod.GET)
	public String getReEvaluationForm(@PathVariable("vendorId") long vendorId, Model model) {
		int businessType = vendorService.getBusiessType(vendorId);
		
		Evaluation evaluation = new Evaluation();
		evaluation.setVendorId(vendorId);
		evaluation.setBusinessType(businessType);

		evaluation.setApplicant(Utils.getUser().getName());
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
		evaluation.setDate2(sdf.format(Calendar.getInstance().getTime()));

		// for show vendorCode
		String vendorCode = vendorService.findVendorCodeByid(vendorId);
		evaluation.setVendorCode(vendorCode);
		
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("identity", vendorService.getIdentity(vendorId));

		model.addAttribute("evaluation", evaluation);
		model.addAttribute("reeval", true);
		// old non trade
		if (businessType >= 3 && businessType <= 9) {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
			return "evaluation/evaluation_non_trade_form";
			// non trade 2012
		} else if (Utils.isNonTrade(businessType)) {
			/* zhengtong add 2012-7-6 17:18:06 再评估界面提示变更标识 */
			model.addAttribute("isReeval", true);
			/* zhengtong add end 2012-7-6 17:18:06 */
			
			model.addAttribute("nonTradeEvaluationForm", evaluationService.getNonTradeEvaluationForm(businessType, REEVAL_FLAG, 0));
			return "evaluation/evaluation_non_trade_form_second";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
			return "evaluation/evaluation_form";
		}
	}

	/**
	 * 
	 * @Title:       getUpdateEvaluationFormForApplication 
	 * @Description: ( 新供应商申请 确认画面响应评估编辑按钮  ) 
	 * @param        @param id
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 12, 2012
	 */
	@RequestMapping(value = "/updateform/{id}", method = RequestMethod.GET)
	public String getUpdateEvaluationFormForApplication(@PathVariable("id") long id, Model model) {

		int kind = processUpdateEvaluationForm(id, model, Evaluation.STATUS_IN_APPLYING);
		// old non trade
		if (kind >= 3 && kind <= 9) {
			return "evaluation/evaluation_update_non_form";
			// non trade 2012
		} else if (Utils.isNonTrade(kind)) {
			return "evaluation/evaluation_update_non_form_second";
		} else {
			return "evaluation/evaluation_update_form";
		}

	}

	// (temp save) evaluation edit //我的申请->一时保存(非贸)->评估编辑按钮
	@RequestMapping(value = "/tmpsave/updateform/{id}/{tmpRuleId}", method = RequestMethod.GET)
	public String getUpdateEvaluationFormForApplication(@PathVariable("id") long id, 
			@PathVariable("tmpRuleId") String tmpRuleId, Model model) {

		model.addAttribute("tmpRuleId", tmpRuleId);

		Evaluation evaluation = evaluationService.findEvaluationById(id, Evaluation.STATUS_IS_TEMP_SAVE);
		// for show vendorCode
		String vendorCode = vendorService.findVendorCodeByid(evaluation.getVendorId());
		evaluation.setVendorCode(vendorCode);
		model.addAttribute("vendor_id", evaluation.getVendorId());
		model.addAttribute("eval_id", id);
		model.addAttribute("identity", vendorService.getIdentity(evaluation.getVendorId()));
		model.addAttribute("evaluation", evaluation);
		
		int kind = evaluation.getBusinessType();
		if (Utils.isNonTrade(kind)) {
			// 判断是否是再评估
			Long fId = evaluationService.findFirstEvalId(evaluation.getVendorId(), Evaluation.STATUS_IS_EFFECTIVE);
			int flag = 0;
			if (fId != 0) {
				flag = (fId == evaluation.getId()) ? 0 : 1;
			}
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(evaluation.getBusinessType(), flag, id));
			return "evaluation/evaluation_update_non_form_second";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(evaluation.getBusinessType()));
			if (kind >= 3 && kind <= 9) {
				// 旧非贸
				return "evaluation/evaluation_update_non_form";
			} else {
				return "evaluation/evaluation_update_form";
			}

		}
	}

	// 再评估时，确认画面修改评估数据
	@RequestMapping(value = "/updateform/reeval/{id}", method = RequestMethod.GET)
	public String getUpdateReEvaluationFormForApplication(@PathVariable("id") long id, Model model) {
		model.addAttribute("isReeval", true);
		int kind = processUpdateEvaluationForm(id, model, Evaluation.STATUS_IN_APPLYING);
		//TODO 只读显示vendor code
		if (kind >= 3 && kind <= 9) {
			return "evaluation/evaluation_update_non_form";
		} else if (Utils.isNonTrade(kind)) {
			return "evaluation/evaluation_update_non_form_second";
		} else {
			return "evaluation/evaluation_update_form";
		}
	}

	// watch old evaluation
	@RequestMapping(value = "/view/{id}", method = RequestMethod.GET)
	public String view(@PathVariable("id") long id, Model model) {

		Evaluation eval = evaluationService.findEvaluationById(id, Evaluation.STATUS_IS_EFFECTIVE);
		model.addAttribute("evaluation", eval);
		if (Utils.isNonTrade(eval.getBusinessType())) {
			// 这里的status与供应商查询中得到过去评估list的status相同
			Long fId = evaluationService.findFirstEvalId(eval.getVendorId(), Evaluation.STATUS_IS_EFFECTIVE);
			int flag = 0;
			if (fId != 0) {
				flag = (fId == eval.getId()) ? 0 : 1;
			}
			model.addAttribute("nonTradeEvaluationForm", evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), flag, id));
			return "evaluation/evaluation_detail_non_trade";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			return "evaluation/evaluation_detail";
		}

	}
	
	@RequestMapping(value = "/updateform/{id}/{wfid}", method = RequestMethod.GET)
	public String getUpdateEvaluationFormForWorkflow(@PathVariable("id") long id, @PathVariable("wfid") String wfid, Model model) {

		int kind = processUpdateEvaluationForm(id, model, Evaluation.STATUS_IN_APPLICATION);

		model.addAttribute("wfid", wfid);
		if (kind >= 3 && kind <= 9) {
			return "evaluation/evaluation_update_non_form";
		} else if (Utils.isNonTrade(kind)) {
			return "evaluation/evaluation_update_non_form_second";
		} else {
			return "evaluation/evaluation_update_form";
		}
		// return "evaluation/evaluation_update_form";
	}

	 
	/**
	 * 
	 * @Title:       getUpdateEvaluationFormForMaintainance 
	 * @Description:  ( Help Desk's permission user edit vendor's evaluation.  ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value = "/updateform/maintainance/{vendor_id}", method = RequestMethod.GET)
	public String getUpdateEvaluationFormForMaintainance(@PathVariable("vendor_id") long vendorId, Model model) {

		Evaluation evaluation = evaluationService.findLatestEvaluationByVendorId(vendorId);
		model.addAttribute("vendor_id", evaluation.getVendorId());
		model.addAttribute("eval_id", evaluation.getId());
		model.addAttribute("identity", vendorService.getIdentity(evaluation.getVendorId()));
		model.addAttribute("evaluation", evaluation);

		model.addAttribute("forMaintainance", true);

		// return "evaluation/evaluation_update_form";
		if (evaluation.getBusinessType() >= 3 && evaluation.getBusinessType() <= 9) {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(evaluation.getBusinessType()));
			return "evaluation/evaluation_update_non_form";
		} else if (Utils.isNonTrade(evaluation.getBusinessType())) {
			// 这里的status与供应商查询中得到过去评估list的status相同
			Long fId = evaluationService.findFirstEvalId(evaluation.getVendorId(), Evaluation.STATUS_IS_EFFECTIVE);
			int flag = 0;
			if (fId != 0) {
				flag = (fId == evaluation.getId()) ? 0 : 1;
			}
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(evaluation.getBusinessType(), flag, evaluation.getId()));
			return "evaluation/evaluation_update_non_form_second";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(evaluation.getBusinessType()));
			return "evaluation/evaluation_update_form";
		}
	}

	/**
	 * 
	 * @param vendorId
	 * @param model
	 * @return
	 */
	private int processUpdateEvaluationForm(long id, Model model, int status) {
		Evaluation evaluation = evaluationService.findEvaluationById(id);
		
		// new BigDecimal(evaluation.getTotalPoint()).compareTo(new
		// BigDecimal(50)) < 0
		if (evaluation.getTotalPointForCompare() != -1) {
			status = evaluation.getTotalPointForCompare() <= 49 ? Evaluation.STATUS_IS_EFFECTIVE : status;
		}
		evaluation = evaluationService.findEvaluationById(id, status);
		// for show vendorCode
		String vendorCode = vendorService.findVendorCodeByid(evaluation.getVendorId());
		evaluation.setVendorCode(vendorCode);
		model.addAttribute("vendor_id", evaluation.getVendorId());
		model.addAttribute("eval_id", id);
		model.addAttribute("identity", vendorService.getIdentity(evaluation.getVendorId()));
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			// 判断是否是再评估
			Long fId = evaluationService.findFirstEvalId(evaluation.getVendorId(), Evaluation.STATUS_IS_EFFECTIVE);
			int flag = 0;
			if (fId != 0) {
				flag = (fId == evaluation.getId()) ? 0 : 1;
			}
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(evaluation.getBusinessType(), flag, id));
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(evaluation.getBusinessType()));
		}
		model.addAttribute("evaluation", evaluation);
		return evaluation.getBusinessType();

	}

	/**
	 * 
	 * @Title:       registerEvaluation 
	 * @Description:  ( 新供应商申请 - 评估 from 提交  ) 
	 * @param        @param vendorId
	 * @param        @param evaluation
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 5, 2012
	 */
	@RequestMapping(value = "/register/{vendorId}", method = RequestMethod.POST)
	public String registerEvaluation(@PathVariable("vendorId") long vendorId, @Valid Evaluation evaluation, Model model) {

		int businessType = vendorService.getBusiessType(vendorId);

		evaluation.setVendorId(vendorId);
		evaluation.setBusinessType(businessType);
		
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
		    /*表单验证*/
			if (!voidateEvaluation(evaluation)) {
				model.addAttribute("vendor_id", vendorId);
				model.addAttribute("identity", vendorService.getIdentity(vendorId));
				model.addAttribute("evaluation", evaluation);
				long evaluationId = 0;
				model.addAttribute("nonTradeEvaluationForm", 
						evaluationService.getNonTradeEvaluationForm(businessType, NOT_REEVAL_FLAG, evaluationId));
				return "evaluation/evaluation_non_trade_form_second";
			}
			
			long id = evaluationService.store(evaluation);
			evaluationService.storeNonTradeEvaluationResultDetail(evaluation);
			return "redirect:/evaluation/confirm/" + vendorId + "/" + id;
		}else{
			// check vendor code 
			if(_voidateEvaluation(evaluation)){
				
				model.addAttribute("vendor_id", vendorId);
				model.addAttribute("identity", vendorService.getIdentity(vendorId));
				model.addAttribute("evaluation", evaluation);
				model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
				return "evaluation/evaluation_form";
			}
		}
		// 评估一，选择既存供应商并且供应商编号不为空
		if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
			vendorService.updateVendorCode(vendorId, evaluation.getVendorCode().trim());
		}
		evaluation.setTotalPoint(evaluationService.calculatePoint(businessType, evaluation.getPoint()));

		long id = evaluationService.store(evaluation);

		return "redirect:/evaluation/confirm/" + vendorId + "/" + id;
	}

	/**
     * 
     * @Title:       registerEvaluation 
     * @Description:  ( 新供应商申请 - 评估 from 提交  ) 
     * @param        @param vendorId
     * @param        @param evaluation
     * @param        @param model
     * @param        @return     
     * @return       String     
     * @throws 
     * User:         dnc
     * DataTime:     Jul 5, 2012
     */
    @RequestMapping(value = "/register/{vendorId}/{id}", method = RequestMethod.POST)
    public String registerEvaluation(@PathVariable("vendorId") long vendorId, 
            @Valid Evaluation evaluation, Model model, @PathVariable("id") String tmpRuleId
            ) {
        
        model.addAttribute("tmpRuleId", tmpRuleId);
        int businessType = vendorService.getBusiessType(vendorId);

        evaluation.setVendorId(vendorId);
        evaluation.setBusinessType(businessType);
        
        if (Utils.isNonTrade(evaluation.getBusinessType())) {
            /*表单验证*/
            if (!voidateEvaluation(evaluation)) {
                model.addAttribute("vendor_id", vendorId);
                model.addAttribute("identity", vendorService.getIdentity(vendorId));
                model.addAttribute("evaluation", evaluation);
                long evaluationId = 0;
                model.addAttribute("nonTradeEvaluationForm", 
                        evaluationService.getNonTradeEvaluationForm(businessType, NOT_REEVAL_FLAG, evaluationId));
                return "evaluation/evaluation_non_trade_form_second";
            }
            evaluation.setStatus(Evaluation.STATUS_TMPSAVE);
            long id = evaluationService.store(evaluation);
            evaluationService.storeNonTradeEvaluationResultDetail(evaluation);
            // 更新新的evalId到临时workflowrule里
            WorkflowRule tmprule = workflowService.getWorkflowRule(tmpRuleId);
            tmprule.setEvalId(id);
            workflowService.updateWorkflowRule(tmprule); 
            return "redirect:/evaluation/tmpsave/confirm/" + evaluation.getVendorId() + "/" + id + "/" + tmpRuleId;
        }else{
            // check vendor code 
            if(_voidateEvaluation(evaluation)){
                
                model.addAttribute("vendor_id", vendorId);
                model.addAttribute("identity", vendorService.getIdentity(vendorId));
                model.addAttribute("evaluation", evaluation);
                model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(businessType));
                return "evaluation/evaluation_form";
            }
        }
        
        // 评估一，选择既存供应商并且供应商编号不为空
        if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
            vendorService.updateVendorCode(vendorId, evaluation.getVendorCode().trim());
        }
        evaluation.setTotalPoint(evaluationService.calculatePoint(businessType, evaluation.getPoint()));
        evaluation.setStatus(Evaluation.STATUS_TMPSAVE);
        long id = evaluationService.store(evaluation);
        // 更新新的evalId到临时workflowrule里
        WorkflowRule tmprule = workflowService.getWorkflowRule(tmpRuleId);
        tmprule.setEvalId(id);
        workflowService.updateWorkflowRule(tmprule); 
        //return "redirect:/evaluation/confirm/" + vendorId + "/" + id;
        return "redirect:/evaluation/tmpsave/confirm/" + evaluation.getVendorId() + "/" + id + "/" + tmpRuleId;
    }
    
	/**
	 * without non-trade
	 * 
	 * @param evaluation
	 * @return
	 */
	private boolean _voidateEvaluation(Evaluation evaluation) {
		return evaluation.getTranCond() == 1 && "".equals(evaluation.getVendorCode().trim());
	}


	/**
	 * 
	 * @Title:       voidateEvaluation 
	 * @Description:  ( test non trade form ) 
	 * @param        @param evaluation
	 * @param        @return     
	 * @return       boolean     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 5, 2012
	 */
	private boolean voidateEvaluation(Evaluation evaluation) {
		boolean result = true;
		int[] mustCheck = evaluation.getMustCheck();
		boolean[] mResult = new boolean[mustCheck.length];

		for (int i = 0; i < mustCheck.length; i++) {
			if (mustCheck[i] == 1) {
				if ("2".equals(evaluation.getYesNo()[i]) && "0".equals(evaluation.getNon_point()[i])) {
					mResult[i] = false;
					result = false;
				} else if ("1".equals(evaluation.getPriority()) && "0".equals(evaluation.getYesNo()[i])) {
					result = false;
				} else {
					mResult[i] = true;
				}
			} else {
				mResult[i] = true;
				continue;
			}
		}
		evaluation.setmResult(mResult);
		evaluation.setAfterValidte(!result);
		if(evaluation.getTranCond() == 1 && "".equals(evaluation.getVendorCode().trim())){
			result = false;
		}else if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
			vendorService.updateVendorCode(evaluation.getVendorId(), evaluation.getVendorCode().trim());
		}
		
		return result;
	}

	@RequestMapping(value = "/reeval/{vendorId}", method = RequestMethod.POST)
	public String registerReEvaluation(@PathVariable("vendorId") long vendorId, @Valid Evaluation evaluation, Model model) {

		int businessType = vendorService.getBusiessType(vendorId);

		evaluation.setVendorId(vendorId);
		evaluation.setBusinessType(businessType);

		if (Utils.isNonTrade(businessType)) {
			long id = evaluationService.store(evaluation);
			evaluationService.storeNonTradeEvaluationResultDetail(evaluation);
			return "redirect:/evaluation/reeval/confirm/" + vendorId + "/" + id;
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(businessType, evaluation.getPoint()));
			long id = evaluationService.store(evaluation);
			return "redirect:/evaluation/reeval/confirm/" + vendorId + "/" + id;
		}
	}

	/**
	 * 
	 * @Title:       confirmEvaluation 
	 * @Description: ( 这里用一句话描述这个方法的作用  ) 
	 * @param        @param vendorId
	 * @param        @param evalId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 12, 2012
	 */
	@RequestMapping(value = "/confirm/{vendorId}/{evalId}", method = RequestMethod.GET)
	public String confirmEvaluation(@PathVariable("vendorId") long vendorId, @PathVariable("evalId") long evalId, Model model) {
		/*郑同 modif 2012年7月13日10:05:52 start*/
		ArrayList<Integer> statusList = new ArrayList<Integer>();
		statusList.add(VendorBasicInfo.STATUS_IN_APPLYING);
		statusList.add(VendorBasicInfo.STATUS_CLOSE);
		
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, statusList);
		/*郑同 modif 2012年7月13日10:05:52 end*/
		model.addAttribute("attachments_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

		//从新new一个list
		statusList = new ArrayList<Integer>();
		statusList.add(Evaluation.STATUS_IN_APPLYING); // 提出
		statusList.add(Evaluation.STATUS_IS_EFFECTIVE); //有效
		Evaluation eval = evaluationService.findEvaluationById(evalId, statusList);
		// new BigDecimal(eval.getTotalPoint()).compareTo(new BigDecimal(50)) <
		// 0
		if (Utils.isNonTrade(eval.getBusinessType())) {
			// approval of non trade need CEO. 
			eval.setTotalPointForCompare(75);
		} else {
			if (eval.getTotalPointForCompare() <= 49) {
				vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, vendorId);
				evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
			}
		}
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("eval_id", evalId);
		model.addAttribute("basic_info", vendorBasicInfo);
		model.addAttribute("evaluation", eval);
		// model.addAttribute("evaluationCategories",
		// evaluationService.getEvaluationCategories(eval.getBusinessType()));
		setL1L2Approval(vendorBasicInfo, model);

		// SSGE、SSV 的审批人员没有CEO以上级别
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		if (parentCompanyIsSSCS) {
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
		}
		model.addAttribute("isSSCS", parentCompanyIsSSCS);

		model.addAttribute("editable", true);
		model.addAttribute("internal", false);
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		model.addAttribute(new Submit());

		model.addAttribute("attachments", vendorService.findAttachment(vendorId));

		if (Utils.isNonTrade(eval.getBusinessType())) {
			long evaluationId = eval.getId();
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
			return "evaluation/non_trade_confirm";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			return "evaluation/confirm";
		}

	}
	//再评估确认画面
	@RequestMapping(value = "/reeval/confirm/{vendorId}/{evalId}", method = RequestMethod.GET)
	public String confirmReEvaluation(@PathVariable("vendorId") long vendorId, @PathVariable("evalId") long evalId, Model model) {
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);

		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

        boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
        model.addAttribute("isSSCS", parentCompanyIsSSCS);

		Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IN_APPLYING);
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("eval_id", evalId);
		model.addAttribute("basic_info", vendorBasicInfo);
		model.addAttribute("evaluation", eval);
		// model.addAttribute("evaluationCategories",
		// evaluationService.getEvaluationCategories(eval.getBusinessType()));

		setL1L2Approval(vendorBasicInfo, model);

		if (parentCompanyIsSSCS) {
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
		}

		model.addAttribute("isReeval", true);
		model.addAttribute("editable", true);
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		model.addAttribute(new Submit());
		model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		if (Utils.isNonTrade(eval.getBusinessType())) {
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), REEVAL_FLAG, evalId));
			return "evaluation/reeval_confirm_non_trade";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			boolean hasEvaluation = evaluationService.hasLatestEvaluationByVendorId(vendorId);
			model.addAttribute("isImportExterVendor", !hasEvaluation && Utils.isTrade(eval.getBusinessType()) && vendorBasicInfo.getIdentity().getVendorType() == 0);
			return "evaluation/reeval_confirm";
		}
	}

	 
	 /**
	  * 
	  * @Title:       updateEvaluationForApplication 
	  * @Description:  (  新供应商申请时，确认画面修改评估信息后提交。  ) 
	  * @param        @param evaluation
	  * @param        @param model
	  * @param        @return     
	  * @return       String     
	  * @throws 
	  * User:         dnc
	  * DataTime:     Jul 12, 2012
	  */
	@RequestMapping(value = "/update", method = RequestMethod.POST)
	public String updateEvaluationForApplication(@Valid Evaluation evaluation, Model model) {
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			evaluationService.updateNonTradeEvaluationResultDetail(evaluation);
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(evaluation.getBusinessType(), evaluation.getPoint()));
			evaluationService.updateEvaluation(evaluation);
		}
		return "redirect:/evaluation/confirm/" + evaluation.getVendorId() + "/" + evaluation.getId();
	}

	/**
	 * （temp save） 新供应商申请时，修改评估信息后提交。
	 * 
	 * @param evaluation
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tmpsave/update/{id}", method = RequestMethod.POST)
	public String updateEvaluationForTmpsaveApplication(@PathVariable("id") String id, @Valid Evaluation evaluation, Model model) {
		// TODO 增加供应商编号的服务器端check &　 
		// 更新
		if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
			vendorService.updateVendorCode(evaluation.getVendorId(), evaluation.getVendorCode().trim());
		}
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			evaluationService.updateNonTradeEvaluationResultDetail(evaluation);
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(evaluation.getBusinessType(), evaluation.getPoint()));
			evaluationService.updateEvaluation(evaluation);
		}
		return "redirect:/evaluation/tmpsave/confirm/" + evaluation.getVendorId() + "/" + evaluation.getId() + "/" + id;
	}

	@RequestMapping(value = "/reeval/update", method = RequestMethod.POST)
	public String updateEvaluationForReeval(@Valid Evaluation evaluation, Model model) {
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			evaluationService.updateNonTradeEvaluationResultDetail(evaluation);
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(evaluation.getBusinessType(), evaluation.getPoint()));
			evaluationService.updateEvaluation(evaluation);
		}
		return "redirect:/evaluation/reeval/confirm/" + evaluation.getVendorId() + "/" + evaluation.getId();
	}

	/**
	 * Help Desk's permission submit edited evaluation.
	 * 
	 * @param evaluation
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update/maintainance", method = RequestMethod.POST)
	public String updateEvaluationForMaintainance(@Valid Evaluation evaluation, Model model) {
		// TODO 更新 应该是新供应商申请的时候可以更新 其它时候不用更新
		/*
		if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
			vendorService.updateVendorCode(evaluation.getVendorId(), evaluation.getVendorCode().trim());
		}
		*/
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			evaluationService.updateNonTradeEvaluationResultDetail(evaluation);
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(evaluation.getBusinessType(), evaluation.getPoint()));
			evaluationService.updateEvaluation(evaluation);
		}
		return "redirect:/data/vendor/search";
	}

	/**
	 * 申请者再申请页面（我的申请页面），编辑后提交。
	 * 
	 * @param wfid
	 * @param evaluation
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/update/{wfid}", method = RequestMethod.POST)
	public String updateEvaluationForWorkflow(@PathVariable("wfid") String wfid, @Valid Evaluation evaluation, Model model) {
		// TODO 更新 & check
		if(evaluation.getTranCond() == 1 && !"".equals(evaluation.getVendorCode().trim())){
			vendorService.updateVendorCode(evaluation.getVendorId(), evaluation.getVendorCode().trim());
		}
		if (Utils.isNonTrade(evaluation.getBusinessType())) {
			evaluationService.updateNonTradeEvaluationResultDetail(evaluation);
		} else {
			evaluation.setTotalPoint(evaluationService.calculatePoint(evaluation.getBusinessType(), evaluation.getPoint()));
			evaluationService.updateEvaluation(evaluation);
		}
		return "redirect:/workflow/application/" + wfid;
	}

	/**
	 * 
	 * @Title:       goEvaluation 
	 * @Description: ( 确认画面提交按钮  ) 
	 * @param        @param vendorId
	 * @param        @param evalId
	 * @param        @param submit
	 * @param        @param result
	 * @param        @param model
	 * @param        @param request
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 12, 2012
	 */
	@RequestMapping(value = "/submit/{vendorId}/{evalId}", method = RequestMethod.GET)
	public String goEvaluation(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @Valid Submit submit, BindingResult result, Model model,
			HttpServletRequest request) {
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_IN_APPLYING);
		vendorBasicInfo.validate(result, vendorService, vendorBasicInfo.getVendorId());
		boolean hasError = false;
		if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
		if (result.hasErrors() || hasError) {
			vendorBasicInfo.getContact().setRegion(
					vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

			vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

			Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IN_APPLYING);
			model.addAttribute("vendor_id", vendorId);
			model.addAttribute("eval_id", evalId);
			model.addAttribute("basic_info", vendorBasicInfo);
			model.addAttribute("evaluation", eval);
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));

			setL1L2Approval(vendorBasicInfo, model);
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
			model.addAttribute("editable", true);
			model.addAttribute("internal", false);

			model.addAttribute(submit);
            if (Utils.isNonTrade(eval.getBusinessType())) {
                long evaluationId = eval.getId();
                model.addAttribute("nonTradeEvaluationForm", 
                        evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
                return "evaluation/non_trade_confirm";
            } else {
                model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
                return "evaluation/confirm";
            }
		}
		if (submit.getResult() == Evaluation.RESULT_START_WORKFLOW 
				|| submit.getResult() == Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO) {
			WorkflowRule workflowRule = new WorkflowRule();
			workflowRule.setId(UUID.randomUUID().toString());
			workflowRule.setVendorId(vendorId);
			workflowRule.setEvalId(evalId);
			workflowRule.setApplicant(Utils.getUser().getUsername());
			workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
			workflowRule.setProcessor(submit.getL1() == null ? submit.getL2() : submit.getL1());
			workflowRule.setL1(submit.getL1());
			workflowRule.setL2(submit.getL2());
			workflowRule.setCeo(submit.getCeo());
			workflowRule.setHd(submit.getHd());
			workflowRule.setFm(submit.getFm());
			workflowRule.setDm(submit.getDm());
			workflowRule.setApplicationDate(new Date());
			workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);

			workflowService.storeWorkflowRule(workflowRule);
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IN_APPLICATION, evalId);

			Map<String, String> templateModel = Utils.newMailTemplateModel(request);
			templateModel.put("wfid", workflowRule.getId());
			String number = vendorService.getVendorNumber(vendorId);
			templateModel.put("number", number == null ? "" : number);
			templateModel.put("comment", "");
			SystemSetting setting = dataService.getSystemSetting();
			dataService.sendMail(workflowRule.getProcessor(), 
					setting.getApprovalRequestMailSubject(), setting.getApprovalRequestMailTemplate(), templateModel
					, null);

		} else if (submit.getResult() == Evaluation.RESULT_IMPROVEMENT) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
		} else if (submit.getResult() == Evaluation.RESULT_CLOSE) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
		}

		return "redirect:/workflow/myapplications";
	}

	/**
	 * 
	 * @Title:       submitEvaluation 
	 * @Description: ( 确认页面提出 ) 
	 * @param        @param vendorId
	 * @param        @param evalId
	 * @param        @param submit
	 * @param        @param result
	 * @param        @param model
	 * @param        @param request
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	@RequestMapping(value = "/submit/{vendorId}/{evalId}", method = RequestMethod.POST)
	public String submitEvaluation(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @Valid Submit submit, BindingResult result, Model model,
			HttpServletRequest request) {
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_IN_APPLYING);
		vendorBasicInfo.validate(result, vendorService, vendorBasicInfo.getVendorId());
		// validate attachment
		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
		boolean hasError = false;
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());

		if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
		
		if (result.hasErrors() || hasError) {
			model.addAttribute("isSSCS", parentCompanyIsSSCS);

			vendorBasicInfo.getContact().setRegion(
					vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

			vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

			// Evaluation eval = evaluationService.findEvaluationById(evalId,
			// Evaluation.STATUS_IN_APPLYING);
			model.addAttribute("vendor_id", vendorId);
			model.addAttribute("eval_id", evalId);
			model.addAttribute("basic_info", vendorBasicInfo);
			// model.addAttribute("evaluation", eval);
			// model.addAttribute("evaluationCategories",
			// evaluationService.getEvaluationCategories(eval.getBusinessType()));

			setL1L2Approval(vendorBasicInfo, model);
			if (parentCompanyIsSSCS) {
				model.addAttribute("ceos", evaluationService.findCeo());
				model.addAttribute("hds", evaluationService.findHelpDeskUsers());
				model.addAttribute("fms", evaluationService.findFinancialManagers());
				model.addAttribute("dms", evaluationService.findDataMaintainers());
			}
			model.addAttribute("editable", true);
			model.addAttribute("internal", false);
			model.addAttribute("attachments", vendorService.findAttachment(vendorId));
			model.addAttribute(submit);

			if (evalId != -1) {
				Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IN_APPLYING);
				model.addAttribute("evaluation", eval);
				model.addAttribute("internal", false);
				if (Utils.isNonTrade(eval.getBusinessType())) {
					long evaluationId = eval.getId();
					model.addAttribute("nonTradeEvaluationForm", 
							evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
					return "evaluation/non_trade_confirm";
				} else {
					model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
					return "evaluation/confirm";
				}
			} else {
				Evaluation eval = new Evaluation();
				eval.setTotalPoint("100");
				model.addAttribute("eval_id", -1);
				model.addAttribute(eval);
				model.addAttribute("internal", true);
				return "evaluation/confirm";
			}
		}

		// 採番
		String number = "VND-";
		String numberTemp = ("00000000" + String.valueOf(vendorId));
		number += numberTemp.substring(numberTemp.length() - 8);
		vendorService.updateVendorNumber(vendorId, number);

		if (submit.getResult() == Evaluation.RESULT_START_WORKFLOW 
				|| submit.getResult() == Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO) {
			WorkflowRule workflowRule = new WorkflowRule();
			workflowRule.setId(UUID.randomUUID().toString());
			workflowRule.setVendorId(vendorId);
			workflowRule.setEvalId(evalId);
			// 所有人都可以提交申请，所以申请人应该为点击提交的那个人
			workflowRule.setApplicant(Utils.getUser().getUsername());
			workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
			workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
			workflowRule.setL1(submit.getL1());
			workflowRule.setL2(submit.getL2());

			if (parentCompanyIsSSCS) {
				workflowRule.setCeo(submit.getCeo());
				workflowRule.setHd(submit.getHd());
				workflowRule.setFm(submit.getFm());
				workflowRule.setDm(submit.getDm());
			}

			workflowRule.setApplicationDate(new Date());
			workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);

			workflowService.storeWorkflowRule(workflowRule);
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IN_APPLICATION, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);

			Map<String, String> templateModel = Utils.newMailTemplateModel(request);
			templateModel.put("wfid", workflowRule.getId());
			String vendorNumber = vendorService.getVendorNumber(vendorId);
			templateModel.put("number", vendorNumber == null ? "" : vendorNumber);
			templateModel.put("comment", "");
			SystemSetting setting = dataService.getSystemSetting();
			dataService.sendMail(workflowRule.getProcessor(), 
					setting.getApprovalRequestMailSubject(), setting.getApprovalRequestMailTemplate(), templateModel
					, null);

		} else if (submit.getResult() == Evaluation.RESULT_IMPROVEMENT) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);
		} else if (submit.getResult() == Evaluation.RESULT_CLOSE) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);
		}
		return "redirect:/workflow/myapplications";
	}

	/**
	 * temp save 新供应商申请详细画面提交操作
	 * 
	 * @param vendorId
	 * @param evalId
	 * @param id
	 * @param submit
	 * @param result
	 * @param model
	 * @param request
	 * @return
	 */
	@RequestMapping(value = "/submit/{vendorId}/{evalId}/{id}", method = RequestMethod.POST)
	public String submitEvaluation(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @PathVariable("id") String id, @Valid Submit submit,
			BindingResult result, Model model, HttpServletRequest request) {

		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_TMPSAVE);
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());

		vendorBasicInfo.validate(result, vendorService, vendorBasicInfo.getVendorId());
		// validate attachment
		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
		boolean hasError = false;
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		model.addAttribute("tmpRuleId", id);
		
        if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
        
		if (result.hasErrors() || hasError) {
			model.addAttribute("isSSCS", parentCompanyIsSSCS);
			vendorBasicInfo.getContact().setRegion(
					vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

			vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

			// Evaluation eval = evaluationService.findEvaluationById(evalId,
			// Evaluation.STATUS_IN_APPLYING);
			model.addAttribute("vendor_id", vendorId);
			model.addAttribute("eval_id", evalId);
			model.addAttribute("basic_info", vendorBasicInfo);
			// model.addAttribute("evaluation", eval);
			// model.addAttribute("evaluationCategories",
			// evaluationService.getEvaluationCategories(eval.getBusinessType()));

			setL1L2Approval(vendorBasicInfo, model);
			if (parentCompanyIsSSCS) {
				model.addAttribute("ceos", evaluationService.findCeo());
				model.addAttribute("hds", evaluationService.findHelpDeskUsers());
				model.addAttribute("fms", evaluationService.findFinancialManagers());
				model.addAttribute("dms", evaluationService.findDataMaintainers());
			}

			model.addAttribute("editable", true);
			model.addAttribute("internal", false);
			model.addAttribute("attachments", vendorService.findAttachment(vendorId));
			model.addAttribute(submit);

			if (evalId != -1) {
				Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_TMPSAVE);
				model.addAttribute("evaluation", eval);
				model.addAttribute("internal", false);
				if (Utils.isNonTrade(eval.getBusinessType())) {
					long evaluationId = eval.getId();
					model.addAttribute("nonTradeEvaluationForm", 
							evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
					return "evaluation/non_trade_confirm";
				} else {
					model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
					return "evaluation/confirm";
				}
			} else {
				Evaluation eval = new Evaluation();
				eval.setTotalPoint("100");
				model.addAttribute("eval_id", -1);
				model.addAttribute(eval);
				model.addAttribute("internal", true);
				return "evaluation/confirm";
			}
		}
		workflowService.updateStatus(id, WorkflowRule.STATUS_TMPSAVE_END);

		if (submit.getResult() == Evaluation.RESULT_START_WORKFLOW 
				|| submit.getResult() == Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO) {
			WorkflowRule workflowRule = new WorkflowRule();
			workflowRule.setId(UUID.randomUUID().toString());
			workflowRule.setVendorId(vendorId);
			workflowRule.setEvalId(evalId);
			workflowRule.setApplicant(Utils.getUser().getUsername());
			workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
			workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
			workflowRule.setL1(submit.getL1());
			workflowRule.setL2(submit.getL2());
			if (parentCompanyIsSSCS) {
				workflowRule.setCeo(submit.getCeo());
				workflowRule.setHd(submit.getHd());
				workflowRule.setFm(submit.getFm());
				workflowRule.setDm(submit.getDm());
			}
			workflowRule.setApplicationDate(new Date());
			workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);

			workflowService.storeWorkflowRule(workflowRule);
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IN_APPLICATION, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);

			Map<String, String> templateModel = Utils.newMailTemplateModel(request);
			templateModel.put("wfid", workflowRule.getId());
			String number = vendorService.getVendorNumber(vendorId);
			templateModel.put("number", number == null ? "" : number);
			templateModel.put("comment", "");
			SystemSetting setting = dataService.getSystemSetting();
			dataService.sendMail(workflowRule.getProcessor(), 
					setting.getApprovalRequestMailSubject(), setting.getApprovalRequestMailTemplate(), templateModel
					, null);

		} else if (submit.getResult() == Evaluation.RESULT_IMPROVEMENT) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);
		} else if (submit.getResult() == Evaluation.RESULT_CLOSE) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
			// Workflow's attachments contact to vendor
			vendorService.fixAttachmentDataWorkflow(vendorBasicInfo.getTranId(), vendorId);
		}
		return "redirect:/workflow/myapplications";
	}

	@RequestMapping(value = "/submit/reeval/{vendorId}/{evalId}", method = RequestMethod.POST)
	public String submitReEvaluation(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @Valid Submit submit, Model model, HttpServletRequest request) {
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		boolean hasError = false;
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		if (hasError) {
			Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IN_APPLYING);
			model.addAttribute("vendor_id", vendorId);
			model.addAttribute("eval_id", evalId);
			model.addAttribute("basic_info", vendorBasicInfo);
			model.addAttribute("evaluation", eval);

			setL1L2Approval(vendorBasicInfo, model);
			if (parentCompanyIsSSCS) {
				model.addAttribute("ceos", evaluationService.findCeo());
				model.addAttribute("hds", evaluationService.findHelpDeskUsers());
				model.addAttribute("fms", evaluationService.findFinancialManagers());
				model.addAttribute("dms", evaluationService.findDataMaintainers());
			}

			model.addAttribute("isReeval", true);
			model.addAttribute("editable", true);

			model.addAttribute(new Submit());
			model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
			if (Utils.isNonTrade(eval.getBusinessType())) {
				long evaluationId = eval.getId();
				model.addAttribute("nonTradeEvaluationForm", 
						evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), REEVAL_FLAG, evaluationId));
				return "evaluation/reeval_confirm_non_trade";
			} else {
				model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
				return "evaluation/reeval_confirm";
			}
		}
		if (submit.getResult() == Evaluation.RESULT_START_WORKFLOW 
				|| submit.getResult() == Evaluation.RESULT_START_WORKFLOW_WITHOUT_CEO) {

			WorkflowRule workflowRule = new WorkflowRule();
			workflowRule.setId(UUID.randomUUID().toString());
			workflowRule.setVendorId(vendorId);
			workflowRule.setEvalId(evalId);
			workflowRule.setApplicant(Utils.getUser().getUsername());
			workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
			workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
			workflowRule.setL1(submit.getL1());
			workflowRule.setL2(submit.getL2());

			workflowRule.setCeo(submit.getCeo());
			workflowRule.setHd(submit.getHd());
			workflowRule.setFm(submit.getFm());
			workflowRule.setDm(submit.getDm());

			workflowRule.setApplicationDate(new Date());
			workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
			workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_REEVAL);

			workflowService.storeWorkflowRule(workflowRule);

			evaluationService.updateStatus(Evaluation.STATUS_IN_APPLICATION, evalId);
			vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

			Map<String, String> templateModel = Utils.newMailTemplateModel(request);
			templateModel.put("wfid", workflowRule.getId());
			String number = vendorService.getVendorNumber(vendorId);
			templateModel.put("number", number == null ? "" : number);
			templateModel.put("comment", "");
			SystemSetting setting = dataService.getSystemSetting();
			dataService.sendMail(workflowRule.getProcessor(), 
					setting.getApprovalRequestMailSubject(), 
					setting.getApprovalRequestMailTemplate(), 
					templateModel, 
					null);
		} else if (submit.getResult() == Evaluation.RESULT_IMPROVEMENT) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_IMPROVING, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
		} else if (submit.getResult() == Evaluation.RESULT_CLOSE) {
			vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE, vendorId);
			evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE, evalId);
		}

		return "redirect:/vendor/search";
	}

	@RequestMapping(value = "/submit_directly/{vendorId}", method = RequestMethod.POST)
	public String submitEvaluationDirectly(@PathVariable("vendorId") long vendorId, @Valid Submit submit, Model model) {

		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(WorkflowRule.STAGE_HD);

		String hd = evaluationService.findHelpDeskUsers().get(0).getId();
		workflowRule.setProcessor(hd);
		workflowRule.setHd(hd);

		workflowRule.setFm(evaluationService.findFinancialManagers().get(0).getId());
		workflowRule.setDm(evaluationService.findDataMaintainers().get(0).getId());

		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_NEW);

		workflowService.storeWorkflowRule(workflowRule);
		vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

		return "redirect:/workflow/myapplications";

	}

	@RequestMapping(value = "/attachment/{tranId}", method = RequestMethod.POST)
	public String postAttachment(@PathVariable("tranId") String tranId, Attachment attachment, Model model) {

		Map<String, Object> data = new HashMap<String, Object>();
		data.put("tran_id", tranId);
		data.put("name", attachment.getFile().getOriginalFilename());
		data.put("content_type", attachment.getFile().getContentType());
		data.put("size", attachment.getFile().getSize());
		try {
			data.put("data", attachment.getFile().getBytes());
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		// It's better to put this method in workflowService.
		long id = evaluationService.storeAttachment(data);

		model.addAttribute("attachment_workflow", attachment);
		model.addAttribute("id", id);
		model.addAttribute("tranId", tranId);

		return "workflow/attachment_response";
	}

	@RequestMapping(value = "/attachment/download/{id}", method = RequestMethod.GET)
	public void downloadAttachment(@PathVariable("id") long id, HttpServletResponse response, HttpServletRequest request) {

		Attachment attachment = vendorService.findAttachmentDataWorkflow(id);

		response.setContentType(attachment.getContentType());
		response.setHeader("Content-Disposition", "filename=\"" + Utils.encodeFileName(request, attachment.getName()) + "\"");

		try {
			OutputStream out = response.getOutputStream();
			out.write(attachment.getData());
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}

	}

	@RequestMapping(value = "/attachment/delete/{id}/{tran_id}", method = RequestMethod.POST)
	public void deleteAttachment(@PathVariable("id") long id, @PathVariable("tran_id") String tranId, HttpServletResponse response) {

		vendorService.deleteAttachmentData(id, tranId);

		try {
			PrintWriter out = response.getWriter();
			out.println("<result>OK</result>");
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@RequestMapping(value = "/attachment_workflow/delete/{id}/{tran_id}", method = RequestMethod.POST)
	public void deleteAttachmentWorkflow(@PathVariable("id") long id, 
			@PathVariable("tran_id") String tranId, HttpServletResponse response) {

		vendorService.deleteAttachmentWorkFlowData(id, tranId);

		try {
			PrintWriter out = response.getWriter();
			out.println("<result>OK</result>");
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@RequestMapping(value = "/tmpsave/{vendorId}/{evalId}", method = RequestMethod.GET)
	public String tempSave(@PathVariable("vendorId") long vendorId, 
			@Valid Submit submit, BindingResult result, @PathVariable("evalId") long evalId, Model model) {
		/**
		 * 1) Comment: temporary save needn't valid.
		 * 2) Method (findVendorBasicInfoByStatus): control the situation witch click temporary save button for second time. 
		 * 
		 */
		vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_IN_APPLYING);
//		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_IN_APPLYING);
//		vendorBasicInfo.validate(result, vendorService, vendorBasicInfo.getVendorId());
//		// validate attachment
//		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
//		boolean hasError = false;
//		if (attaList == null || attaList.size() == 0) {
//			model.addAttribute("attachmentsIsRequired", true);
//			hasError = true;
//		}
//		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
//        .getCompanyType().getSuperCompanyType());
//
//		if (result.hasErrors() || hasError) {
//			model.addAttribute("isSSCS", parentCompanyIsSSCS);
//
//			vendorBasicInfo.getContact().setRegion(
//					vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));
//
//			vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));
//
//			model.addAttribute("vendor_id", vendorId);
//			model.addAttribute("eval_id", evalId);
//			model.addAttribute("basic_info", vendorBasicInfo);
//
//			model.addAttribute("l1s", evaluationService.findL1Users(Utils.getUser().getDivision()));
//			model.addAttribute("l2s", evaluationService.findL2Users(Utils.getUser().getDivision()));
//			if (parentCompanyIsSSCS) {
//				model.addAttribute("ceos", evaluationService.findCeo());
//				model.addAttribute("hds", evaluationService.findHelpDeskUsers());
//				model.addAttribute("fms", evaluationService.findFinancialManagers());
//				model.addAttribute("dms", evaluationService.findDataMaintainers());
//			}
//			model.addAttribute("editable", true);
//			model.addAttribute("internal", false);
//			model.addAttribute("attachments", vendorService.findAttachment(vendorId));
//			model.addAttribute(submit);
//
//			if (evalId != -1) {
//				Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IN_APPLYING);
//				model.addAttribute("evaluation", eval);
//				model.addAttribute("internal", false);
//				if (Utils.isNonTrade(eval.getBusinessType())) {
//					long evaluationId = eval.getId();
//					model.addAttribute("nonTradeEvaluationForm", 
//							evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
//					return "evaluation/non_trade_confirm";
//				} else {
//					model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
//					return "evaluation/confirm";
//				}
//			} else {
//				Evaluation eval = new Evaluation();
//				eval.setTotalPoint("100");
//				model.addAttribute("eval_id", -1);
//				model.addAttribute(eval);
//				model.addAttribute("internal", true);
//				return "evaluation/confirm";
//			}
//		}
		// 採番
		String number = "VND-";
		String numberTemp = ("00000000" + String.valueOf(vendorId));
		number += numberTemp.substring(numberTemp.length() - 8);
		vendorService.updateStatusAndNumber(VendorBasicInfo.STATUS_TMPSAVE, vendorId, number);
		evaluationService.updateStatus(Evaluation.STATUS_TMPSAVE, evalId);
		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setEvalId(evalId);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(-1);
		workflowRule.setProcessor(Utils.getUser().getUsername());
		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_TMPSAVE);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_TMPSAVE);
		workflowService.storeWorkflowRule(workflowRule);
		return "redirect:/workflow/myapplications";
	}

	@RequestMapping(value = "/retmpsave/{vendorId}/{evalId}/{id}", method = RequestMethod.GET)
	public String reTempSave(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @PathVariable("id") String id, Model model) {

		return "redirect:/workflow/myapplications";
	}
	
	/**
	 * 点击我的申请里的临时保存供应商的详细按钮进入该方法。
	 * 
	 * @param vendorId
	 * @param evalId
	 * @param id
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "/tmpsave/confirm/{vendorId}/{evalId}/{id}", method = RequestMethod.GET)
	public String tmpSaveConfirm(@PathVariable("vendorId") long vendorId, 
			@PathVariable("evalId") long evalId, @PathVariable("id") String id, Model model) {

		// vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLYING,
		// vendorId);
		// evaluationService.updateStatus(Evaluation.STATUS_IN_APPLYING,
		// evalId);

		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, VendorBasicInfo.STATUS_TMPSAVE);

		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));
		
		Evaluation eval = evaluationService.findEvaluationById(evalId, Evaluation.STATUS_IS_TEMP_SAVE);
		// new BigDecimal(eval.getTotalPoint()).compareTo(new BigDecimal(50)) <
		// 0
		if(eval != null){
		    if (Utils.isNonTrade(eval.getBusinessType())) {
	            //
	            eval.setTotalPointForCompare(75);
	        } else {
	            if (eval.getTotalPointForCompare() <= 49) {
	                // vendorService.updateStatus(VendorBasicInfo.STATUS_CLOSE,
	                // vendorId);
	                // evaluationService.updateStatus(Evaluation.STATUS_IS_EFFECTIVE,
	                // evalId);
	            }
	        }
	        model.addAttribute("evaluation", eval);
		}
		
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("eval_id", evalId);
		model.addAttribute("basic_info", vendorBasicInfo);
		
		model.addAttribute("tmpRuleId", id);
		// model.addAttribute("evaluationCategories",
		// evaluationService.getEvaluationCategories(eval.getBusinessType()));

		setL1L2Approval(vendorBasicInfo, model);

		// SSGE、SSV 的审批人员没有CEO以上级别
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		if (parentCompanyIsSSCS) {
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
		}
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		model.addAttribute("editable", true);
		model.addAttribute("internal", false);
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		model.addAttribute(new Submit());

		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		
		if(eval == null){
		    return "evaluation/confirm";
		}
		
		if (Utils.isNonTrade(eval.getBusinessType())) {
			long evaluationId = eval.getId();
			model.addAttribute("nonTradeEvaluationForm", 
					evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), NOT_REEVAL_FLAG, evaluationId));
			return "evaluation/non_trade_confirm";
		} else {
			model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
			return "evaluation/confirm";
		}
	}

	@RequestMapping(value = "/tmpsave/{vendorId}/{evalId}/{id}", method = RequestMethod.GET)
	public String tempSave(@PathVariable("vendorId") long vendorId, @PathVariable("evalId") long evalId, 
			@PathVariable("id") String id, Model model) {
		vendorService.updateStatus(VendorBasicInfo.STATUS_TMPSAVE, vendorId);
		evaluationService.updateStatus(Evaluation.STATUS_TMPSAVE, evalId);
		return "redirect:/workflow/myapplications";
	}
	
	public void setL1L2Approval(VendorBasicInfo vendorBasicInfo, Model model) {
        // vendor super company
        VndMgmntUser user = Utils.getUser();
        String supercompany = vendorBasicInfo.getCompanyType().getSuperCompanyType();
        // init division
        List<String> costcenter = evaluationService.getCostcenterBySuperCompany(supercompany);
        if(costcenter == null || costcenter.size() == 0){
            throw new GeneralException("This vendor's super company has no employes");
        }
        int selectedDivision = Integer.parseInt(costcenter.get(0));
        if(supercompany.equals(user.getUserCompany())){
            selectedDivision = user.getDivision();
        }
        model.addAttribute("supercompany", supercompany);
        model.addAttribute("costcenter", costcenter);
        model.addAttribute("selectedDivision", selectedDivision);
        model.addAttribute("l1s", evaluationService.findL1Users(selectedDivision, supercompany));
        model.addAttribute("l2s", evaluationService.findL2Users(selectedDivision, supercompany));
    }
	
    @RequestMapping(value = "/changeCostcenterL1/{supercompany}/{costcenter}", method = RequestMethod.GET)
    public String changeCostcenterL1(
            @PathVariable("supercompany") String supercompany,
            @PathVariable("costcenter") int costcenter, Model model) {
        model.addAttribute("l1s", evaluationService.findL1Users(costcenter, supercompany));
        return "evaluation/l1";
    }
    
    @RequestMapping(value = "/changeCostcenterL2/{supercompany}/{costcenter}", method = RequestMethod.GET)
    public String changeCostcenterL2(
            @PathVariable("supercompany") String supercompany,
            @PathVariable("costcenter") int costcenter, Model model) {
        model.addAttribute("l2s", evaluationService.findL2Users(costcenter, supercompany));
        return "evaluation/l2";
    }
}
