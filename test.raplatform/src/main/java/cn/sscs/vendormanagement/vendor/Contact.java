package cn.sscs.vendormanagement.vendor;

import java.util.Map;

import org.springframework.validation.Errors;

/**
 * 
 * @author kazzy
 *
 */
public class Contact {

	private String country = "CN";
	
	private String region = "20";
	
	private String city = null;
	
	private String address = null;
	
	private String tel = null;
	
	private String postalCode = null;
	
	private String person = null;
	
	private String homepage = null;
	
	private String fax = null;
	
	/**
	 * 
	 */
	Contact() {}
	
	/**
	 * 
	 * @param rowMap
	 */
	Contact(Map<String, Object> rowMap) {
		country = (String)rowMap.get("country");
		region = (String) rowMap.get("region");
		city = (String) rowMap.get("city");
		address = (String) rowMap.get("address");
		tel = (String) rowMap.get("tel");
		postalCode = (String) rowMap.get("postal_code");
		person = (String) rowMap.get("person");
		homepage = (String) rowMap.get("homepage");
		fax = (String) rowMap.get("fax");
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCountry() {
		return country;
	}
	
	/**
	 * 
	 * @param country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getRegion() {
		return region;
	}
	
	/**
	 * 
	 * @param region
	 */
	public void setRegion(String region) {
		this.region = region;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * 
	 * @param city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getAddress() {
		return address;
	}
	
	/**
	 * 
	 * @param address
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTel() {
		return tel;
	}
	
	/**
	 * 
	 * @param tel
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPostalCode() {
		return postalCode;
	}
	
	/**
	 * 
	 * @param postalCode
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getPerson() {
		return person;
	}
	
	/**
	 * 
	 * @param person
	 */
	public void setPerson(String person) {
		this.person = person;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getHomepage() {
		return homepage;
	}
	
	/**
	 * 
	 * @param homepage
	 */
	public void setHomepage(String homepage) {
		this.homepage = homepage;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getFax() {
		return fax;
	}
	
	/**
	 * 
	 * @param fax
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}
	
	/**
	 * 
	 * @param errors
	 */
	public void validate(Errors errors) {
		if ("".equals(address)) {
			errors.rejectValue("contact.address", "error.required");
		}
		if ("".equals(tel)) {
			errors.rejectValue("contact.tel", "error.required");
		}
		if ("".equals(city)) {
			errors.rejectValue("contact.city", "error.required");
		}
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("country", country);
		rowMap.put("region", region);
		rowMap.put("city", city);
		rowMap.put("address", address);
		rowMap.put("tel", tel);
		rowMap.put("postal_code", postalCode);
		rowMap.put("person", person);
		rowMap.put("homepage", homepage);
		rowMap.put("fax", fax);
	}
}
