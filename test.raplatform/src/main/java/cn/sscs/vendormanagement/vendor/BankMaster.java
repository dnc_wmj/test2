package cn.sscs.vendormanagement.vendor;

public class BankMaster {

	private String netBankJointCode = null;
	
	private String name = null;
	
	private String institutionNo = null;
	
	private String jointCode = null;
	
	private String parentInstitutionNo = null;
	
	private String parentJointCode = null;

	/**
	 * 
	 * @return
	 */
	public String getNetBankJointCode() {
		return netBankJointCode;
	}

	/**
	 * 
	 * @param netBankJointCode
	 */
	public void setNetBankJointCode(String netBankJointCode) {
		this.netBankJointCode = netBankJointCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getInstitutionNo() {
		return institutionNo;
	}

	/**
	 * 
	 * @param institutionNo
	 */
	public void setInstitutionNo(String institutionNo) {
		this.institutionNo = institutionNo;
	}

	/**
	 * 
	 * @return
	 */
	public String getJointCode() {
		return jointCode;
	}

	/**
	 * 
	 * @param jointCode
	 */
	public void setJointCode(String jointCode) {
		this.jointCode = jointCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getParentInstitutionNo() {
		return parentInstitutionNo;
	}

	/**
	 * 
	 * @param parentInstitutionNo
	 */
	public void setParentInstitutionNo(String parentInstitutionNo) {
		this.parentInstitutionNo = parentInstitutionNo;
	}

	/**
	 * 
	 * @return
	 */
	public String getParentJointCode() {
		return parentJointCode;
	}

	/**
	 * 
	 * @param parentJointCode
	 */
	public void setParentJointCode(String parentJointCode) {
		this.parentJointCode = parentJointCode;
	}
}
