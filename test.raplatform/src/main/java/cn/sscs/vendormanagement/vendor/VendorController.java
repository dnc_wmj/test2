package cn.sscs.vendormanagement.vendor;

import java.beans.PropertyEditorSupport;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import cn.sscs.vendormanagement.GeneralException;
import cn.sscs.vendormanagement.SuperCompany;
import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.VndMgmntUser;
import cn.sscs.vendormanagement.data.IDataService;
import cn.sscs.vendormanagement.data.InterfaceData;
import cn.sscs.vendormanagement.data.ModificationHistory;
import cn.sscs.vendormanagement.data.SystemSetting;
import cn.sscs.vendormanagement.evaluation.Evaluation;
import cn.sscs.vendormanagement.evaluation.EvaluationHistory;
import cn.sscs.vendormanagement.evaluation.IEvaluationService;
import cn.sscs.vendormanagement.evaluation.Submit;
import cn.sscs.vendormanagement.workflow.IWorkflowService;
import cn.sscs.vendormanagement.workflow.WorkflowRule;

@Transactional
@Controller
@RequestMapping(value="/vendor")
public class VendorController{
	
	@Autowired
	private IVendorService vendorService = null;
	
	@Autowired
	private IEvaluationService evaluationService = null;
	
	@Autowired
	private IDataService dataService = null;

	@Autowired
	private IWorkflowService workflowService = null;
	
	@InitBinder  
	public void InitBinder(WebDataBinder dataBinder) {  
	    dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {  
	        public void setAsText(String value) {  
	            try {  
	                setValue(new SimpleDateFormat("yyyy/MM/dd").parse(value));  
	            } catch(ParseException e) {  
	                setValue(null);  
	            }
	        }  
	        public String getAsText() {
	        	return (getValue() == null) ? null : new SimpleDateFormat("yyyy/MM/dd").format((Date) getValue());
	        }          
	  
	    });  
	}
	
	@RequestMapping(value="/bank_form", method=RequestMethod.GET)
	public String getBankInformationForm() {
		return "vendor/bank_information_form";
	}
	
	@RequestMapping(value="/attachment/{tranId}", method=RequestMethod.POST)
	public String postAttachment(@PathVariable("tranId") String tranId, 
			Attachment attachment, Model model) {
		
		Map<String, Object> data = new HashMap<String, Object>();
		data.put("tran_id", tranId);
		data.put("name", attachment.getFile().getOriginalFilename());
		data.put("content_type", attachment.getFile().getContentType());
		data.put("size", attachment.getFile().getSize());
		try {
			data.put("data", attachment.getFile().getBytes());
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
		
		long id = vendorService.storeAttachment(data);
		
		model.addAttribute("attachment", attachment);
		model.addAttribute("id", id);
		model.addAttribute("tranId", tranId);

		return "vendor/attachment_response";
	}
	
	@RequestMapping(value="/attachment/download/{id}", method=RequestMethod.GET)
	public void downloadAttachment(@PathVariable("id") long id, 
			HttpServletResponse response,
			HttpServletRequest request) {
		
		Attachment attachment = vendorService.findAttachmentData(id);
		
		response.setContentType(attachment.getContentType());
		response.setHeader(
		        "Content-Disposition",
		        "filename=\"" + Utils.encodeFileName(request, attachment.getName()) + "\"");
		
		try {
			OutputStream out = response.getOutputStream();
			out.write(attachment.getData());
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}		
		
	}
	
	@RequestMapping(value="/attachment/delete/{id}/{tran_id}", method=RequestMethod.POST)
	public void deleteAttachment(@PathVariable("id") long id,
			@PathVariable("tran_id") String tranId,
			HttpServletResponse response) {
		
		vendorService.deleteAttachmentData(id, tranId);
		
		try {
			PrintWriter out = response.getWriter();
			out.println("<result>OK</result>");
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@RequestMapping(value="/bizinfo/delete/{id}", method=RequestMethod.POST)
	public void deleteBizInfo(
		@PathVariable("id") long id,
		HttpServletResponse response) {
	
		vendorService.deleteBizInfoPerformance(id);
		
		try {
			PrintWriter out = response.getWriter();
			out.println("<result>OK</result>");
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	@RequestMapping(value="/bank/delete/{id}", method=RequestMethod.POST)
	public void deleteBankAccount(
		@PathVariable("id") long id,
		HttpServletResponse response) {
		
		long vendorId = vendorService.getVendorIDByBankAccountID(id);
		vendorService.deleteBankAccount(id);
		// 删除时重新排序
		List<BankAccount> bankAccounts = vendorService.findBankAccount(vendorId, BankAccount.STATUS_ALIVE);
		int i = 0;
		for(BankAccount bank : bankAccounts){
			String subCode = "";
			// i 是2位数以上
			if(i / 10 >= 1){
				subCode = String.valueOf(i);
			}else{
				subCode = "0" + String.valueOf(i);
			}
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("id", bank.getBankAccountId());
			map.put("vendor_sub_code", subCode);
			vendorService.updateVendorSubCode(map);
			i++;
		}
		/*
		ModificationHistory history = 
			new ModificationHistory(vendorId, id, ModificationHistory.CLASS_BANK_ACCOUNT_DELETE);
		dataService.storeModificationHistory(history);
		*/
		
		try {
			PrintWriter out = response.getWriter();
			out.println("<result>OK</result>");
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}
	}

	/**
	 * 
	 * @Title:       getBasicInfomationForm 
	 * @Description:   ( 点击菜单新供应商申请  - 准备数据跳转新供应商申请界面 ) 
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 2, 2012
	 */
	@RequestMapping(value="/register/basic", method=RequestMethod.GET)
	public String getBasicInfomationForm(Model model) {
		VendorBasicInfo vendorBasicInfo = new VendorBasicInfo();
		vendorBasicInfo.setTranId(UUID.randomUUID().toString());
		
		model.addAttribute("vendorBasicInfo", vendorBasicInfo);
		model.addAttribute("countries", vendorService.getCountries());
		model.addAttribute("regions", vendorService.getRegion("CN"));
		model.addAttribute("action", "register");
		modelAddSuperCompany(model);
		return "vendor/basic_information_form";
	}
	
	/**
	 * 基本信息编辑确认画面点击编辑按钮进入该方法
	 * 新供应商申请确认画面点击编辑按钮进入该方法
	 * 
	 * @param vendorId
	 * @param evalId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/updateform/application/basic/{vendorId}/{evalId}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormForApplication(@PathVariable("vendorId") long vendorId,
			@PathVariable("evalId") long evalId,
			Model model) {
		
		if (evalId == -2) {
			processUpdateBasicInformationForm(vendorId, VendorBasicInfo.STATUS_EDITING, model);
			model.addAttribute("forEdit", true);
		} else if(evalId == -1){
			processUpdateBasicInformationForm(vendorId, VendorBasicInfo.STATUS_IN_APPLYING, model);
			//model.addAttribute("hasEvaluation", false);
		}else {
			Evaluation evaluation = evaluationService.findEvaluationById(evalId);
			int status = VendorBasicInfo.STATUS_IN_APPLYING;
			// new BigDecimal(evaluation.getTotalPoint()).compareTo(new BigDecimal(50)) < 0
			if(evaluation.getTotalPointForCompare() != -1){
				status =  evaluation.getTotalPointForCompare() <= 49
							? VendorBasicInfo.STATUS_CLOSE
							: status;
			}
			processUpdateBasicInformationForm(vendorId, status, model);
			model.addAttribute("eval_id", evalId);
			//model.addAttribute("hasEvaluation", true);
		}
		
		// Although it is obvious for judging that user had ability to change the business type in this method,
		// should make the flag common for maintaining the project.
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}
	
	//临时保存的供应商，我的申请详细页面点击编辑按钮
	@RequestMapping(value="/updateform/application/basic/{vendorId}/{evalId}/{id}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormForApplication(@PathVariable("vendorId") long vendorId,
			@PathVariable("evalId") long evalId,
			@PathVariable("id") String id,
			Model model) {
		
		int status = VendorBasicInfo.STATUS_TMPSAVE;
		processUpdateBasicInformationForm(vendorId, status, model);
		model.addAttribute("eval_id", evalId);
		model.addAttribute("tmpRuleId", id);
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}
	
	@RequestMapping(value="/updateform/workflow/basic/{vendorId}/{wfid}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormWorkflow(@PathVariable("vendorId") long vendorId, 
			@PathVariable("wfid") String wfid,
			Model model) {
		
		processUpdateBasicInformationForm(vendorId, VendorBasicInfo.STATUS_IN_APPLICATION, model);
		model.addAttribute("wfid", wfid);
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}
	/**
	 * 审批流程到HD时，HD点击编辑供应商信息按钮
	 * 
	 * @param vendorId
	 * @param wfid
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/updateform/workflow/hd/basic/{vendorId}/{wfid}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormWorkflowForHD(@PathVariable("vendorId") long vendorId, 
			@PathVariable("wfid") String wfid,
			Model model) {
		
		processUpdateBasicInformationForm(vendorId, VendorBasicInfo.STATUS_IN_APPLICATION, model);
		model.addAttribute("wfid", wfid);
		model.addAttribute("isRoleHD", true);
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}

	/**
	 * 
	 * @Title:       getUpdateBasicInformationFormForMaintainance 
	 * @Description:   ( 供应商维护界面 - 供应商编辑 按钮 ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/updateform/maintainance/basic/{vendorId}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormForMaintainance(@PathVariable("vendorId") long vendorId, 
			Model model) {
				
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		vendorBasicInfo.init();
		
		model.addAttribute(vendorBasicInfo);
		model.addAttribute("countries", vendorService.getCountries());
		model.addAttribute("regions", vendorService.getRegion(vendorBasicInfo.getContact().getCountry()));
		model.addAttribute("action", "update");
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		model.addAttribute("bankAccounts", vendorBasicInfo.getBankInformation().getBankAccounts());

		model.addAttribute("forMaintainance", true);
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}
	
	/**
	 * 进入基本信息更新画面
	 * 
	 * @param vendorId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/updateform/edit/basic/{vendorId}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormForEdit(@PathVariable("vendorId") long vendorId, 
			Model model) {
		
		preUpdateBasicInformationFormForEdit(vendorId, model);
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		model.addAttribute("importVendorEditInfo", true);
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}

	
	/**
	 * 对于导入数据（ 没有评估的供应商） 在初次评估的时候， 允许用户更改基本信息。
	 * 进入基本信息更新画面
	 * 
	 * @param vendorId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reeval/{vendorId}", method=RequestMethod.GET)
	public String getUpdateBasicInformationFormForEditFromReeval(@PathVariable("vendorId") long vendorId, 
			Model model) {
				
		preUpdateBasicInformationFormForEdit(vendorId, model);
		model.addAttribute("isImportExterVendor", true);
		//TODO url
		model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorId));
		modelAddSuperCompany(model);
		return "vendor/basic_information_update_form";
	}
	
	/**
	 * 基本信息更新画面准备数据
	 * 
	 * @param vendorId
	 * @param model
	 */
	private void preUpdateBasicInformationFormForEdit(long vendorId, Model model) {
				
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		vendorBasicInfo.init();
		
		model.addAttribute(vendorBasicInfo);
		model.addAttribute("countries", vendorService.getCountries());
		model.addAttribute("regions", vendorService.getRegion(vendorBasicInfo.getContact().getCountry()));
		model.addAttribute("action", "update");
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		model.addAttribute("bankAccounts", vendorBasicInfo.getBankInformation().getBankAccounts());

		model.addAttribute("forEdit", true);
	}
	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @param model
	 */
	private void processUpdateBasicInformationForm(long vendorId, int status, Model model) {
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, status);
		vendorBasicInfo.init();
		// bug: 2012(2)15
		if(vendorBasicInfo.getTranId() == null || "".equals(vendorBasicInfo.getTranId())){
			vendorBasicInfo.setTranId(UUID.randomUUID().toString());
		}
		model.addAttribute(vendorBasicInfo);
		model.addAttribute("countries", vendorService.getCountries());
		model.addAttribute("regions", vendorService.getRegion(vendorBasicInfo.getContact().getCountry()));
		model.addAttribute("action", "update");
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		model.addAttribute("bankAccounts", vendorBasicInfo.getBankInformation().getBankAccounts());

	}
	/**
	 * 
	 * @Title:       registerBasicInfomation 
	 * @Description:   ( 新供应商申请, 响应from提交请求  ) 
	 * @param        @param tranId
	 * @param        @param vendorBasicInfo
	 * @param        @param result
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 2, 2012
	 */
	@RequestMapping(value="/register/basic/{tranId}", method=RequestMethod.POST)
	public String registerBasicInfomation(
			@PathVariable("tranId") String tranId,
			@ModelAttribute("vendorBasicInfo") VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		/* 表单验证 */
		vendorBasicInfo.validate(result, vendorService, vendorBasicInfo.getVendorId());
		boolean hasError = false;
		/* 解析界面储存的Bank信息 */
		List<BankAccount> accounts = vendorBasicInfo.getBankInformation().getBankAccounts();
		for (BankAccount account : accounts) {
			hasError = account.validate(result, vendorService);
		}
		
		// validate attachment
		List<Attachment> attaList = vendorService.findAttachment(tranId);
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		
		if (result.hasErrors() || hasError) {
			model.addAttribute(vendorBasicInfo);
			model.addAttribute("countries", vendorService.getCountries());
			model.addAttribute("regions", vendorService.getRegion(vendorBasicInfo.getContact().getCountry()));
			model.addAttribute("action", "register");
			model.addAttribute("tranId", tranId);
			model.addAttribute("attachments", attaList);
			model.addAttribute("bankAccounts", accounts);
			model.addAttribute("tradeCondition", vendorBasicInfo.getTradeCondition());
			modelAddSuperCompany(model);
			return "vendor/basic_information_form";			
		}
		
		vendorBasicInfo.setTranId(tranId);
		
		int id = vendorService.storeVendorBasicInfo(vendorBasicInfo, accounts);
		
		vendorService.fixAttachmentData(tranId, id);
		/*Sony内公司、或其他*/
		if (vendorBasicInfo.getIdentity().getVendorType() == Identity.TYPE_INTERNAL ||
				vendorBasicInfo.getIdentity().getVendorType() == Identity.TYPE_OTHER) {
			vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(id, VendorBasicInfo.STATUS_IN_APPLYING);
			/* 区域 */
			vendorBasicInfo.getContact().setRegion(
					vendorService.getRegionName(
							vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));
				
			vendorBasicInfo.getContact().setCountry(
					vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));

			model.addAttribute("basic_info", vendorBasicInfo);
			
			return "redirect:/vendor/confirm/" + id;
		}
		
		return "redirect:/evaluation/register/" + id;
	}

	/**
	 * 
	 * @Title:       confirm0 
	 * @Description:   ( 确认界面审核数据  ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @param status     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 4, 2012
	 */
	private void confirm0(@PathVariable("vendorId") long vendorId, Model model, int status) {
		VendorBasicInfo vendorBasicInfo = vendorService.findVendorBasicInfoByStatus(vendorId, status);
		
		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(
						vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(
				vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("basic_info", vendorBasicInfo);
		// SSGE、SSV 的审批人员没有CEO以上级别
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		if(parentCompanyIsSSCS){
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
		}
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		
		setL1L2Approval(vendorBasicInfo, model);
		
		model.addAttribute("attachments", vendorService.findAttachment(vendorBasicInfo.getTranId()));
		model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		model.addAttribute("editable", true);
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		Evaluation eval = new Evaluation();
		eval.setTotalPoint("100");
		model.addAttribute("eval_id", -1);
		model.addAttribute(eval);
		model.addAttribute(new Submit());
		
	}
	
	/**
	 * 
	 * @Title:       confirm 
	 * @Description:   ( 跳转确认画面  ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 4, 2012
	 */
	@RequestMapping(value="/confirm/{vendorId}", method=RequestMethod.GET)
	public String confirm(@PathVariable("vendorId") long vendorId, Model model) {
		confirm0(vendorId, model, VendorBasicInfo.STATUS_IN_APPLYING);
		
		return "evaluation/confirm";
	}
	
	@RequestMapping(value="tmpsave/noeval/confirm/{vendorId}/{id}", method=RequestMethod.GET)
	public String tmpsaveConfirm(
			@PathVariable("vendorId") long vendorId,
			@PathVariable("id") String id,
			Model model) {
		confirm0(vendorId, model, VendorBasicInfo.STATUS_TMPSAVE);
		model.addAttribute("tmpRuleId", id);
//		WorkflowRule tmprule = workflowService.getWorkflowRule(id);
		return "evaluation/confirm";
	}
	@RequestMapping(value="/edit/confirm/{vendorId}", method=RequestMethod.GET)
	public String editConfirm(@PathVariable("vendorId") long vendorId, Model model) {
		confirm0(vendorId, model, VendorBasicInfo.STATUS_EDITING);
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		model.addAttribute("eval_id", -2);
		model.addAttribute("basicInfoUpdate", true);
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		return "evaluation/confirm";
	}
	
	public void _confirm(long vendorId, Model model){
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);

		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(
						vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(
				vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));
		
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("basic_info", vendorBasicInfo);

		setL1L2Approval(vendorBasicInfo, model);
		
		// SSGE、SSV 的审批人员没有CEO以上级别
		boolean parentCompanyIsSSCS = Utils.parentCompanyIsSSCS(vendorBasicInfo
                .getCompanyType().getSuperCompanyType());
		if(parentCompanyIsSSCS){
			model.addAttribute("ceos", evaluationService.findCeo());
			model.addAttribute("hds", evaluationService.findHelpDeskUsers());
			model.addAttribute("fms", evaluationService.findFinancialManagers());
			model.addAttribute("dms", evaluationService.findDataMaintainers());
		}
		model.addAttribute("isSSCS", parentCompanyIsSSCS);
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		//model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorId));
		model.addAttribute("attachment_workflow", vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId()));
		model.addAttribute("attaTemplateFileName", "VendorCompare.xls");
		
		Evaluation eval = new Evaluation();
		eval.setTotalPoint("100");
		model.addAttribute(eval);
		
		model.addAttribute(new Submit());
		
		model.addAttribute("eval_id", -3);
	}
	
	@RequestMapping(value="/delete/confirm/{vendorId}", method=RequestMethod.GET)
	public String deleteConfirm(@PathVariable("vendorId") long vendorId, Model model) {
		
		_confirm(vendorId, model);
		model.addAttribute("basicInfoDelete", true);
		
		return "evaluation/confirm";
	}
	

	
	@RequestMapping(value="/freeze/confirm/{vendorId}", method=RequestMethod.GET)
	public String freezeConfirm(@PathVariable("vendorId") long vendorId, Model model) {
		
		_confirm(vendorId, model);
		model.addAttribute("basicInfoFreeze", true);
		
		return "evaluation/confirm";
	}
	
	/**
	 * 
	 * @Title:       unfreezeConfirm 
	 * @Description:   ( 解冻 ) 
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/unfreeze/confirm/{vendorId}", method=RequestMethod.GET)
	public String unfreezeConfirm(@PathVariable("vendorId") long vendorId, Model model) {
		
		_confirm(vendorId, model);
		model.addAttribute("basicInfoUnfreeze", true);
		
		return "evaluation/confirm";
	}
	
	@RequestMapping(value="/edit/submit/{vendorId}", method=RequestMethod.POST)
	public String editSubmit(@PathVariable("vendorId") long vendorId,
			@Valid Submit submit, Model model, 
			HttpServletRequest request) {
		boolean hasError = false;
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		
		// validate attachment
		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		
		if(submit.getL2() == null || "".equals(submit.getL2())){
		    model.addAttribute("l2NullErrer", true);
		    hasError = true;
		}
		
		if (hasError) {
			confirm0(vendorId, model, VendorBasicInfo.STATUS_EDITING);
			model.addAttribute("eval_id", -2);
			model.addAttribute("basicInfoUpdate", true);
			model.addAttribute("attachments", vendorService.findAttachment(vendorId));
			return "evaluation/confirm";
			
		}
		if (vendorService.getVendorStatus(vendorId) != VendorBasicInfo.STATUS_EDITING) {
			throw new GeneralException("error.application.failed");
		}
		
		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setEvalId(-1);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
		workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
		workflowRule.setL1(submit.getL1());
		workflowRule.setL2(submit.getL2());
		workflowRule.setCeo(submit.getCeo());
		workflowRule.setHd(submit.getHd());
		workflowRule.setFm(submit.getFm());
		workflowRule.setDm(submit.getDm());
		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_EDIT);
		
		workflowService.storeWorkflowRule(workflowRule);
		vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

		
		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", workflowRule.getId());
		String number = vendorService.getVendorNumber(vendorId);
		templateModel.put("number", number == null ? "" : number);
		templateModel.put("comment", "");
		SystemSetting setting = dataService.getSystemSetting();
		dataService.sendMail(workflowRule.getProcessor(), 
				setting.getApprovalRequestMailSubject(), 
				setting.getApprovalRequestMailTemplate(), 
				templateModel, null);
		
		return "redirect:/vendor/search";
	}
	private boolean _validateWorkflowAttachment(Long vendorId, Model model){
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		boolean hasError = false;
		// validate attachment
		List<Attachment> attaList = vendorService.findAttachmentWorkflow(vendorBasicInfo.getTranId());
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		return hasError;
	}
	@RequestMapping(value="/delete/submit/{vendorId}", method=RequestMethod.POST)
	public String deleteSubmit(@PathVariable("vendorId") long vendorId,
			@Valid Submit submit, Model model, 
			HttpServletRequest request) {
	    boolean hasError = false;
        if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
		if(_validateWorkflowAttachment(vendorId, model) || hasError){
			_confirm(vendorId, model);
			return "evaluation/confirm";
		}
		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setEvalId(-1);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
		workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
		workflowRule.setL1(submit.getL1());
		workflowRule.setL2(submit.getL2());
		workflowRule.setCeo(submit.getCeo());
		workflowRule.setHd(submit.getHd());
		workflowRule.setFm(submit.getFm());
		workflowRule.setDm(submit.getDm());
		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_DELETE);
		
		workflowService.storeWorkflowRule(workflowRule);
		vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

		
		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", workflowRule.getId());
		String number = vendorService.getVendorNumber(vendorId);
		templateModel.put("number", number == null ? "" : number);
		templateModel.put("comment", "");
		SystemSetting setting = dataService.getSystemSetting();
		dataService.sendMail(workflowRule.getProcessor(), 
				setting.getApprovalRequestMailSubject(), 
				setting.getApprovalRequestMailTemplate(), 
				templateModel, null);
		
		return "redirect:/vendor/search";
	}
	
	@RequestMapping(value="/freeze/submit/{vendorId}", method=RequestMethod.POST)
	public String freezeSubmit(@PathVariable("vendorId") long vendorId,
			@Valid Submit submit, Model model, 
			HttpServletRequest request) {
	    boolean hasError = false;
        if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
		if(_validateWorkflowAttachment(vendorId, model) || hasError){
			_confirm(vendorId, model);
			model.addAttribute("basicInfoFreeze", true);
			return "evaluation/confirm";
		}
		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setEvalId(-1);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
		workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
		workflowRule.setL1(submit.getL1());
		workflowRule.setL2(submit.getL2());
		
		workflowRule.setCeo(submit.getCeo());
		workflowRule.setHd(submit.getHd());
		workflowRule.setFm(submit.getFm());
		workflowRule.setDm(submit.getDm());
		
		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_FREEZE);
		
		workflowService.storeWorkflowRule(workflowRule);
		vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

		
		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", workflowRule.getId());
		String number = vendorService.getVendorNumber(vendorId);
		templateModel.put("number", number == null ? "" : number);
		templateModel.put("comment", "");
		SystemSetting setting = dataService.getSystemSetting();
		dataService.sendMail(workflowRule.getProcessor(), 
				setting.getApprovalRequestMailSubject(), 
				setting.getApprovalRequestMailTemplate(), 
				templateModel, null);
		
		return "redirect:/vendor/search";
	}
	
	@RequestMapping(value="/unfreeze/submit/{vendorId}", method=RequestMethod.POST)
	public String unfreezeSubmit(@PathVariable("vendorId") long vendorId,
			@Valid Submit submit, Model model, 
			HttpServletRequest request) {
	    boolean hasError = false;
        if(submit.getL2() == null || "".equals(submit.getL2())){
            model.addAttribute("l2NullErrer", true);
            hasError = true;
        }
		if(_validateWorkflowAttachment(vendorId, model) || hasError){
			_confirm(vendorId, model);
			model.addAttribute("basicInfoUnfreeze", true);
			return "evaluation/confirm";
		}
		WorkflowRule workflowRule = new WorkflowRule();
		workflowRule.setId(UUID.randomUUID().toString());
		workflowRule.setVendorId(vendorId);
		workflowRule.setEvalId(-1);
		workflowRule.setApplicant(Utils.getUser().getUsername());
		workflowRule.setStage(submit.getL1() == null || "".equals(submit.getL1()) ? WorkflowRule.STAGE_L2 : WorkflowRule.STAGE_L1);
		workflowRule.setProcessor(submit.getL1() == null || "".equals(submit.getL1()) ? submit.getL2() : submit.getL1());
		workflowRule.setL1(submit.getL1());
		workflowRule.setL2(submit.getL2());
		workflowRule.setCeo(submit.getCeo());
		workflowRule.setHd(submit.getHd());
		workflowRule.setFm(submit.getFm());
		workflowRule.setDm(submit.getDm());
		workflowRule.setApplicationDate(new Date());
		workflowRule.setStatus(WorkflowRule.STATUS_RUNNING);
		workflowRule.setModifiedDataType(WorkflowRule.MODIFIED_DATA_TYPE_UNFREEZE);
		
		workflowService.storeWorkflowRule(workflowRule);
		vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLICATION, vendorId);

		
		Map<String, String> templateModel = Utils.newMailTemplateModel(request);
		templateModel.put("wfid", workflowRule.getId());
		String number = vendorService.getVendorNumber(vendorId);
		templateModel.put("number", number == null ? "" : number);
		templateModel.put("comment", "");
		SystemSetting setting = dataService.getSystemSetting();
		dataService.sendMail(workflowRule.getProcessor(), 
				setting.getApprovalRequestMailSubject(), 
				setting.getApprovalRequestMailTemplate(), 
				templateModel, null);
		
		return "redirect:/vendor/search";
	}
	
	/**
	 * 
	 * 新供应商申请，确认画面修改供应商基本信息后提交
	 * 基本信息编辑，确认画面修改供应商基本信息后提交
	 * 
	 * @param evalId
	 * @param vendorBasicInfo
	 * @param result
	 * @param model
	 * @return
	 */
	
	@RequestMapping(value="/update/application/basic/{eval_id}", method=RequestMethod.POST)
	public String updateBasicInfomationForApplication(
			@PathVariable("eval_id") long evalId,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("eval_id", evalId);
			return retUrl;
		}
		int businessType = vendorService.getBusiessType(vendorBasicInfo.getVendorId());
		processUpdateBasicInformation(vendorBasicInfo);
		// TODO situation for need update status of vendor
		//vendorService.updateStatus(VendorBasicInfo.STATUS_IN_APPLYING, vendorBasicInfo.getVendorId());
		//evaluationService.updateStatus(Evaluation.STATUS_IN_APPLYING, evalId);
		
		// no eval
		if (evalId == -1) {
			return "redirect:/vendor/confirm/" + vendorBasicInfo.getVendorId();
		// special situation for definition by self（基本信息编辑，确认画面修改供应商基本信息后提交）
		} else if (evalId == -2) {
			return "redirect:/vendor/edit/confirm/" + vendorBasicInfo.getVendorId();
		} 
		
		// if business type is changed, need create a new evaluation 
		
		
		if(businessType != vendorBasicInfo.getCompanyType().getBusinessType()){
		    evaluationService.updateStatus(Evaluation.STATUS_IN_DELETE, evalId);
		    return "redirect:/evaluation/register/" + vendorBasicInfo.getVendorId();
		}
		// 新供应商申请(外部公司)，确认画面修改供应商基本信息后提交
		return "redirect:/evaluation/confirm/"+ vendorBasicInfo.getVendorId() + "/" + evalId;
	}
	
	//(temp save) update vendor info
	@RequestMapping(value="/update/application/basic/{eval_id}/{id}", method=RequestMethod.POST)
	public String updateBasicInfomationForApplication(
			@PathVariable("eval_id") long evalId,
			@PathVariable("id") String id,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("eval_id", evalId);
			return retUrl;
		}
		int businessType = vendorService.getBusiessType(vendorBasicInfo.getVendorId());
		
		processUpdateBasicInformation(vendorBasicInfo);
		// no evaluation
		if (evalId == -1) {
			return "redirect:/vendor/tmpsave/noeval/confirm/" + vendorBasicInfo.getVendorId()+"/"+id;
		} 
		
		// if business type is changed, need create a new evaluation 
        
        if(businessType != vendorBasicInfo.getCompanyType().getBusinessType()){
            evaluationService.updateStatus(Evaluation.STATUS_IN_DELETE, evalId);
            WorkflowRule tmprule = workflowService.getWorkflowRule(id);
            tmprule.setEvalId(-1);
            workflowService.updateWorkflowRule(tmprule);
            // 重新添加评估信息
            return "redirect:/evaluation/register/" + vendorBasicInfo.getVendorId() + "/" + id;
        }
		
		// 新供应商申请(外部公司)，临时保存确认画面修改供应商基本信息后提交
		return "redirect:/evaluation/tmpsave/confirm/"+ vendorBasicInfo.getVendorId() + "/" + evalId+"/" + id;
	}
	/**
	 * 基本信息编辑后点击提交按钮
	 * 
	 * @param tranId
	 * @param vendorBasicInfo
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/update/edit/basic/{tranId}", method=RequestMethod.POST)
	public String updateBasicInfomationForEdit(
			@PathVariable("tranId") String tranId,
			@ModelAttribute("vendorBasicInfo")VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
		 //= (VendorBasicInfo)command;
		if (vendorService.getVendorStatus(vendorBasicInfo.getVendorId()) == VendorBasicInfo.STATUS_IN_APPLICATION) {
			throw new GeneralException("error.application.failed");
		}
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("forEdit", true);
			return retUrl;
		}
		if (!vendorService.alreadyHasCopy(vendorBasicInfo.getVendorId())) {
			vendorService.copyVendorInfo(vendorBasicInfo.getVendorId());
			vendorBasicInfo.setStatus(VendorBasicInfo.STATUS_EDITING);
		}
		processUpdateBasicInformation(vendorBasicInfo);
		
		return "redirect:/vendor/edit/confirm/" + vendorBasicInfo.getVendorId();
	}
	
	/**
	 * 初期导入的供应商，在更新页面点击变更按钮
	 * 
	 * @param tranId
	 * @param vendorBasicInfo
	 * @param result
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/reeval/update/edit/basic/{tranId}", method=RequestMethod.POST)
	public String updateBasicInForEdit(
			@PathVariable("tranId") String tranId,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
		
		if (vendorService.getVendorStatus(vendorBasicInfo.getVendorId()) == VendorBasicInfo.STATUS_IN_APPLICATION) {
			throw new GeneralException("error.application.failed");
		}
		
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("forEdit", true);
			model.addAttribute("isImportExterVendor", true);
			return retUrl;
		}
		
		if (!vendorService.alreadyHasCopy(vendorBasicInfo.getVendorId())) {
			vendorService.copyVendorInfo(vendorBasicInfo.getVendorId());
			vendorBasicInfo.setStatus(VendorBasicInfo.STATUS_EDITING);
		}
		
		processUpdateBasicInformation(vendorBasicInfo);
		return "redirect:/evaluation/reeval/" + vendorBasicInfo.getVendorId();
	}
	@RequestMapping(value="/update/maintainance/basic/{tranId}", method=RequestMethod.POST)
	public String updateBasicInfomationForMaintainance(
			@PathVariable("tranId") String tranId,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		
		if (retUrl != null) {
			model.addAttribute("forMaintainance", true);
			return retUrl;
		}
//		boolean parentCompanyIsSSCS = true;
//		
//		// 所属公司SSCS值为1，默认为SSCS
//		if("2".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())  
//				|| "3".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())){
//			parentCompanyIsSSCS = false;
//		}		
		processUpdateBasicInformation(vendorBasicInfo);
//		if(parentCompanyIsSSCS){
		ModificationHistory history = 
				new ModificationHistory(vendorBasicInfo.getVendorId(), -1, ModificationHistory.CLASS_UPDATE);
		
		dataService.storeModificationHistory(history);
//		}
		
		return "redirect:/data/vendor/search";
	}
	
	@RequestMapping(value="/update/workflow/basic/{wfid}", method=RequestMethod.POST)
	public String updateBasicInfomationForApplication(
			@PathVariable("wfid") String wfid,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		vendorBasicInfo.setStatus(VendorBasicInfo.STATUS_IN_APPLICATION);
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("wfid", wfid);
			return retUrl;
		}
		processUpdateBasicInformation(vendorBasicInfo);
		
		return "redirect:/workflow/application/" + wfid;
	}

	@RequestMapping(value="/update/workflow/hd/basic/{wfid}", method=RequestMethod.POST)
	public String updateBasicInfomationForHD(
			@PathVariable("wfid") String wfid,
			@Valid VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model) {
	
		String retUrl = validateBasicInformation(vendorBasicInfo, result, model);
		if (retUrl != null) {
			model.addAttribute("wfid", wfid);
			model.addAttribute("isRoleHD", true);

			return retUrl;
		}
		processUpdateBasicInformation(vendorBasicInfo);
		return "redirect:/workflow/approval/" + wfid;
	}
/**
 * 
 */
	private String validateBasicInformation(VendorBasicInfo vendorBasicInfo,
			BindingResult result,
			Model model){
		vendorBasicInfo.validate(result,vendorService,vendorBasicInfo.getVendorId());
		List<BankAccount> accounts = vendorBasicInfo.getBankInformation().getBankAccounts();
		boolean hasError = false;
		for (BankAccount account : accounts) {
			hasError = account.validate(result, vendorService);
		}

		// validate attachment
		List<Attachment> attaList = vendorService.findAttachment(vendorBasicInfo.getTranId());
		//attaList.addAll(vendorService.findAttachment(vendorBasicInfo.getVendorId()));
		if (attaList == null || attaList.size() == 0) {
			model.addAttribute("attachmentsIsRequired", true);
			hasError = true;
		}
		
		if (result.hasErrors() || hasError) {
			model.addAttribute(vendorBasicInfo);
			model.addAttribute("countries", vendorService.getCountries());
			model.addAttribute("regions", vendorService.getRegion(vendorBasicInfo.getContact().getCountry()));
			model.addAttribute("action", "update");
			model.addAttribute("attachments", attaList);
			model.addAttribute("bankAccounts", accounts);
			model.addAttribute("canChangeBusinessType", vendorService.isHasAbilityToChangeBusinessType(vendorBasicInfo.getVendorId()));
			modelAddSuperCompany(model);
			return "vendor/basic_information_update_form";
		}
		return null;
	}
	/**
	 * 供应商基本信息编辑后点击提交按钮中调用该方法
	 * 
	 * @param vendorBasicInfo
	 * @param result
	 * @param model
	 * @return
	 */
	private void processUpdateBasicInformation(VendorBasicInfo vendorBasicInfo) {
	
		List<BankAccount> accounts = vendorBasicInfo.getBankInformation().getBankAccounts();
		
		vendorService.updateVendorBasicInfo(vendorBasicInfo, accounts);
		
		vendorService.fixAttachmentData(vendorBasicInfo.getTranId(), vendorBasicInfo.getVendorId());
		
	}
	
	@RequestMapping(value="/regions/{cid}", method=RequestMethod.GET)
	public String getRegions(@PathVariable("cid") String cid, Model model) {
		model.addAttribute("regions", vendorService.getRegion(cid));
		
		return "vendor/regions";
	}
	
	/**
	 * 
	 * @Title:       viewBasicInfomation 
	 * @Description: 供应商查询列表中点击详细按钮响应
	 * @param        @param vendorId
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	@RequestMapping(value="/view/{vendorId}", method=RequestMethod.GET)
	public String viewBasicInfomation(@PathVariable("vendorId") long vendorId, Model model) {
		
		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
		
		vendorBasicInfo.getContact().setRegion(
				vendorService.getRegionName(
						vendorBasicInfo.getContact().getCountry(), vendorBasicInfo.getContact().getRegion()));

		vendorBasicInfo.getContact().setCountry(
				vendorService.getCountryName(vendorBasicInfo.getContact().getCountry()));
		
		model.addAttribute("freeze", "1".equals(vendorBasicInfo.getFreeze()) ? true : false);
		
		model.addAttribute("vendor_id", vendorId);
		model.addAttribute("basic_info", vendorBasicInfo);
		model.addAttribute("attachments", vendorService.findAttachment(vendorId));
		boolean hasEvaluation = evaluationService.hasLatestEvaluationByVendorId(vendorId);
		model.addAttribute("hasEvaluation", hasEvaluation);
		model.addAttribute("isOldNonTrade", Utils.isOldNonTrade(vendorBasicInfo.getCompanyType().getBusinessType()));
		if (hasEvaluation) {
			Evaluation eval = evaluationService.findLatestEvaluationByVendorId(vendorId);
			model.addAttribute("evaluation", eval);
			model.addAttribute("canReEval", true);
			if(Utils.isNonTrade(eval.getBusinessType())){
				List<EvaluationHistory> evaluationHistoryList = evaluationService.getNonTradeEvaluationHistories(vendorId);
				int flag = evaluationHistoryList.size() > 0 ? 1 : 0;
				model.addAttribute("nonTradeEvaluationForm", 
						evaluationService.getNonTradeEvaluationForm(eval.getBusinessType(), flag, eval.getId()));	
				model.addAttribute("pastEvaluations", evaluationHistoryList);
				return "vendor/detail_non_trade";
			}else{
				model.addAttribute("evaluationCategories", evaluationService.getEvaluationCategories(eval.getBusinessType()));
				model.addAttribute("pastEvaluations", evaluationService.getEvaluationHistories(vendorId));
			}
		}else{
			model.addAttribute("isImportExterVendor", !hasEvaluation && vendorBasicInfo.getIdentity().getVendorType() == 0);
		}
		
		return "vendor/detail";
	}


	@RequestMapping(value="/delete/{vendorId}", method=RequestMethod.GET)
	public String deleteVendorBasicInfo(@PathVariable("vendorId") long vendorId) {
		
		vendorService.updateStatus(VendorBasicInfo.STATUS_DELETED, vendorId);
//		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
//		boolean parentCompanyIsSSCS = true;
//		
//		// 所属公司SSCS值为1，默认为SSCS
//		if("2".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())  
//				|| "3".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())){
//			parentCompanyIsSSCS = false;
//		}
//		if(parentCompanyIsSSCS){
		ModificationHistory history = 
				new ModificationHistory(vendorId, -1, ModificationHistory.CLASS_DELETE);
		dataService.storeModificationHistory(history);
//		}

		return "redirect:/data/vendor/search";
	}
	
	/**
	 * 
	 * @Title:       freezeVendorBasicInfo 
	 * @Description:   ( 冻结  ) 
	 * @param        @param vendorId
	 * @param        @return     
	 * @return       String      
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/freeze/{vendorId}", method=RequestMethod.GET)
	public String freezeVendorBasicInfo(@PathVariable("vendorId") long vendorId) {
		
		vendorService.updateForFreeze(vendorId);
		
//		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
//		boolean parentCompanyIsSSCS = true;
//		
//		// 所属公司SSCS值为1，默认为SSCS
//		if("2".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())  
//				|| "3".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())){
//			parentCompanyIsSSCS = false;
//		}
//		if(parentCompanyIsSSCS){
		ModificationHistory history = new ModificationHistory(
				vendorId, -1,
				ModificationHistory.CLASS_FREEZE);
		
		dataService.storeModificationHistory(history);
//		}
		return "redirect:/data/vendor/search";
	}
	
	@RequestMapping(value="/unfreeze/{vendorId}", method=RequestMethod.GET)
	public String unfreezeVendorBasicInfo(@PathVariable("vendorId") long vendorId) {
		
		vendorService.updateForUnfreeze(vendorId);
//		VendorBasicInfo vendorBasicInfo = vendorService.findAvailableVendorBasicInfo(vendorId);
//		boolean parentCompanyIsSSCS = true;
//		
//		// 所属公司SSCS值为1，默认为SSCS
//		if("2".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())  
//				|| "3".equals(vendorBasicInfo.getCompanyType().getSuperCompanyType())){
//			parentCompanyIsSSCS = false;
//		}
//		if(parentCompanyIsSSCS){
			ModificationHistory history = new ModificationHistory(
					vendorId, -1,
					ModificationHistory.CLASS_UNFREEZE);
			
			dataService.storeModificationHistory(history);
//		}

		return "redirect:/data/vendor/search";
	}
	
	/**
	 * 
	 * @Title:       searchVendor 
	 * @Description: ( 供应商查询 响应查询按钮action  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws IOException 
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 9, 2012
	 */
	@RequestMapping(value="/search", method=RequestMethod.POST)
	public String searchVendor(VendorSearchCriteria criteria, Model model,@RequestParam(required = true) String todoAction, HttpServletResponse response) throws Exception {
		model.addAttribute("criteria", criteria);
		modelAddSuperCompany(model);
		if (criteria.getText() == null 
		        || !vendorService.validate(String.valueOf(criteria.getSuperCompanyType()),
		                Utils.getUser().getUsername())) {
			return "vendor/search_vendor_form";
		}
		if("download".equals(todoAction)){
			downloadVendor(criteria, model, response);
		}else if("search".equals(todoAction)){
			int pages = -1;
			List<SimpleVendorInfo> vendors = null;
			
			
			//设置sql条件
			criteria.sb = new StringBuffer();
			//criteria.sb.append("WHERE (status = 5 OR status = 20 OR status = 30 OR status = 40) " );
			criteria.sb.append("WHERE status = 40 " );
			/* 拼接的是sql写一个方法终结*/
			if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
				if (criteria.getType().equals(VendorSearchCriteria.TYPE_NAME)) {
					/* 拼接sql  => name */
					criteria.and().Left()
					 		 .vendorChineseNameLike(criteria.getText().trim()).
					 		 or()
					 		 .vendorEnglishNameLike(criteria.getText().trim())
					 		 			.Right();
					
				} else if (criteria.getType().equals(VendorSearchCriteria.TYPE_CODE)) {//
					criteria.and().vendorCodeEqual(criteria.getText().trim());
//					criteria.and().vendorTypeEqual(criteria.getText().trim());
				} else if (criteria.getType().equals(VendorSearchCriteria.TYPE_NUMBER)){
					criteria.and().numberEqual(criteria.getText().trim());
				}
			}
			
			/* 拼接sql  => vendor_type = category */
			if(3 != criteria.getCategory()){
				criteria.and().vendorTypeEqual(criteria.getCategory());
			}
			
			/* 拼接sql  => super_company = superCompany */
			if(0 != criteria.getSuperCompanyType()){
				criteria.and().superCompanyEqual(criteria.getSuperCompanyType());
			}
			/**
			 * 
			 */
			criteria.setSbforDownload(criteria.sb.toString());
			/*  (保证searchCount的条件和searchVendor的条件是一致的就可以了)*/
			int searchVendorCount = vendorService.searchVendorCountForAll(criteria.sb.toString());
			//分页查询
			criteria.sb.append("  LIMIT " + criteria.getRecordFrom() + ", " + 20 );
			vendors = vendorService.searchVendorInfoForAll(criteria.sb.toString());
			
			pages = vendors.size() > 0 ? ( searchVendorCount % 20 == 0 ? searchVendorCount / 20 - 1 : searchVendorCount / 20 ) : 0;
			
			if (vendors != null && vendors.size() > 0) {
				model.addAttribute("vendors", vendors);
				model.addAttribute("pages", pages);
			}
			
		}
		
		return "vendor/search_vendor_form";
	}
	
	/**
	 * 
	 * @Title:       downloadVendor 
	 * @Description:  ( 响应供应商数据下载页面下载按钮下载功能  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @param response
	 * @param        @throws IOException     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	public void downloadVendor(VendorSearchCriteria criteria, Model model, HttpServletResponse response) throws Exception {
		//全员都可以下载
		//Utils.checkHasRoleHD();
		// 下载所有登记状态供应商
		List<InterfaceData> datas = dataService.findInterfaceData(criteria);
		//使用模板
		HSSFWorkbook workBook = new HSSFWorkbook(getClass().getClassLoader().getResourceAsStream("vendors.xls"));
		workBook.setSheetName(0, "Vendors");
		HSSFSheet sheet = workBook.getSheetAt(0);
		// follow the template at the third line begin.
		int row = 2;
		
		for (InterfaceData data : datas){
			String date = evaluationService.getLatestEval(data.getVendorId()); //
			write(date, data, sheet.createRow(row++));
		}
		response.setContentType("application/vnd.ms-excel");
		response.setHeader(
		        "Content-Disposition",
		        "filename=\"" + "vendors.xls" + "\"");
		
		
		OutputStream out = response.getOutputStream();
		workBook.write(out);
		out.flush();
		out.close();
	}
	
	@SuppressWarnings("deprecation")
	private void write(String date, InterfaceData data, HSSFRow hssfRow) {
		String vendorCode = data.getVendorCode() == null || ("").equals(data.getVendorCode()) ? "" : data.getVendorCode();
		String vendorSubCode = "00".equals(data.getVendorSubCode()) ? "" : "-" + data.getVendorSubCode();
		//郑同 2012年7月12日15:38:08 modify 插入所属公司0、业务类型3、是否评估4、是否review字段
		//所属公司
		String superCompany = "";
		switch (data.getSuperCompany()) {
    		case 1:
    			superCompany = "SSCS";		break;
    		case 2:
    			superCompany = "SSGE";		break;
    		case 3:
    			superCompany = "SSV";		break;
    		case 4:
                superCompany = "SEW";       break;
    		case 5:
                superCompany = "SDPW";       break;
    		case 6:
                superCompany = "SPDH";       break;
    		case 7:
                superCompany = "SEH";       break;
    		default:
                superCompany = "SSCS";
		}
		hssfRow.createCell((short)0).setCellValue(
				new HSSFRichTextString( superCompany ));		
		// 供应商编号
		hssfRow.createCell((short)1).setCellValue(
				new HSSFRichTextString( vendorCode + vendorSubCode));
		/*
		 * 名称
		 * 
		 * 如果供应商变更数据下载表格里，供应商的名称，根据银行账户币种，抓取不同的名称
		 * USD,JPY,HKD账户抓取英文名称， RMB账户抓取 中文名称
		 * 比方说  		313800   	索尼物流       币种：RMB，       	 中国银行
		 *              313800-01   SSCS      币种：USD       花旗银行
		 *              313800-02   SSCS      币种：JPY       东三菱银行"		
		 *  
		 *注：	USD,JPY,HKD时，优先抓英文名称，如果为空，则抓中文名
		 *  	RMB时，优先抓中文名称，如果为空，则抓英文名
		 *		币种为空时，优先抓中文（根据实际情况分析后的结果）
		 */
		if((data.getCurrency() != null // 币种不为空
				&& ("USD".equals(data.getCurrency()) || "JPY".equals(data.getCurrency()) || "HKD".equals(data.getCurrency()) // 币种为3种之一
						)) && data.getEnglishName() != null //英文名不为空
						){
			hssfRow.createCell((short)2).setCellValue(
					new HSSFRichTextString(data.getEnglishName()));
		}else{
			hssfRow.createCell((short)2).setCellValue(
					new HSSFRichTextString(data.getName()));
		}
		
		//业务类型
		String businessType = "";
		switch (data.getBusinessType()) {
		case 10:
			businessType = "物流（仓库）";		break;
		case 11:
			businessType = "物流（运输）";		break;
		case 12:
			businessType = "物流（仓库&运输）";break;
		case 13:
			businessType = "物流（海关）";		break;
		case 14:
			businessType = "物流（仓库&海关）";break;
		case 15:
			businessType = "物流（运输&海关）";break;
		case 16:
			businessType = "物流（仓库&运输&海关）";break;
		case 1:
			businessType = "贸易";		break;
		case 2:
			businessType = "共通";		break;
		default:
			break;
		}
		hssfRow.createCell((short)3).setCellValue(
						new HSSFRichTextString(businessType ));
		//获取供应商的有效评估信息数量
		int evaCount = evaluationService.hasEvals(data.getVendorId(),Evaluation.STATUS_IS_EFFECTIVE);
		//是否评估	
		hssfRow.createCell((short)4).setCellValue(
				new HSSFRichTextString( evaCount >= 1?"是":"否" ));		
		//是否Review
		hssfRow.createCell((short)5).setCellValue(
				new HSSFRichTextString( evaCount >= 2?"是":"否"));		

		// 传真
		hssfRow.createCell((short)6).setCellValue(
				new HSSFRichTextString(data.getFax()));
		// 银行
		hssfRow.createCell((short)7).setCellValue(
				new HSSFRichTextString(data.getBankName()));
		// 帐号
		hssfRow.createCell((short)8).setCellValue(
				new HSSFRichTextString(data.getBankAccount()));
		
		// 是否本地银行
		String bankCityType = "FALSE";
		if(data.getBankCityType() == 1){
			bankCityType = "TRUE";
		}
		hssfRow.createCell((short)9).setCellValue(new HSSFRichTextString(bankCityType));
		// 分组
		hssfRow.createCell((short)10).setCellValue(new HSSFRichTextString(data.getVendorType()));
		// 类别
		hssfRow.createCell((short)11).setCellValue(new HSSFRichTextString(data.getVendorGroup()));
		// 联系电话
		hssfRow.createCell((short)12).setCellValue(
				new HSSFRichTextString(data.getTel()));
		// 联系地址
		hssfRow.createCell((short)13).setCellValue(
				new HSSFRichTextString(data.getAddress()));
		// 联系人
		hssfRow.createCell((short)14).setCellValue(
				new HSSFRichTextString(data.getContactPerson()));
		// 银行地址	
		hssfRow.createCell((short)15).setCellValue(
				new HSSFRichTextString(data.getBankAddress()));
		// Swift代码
		hssfRow.createCell((short)16).setCellValue(
				new HSSFRichTextString(data.getSwiftCode()));
		// 第17列为银行Routing代码
		// 下载变更数据时没有，所以不对应
		// 收款期限
		hssfRow.createCell((short)18).setCellValue(new HSSFRichTextString(data.getPaymentTerm()));
		// 是否有效
		String effective = "TRUE";
		if(data.getFreeze() != null && "1".equals(data.getFreeze())){
			effective = "FALSE";
		}
		hssfRow.createCell((short)19).setCellValue(new HSSFRichTextString(effective));
		// 账户类型
		String vendorAccountType = data.getVendorAccountType();
		vendorAccountType = vendorAccountType == null || vendorAccountType.equals("vendor") ? "" : vendorAccountType;
		hssfRow.createCell((short)20).setCellValue(new HSSFRichTextString(vendorAccountType));
		// 银行国家	
		hssfRow.createCell((short)21).setCellValue(
				new HSSFRichTextString(data.getBankProvinceAndRegion()));
		
		//22币种	23是否中行	24联行号	25机构号	26CNAPS
		hssfRow.createCell((short)22).setCellValue(
				new HSSFRichTextString(data.getCurrency() == null 
										? "" :(data.getCurrency().equals("RMB") ? "CNY" : data.getCurrency())
								));
		
		String bankType = "TRUE";
		if(data.getBankType() == 1){
			bankType = "FALSE";
		}
		hssfRow.createCell((short)23).setCellValue(new HSSFRichTextString(bankType));
		hssfRow.createCell((short)24).setCellValue(
				new HSSFRichTextString(data.getJointCode()));
		hssfRow.createCell((short)25).setCellValue(
				new HSSFRichTextString(data.getInstitutionNo()));
		hssfRow.createCell((short)26).setCellValue(
				new HSSFRichTextString(data.getCnaps()));
		//银行是否有效
		String bankValid = "true";
		if(data.getBankStatus() != 0){
			bankValid = "false";
		}
		hssfRow.createCell((short)27).setCellValue(
				new HSSFRichTextString(bankValid));
		
		// 省市和地区
		hssfRow.createCell((short)28).setCellValue(
				new HSSFRichTextString(data.getRegion()));
		// 城市
		hssfRow.createCell((short)29).setCellValue(
				new HSSFRichTextString(data.getCity()));
		// 组代码
		hssfRow.createCell((short)30).setCellValue(
				new HSSFRichTextString(data.getGroupKey()));
		// 申请人
		hssfRow.createCell((short)31).setCellValue(
				new HSSFRichTextString(data.getApplient_name()));
		// 最新评估时间列
		hssfRow.createCell((short)32).setCellValue(
				new HSSFRichTextString(date));
	}
	/**
	 * 
	 * @Title:       goSearchVendor 
	 * @Description: ( 菜单点击供应商查询按钮  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	@RequestMapping(value="/search", method=RequestMethod.GET)
	public String goSearchVendor(VendorSearchCriteria criteria, Model model) {
		model.addAttribute("criteria", criteria);
		modelAddSuperCompany(model);
		return "vendor/search_vendor_form";
	}


	private void modelAddSuperCompany(Model model) {
	    String id = Utils.getUser().getUsername();
        List<String> supercompanyList = workflowService.getUsercompanyById(id);
        List<SuperCompany> superCompanies = new ArrayList<SuperCompany>();
        
        for(String spc : supercompanyList){
            if("1".equals(spc)){
                model.addAttribute("isSSCS", true);
                superCompanies.clear();
                break;
            }
            SuperCompany superCompany = new SuperCompany(spc);
            if(!superCompanies.contains(superCompany)){
                superCompanies.add(superCompany);
            }
            
        }
        model.addAttribute("supercompany", superCompanies);
    }

    @RequestMapping(value="/search/bank", method=RequestMethod.GET)
	public String getBankSearch(BankSearchCriteria criteria, Model model) {
		model.addAttribute("criteria", criteria);
		return "vendor/search_bank_form";
	}

	@RequestMapping(value="/search/bank", method=RequestMethod.POST)
	public String bankSearch(BankSearchCriteria criteria, Model model) {
		model.addAttribute("criteria", criteria);
		// keyword null, bank search 
		//if (criteria.getName() != null && !"".equals(criteria.getName())) {
		
			switch (criteria.getType()) {
			case BankSearchCriteria.TYPE_BANK_NO:
			{
				int count = vendorService.searchBankMasterCount(criteria);
				int pages = count % 20 ==0?count / 20 - 1:count / 20;
				if (count == 0) {
					break;
				}
				model.addAttribute("pages", pages);
				model.addAttribute("bankMasters", vendorService.searchBankMaster(criteria));
				break;
			}
			case BankSearchCriteria.TYPE_CNAPS:
			{
				int count = vendorService.searchCnapsCount(criteria);
				int pages = count % 20 ==0?count / 20 - 1:count / 20;
				if (count == 0) {
					break;
				}
				model.addAttribute("pages", pages);
				model.addAttribute("cnaps", vendorService.searchCnaps(criteria));
				break;
			}
			}
		//}
		
		return "vendor/search_bank_form";
	}

	//供应商查询详细页面，取消编辑按钮
	@RequestMapping(value="/edit/cancel/basic/{vendorId}", method=RequestMethod.GET)
	public String cancelEditing(@PathVariable("vendorId") long vendorId, Model model) {
		
		vendorService.restoreVendorInfo(vendorId);
		
		return "redirect:/vendor/search";
	}
	@RequestMapping(value="/attachmentInfo", method=RequestMethod.GET)
	public String showAttachmentInfo() {
		return "vendor/attachment_info";
	}
	/**
	 * 一时保存供应商信息
	 * 
	 * @param vendorId
	 * @param evalId
	 * @param model
	 * @return
	 */
	@RequestMapping(value="/temp/save/{vendorId}/{evalId}", method=RequestMethod.GET)
	public String submitEvaluation(@PathVariable("vendorId") long vendorId,
			@PathVariable("evalId") long evalId, Model model) {
		// 生成 number
		String number = "VND-";
		String numberTemp = ("00000000" + vendorId);
		number += numberTemp.substring(numberTemp.length() - 8);
		// 更新供应商status number
		vendorService.tempSaveVendorInfo(vendorId, number);
		// 一个供应商可能存在多个评估，有的评估是垃圾数据。所以更新评估状态为30
		evaluationService.updateStatus(Evaluation.STATUS_IS_TEMP_SAVE, evalId);
		return "redirect:/vendor/search";
	}
	
	public void setL1L2Approval(VendorBasicInfo vendorBasicInfo, Model model) {
        // vendor super company
        VndMgmntUser user = Utils.getUser();
        String supercompany = vendorBasicInfo.getCompanyType().getSuperCompanyType();
        // init division
        List<String> costcenter = evaluationService.getCostcenterBySuperCompany(supercompany);
        if(costcenter == null || costcenter.size() == 0){
            throw new GeneralException("This vendor's super company has no employes");
        }
        int selectedDivision = Integer.parseInt(costcenter.get(0));
        if(supercompany.equals(user.getUserCompany())){
            selectedDivision = user.getDivision();
        }
        model.addAttribute("supercompany", supercompany);
        model.addAttribute("costcenter", costcenter);
        model.addAttribute("selectedDivision", selectedDivision);
        model.addAttribute("l1s", evaluationService.findL1Users(selectedDivision, supercompany));
        model.addAttribute("l2s", evaluationService.findL2Users(selectedDivision, supercompany));
    }
}
