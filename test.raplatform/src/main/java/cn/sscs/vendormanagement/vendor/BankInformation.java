package cn.sscs.vendormanagement.vendor;

import java.util.ArrayList;

import java.util.List;

/**
 * 
 * @author kazzy
 *
 */
public class BankInformation {

	private long[] bankAccountId = null;
	
	private String[] vendorSubcode = null;
	
	private String[] provinceAndRegion = null;
	
	private String[] swiftCode = null;
	
	private String[] name = null;
	
	private String[] account = null;
	
	private int[] city = null;
	
	private String[] currency = null;
	
	private int[] type = null;
	
	private String[] institutionNo = null;
	
	private String[] jointCode = null;
	
	private String[] cnaps = null;
	
	private List<BankAccount> bankAccounts = null;
	
	/**
	 * 
	 */
	BankInformation() {}
	
	/**
	 * 
	 * @param bankAccounts
	 */
	BankInformation(List<BankAccount> bankAccounts) {
		this.bankAccounts = bankAccounts;
	}
	
	void init() {
		int size = bankAccounts.size();
		
		bankAccountId = new long[size];
		vendorSubcode = new String[size];
		provinceAndRegion = new String[size];
		swiftCode = new String[size];
		name = new String[size];
		account = new String[size];
		city = new int[size];
		currency = new String[size];
		type = new int[size];
		institutionNo = new String[size];
		jointCode = new String[size];
		cnaps = new String[size];
		
		for (int i=0; i<bankAccounts.size(); i++) {
			BankAccount bankAccount = bankAccounts.get(i);
			
			bankAccountId[i] = bankAccount.getBankAccountId();
			vendorSubcode[i] = bankAccount.getVendorSubcode();
			provinceAndRegion[i] = bankAccount.getProvinceAndRegion();
			swiftCode[i] = bankAccount.getSwiftCode();
			name[i] = bankAccount.getName();
			account[i] = bankAccount.getAccount();
			city[i] = bankAccount.getCity();
			currency[i] = bankAccount.getCurrency();
			type[i] = bankAccount.getType();
			institutionNo[i] = bankAccount.getInstitutionNo();
			jointCode[i] = bankAccount.getJointCode();
			cnaps[i] = bankAccount.getCnaps();
		}
		
	}
	
	/**
	 * 
	 * @return
	 */
	public long[] getBankAccountId() {
		return bankAccountId;
	}
	
	/**
	 * 
	 * @param bankAccountId
	 */
	public void setBankAccountId(long[] bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getVendorSubcode() {
		return vendorSubcode;
	}
	
	/**
	 * 
	 * @param vendorSubcode
	 */
	public void setVendorSubcode(String[] vendorSubcode) {
		this.vendorSubcode = vendorSubcode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getProvinceAndRegion() {
		return provinceAndRegion;
	}
	
	/**
	 * 
	 * @param provinceAndRegion
	 */
	public void setProvinceAndRegion(String[] provinceAndRegion) {
		this.provinceAndRegion = provinceAndRegion;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getSwiftCode() {
		return swiftCode;
	}
	
	/**
	 * 
	 * @param swiftCode
	 */
	public void setSwiftCode(String[] swiftCode) {
		this.swiftCode = swiftCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getName() {
		return name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String[] name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getAccount() {
		return account;
	}
	
	/**
	 * 
	 * @param account
	 */
	public void setAccount(String[] account) {
		this.account = account;
	}
	
	/**
	 * 
	 * @return
	 */
	public int[] getCity() {
		return city;
	}
	
	/**
	 * 
	 * @param city
	 */
	public void setCity(int[] city) {
		this.city = city;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getCurrency() {
		return currency;
	}
	
	/**
	 * 
	 * @param currency
	 */
	public void setCurrency(String[] currency) {
		this.currency = currency;
	}
	
	/**
	 * 
	 * @return
	 */
	public int[] getType() {
		return type;
	}
	
	/**
	 * 
	 * @param type
	 */
	public void setType(int[] type) {
		this.type = type;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getInstitutionNo() {
		return institutionNo;
	}
	
	/**
	 * 
	 * @param institutionNo
	 */
	public void setInstitutionNo(String[] institutionNo) {
		this.institutionNo = institutionNo;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getJointCode() {
		return jointCode;
	}
	
	/**
	 * 
	 * @param jointCode
	 */
	public void setJointCode(String[] jointCode) {
		this.jointCode = jointCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getCnaps() {
		return cnaps;
	}
	
	/**
	 * 
	 * @param cnaps
	 */
	public void setCnaps(String[] cnaps) {
		this.cnaps = cnaps;
	}
	
	/**
	 * 
	 * @Title:       getBankAccounts 
	 * @Description: 获取银行的最大数组(最多条数)并逐条解析成javabean对象
	 * @param        @return     
	 * @return       List<BankAccount>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 5, 2012
	 */
	public List<BankAccount> getBankAccounts() {
		if (bankAccountId == null) {
			return null;
		}
		
		List<BankAccount> accounts = new ArrayList<BankAccount>();
		
		int length = Math.max(bankAccountId.length, vendorSubcode.length);
		length = Math.max(length, provinceAndRegion.length);
		length = Math.max(length, swiftCode.length);
		length = Math.max(length, name.length);
		length = Math.max(length, account.length);
		length = Math.max(length, city.length);
		length = Math.max(length, currency.length);
		length = Math.max(length, type.length);
		length = Math.max(length, institutionNo.length);
		length = Math.max(length, jointCode.length);
		length = Math.max(length, cnaps.length);
		
		for (int i=0; i<length; i++) {
			BankAccount bankAccount = new BankAccount(
					i < bankAccountId.length ? bankAccountId[i] : -1,
					i < vendorSubcode.length ? vendorSubcode[i] : null,
					i < provinceAndRegion.length ? provinceAndRegion[i] : null,
					i < swiftCode.length ? swiftCode[i] : null,
					i < name.length ? name[i] : null,
					i < account.length ? account[i] : null,
					i < city.length ? city[i] : -1,
					i < currency.length ? currency[i] : null,
					i < type.length ? type[i] : -1,
					i < institutionNo.length ? institutionNo[i] : null,
					i < jointCode.length ? jointCode[i] : null,
					i < cnaps.length ? cnaps[i] : null,
					BankAccount.STATUS_ALIVE
				);
			
			accounts.add(bankAccount);
		}
		
		return accounts;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BankAccount> getStaticBankAccounts() {
		return bankAccounts;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getStaticBankAccountCount() {
		return bankAccounts.size();
	}
}
