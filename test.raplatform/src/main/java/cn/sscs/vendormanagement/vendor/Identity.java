package cn.sscs.vendormanagement.vendor;

import java.util.Locale;
import java.util.Map;

import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.validation.Errors;

/**
 * 
 * @author kazzy
 *
 */
public class Identity {

	static int TYPE_EXTERNAL = 0;
	
	static int TYPE_INTERNAL = 1;
	
	static int TYPE_OTHER = 2;
	
	private String vendorCode = null;
	
	private String vendorNumber = null;
	
	private int vendorType = 0;
	
	private String chineseName = null;
	
	private String englishName = null;
	
	/**
	 * 
	 */
	public Identity(){}
	
	/**
	 * 
	 * @param rowMap
	 */
	Identity(Map<String, Object> rowMap) {
		vendorCode = (String) rowMap.get("vendor_code");
		vendorType = (Integer) rowMap.get("vendor_type");
		chineseName = (String) rowMap.get("vendor_chinese_name");
		englishName = (String) rowMap.get("vendor_english_name");
		vendorNumber = (String) rowMap.get("number");
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVendorCode() {
		return vendorCode;
	}
	
	/**
	 * 
	 * @param vendorCode
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getVendorType() {
		return vendorType;
	}
	
	/**
	 * 
	 * @param vendorType
	 */
	public void setVendorType(int vendorType) {		
		this.vendorType = vendorType;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getChineseName() {
		return chineseName;
	}
	
	/**
	 * 
	 * @param chineseName
	 */
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getEnglishName() {
		return englishName;
	}
	
	/**
	 * 
	 * @param englishName
	 */
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	public String getVendorNumber() {
		return vendorNumber;
	}

	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}

	/**
	 * 
	 * @param errors
	 */
	public void validate(Errors errors, IVendorService vendorService, long vendorId, String superCompanyType) {
		if ("".equals(chineseName) && "".equals(englishName)) {
			errors.rejectValue("identity.chineseName", "error.vendor_name");
			errors.rejectValue("identity.englishName", "error.vendor_name");
		} else {
			if(vendorService.isValidIdentityCName(chineseName, vendorId, superCompanyType)){
				errors.rejectValue("identity.chineseName", "error.vendor_cname");
			}
			if(vendorService.isValidIdentityEName(englishName, vendorId, superCompanyType)){
				errors.rejectValue("identity.englishName", "error.vendor_ename");
			}
		} 
	}

	/**
	 * 
	 * @return
	 */
	public String getName() {
		Locale locale = LocaleContextHolder.getLocale();
		if ("zh".equals(locale.getLanguage())) {
			return chineseName != null && !"".equals(chineseName) ? chineseName : englishName;
		} else if ("en".equals(locale.getLanguage())){
			return englishName != null && !"".equals(englishName) ? englishName : chineseName;
		} else {
			if (chineseName != null && !"".equals(chineseName)) {
				return chineseName;
			} else if (englishName != null && !"".equals(englishName)) {
				return englishName;
			} else {
				return null;
			}
		}
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("vendor_code", vendorCode);
		rowMap.put("vendor_type", vendorType);
		rowMap.put("number", vendorNumber);
		rowMap.put("vendor_chinese_name", chineseName);
		rowMap.put("vendor_english_name", englishName);
	}
}
