package cn.sscs.vendormanagement.vendor;

/**
 * 
 * @author kazzy
 * 
 */
public class VendorSearchCriteria {

	public static final String TYPE_NAME = "name";

	public static final String TYPE_CODE = "code";

	public static final String TYPE_NUMBER = "number";

	public static final int CATEGORY_EXTERNAL = 0;

	public static final int CATEGORY_INTERNAL = 1;
	
	public static final int CATEGORY_ALL = 3;
	
	public static final int DOWNLOAD_ALL = 0;
	
	public static final int DOWNLOAD_FREEZE = 1;
	
	public static final int DOWNLOAD_VALID = 2;
	
	
	private String text = "";

	private String type = TYPE_NAME;

	private int category = CATEGORY_ALL;
	
	/**
	 * 郑同
	 * 2012年7月9日17:10:30
	 * 供应商查询
	 */
	public static final int COMPANYTYPE_ALL = 0;
	
	public static final int COMPANYTYPE_SSCS = 1;
	
	public static final int COMPANYTYPE_SSGE = 2;
	
	public static final int COMPANYTYPE_SSV = 3;
	
	private String sbforDownload;
	
	public StringBuffer sb;
	/* 设置条件函数 */
	private String alias = "vendor_basic_info"; // 多表用语设置别名

	@SuppressWarnings("unused")
	private String prefix = alias+"."; //前缀
	
	public VendorSearchCriteria and()
	{sb.append("AND ");return this;}
	
	public VendorSearchCriteria or()
	{sb.append("OR ");return this;}
	
	public VendorSearchCriteria Left()
	{sb.append("( ");return this;}
	
	public VendorSearchCriteria Right()
	{sb.append(") ");return this;}
	
	
	public VendorSearchCriteria append(String sql)
	{sb.append(sql);return this;}
	
	//vendor_chinese_name 
	public VendorSearchCriteria vendorChineseNameEqual(String nameZh)
	{sb.append( "vendor_chinese_name  = '"+nameZh+"' ");return this;}

	public VendorSearchCriteria vendorChineseNameNotEqual(String nameZh)
	{sb.append( "vendor_chinese_name  <> '"+nameZh+"' ");return this;}
	
	public VendorSearchCriteria vendorChineseNameLikeLeft(String nameZh)
	{sb.append( "vendor_chinese_name  LIKE '"+nameZh+"%' ");return this;}
	
	public VendorSearchCriteria vendorChineseNameLikeRight(String nameZh)
	{sb.append( "vendor_chinese_name  LIKE '%"+nameZh+"' ");return this;}	
	
	public VendorSearchCriteria vendorChineseNameLike(String nameZh)
	{sb.append( "vendor_chinese_name  LIKE '%"+nameZh+"%' ");return this;}	
	
	public VendorSearchCriteria vendorChineseNameNotLike(String nameZh)
	{sb.append( "vendor_chinese_name  LIKE '%"+nameZh+"%' ");return this;}	
	

	//vendor_english_name
	public VendorSearchCriteria vendorEnglishNameEqual(String nameZh)
	{sb.append( "vendor_english_name  = '"+nameZh+"' ");return this;}

	public VendorSearchCriteria vendorEnglishNameNotEqual(String nameZh)
	{sb.append( "vendor_english_name  <> '"+nameZh+"' ");return this;}
	
	public VendorSearchCriteria vendorEnglishNameLikeLeft(String nameZh)
	{sb.append( "vendor_english_name  LIKE '"+nameZh+"%' ");return this;}
	
	public VendorSearchCriteria vendorEnglishNameLikeRight(String nameZh)
	{sb.append( "vendor_english_name  LIKE '%"+nameZh+"' ");return this;}	
	
	public VendorSearchCriteria vendorEnglishNameLike(String nameZh)
	{sb.append( "vendor_english_name  LIKE '%"+nameZh+"%' ");return this;}	
	
	public VendorSearchCriteria vendorEnglishNameNotLike(String nameZh)
	{sb.append( "vendor_english_name  LIKE '%"+nameZh+"%' ");return this;}	
	
	//vendor_type 
	public VendorSearchCriteria vendorTypeEqual(Object vendorType)
	{sb.append( "vendor_type  = "+vendorType+" ");return this;}
	
	//super_company 
	public VendorSearchCriteria superCompanyEqual(Object superCompany)
	{sb.append( "super_company  = "+superCompany+" ");return this;}
	
	//vendor_code
	public VendorSearchCriteria vendorCodeEqual(String vendorCode)
	{sb.append( "vendor_code  = '"+vendorCode+"' ");return this;}

	public VendorSearchCriteria vendorCodeNotEqual(String vendorCode)
	{sb.append( "vendor_code  <> '"+vendorCode+"' ");return this;}
	
	public VendorSearchCriteria vendorCodeLikeLeft(String vendorCode)
	{sb.append( "vendor_code  LIKE '"+vendorCode+"%' ");return this;}
	
	public VendorSearchCriteria vendorCodeLikeRight(String vendorCode)
	{sb.append( "vendor_code  LIKE '%"+vendorCode+"' ");return this;}	
	
	public VendorSearchCriteria vendorCodeLike(String vendorCode)
	{sb.append( "vendor_code  LIKE '%"+vendorCode+"%' ");return this;}	
	
	public VendorSearchCriteria vendorCodeNotLike(String vendorCode)
	{sb.append( "vendor_code  LIKE '%"+vendorCode+"%' ");return this;}	
	
	//number
	public VendorSearchCriteria numberEqual(String vendorCode)
	{sb.append( "number  = '"+vendorCode+"' ");return this;}

	public VendorSearchCriteria numberNotEqual(String vendorCode)
	{sb.append( "number  <> '"+vendorCode+"' ");return this;}
	
	public VendorSearchCriteria numberLikeLeft(String vendorCode)
	{sb.append( "number  LIKE '"+vendorCode+"%' ");return this;}
	
	public VendorSearchCriteria numberLikeRight(String vendorCode)
	{sb.append( "number  LIKE '%"+vendorCode+"' ");return this;}	
	
	public VendorSearchCriteria numberLike(String vendorCode)
	{sb.append( "number  LIKE '%"+vendorCode+"%' ");return this;}	
	
	public VendorSearchCriteria numberNotLike(String vendorCode)
	{sb.append( "number  LIKE '%"+vendorCode+"%' ");return this;}
	
	
	
	
	/*供应商查询 所属公司*/
	private int superCompanyType = 0;
	
	public int getSuperCompanyType() {
		return superCompanyType;
	}

	public void setSuperCompanyType(int superCompanyType) {
		this.superCompanyType = superCompanyType;
	}
	
	private int page = 0;

	@SuppressWarnings("unused")
	private int recordFrom = 0;

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * 
	 * @return
	 */
	public String getAmbiguousText() {
		return "%" + text + "%";
	}

	/**
	 * @param text
	 *            the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type
	 *            the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * 
	 * @return
	 */
	public int getCategory() {
		return category;
	}

	/**
	 * 
	 * @param category
	 */
	public void setCategory(int category) {
		this.category = category;
	}

	/**
	 * 
	 * @return
	 */
	public int getPage() {
		return page;
	}

	/**
	 * 
	 * @param page
	 */
	public void setPage(int page) {
		this.page = page;
	}

	public int getRecordFrom() {
		return this.page * 20;
	}

	public void setRecordFrom(int recordFrom) {
		this.recordFrom = recordFrom;
	}

	public String getSbforDownload() {
		return sbforDownload;
	}

	public void setSbforDownload(String sbforDownload) {
		this.sbforDownload = sbforDownload;
	}
}
