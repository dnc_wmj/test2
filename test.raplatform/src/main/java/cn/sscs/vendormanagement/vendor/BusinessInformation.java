package cn.sscs.vendormanagement.vendor;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.springframework.format.annotation.NumberFormat;

/**
 * 
 * @author kazzy
 *
 */
public class BusinessInformation { 
	
	private String date = null;
	
	@NumberFormat
	private Long numberOfDirectStaffs = null;
	@NumberFormat
	private Long numberOfIndirectStaffs = null;
	@NumberFormat//TODO test
	private Long totalStaffs = null;
	
	private long[] bizInfoId = null;
	
	private String[] year = null;
	
	private String[] salesAmount = null;

	private String[] profit = null;
	
	private String[] profitability = null;
	
	List<BizInfoPerformance> performances = null;
	
	/**
	 * 
	 */
	BusinessInformation() {}
	
	/**
	 * 
	 * @param rowMap
	 */
	BusinessInformation(Map<String, Object> rowMap, List<BizInfoPerformance> performances) {
		this.date = (String)rowMap.get("date");
		this.numberOfDirectStaffs = (Long)rowMap.get("number_of_direct_staffs");
		this.numberOfIndirectStaffs = (Long)rowMap.get("number_of_indirect_staffs");
		this.totalStaffs = (Long)rowMap.get("total_staffs");
		this.performances = performances;
	}
	
	void init() {
		int size = performances.size();
		
		bizInfoId = new long[size];
		year = new String[size];
		salesAmount = new String[size];
		profit = new String[size];
		profitability = new String[size];
		
		for (int i=0; i<size; i++) {
			BizInfoPerformance performance = performances.get(i);
			
			bizInfoId[i] = performance.getBizInfoId();
			year[i] = performance.getYear();
			salesAmount[i] = performance.getSalesAmount();
			profit[i] = performance.getProfit();
			profitability[i] = performance.getProfitability(); 
		}
	}

	/**
	 * 
	 * @return
	 */
	public String getDate() {
		return date;
	}
	
	/**
	 * 
	 * @param date
	 */
	public void setDate(String date) {
		this.date = date;
	}
	
	/**
	 * @return the 
	 */
	public Long getNumberOfDirectStaffs() {
		return numberOfDirectStaffs;
	}

	/**
	 * @param 
	 */
	public void setNumberOfDirectStaffs(Long numberOfDirectStaffs) {
		this.numberOfDirectStaffs = numberOfDirectStaffs;
	}

	/**
	 * @return the numberOfIndirectStaffs
	 */
	public Long getNumberOfIndirectStaffs() {
		return numberOfIndirectStaffs;
	}

	/**
	 * @param numberOfIndirectStaffs the numberOfIndirectStaffs to set
	 */
	public void setNumberOfIndirectStaffs(Long numberOfIndirectStaffs) {
		this.numberOfIndirectStaffs = numberOfIndirectStaffs;
	}

	/**
	 * @return the totalStaffs
	 */
	public Long getTotalStaffs() {
		return totalStaffs;
	}

	/**
	 * @param totalStaffs the totalStaffs to set
	 */
	public void setTotalStaffs(Long totalStaffs) {
		this.totalStaffs = totalStaffs;
	}
	
	/**
	 * 
	 * @return
	 */
	public long[] getBizInfoId() {
		return bizInfoId;
	}
	
	/**
	 * 
	 * @param bizInfoId
	 */
	public void setBizInfoId(long[] bizInfoId) {
		this.bizInfoId = bizInfoId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getYear() {
		return year;
	}
	
	/**
	 * 
	 * @param year
	 */
	public void setYear(String[] year) {
		this.year = year;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getSalesAmount() {
		return salesAmount;
	}
	
	/**
	 * 
	 * @param salesAmount
	 */
	public void setSalesAmount(String[] salesAmount) {
		this.salesAmount = salesAmount;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getProfit() {
		return profit;
	}
	
	/**
	 * 
	 * @param profit
	 */
	public void setProfit(String[] profit) {
		this.profit = profit;
	}
	
	/**
	 * 
	 * @return
	 */
	public String[] getProfitability() {
		return profitability;
	}
	
	/**
	 * 
	 * @param profitability
	 */
	public void setProfitability(String[] profitability) {
		this.profitability = profitability;
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("date", date);
		rowMap.put("number_of_direct_staffs", numberOfDirectStaffs);
		rowMap.put("number_of_indirect_staffs", numberOfIndirectStaffs);
		rowMap.put("total_staffs", totalStaffs);
		
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BizInfoPerformance> getPerformance() {
		if (bizInfoId == null) {
			return null;
		}
				
		int length = Math.max(bizInfoId.length, year.length);
		length = Math.max(length, salesAmount.length);
		length = Math.max(length, profit.length);
		length = Math.max(length, profitability.length);
		
		List<BizInfoPerformance> performances = new ArrayList<BizInfoPerformance>();
		
		for (int i=0; i<length; i++) {
			BizInfoPerformance performance = 
				new BizInfoPerformance(
						i < bizInfoId.length ? bizInfoId[i] : -1,
						i < year.length ? year[i] : null,
						i < salesAmount.length ? salesAmount[i] : null,
						i < profit.length ? profit[i] : null,
						i < profitability.length ? profitability[i] : null);
			
			
			if (performance.hasContents()) {
				performances.add(performance);
			}
		}
		
		if (performances.size() == 0) {
			return null;
		}
		
		return performances;
	}
	
	/**
	 * 
	 * @return
	 */
	public List<BizInfoPerformance> getStaticPerformanceList() {
		return performances;
	}

}
