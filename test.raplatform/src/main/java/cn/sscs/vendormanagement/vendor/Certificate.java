package cn.sscs.vendormanagement.vendor;

import java.util.Date;

import java.util.Map;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.Errors;

/**
 * 
 * @author kazzy
 *
 */
public class Certificate {

	private String bizLicenceId = null;
	
	private String legalRepresentative = null;
	
	private String regCapital = null;
	
	private String regAddress = null;
	
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date regDate = null;
	
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date bizLicenceExpirationDate = null;
	
	private String stateTaxRegId = null;
	
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date stateTaxExpirationDate = null;
	
	private String localTaxRegId = null;
	
	@DateTimeFormat(pattern="yyyy/MM/dd")
	private Date localTaxExpirationDate = null;
	
	private String regDateAndAddress = null;
	
	private String cooperationDateOrPeriod = null;
	
	private Boolean certIso9000 = Boolean.FALSE;
	
	private Boolean certIso14000 = Boolean.FALSE;
	
	private Boolean certQs9000 = Boolean.FALSE;
	
	private String certOther = null;
	
	private boolean listedCompany = false;
	
	private boolean haveTransaction = false;
	
	/**
	 * 
	 */
	Certificate() {}
	
	/**
	 * 
	 * @param rowMap
	 */
	Certificate(Map<String, Object> rowMap) {
		this.bizLicenceId = (String) rowMap.get("business_licence_id");
		this.legalRepresentative = (String) rowMap.get("legal_representative");
		this.regCapital = (String) rowMap.get("registration_capital");
		this.regAddress = (String) rowMap.get("registration_address");
		this.regDate = (Date)rowMap.get("registration_date");
		this.bizLicenceExpirationDate = (Date)rowMap.get("business_licence_expiration_date");
		this.stateTaxRegId = (String) rowMap.get("state_tax_reg_id");
		this.stateTaxExpirationDate = (Date)rowMap.get("state_tax_reg_expiration_date");
		this.localTaxRegId = (String) rowMap.get("local_tax_reg_id");
		this.localTaxExpirationDate = (Date)rowMap.get("local_tax_reg_expiration_date");
		this.regDateAndAddress = (String) rowMap.get("reg_date_and_address");
		this.cooperationDateOrPeriod = (String) rowMap.get("cooperation_date_or_period");
		this.certIso9000 = (Boolean)rowMap.get("cert_iso9000");
		this.certIso14000 = (Boolean)rowMap.get("cert_iso14000");
		this.certQs9000 = (Boolean)rowMap.get("cert_qs9000");
		this.certOther = (String)rowMap.get("cert_other");
		this.listedCompany = (Boolean)rowMap.get("listed_company");
		this.haveTransaction = (Boolean)rowMap.get("have_transaction");
	}
	
	/**
	 * 
	 * @return
	 */
	public String getBizLicenceId() {
		return bizLicenceId;
	}
	
	/**
	 * 
	 * @param bizLicenceId
	 */
	public void setBizLicenceId(String bizLicenceId) {
		this.bizLicenceId = bizLicenceId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLegalRepresentative() {
		return legalRepresentative;
	}
	
	/**
	 * 
	 * @param legalRepresentative
	 */
	public void setLegalRepresentative(String legalRepresentative) {
		this.legalRepresentative = legalRepresentative;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getRegCapital() {
		return regCapital;
	}
	
	/**
	 * 
	 * @param regCapital
	 */
	public void setRegCapital(String regCapital) {
		this.regCapital = regCapital;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getRegAddress() {
		return regAddress;
	}
	
	/**
	 * 
	 * @param regAddress
	 */
	public void setRegAddress(String regAddress) {
		this.regAddress = regAddress;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getRegDate() {
		return regDate;
	}
	
	/**
	 * 
	 * @param regDate
	 */
	public void setRegDate(Date regDate) {
		this.regDate = regDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getBizLicenceExpirationDate() {
		return bizLicenceExpirationDate;
	}
	
	/**
	 * 
	 * 
	 * @param bizLicenceExpirationDate
	 */
	public void setBizLicenceExpirationDate(Date bizLicenceExpirationDate) {
		this.bizLicenceExpirationDate = bizLicenceExpirationDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getStateTaxRegId() {
		return stateTaxRegId;
	}
	
	/**
	 * 
	 * @param stateTaxRegId
	 */
	public void setStateTaxRegId(String stateTaxRegId) {
		this.stateTaxRegId = stateTaxRegId;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getStateTaxExpirationDate() {
		return stateTaxExpirationDate;
	}
	
	/**
	 * 
	 * @param stateTaxExpirationDate
	 */
	public void setStateTaxExpirationDate(Date stateTaxExpirationDate) {
		this.stateTaxExpirationDate = stateTaxExpirationDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getLocalTaxRegId() {
		return localTaxRegId;
	}
	
	/**
	 * 
	 * @param localTaxRegId
	 */
	public void setLocalTaxRegId(String localTaxRegId) {
		this.localTaxRegId = localTaxRegId;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getLocalTaxExpirationDate() {
		return localTaxExpirationDate;
	}
	
	/**
	 * 
	 * @param localTaxExpirationDate
	 */
	public void setLocalTaxExpirationDate(Date localTaxExpirationDate) {
		this.localTaxExpirationDate = localTaxExpirationDate;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getRegDateAndAddress() {
		return regDateAndAddress;
	}
	
	/**
	 * 
	 * @param regDateAndAddress
	 */
	public void setRegDateAndAddress(String regDateAndAddress) {
		this.regDateAndAddress = regDateAndAddress;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCooperationDateOrPeriod() {
		return cooperationDateOrPeriod;
	}
	
	/**
	 * 
	 * @param cooperationDateOrPeriod
	 */
	public void setCooperationDateOrPeriod(String cooperationDateOrPeriod) {
		this.cooperationDateOrPeriod = cooperationDateOrPeriod;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getCertIso9000() {
		return certIso9000;
	}
	
	/**
	 * 
	 * @param certIso9000
	 */
	public void setCertIso9000(Boolean certIso9000) {		
		this.certIso9000 = certIso9000;
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean getCertIso14000() {
		return certIso14000;
	}
	
	/**
	 * 
	 * @param certIso14000
	 */
	public void setCertIso14000(Boolean certIso14000) {
		this.certIso14000 = certIso14000;
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean getCertQs9000() {
		return certQs9000;
	}
	
	/**
	 * 
	 * @param certQs9000
	 */
	public void setCertQs9000(Boolean certQs9000) {
		this.certQs9000 = certQs9000;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCertOther() {
		return certOther;
	}
	
	/**
	 * 
	 * @param certOther
	 */
	public void setCertOther(String certOther) {
		this.certOther = certOther;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getListedCompany() {
		return listedCompany;
	}
	
	/**
	 * 
	 * @param listedCompany
	 */
	public void setListedCompany(boolean listedCompany) {
		this.listedCompany = listedCompany;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getHaveTransaction() {
		return haveTransaction;
	}
	
	/**
	 * 
	 * @param haveTransaction
	 */
	public void setHaveTransaction(boolean haveTransaction) {
		this.haveTransaction = haveTransaction;
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("business_licence_id", bizLicenceId);
		rowMap.put("business_licence_expiration_date", getBizLicenceExpirationDate());
		rowMap.put("legal_representative", legalRepresentative);
		rowMap.put("registration_capital", regCapital);
		rowMap.put("registration_address", regAddress);
		rowMap.put("registration_date", getRegDate());
		rowMap.put("state_tax_reg_id", stateTaxRegId);
		rowMap.put("state_tax_reg_expiration_date", getStateTaxExpirationDate());
		rowMap.put("local_tax_reg_id", localTaxRegId);
		rowMap.put("local_tax_reg_expiration_date", getLocalTaxExpirationDate());
		rowMap.put("reg_date_and_address", regDateAndAddress);
		rowMap.put("cooperation_date_or_period", cooperationDateOrPeriod);
		rowMap.put("cert_iso9000", certIso9000);
		rowMap.put("cert_iso14000", certIso14000);
		rowMap.put("cert_qs9000", certQs9000);
		rowMap.put("cert_other", certOther);
		rowMap.put("listed_company", listedCompany);
		rowMap.put("have_transaction", haveTransaction);
	}

	public void validate(Errors errors) {
	
		if ("".equals(bizLicenceId)) {
			errors.rejectValue("certificate.bizLicenceId", "error.required");
		}
		
		if ("".equals(regCapital)) {
			errors.rejectValue("certificate.regCapital", "error.required");
		}
		
		if (!certIso9000 && !certIso14000 && !certQs9000 && "".equals(certOther)) {
			errors.rejectValue("certificate.certIso9000", "error.required");
			errors.rejectValue("certificate.certIso14000", "error.required");
			errors.rejectValue("certificate.certQs9000", "error.required");
			errors.rejectValue("certificate.certOther", "error.required");
		}
	}
}
