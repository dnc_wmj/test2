package cn.sscs.vendormanagement.vendor;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import cn.sscs.vendormanagement.Utils;

/**
 * 
 * @author kazzy
 *
 */
@Service("vendorService")
@Transactional
public class VendorServiceImpl implements IVendorService {

	private SimpleJdbcInsert insertVendorBasicInfo = null;
	
	private SimpleJdbcInsert insertTmpVendorBasicInfo = null;
	
	private SimpleJdbcInsert insertBizInfoPerformance = null;
	
	private SimpleJdbcInsert insertTmpBizInfoPerformance = null;
	
	private SimpleJdbcInsert insertBankAccount = null;
	
	private SimpleJdbcInsert insertTmpBankAccount = null;
	
	private SimpleJdbcInsert insertAttachment = null;
	
	private SimpleJdbcInsert insertTmpAttachment = null;
	
	private SimpleJdbcTemplate jdbcTemplate = null;
	
	private List<Country> countries = null;
	
	
	/**
	 * 
	 * @param dataSource
	 */
	@SuppressWarnings("deprecation")
	@Autowired
	public void init(DataSource dataSource) {
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
		
		insertVendorBasicInfo = 
			new SimpleJdbcInsert(dataSource)
				.withTableName("vendor_basic_info")
				.usingGeneratedKeyColumns("id");
		
		insertTmpVendorBasicInfo = 
			new SimpleJdbcInsert(dataSource)
				.withTableName("tmp_vendor_basic_info");

		insertBizInfoPerformance =
			new SimpleJdbcInsert(dataSource)
				.withTableName("business_info_performance");

		insertTmpBizInfoPerformance =
			new SimpleJdbcInsert(dataSource)
			.withTableName("tmp_business_info_performance");
		
		insertBankAccount =
			new SimpleJdbcInsert(dataSource)
				.withTableName("bank_account");

		insertTmpBankAccount =
			new SimpleJdbcInsert(dataSource)
				.withTableName("tmp_bank_account");

		
		insertAttachment =
			new SimpleJdbcInsert(dataSource)
				.withTableName("attachment")
				.usingGeneratedKeyColumns("id");

		insertTmpAttachment =
			new SimpleJdbcInsert(dataSource)
				.withTableName("tmp_attachment");
				
		countries = jdbcTemplate.query("SELECT * FROM countries order by convert(name using gb2312) asc", 
				ParameterizedBeanPropertyRowMapper.newInstance(Country.class));
	}
	
	
	@Override
	public List<Country> getCountries() {
		return countries;
	}
	
	@Override
	public String getCountryName(String code) {
		// batch import vendors has one situation about code is null
		if(code==null || "".equals(code)){
			return null;
		}
		return jdbcTemplate.queryForObject(
				"SELECT name FROM countries WHERE code = ?", 
				String.class, 
				code);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<Region> getRegion(String countryCode) {
		//导入的供应商有可能国家信息未填写，点击基本信息编辑或其它更新的时候国家默认选择是第一个，所以省市也要默认查出来是第一个国家的省市
		if("".equals(countryCode)){
			return jdbcTemplate.query(
					"SELECT " +
						"name, region_code as code FROM regions " +
					"WHERE " +
						"country = (SELECT CODE FROM countries order by convert(name using gb2312) asc LIMIT 1) " +
					"order by convert(name using gb2312) asc", 
					ParameterizedBeanPropertyRowMapper.newInstance(Region.class));
		}else{
			return jdbcTemplate.query("SELECT name, region_code as code FROM regions WHERE country = ? order by convert(name using gb2312) asc", 
					ParameterizedBeanPropertyRowMapper.newInstance(Region.class),
					countryCode);
		}
	}

	@Override
	public String getRegionName(String countryCode, String regionCode) {
		//  there is one situation about countryCode & regionCode's null value  of batch vendors
		if("".equals(countryCode)||"".equals(regionCode)){
			return null;
		}
		return jdbcTemplate.queryForObject(
				"SELECT name FROM regions WHERE country = ? AND region_code = ?", 
				String.class, 
				countryCode, regionCode);
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: getBusiessType</p> 
	 * <p>Description: 根据 vendorId 获取供应商 business_type</p> 
	 * @param vendorId
	 * @return 
	 * @see cn.sscs.vendormanagement.vendor.IVendorService#getBusiessType(long)
	 */
	@Override
	public int getBusiessType(long vendorId) {
		return jdbcTemplate.queryForObject(
				"SELECT business_type FROM vendor_basic_info WHERE id = ?", 
				Integer.class,
				vendorId);
	}
	
	@Override
	public int storeVendorBasicInfo(VendorBasicInfo vendorBasicInfo, List<BankAccount> accounts) {
		Map<String, Object> rowMap = vendorBasicInfo.toRowMap();
		rowMap.put("registered_date", new Date());
		
		int id = insertVendorBasicInfo.executeAndReturnKey(rowMap).intValue();
		
		List<BizInfoPerformance> performances = vendorBasicInfo.getBusinessInformation().getPerformance();	
		if (performances != null && performances.size() != 0) {
			storeBizInfoPerformance(id, performances);
		}
		
		storeBankAccount(id, accounts);
		
		return id;
	}
	
	@Override
	public void updateVendorBasicInfo(VendorBasicInfo vendorBasicInfo, List<BankAccount> accounts) {
		Map<String, Object> rowMap = vendorBasicInfo.toRowMap();
		rowMap.put("id",vendorBasicInfo.getVendorId());
		updateVendorBasicInfo(rowMap);
		
		
		List<BizInfoPerformance> performances = vendorBasicInfo.getBusinessInformation().getPerformance();	
		if (performances != null && performances.size() != 0) {
			updateOrInsertBizInfoPerformance(vendorBasicInfo.getVendorId(), performances);
		}
		
		updateOrInsertBankAccount(vendorBasicInfo.getVendorId(), accounts);
		
		
	}
	//TODO
	private void updateVendorBasicInfo(Map<String, Object> rowMap) {
		if(Utils.isRoleHD()){
			jdbcTemplate.update(
					"UPDATE vendor_basic_info " +
					"SET " +
					"vendor_code =:vendor_code, " +
					"vendor_type =:vendor_type," +
					"vendor_chinese_name =:vendor_chinese_name, " +
					"vendor_english_name =:vendor_english_name, " +
					"country =:country," +
					"region =:region," +
					"city =:city, " +
					"address =:address," +
					"tel =:tel," +
					"postal_code =:postal_code," +
					"person =:person," +
					"homepage =:homepage," +
					"fax =:fax, " +
					"business_licence_id =:business_licence_id, " +
					"business_licence_expiration_date =:business_licence_expiration_date," +
					"legal_representative =:legal_representative," +
					"registration_capital =:registration_capital," +
					"registration_address =:registration_address," +
					"registration_date =:registration_date," +
					"state_tax_reg_id =:state_tax_reg_id," +
					"state_tax_reg_expiration_date =:state_tax_reg_expiration_date," +
					"local_tax_reg_id =:local_tax_reg_id," +
					"local_tax_reg_expiration_date =:local_tax_reg_expiration_date," +
					"cert_iso9000 =:cert_iso9000," +
					"cert_iso14000 =:cert_iso14000," +
					"cert_qs9000 =:cert_qs9000," +
					"cert_other =:cert_other," +
					"listed_company =:listed_company," +
					"reg_date_and_address =:reg_date_and_address," +
					"cooperation_date_or_period =:cooperation_date_or_period," +
					"have_transaction =:have_transaction," +
					"domestic_investment =:domestic_investment," +
					"foreign_investment =:foreign_investment," +
					"foreign_investment_other =:foreign_investment_other," +
					"joint_venture_investment_ratio =:joint_venture_investment_ratio," +
					"business_type =:business_type," +
					"production =:production," +
					"business_relation =:business_relation," +
					"payment_term =:payment_term," +
					"lead_time =:lead_time," +
					"minimum_lot =:minimum_lot," +
					"delivery_types_other_value =:delivery_types_other_value," +
					"delivery_types =:delivery_types," +
					"date =:date," +
					"number_of_direct_staffs =:number_of_direct_staffs," +
					"number_of_indirect_staffs =:number_of_indirect_staffs," +
					"total_staffs =:total_staffs," +
					"special_statement =:special_statement," +
					"company_code =:company_code," +
					"vendor_account_type =:vendor_account_type," +
					"vendor_group =:vendor_group," +
					"group_key =:group_key," +
					"recon_account =:recon_account," +
					"payment_term_for_accounting =:payment_term_for_accounting," +
					"chk_double_inv =:chk_double_inv, " +
					"status = :status," +
					"super_company = :super_company " +
					// current version about VND, neednn't update follow attributes.
					//applicant
					//action_time
					//applicant_name
					//freeze
					"WHERE id=:id ",
					rowMap);
		}else{
			jdbcTemplate.update(
					"UPDATE vendor_basic_info " +
					"SET " +
					"vendor_code =:vendor_code, " +
					"vendor_type =:vendor_type," +
					"vendor_chinese_name =:vendor_chinese_name, " +
					"vendor_english_name =:vendor_english_name, " +
					"country =:country," +
					"region =:region," +
					"city =:city, " +
					"address =:address," +
					"tel =:tel," +
					"postal_code =:postal_code," +
					"person =:person," +
					"homepage =:homepage," +
					"fax =:fax, " +
					"business_licence_id =:business_licence_id, " +
					"business_licence_expiration_date =:business_licence_expiration_date," +
					"legal_representative =:legal_representative," +
					"registration_capital =:registration_capital," +
					"registration_address =:registration_address," +
					"registration_date =:registration_date," +
					"state_tax_reg_id =:state_tax_reg_id," +
					"state_tax_reg_expiration_date =:state_tax_reg_expiration_date," +
					"local_tax_reg_id =:local_tax_reg_id," +
					"local_tax_reg_expiration_date =:local_tax_reg_expiration_date," +
					"cert_iso9000 =:cert_iso9000," +
					"cert_iso14000 =:cert_iso14000," +
					"cert_qs9000 =:cert_qs9000," +
					"cert_other =:cert_other," +
					"listed_company =:listed_company," +
					"reg_date_and_address =:reg_date_and_address," +
					"cooperation_date_or_period =:cooperation_date_or_period," +
					"have_transaction =:have_transaction," +
					"domestic_investment =:domestic_investment," +
					"foreign_investment =:foreign_investment," +
					"foreign_investment_other =:foreign_investment_other," +
					"joint_venture_investment_ratio =:joint_venture_investment_ratio," +
					"business_type =:business_type," +
					"production =:production," +
					"business_relation =:business_relation," +
					"payment_term =:payment_term," +
					"lead_time =:lead_time," +
					"minimum_lot =:minimum_lot," +
					"delivery_types_other_value =:delivery_types_other_value," +
					"delivery_types =:delivery_types," +
					"date =:date," +
					"number_of_direct_staffs =:number_of_direct_staffs," +
					"number_of_indirect_staffs =:number_of_indirect_staffs," +
					"total_staffs =:total_staffs," +
					"special_statement =:special_statement," +
					// if current user's role is not HD, shouldn't update these value
					//"company_code =:company_code," +
					//"vendor_account_type =:vendor_account_type," +
					//"vendor_group =:vendor_group," +
					//"group_key =:group_key," +
					//"recon_account =:recon_account," +
					//"payment_term_for_accounting =:payment_term_for_accounting," +
					//"chk_double_inv =:chk_double_inv, " +
					"status = :status," +
					"super_company = :super_company " +
					// current version about VND, neednn't update follow attributes.
					//applicant
					//action_time
					//applicant_name
					//freeze
					"WHERE id=:id ",
					rowMap);
		}
		
	}
	
	@Override
	public void updateVendorCode(long id, String vendorCode) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET vendor_code = ? WHERE id = ?", 
				vendorCode, 
				id);
	}
	
	@Override
	public boolean exists(String vendorCode, String superCompanyType) {
	    // 20121008
		return jdbcTemplate.queryForInt("SELECT count(*) FROM vendor_basic_info" +
				" WHERE vendor_code = ? and super_company = ?", vendorCode, superCompanyType) >= 1;
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	private boolean bizInfoPerformanceExists(long id) {
		return jdbcTemplate.queryForInt("SELECT count(*) FROM business_info_performance WHERE id = ?", id) == 1;
	}
	
	@Override
	public void updateOrInsertBizInfoPerformance(long vendorId, List<BizInfoPerformance> performances) {
		for (BizInfoPerformance performance : performances) {
			Map<String, Object> map = performance.toMap();
			
			
			if (bizInfoPerformanceExists(performance.getBizInfoId())) {
				map.put("id", performance.getBizInfoId());
				
				jdbcTemplate.update(
						"UPDATE business_info_performance " +
						"SET " +
						"year =:year," +
						"sales_amount =:sales_amount," +
						"profit =:profit," +
						"profitability =:profitability " +
						"WHERE id=:id", 
						map);
			} else {
				map.put("vendor_id", vendorId);	
				
				insertBizInfoPerformance.execute(map);;
			}
		}
		
	}
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	private boolean bankAccountExists(long id) {
		return jdbcTemplate.queryForInt("SELECT count(*) FROM bank_account WHERE id = ?", id) == 1;
	}
	
	@Override
	public void updateOrInsertBankAccount(long vendorId, List<BankAccount> accounts) {
		for (BankAccount bankAccount : accounts) {
			Map<String, Object> map = bankAccount.toMap();
			
			if (bankAccountExists(bankAccount.getBankAccountId())) {
				map.put("id", bankAccount.getBankAccountId());
				
				jdbcTemplate.update(
						"UPDATE bank_account " +
						"SET " +
						"vendor_sub_code =:vendor_sub_code," +
						"province_and_region =:province_and_region, " +
						"swift_code =:swift_code," +
						"name =:name," +
						"account =:account," +
						"city =:city," +
						"currency =:currency," +
						"type =:type," +
						"institution_no =:institution_no," +
						"joint_code =:joint_code," +
						"cnaps =:cnaps " +
						"WHERE id=:id", 
						map);

			} else {
				map.put("vendor_id", vendorId);
				// 供应商数据变更 
				map.put("status", BankAccount.STATUS_ALIVE);				
				insertBankAccount.execute(map);
			}
			
		}
		
	}
	
	@Override
	public long storeAttachment(Map<String, Object> attachment) {
		return insertAttachment.executeAndReturnKey(attachment).longValue();
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public List<Attachment> findAttachment(String tranId) {
		return jdbcTemplate.query(
				"SELECT id, name, size, content_type as contentType FROM attachment WHERE tran_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				tranId);
	}
	@Override
	@SuppressWarnings("deprecation")
	public List<Attachment> findAttachmentWorkflow(String tranId) {
		return jdbcTemplate.query(
				"SELECT id, name, size, content_type as contentType, tran_id as tranId FROM attachment_workflow WHERE tran_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				tranId);
	}	
	@Override
	@SuppressWarnings("deprecation")
	public List<Attachment> findAttachment(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, name, size, content_type as contentType FROM attachment WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				vendorId);
	}
	@Override
	@SuppressWarnings("deprecation")
	public List<Attachment> findAttachmentWorkflow(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, name, size, content_type as contentType, tran_id as tranId FROM attachment_workflow WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				vendorId);
	}
	@Override
	@SuppressWarnings("deprecation")
	public Attachment findAttachmentData(long id) {
		return jdbcTemplate.queryForObject(
				"SELECT id, name, size, content_type as contentType, data FROM attachment WHERE id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				id);
	}
	@Override
	@SuppressWarnings("deprecation")
	public Attachment findAttachmentDataWorkflow(long id) {
		return jdbcTemplate.queryForObject(
				"SELECT id, name, size, content_type as contentType, data FROM attachment_workflow WHERE id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				id);
	}
	
	@Override
	public void storeBizInfoPerformance(long vendorId, List<BizInfoPerformance> performances) {
		@SuppressWarnings("unchecked")
		Map<String, Object>[] batch = new Map[performances.size()];
		
		for (int i=0; i<performances.size(); i++) {
			Map<String, Object> performance = performances.get(i).toMap();
			performance.put("vendor_id", vendorId);
			
			batch[i] = performance;
		}
		
		insertBizInfoPerformance.executeBatch(batch);
	}
	
	@Override
	public void storeBankAccount(long vendorId, List<BankAccount> accounts) {
		@SuppressWarnings("unchecked")
		Map<String, Object>[] batch = new Map[accounts.size()]; 
		
		for (int i=0; i<accounts.size(); i++) {
			Map<String, Object> account = accounts.get(i).toMap();
			account.put("vendor_id", vendorId);
			account.put("status", BankAccount.STATUS_ALIVE);
			
			batch[i] = account;
		}
		
		insertBankAccount.executeBatch(batch);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public Identity getIdentity(long vendorId) {
		return jdbcTemplate.queryForObject(
				"SELECT vendor_code as vendorCode," +
				"	vendor_type as vendorType," +
				"	vendor_chinese_name as chineseName," +
				"	vendor_english_name as englishName " +
				"FROM vendor_basic_info " +
				"WHERE id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Identity.class), 
				vendorId);
	}
	
	@Override
	public VendorBasicInfo findVendorBasicInfoByStatus(long vendorId, int status) {
		Map<String, Object> rowMap = jdbcTemplate.queryForMap(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE id = ? " +
				"  AND status = ?", 
				vendorId, status);
		
		List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
		List<BankAccount> bankAccounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);

		return new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts);
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: findVendorBasicInfoByStatus</p> 
	 * <p>Description: 多状态查询 郑同 2012年7月13日9:56:53 add</p> 
	 * @param vendorId
	 * @param status
	 * @return 
	 * @see cn.sscs.vendormanagement.vendor.IVendorService#findVendorBasicInfoByStatus(long, int)
	 */
	@Override
	public VendorBasicInfo findVendorBasicInfoByStatus(long vendorId, List<Integer> status) {
		String statusCondition = "";
		
		for (int i = 1 ; i <= status.size() ; i++ ){
			statusCondition =  statusCondition + status.get(i - 1) + (i == status.size() ? "":",")  ;
		}
		Map<String, Object> rowMap = jdbcTemplate.queryForMap(
				" SELECT * " +
				" FROM vendor_basic_info " +
				" WHERE id = ? " +
				" AND status in ( "+statusCondition+" )",
				vendorId);
		
		List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
		List<BankAccount> bankAccounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);

		return new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts);
	}
	@Override
	public VendorBasicInfo findAvailableVendorBasicInfo(long vendorId) {
		Map<String, Object> rowMap = jdbcTemplate.queryForMap(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE id = ? " +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40 OR status = 50)", 
				vendorId);
		
		List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
		List<BankAccount> bankAccounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);

		return new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts);
	}

	
	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<BizInfoPerformance> findBizInfoPerformance(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id as bizInfoId, year, sales_amount as salesAmount, profit, profitability " +
				"FROM business_info_performance " +
				"WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(BizInfoPerformance.class),
				vendorId);
	}
	
	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	@SuppressWarnings("deprecation")
	@Override
	public List<BankAccount> findBankAccount(long vendorId, int status) {
		return jdbcTemplate.query(
				"SELECT id as bankAccountId, " +
				"  vendor_sub_code as vendorSubcode," +
				"  province_and_region as provinceAndRegion," +
				"  swift_code as swiftCode," +
				"  name," +
				"  account, " +
				"  city," +
				"  currency," +
				"  type," +
				"  institution_no as institutionNo," +
				"  joint_code as jointCode," +
				"  cnaps, " +
				"  status " +
				"FROM bank_account " +
				"WHERE vendor_id = ?" +
				"  AND status = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(BankAccount.class),
				vendorId,
				status);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public BankAccount findBankAccountById(long id) {
		return jdbcTemplate.queryForObject(
				"SELECT id as bankAccountId, " +
				"  vendor_sub_code as vendorSubcode," +
				"  province_and_region as provinceAndRegion," +
				"  swift_code as swiftCode," +
				"  name," +
				"  account, " +
				"  city," +
				"  currency," +
				"  type," +
				"  institution_no as institutionNo," +
				"  joint_code as jointCode," +
				"  cnaps " +
				"FROM bank_account " +
				"WHERE id = ?",
				ParameterizedBeanPropertyRowMapper.newInstance(BankAccount.class),
				id);
	}
	
	/**
	 * 
	 * @param sql
	 * @param criteria
	 * @return
	 */
	private int searchVendorCount(String sql, VendorSearchCriteria criteria) {
		return jdbcTemplate.queryForInt(sql, new BeanPropertySqlParameterSource(criteria));		
	}
	/**
	 * 
	 * @Title:       searchVendorCount 
	 * @Description: ( 重写  查询方法 ) 
	 * @param        @param sql
	 * @param        @param criteria
	 * @param        @return     
	 * @return       int     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	private int searchVendorCount(String sql) {
		return jdbcTemplate.queryForInt(sql);		
	}
	
	/**
	 * 
	 * @param sql
	 * @param criteria
	 * @return
	 */
	@SuppressWarnings("deprecation")
	private List<SimpleVendorInfo> searchVendorInfo(String sql, VendorSearchCriteria criteria) {
		return jdbcTemplate.query(sql, 
				ParameterizedBeanPropertyRowMapper.newInstance(SimpleVendorInfo.class),
				new BeanPropertySqlParameterSource(criteria));
	}
	/**
	 * 
	 * @Title:       searchVendorInfo 
	 * @Description: 重写查询供应商方法 
	 * @param        @param sql
	 * @param        @return     
	 * @return       List<SimpleVendorInfo>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	@SuppressWarnings("deprecation")
	private List<SimpleVendorInfo> searchVendorInfo(String sql) {
		return jdbcTemplate.query(sql, 
				ParameterizedBeanPropertyRowMapper.newInstance(SimpleVendorInfo.class));
	}
	
	/**
	 * (非 Javadoc) 
	 * <p>Title: searchFullVendorInfo</p> 
	 * <p>Description:  供应商查询方法 full </p> 
	 * @param sql
	 * @return 
	 * @see cn.sscs.vendormanagement.vendor.IVendorService#searchFullVendorInfo(java.lang.String)
	 */
	@Override
	public List<VendorBasicInfo> searchFullVendorInfoForAll(String sql) {
		
		return searchFullVendorInfo(
				"SELECT * " +
				"FROM vendor_basic_info " +
				sql,
				new VendorSearchCriteria());
	}
	
	/**
	 * 
	 * @Title:       searchFullVendorInfo 
	 * @Description: 供应商查询方法 
	 * @param        @param sql
	 * @param        @param criteria
	 * @param        @return     
	 * @return       List<VendorBasicInfo>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	private List<VendorBasicInfo> searchFullVendorInfo(String sql, VendorSearchCriteria criteria) {
		List<Map<String, Object>> vendors = jdbcTemplate.queryForList(sql,
				new BeanPropertySqlParameterSource(criteria));
		
		List<VendorBasicInfo> retval = new ArrayList<VendorBasicInfo>(vendors.size());
		
		for (int i=0; i<vendors.size(); i++) {
			long vendorId = ((java.math.BigInteger)vendors.get(i).get("id")).longValue();
			//List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
			List<BankAccount> accounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);
			
			retval.add(new VendorBasicInfo(vendorId, vendors.get(i), null, accounts));
		}
		
		return retval;
	}
	
	/**
	 * (非 Javadoc) 
	 * <p>Title: searchVendorCountByName</p> 
	 * <p>Description: 查询供应商总数 ，配合供应商查询函数，实现分页效果 (注： 每一次查询条件需同供应商查询函数一致)  </p> 
	 * @param criteria
	 * @return 
	 * @see cn.sscs.vendormanagement.vendor.IVendorService#searchVendorCountByName(cn.sscs.vendormanagement.vendor.VendorSearchCriteria)
	 */
	@Override
	public int searchVendorCountForAll(String sql) {
		return searchVendorCount(
				"SELECT count(*) " +
				"FROM vendor_basic_info " +
				sql
				);
	}
	
	@Override
	public int searchVendorCountByName(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory() ? "": "  AND vendor_type = :category";
		return searchVendorCount(
				"SELECT count(*) " +
				"FROM vendor_basic_info " +
				"WHERE (vendor_chinese_name LIKE :ambiguousText OR vendor_english_name LIKE :ambiguousText)" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40) " +
				//"  AND vendor_type = :category" +
				vendor_type, 
				criteria);
	}
	
	@Override
	public List<SimpleVendorInfo> searchVendorInfoByName(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category";
		
		return searchVendorInfo(
				"SELECT vendor_chinese_name as chineseName," +
				"  vendor_english_name as englishName," +
				"  vendor_code as vendorCode," +
				"  number as vendorNumber," +
				"  status," +
				"  id " +
				"FROM vendor_basic_info " +
				"WHERE (vendor_chinese_name LIKE :ambiguousText OR vendor_english_name LIKE :ambiguousText)" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40) " +
				//"  AND vendor_type = :category" +
				vendor_type  +
				" ORDER BY vendor_code " +
				"  LIMIT :recordFrom, 20", 
				criteria);
	}
	
	@Override
	public List<VendorBasicInfo> searchFullVendorInfoByName(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category";
		
		return searchFullVendorInfo(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE (vendor_chinese_name LIKE :ambiguousText OR vendor_english_name LIKE :ambiguousText)" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40) " +
				//"  AND vendor_type = :category" +
				vendor_type +
				" ORDER BY vendor_code " +
				"  LIMIT :recordFrom, 20",
				criteria);
		
	}
	
	@Override
	public int searchVendorCountByCode(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category ";
		
		if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
		return searchVendorCount(
				"SELECT count(*) " +
				"FROM vendor_basic_info " +
				"WHERE vendor_code = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40 ) " +
				//"  AND vendor_type = :category", 
				vendor_type,
				criteria);
		}else{
			return searchVendorCount(
					"SELECT count(*) " +
					"FROM vendor_basic_info " +
					"WHERE " +
					"  (status = 5 OR status = 20 OR status = 30 OR status = 40 ) " +
					//"  AND vendor_type = :category", 
					vendor_type,
					criteria);
		}
		
	}
	@Override
	public int searchVendorCountByNumber(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category ";
		
		if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
		return searchVendorCount(
				"SELECT count(*) " +
				"FROM vendor_basic_info " +
				"WHERE number = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40 ) " +
				//"  AND vendor_type = :category", 
				vendor_type,
				criteria);
		}else{
			return searchVendorCount(
					"SELECT count(*) " +
					"FROM vendor_basic_info " +
					"WHERE " +
					"  (status = 5 OR status = 20 OR status = 30 OR status = 40) " +
					//"  AND vendor_type = :category", 
					vendor_type,
					criteria);
		}
		
	}
	@Override
	public List<SimpleVendorInfo> searchVendorInfoByCode(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category ";
		
		if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
		return searchVendorInfo(
				"SELECT vendor_chinese_name as chineseName," +
				"  vendor_english_name as englishName," +
				"  vendor_code as vendorCode," +
				"  number as vendorNumber," +
				"  status," +
				"  id " +
				"FROM vendor_basic_info " +
				"WHERE vendor_code = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40)" +
				//"  AND vendor_type = :category" +
				vendor_type +
				"  LIMIT :recordFrom, 20", 
				criteria);
		}else{
			return searchVendorInfo(
					"SELECT vendor_chinese_name as chineseName," +
					"  vendor_english_name as englishName," +
					"  vendor_code as vendorCode," +
					"  number as vendorNumber," +
					"  status," +
					"  id " +
					"FROM vendor_basic_info " +
					"WHERE " +
					"  (status = 5 OR status = 20 OR status = 30 OR status = 40)" +
					//"  AND vendor_type = :category" +
					vendor_type +
					"  LIMIT :recordFrom, 20", 
					criteria);
	}

	}
	@Override
	public List<SimpleVendorInfo> searchVendorInfoByNumber(VendorSearchCriteria criteria) {
		String vendor_type = 3==criteria.getCategory()?"":"  AND vendor_type = :category ";
		
		if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
		return searchVendorInfo(
				"SELECT vendor_chinese_name as chineseName," +
				"  vendor_english_name as englishName," +
				"  vendor_code as vendorCode," +
				"  number as vendorNumber," +
				"  status," +
				"  id " +
				"FROM vendor_basic_info " +
				"WHERE number = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40 ) " +
				//"  AND vendor_type = :category" +
				vendor_type +
				"  LIMIT :recordFrom, 20", 
				criteria);
		}else{
			return searchVendorInfo(
					"SELECT vendor_chinese_name as chineseName," +
					"  vendor_english_name as englishName," +
					"  vendor_code as vendorCode," +
					"  number as vendorNumber," +
					"  status," +
					"  id " +
					"FROM vendor_basic_info " +
					"WHERE " +
					"  (status = 5 OR status = 20 OR status = 30 OR status = 40) " +
					//"  AND vendor_type = :category" +
					vendor_type +
					"  LIMIT :recordFrom, 20", 
					criteria);
	}

	}

	@Override
	public List<VendorBasicInfo> searchFullVendorInfoByCode(VendorSearchCriteria criteria) {
		return searchFullVendorInfo(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE vendor_code = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40)" +
				"  AND vendor_type = :category " +
				"ORDER BY vendor_code " +
				"LIMIT :recordFrom, 20",
				criteria);		
	}

	@Override
	public List<VendorBasicInfo> searchFullVendorInfoByNumber(VendorSearchCriteria criteria) {
		return searchFullVendorInfo(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE number = :text" +
				"  AND (status = 5 OR status = 20 OR status = 30 OR status = 40)" +
				"  AND vendor_type = :category " +
				"ORDER BY vendor_code " +
				"LIMIT :recordFrom, 20",
				criteria);		
	}
	
	@Override
	public int searchBankMasterCount(BankSearchCriteria criteria) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM banks WHERE name LIKE ?", 
				"%" + criteria.getName() + "%");
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<BankMaster> searchBankMaster(BankSearchCriteria criteria) {
		return jdbcTemplate.query(
				"SELECT net_bank_joint_code as netBankJointCode," +
				"  name," +
				"  institution_no as institution_No," +
				"  joint_code as jointCode," +
				"  parent_institution_no as parentInstitutionNo," +
				"  parent_joint_code as parentJointCode " +
				"FROM banks " +
				"WHERE name LIKE ? " +
				"ORDER BY name " +
				"LIMIT ?, 20", 
				ParameterizedBeanPropertyRowMapper.newInstance(BankMaster.class), 
				"%" + criteria.getName() + "%",
				criteria.getRecordFrom());
	}
	
	@Override
	public boolean isValidBankName(String name) {
		return jdbcTemplate.queryForInt("SELECT count(*) FROM banks WHERE name=?", name) == 1
			|| jdbcTemplate.queryForInt("SELECT count(*) FROM cnaps WHERE name=?", name) == 1;
	}
	
	@Override
	public boolean isValidInstitutionNo(String name, String institutionNo) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM banks WHERE name=? AND institution_no=?"
				, name, institutionNo) == 1;
	}
	
	@Override
	public boolean isValidJointCode(String name, String jointCode) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM banks WHERE name=? AND joint_code=?"
				, name, jointCode) == 1;
	}
	
	@Override
	public boolean isValidCnaps(String name, String cnaps) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM cnaps WHERE name=? AND no=?", 
				name, cnaps) == 1;
	}
	
	@Override
	public boolean isBankOfChina(String name) {
		if (jdbcTemplate.queryForInt("SELECT count(*) FROM banks WHERE name=?", name) != 1) {
			return false;
		}
		
		return true;
		/*
		String parentJointCode = jdbcTemplate.queryForObject(
				
				"SELECT parent_joint_code FROM banks WHERE name=?", String.class, name);
		
		// 00000 is a parent institution no of a bank of CHINA.
		return "00000".equals(parentJointCode);
		*/
	}
	
	
	@Override
	public int searchCnapsCount(BankSearchCriteria criteria) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM cnaps WHERE name LIKE :ambiguousName", 
				new BeanPropertySqlParameterSource(criteria));
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<Cnap> searchCnaps(BankSearchCriteria criteria) {
		return jdbcTemplate.query(
				"SELECT * " +
				"FROM cnaps " +
				"WHERE name LIKE :ambiguousName ORDER BY name LIMIT :recordFrom, 20",
				ParameterizedBeanPropertyRowMapper.newInstance(Cnap.class), 
				new BeanPropertySqlParameterSource(criteria));
	}

	
	@Override
	public void fixAttachmentData(String tranId, long vendorId) {
		jdbcTemplate.update(
				"UPDATE attachment SET vendor_id = ? WHERE tran_id = ?", 
				vendorId, tranId);
	
	}
	@Override
	public void fixAttachmentDataWorkflow(String tranId, long vendorId) {
		jdbcTemplate.update(
				"UPDATE attachment_workflow SET vendor_id = ? WHERE tran_id = ?", 
				vendorId, tranId);
	
	}
	@Override
	public void deleteAttachmentData(long id, String tranId) {
		jdbcTemplate.update(
				"DELETE FROM attachment WHERE id = ?", 
				id);
	}
	
	@Override
	public void deleteAttachmentWorkFlowData(long id, String tranId) {
		jdbcTemplate.update(
				"DELETE FROM attachment_workflow WHERE id = ?", 
				id);
	}
	@Override
	public void deleteBizInfoPerformance(long id) {
		jdbcTemplate.update(
				"DELETE FROM business_info_performance WHERE id = ?", 
				id);
	}


	@Override
	public void deleteBankAccount(long id) {
		jdbcTemplate.update(
				"UPDATE bank_account SET status = 10 WHERE id = ?", 
				id);
	}

	@Override
	public void deleteBankAccountPhysically(long id) {
		jdbcTemplate.update(
				"delete from bank_account WHERE id = ?", 
				id);
	}

	@Override
	public void updateStatus(int status, long vendorId) {
		jdbcTemplate.update(
				"UPDATE vendor_basic_info SET status = ? " +
				"WHERE id = ? ",
				status, vendorId);
	}
	
	@Override
	public void updateStatusAndNumber(int status, long vendorId, String number) {
		jdbcTemplate.update(
				"UPDATE vendor_basic_info SET status = ?, number = ?  " +
				"WHERE id = ? ",
				status, number, vendorId);
	}
	@Override
	public long getVendorIDByBankAccountID(long bankAccountID) {
		return jdbcTemplate.queryForLong("SELECT vendor_id FROM bank_account WHERE id = ?", bankAccountID);
	}


	@Override
	public boolean isValidJCodeINo(String name, String jointCode,
			String institutionNo) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM banks WHERE name=? AND joint_code=? AND institution_no=?"
				, name, jointCode ,institutionNo) >= 1;
	}


	@Override
	public boolean isValidIdentityCName(String chineseName, 
	        long vendorId, String superCompanyType) {
		if(chineseName != null && !"".equals(chineseName.trim())){
			List<Map<String,Object>> list = jdbcTemplate.queryForList(
			        "SELECT id FROM vendor_basic_info " +
			        "WHERE vendor_chinese_name=? AND status NOT IN (0, 50) AND super_company = ?"
			                , chineseName
			                , superCompanyType);
			Object id = null;
			if(list != null && list.size() > 0){
				id = list.get(0).get("id");
			}
			if(vendorId == -1 && id != null){
				return true;
			}else if(vendorId != -1 && id != null && !String.valueOf(vendorId).equals(id.toString())){
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean isValidIdentityEName(String englishName, 
			long vendorId, String superCompanyType) {
		if(englishName != null && !"".equals(englishName.trim())){
			List<Map<String,Object>> list = jdbcTemplate.queryForList(
			        "SELECT id FROM vendor_basic_info " +
			        "WHERE vendor_english_name=? AND status NOT IN (0, 50)  AND super_company = ?"
			        , englishName
			        , superCompanyType);
			Object id = null;
			if(list != null && list.size() > 0){
				id = list.get(0).get("id");
			}
			if(vendorId == -1 && id != null){
				return true;
			}else if(vendorId != -1 && id != null && !String.valueOf(vendorId).equals(id.toString())){
				return true;
			}
		}
		return false;
	}



	@Override
	@SuppressWarnings("unchecked")
	public void copyVendorInfo(long vendorId) {
		jdbcTemplate.update("delete from tmp_attachment where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_business_info_performance where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_bank_account where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_vendor_basic_info where id = ?", vendorId);
		
		VendorBasicInfo info = findAvailableVendorBasicInfo(vendorId);
		info.init();
		
		Map<String, Object> rowMap = info.toRowMap();
		rowMap.put("id", info.getVendorId());
		insertTmpVendorBasicInfo.execute(rowMap);
		
		List<BizInfoPerformance> performances = info.getBusinessInformation().getPerformance();	
		if (performances != null && performances.size() != 0) {
			Map<String, Object>[] batch = new Map[performances.size()];
			
			for (int i=0; i<performances.size(); i++) {
				BizInfoPerformance performance = performances.get(i);
				Map<String, Object> performanceMap = performances.get(i).toMap();
				performanceMap.put("id", performance.getBizInfoId());
				performanceMap.put("vendor_id", vendorId);
				
				batch[i] = performanceMap;
			}
			
			insertTmpBizInfoPerformance.executeBatch(batch);
		}
		
		List<BankAccount> accounts = info.getBankInformation().getBankAccounts();
		Map<String, Object>[] batch = new Map[accounts.size()];
		
		for (int i=0; i<accounts.size(); i++) {
			BankAccount account = accounts.get(i);
			Map<String, Object> accountMap = account.toMap();
			accountMap.put("id", account.getBankAccountId());
			accountMap.put("vendor_id", vendorId);
			
			batch[i] = accountMap;
		}
		
		insertTmpBankAccount.executeBatch(batch);
		
		List<Attachment> attachments = findAttachmentWithData(vendorId);
		batch = new Map[attachments.size()];
		for (int i=0; i<attachments.size(); i++) {
			Attachment attachment = attachments.get(i);
			Map<String, Object> attachmentMap = attachment.toMap();
			attachmentMap.put("vendor_id", vendorId);
			attachmentMap.put("tran_id", info.getTranId());
			
			batch[i] = attachmentMap;
 		}
		
		insertTmpAttachment.executeBatch(batch);
	}

	@SuppressWarnings("deprecation")
	private List<Attachment> findAttachmentWithData(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, name, size, content_type as contentType, data FROM attachment WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				vendorId);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void restoreVendorInfo(long vendorId) {
		jdbcTemplate.update("delete from attachment where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from business_info_performance where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from bank_account where vendor_id = ?", vendorId);
		

		VendorBasicInfo info = findTmpVendorBasicInfo(vendorId);
		info.init();
		
		Map<String, Object> rowMap = info.toRowMap();
		rowMap.put("id",info.getVendorId());
		
		updateVendorBasicInfo(rowMap);
		
		List<BizInfoPerformance> performances = info.getBusinessInformation().getPerformance();	
		if (performances != null && performances.size() != 0) {
			Map<String, Object>[] batch = new Map[performances.size()];
			
			for (int i=0; i<performances.size(); i++) {
				BizInfoPerformance performance = performances.get(i);
				Map<String, Object> performanceMap = performances.get(i).toMap();
				performanceMap.put("id", performance.getBizInfoId());
				performanceMap.put("vendor_id", vendorId);
				
				batch[i] = performanceMap;
			}
			
			insertBizInfoPerformance.executeBatch(batch);
		}
		
		List<BankAccount> accounts = info.getBankInformation().getBankAccounts();
		Map<String, Object>[] batch = new Map[accounts.size()];
		
		for (int i=0; i<accounts.size(); i++) {
			BankAccount account = accounts.get(i);
			Map<String, Object> accountMap = account.toMap();
			accountMap.put("id", account.getBankAccountId());
			accountMap.put("vendor_id", vendorId);
			
			batch[i] = accountMap;
		}
		
		insertBankAccount.executeBatch(batch);
		
		List<Attachment> attachments = findTmpAttachmentWithData(vendorId);
		batch = new Map[attachments.size()];
		for (int i=0; i<attachments.size(); i++) {
			Attachment attachment = attachments.get(i);
			Map<String, Object> attachmentMap = attachment.toMap();
			attachmentMap.put("vendor_id", vendorId);
			attachmentMap.put("tran_id", attachment.getTranId());
			
			batch[i] = attachmentMap;
 		}
		
		insertAttachment.executeBatch(batch);
	
		jdbcTemplate.update("delete from tmp_attachment where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_business_info_performance where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_bank_account where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_vendor_basic_info where id = ?", vendorId);
		
	}
	
	public void deleteTmp(long vendorId){
		jdbcTemplate.update("delete from tmp_attachment where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_business_info_performance where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_bank_account where vendor_id = ?", vendorId);
		jdbcTemplate.update("delete from tmp_vendor_basic_info where id = ?", vendorId);
	}
	
	private VendorBasicInfo findTmpVendorBasicInfo(long vendorId) {
		Map<String, Object> rowMap = jdbcTemplate.queryForMap(
				"SELECT * " +
				"FROM tmp_vendor_basic_info " +
				"WHERE id = ? ", 
				vendorId);
		
		List<BizInfoPerformance> performances = findTmpBizInfoPerformance(vendorId);
		List<BankAccount> bankAccounts = findTmpBankAccount(vendorId, BankAccount.STATUS_ALIVE);

		return new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts);
	}
	
	@SuppressWarnings("deprecation")
	private List<BizInfoPerformance> findTmpBizInfoPerformance(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id as bizInfoId, year, sales_amount as salesAmount, profit, profitability " +
				"FROM tmp_business_info_performance " +
				"WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(BizInfoPerformance.class),
				vendorId);
	}
	
	@SuppressWarnings("deprecation")
	private List<BankAccount> findTmpBankAccount(long vendorId, int status) {
		return jdbcTemplate.query(
				"SELECT id as bankAccountId, " +
				"  vendor_sub_code as vendorSubcode," +
				"  province_and_region as provinceAndRegion," +
				"  swift_code as swiftCode," +
				"  name," +
				"  account, " +
				"  city," +
				"  currency," +
				"  type," +
				"  institution_no as institutionNo," +
				"  joint_code as jointCode," +
				"  cnaps, " +
				"  status " +
				"FROM tmp_bank_account " +
				"WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(BankAccount.class),
				vendorId);
	}
	
	@SuppressWarnings("deprecation")
	private List<Attachment> findTmpAttachmentWithData(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, tran_id as tranId, name, size, content_type as contentType, data FROM tmp_attachment WHERE vendor_id = ?", 
				ParameterizedBeanPropertyRowMapper.newInstance(Attachment.class), 
				vendorId);
	}
	
	@Override
	public int getVendorStatus(long vendorId) {
		return jdbcTemplate.queryForInt("SELECT status FROM vendor_basic_info WHERE id = ?", vendorId);
	}
	
	@Override
	public boolean alreadyHasCopy(long vendorId) {
		return jdbcTemplate.queryForInt("SELECT count(*) FROM tmp_vendor_basic_info WHERE id = ?", vendorId) == 1;
	}


	@Override
	public void updateVendorNumber(long vendorId, String number) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET  number = ? WHERE id = ?", 
				number,
				vendorId);
	}
	
	@Override
	public String getVendorNumber(long vendorId) {
		return jdbcTemplate.queryForObject(
				"SELECT number FROM vendor_basic_info WHERE id = ?", 
				String.class, 
				vendorId);
	}


	@Override
	public VendorBasicInfo findVendorBasicInfo(long vendorId) {
		Map<String, Object> rowMap = jdbcTemplate.queryForMap(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE id = ? ", 
				vendorId);
		
		List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
		List<BankAccount> bankAccounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);

		return new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts);
	}

	
	@Override
	public void tempSaveVendorInfo(long vendorId, String number) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET  number = ?, status = 60 WHERE id = ?", 
				number,
				vendorId);
	}


	@Override
	public void updateForFreeze(long vendorId) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET  freeze = 1, action_time = ? WHERE id = ?", 
				new Date(),
				vendorId);
	}


	@Override
	public void updateForUnfreeze(long vendorId) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET  freeze = 0, action_time = ? WHERE id = ?", 
				new Date(),
				vendorId);
	}


	@Override
	public void updateApplicant(String applicant, long vendorId) {
		jdbcTemplate.update("UPDATE vendor_basic_info SET  applicant = ?, applicant_name = (select name from user where id = ?) WHERE id = ?", 
				applicant,
				applicant,
				vendorId);
	}


	@Override
	public List<VendorBasicInfo> findAvailableVendorBasicInfos(int category) {
		List<VendorBasicInfo> vendorBasicInfos= new ArrayList<VendorBasicInfo>();
		String args = "";
		switch(category){
			case VendorSearchCriteria.DOWNLOAD_ALL:
				break;
			case VendorSearchCriteria.DOWNLOAD_FREEZE:
				args = "AND freeze = 1";
				break;
			case VendorSearchCriteria.DOWNLOAD_VALID:
				args = "AND (freeze = 0 or freeze is null)";
				break;
		}
		List<Map<String, Object>> listMap = jdbcTemplate.queryForList(
				"SELECT * " +
				"FROM vendor_basic_info " +
				"WHERE " +
				"status = 40 "+
				args);
		for(Map<String, Object> rowMap : listMap){
			long vendorId = Long.valueOf(rowMap.get("id").toString());
			List<BizInfoPerformance> performances = findBizInfoPerformance(vendorId);
			List<BankAccount> bankAccounts = findBankAccount(vendorId, BankAccount.STATUS_ALIVE);
			vendorBasicInfos.add(new VendorBasicInfo(vendorId, rowMap, performances, bankAccounts));
		}
		
		return vendorBasicInfos;
	}


	/** (非 Javadoc) 
	 * <p>Title: searchVendorInfoForAll</p> 
	 * <p>Description: </p> 
	 * @param sql
	 * @return 
	 * @see cn.sscs.vendormanagement.vendor.IVendorService#searchVendorInfoForAll(java.lang.String) 
	*/
	@Override
	public List<SimpleVendorInfo> searchVendorInfoForAll(String sql) {
		return searchVendorInfo(
				"SELECT vendor_chinese_name as chineseName," +
				"  vendor_english_name as englishName," +
				"  vendor_code as vendorCode," +
				"  number as vendorNumber," +
				"  status," +
				"  id, " +
				"  freeze " +
				"FROM vendor_basic_info "  +
				sql
				);
	}


	@Override
	public String findVendorCodeByid(long vendorId) {
		return jdbcTemplate.queryForObject(
				"select vendor_code from vendor_basic_info where id = ?", String.class, vendorId);
	}


	@Override
	public void updateVendorSubCode(Map<String, Object> map) {
		jdbcTemplate.update(
				"UPDATE bank_account " +
				"SET " +
				"vendor_sub_code =:vendor_sub_code " +
				"WHERE id=:id", 
				map);
	}

	/**
	 * The method for judging that user had or not ability to change business type. 
	 * Judge condition:
	 *     1. hasn't evaluation(status : 10 or 20 or 30)
	 *     2. isn't EXTER vendor
	 * 
	 * @param vendorId
	 *         
//	 * @param isInConfirmPage
//	 *         this param is for that when the vendor having 0 status for evaluation, 
//	 *         it couldn't change its business type.
     * @return
	 */
    @Override
    public boolean isHasAbilityToChangeBusinessType(long vendorId) {
        return !hasEvaluationByVendorId(vendorId);
    }
	
    /**
     * status:
     *  10, HD update
     *  20, user update
     *  30, temp save
     *  0, needn't care
     * @param vendorId
     * @return
     */
    public boolean hasEvaluationByVendorId(long vendorId) {
        return jdbcTemplate.queryForInt(
                "SELECT count(*) " +
                "FROM evaluation " +
                "WHERE vendor_id = ? AND registered_date = (" +
                "  SELECT MAX(registered_date) FROM evaluation WHERE vendor_id = ? AND status in(10, 20) " +
                ")", vendorId, vendorId) > 0;
    }

    @Override
    public boolean validate(String superCompanyType, String username) {
        String sql = "SELECT count(*) " +
        		"from user u, user_info ui " +
        		"where u.id = ui.id and u.id = ? and (ui.usercompany = 1 or ui.usercompany = ?)";
        return jdbcTemplate.queryForInt(sql, username, superCompanyType) > 0;
    }
}
