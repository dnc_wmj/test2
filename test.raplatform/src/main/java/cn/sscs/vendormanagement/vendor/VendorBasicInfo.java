package cn.sscs.vendormanagement.vendor;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.validation.Errors;

import cn.sscs.vendormanagement.Utils;

/**
 * 
 * @author kazzy
 *
 */
public class VendorBasicInfo {
	
	private String[] test;
	
	public static final int STATUS_IN_APPLYING = 0; //申请状态    提出   
	
	public static final int STATUS_EDITING = 5;//申请状态    编辑中
	
	public static final int STATUS_IN_APPLICATION = 10;//申请状态    申请中
		
	public static final int STATUS_IMPROVING = 20;//申请状态    改善 
	
	public static final int STATUS_CLOSE = 30;//申请状态    关闭
	
	public static final int STATUS_REGISTERED = 40;//申请状态    登记
	
	public static final int STATUS_DELETED = 50;//申请状态    删除

	public static final int STATUS_TMPSAVE = 60;//申请状态    时保存  
	
	public static final String ISFREEZE = "1";  
	
	private long vendorId = -1;
	
	private String tranId = null;
	
	private Identity identity = null;
	
	private Contact contact = null;
	
	private Certificate certificate = null;
	
	private CompanyType companyType = null;
	
	private TradeCondition tradeCondition = null;
	
	private BusinessInformation businessInformation = null;
	
	private String specialStatement = null;
	
	private BankInformation bankInformation = null;
	
	private Accounting accounting = null;
	
	private int status = STATUS_IN_APPLYING;
	
	private String freeze = null;
	
	private boolean hasEvaluation = false;
	
	private Date registeredDate = null;
	
	private Date actionTime = null;
	private String applicant = null;
	private String applicantName = null;
	/**
	 * 
	 */
	VendorBasicInfo() {
		identity = new Identity();
		contact = new Contact();
		certificate = new Certificate();
		companyType = new CompanyType();
		tradeCondition = new TradeCondition();
		businessInformation = new BusinessInformation();
		bankInformation = new BankInformation();
		accounting = new Accounting();
	}
	
	/**
	 * 
	 * @param vendorId
	 * @param rowMap
	 * @param performances
	 * @param bankAccounts
	 */
	VendorBasicInfo(long vendorId, 
			Map<String, Object> rowMap, 
			List<BizInfoPerformance> performances,
			List<BankAccount> bankAccounts) {
		this.vendorId = vendorId;
		identity = new Identity(rowMap);
		contact = new Contact(rowMap);
		certificate = new Certificate(rowMap);
		companyType = new CompanyType(rowMap);
		tradeCondition = new TradeCondition(rowMap);
		businessInformation = new BusinessInformation(rowMap, performances);
		bankInformation = new BankInformation(bankAccounts);
		accounting = new Accounting(rowMap);
		
		tranId = (String)rowMap.get("tran_id");
		specialStatement = (String) rowMap.get("special_statement");
		status = (Integer) rowMap.get("status");
		freeze = (String)rowMap.get("freeze");
		registeredDate = (Date) rowMap.get("registered_date");
		setActionTime((Date) rowMap.get("action_time"));
		setApplicant((String)rowMap.get("applicant"));
		setApplicantName((String)rowMap.get("applicant_name"));
	}

	public VendorBasicInfo init() {
		businessInformation.init();
		bankInformation.init();
		
		return this;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getVendorId() {
		return vendorId;
	}
	
	/**
	 * 
	 * @param vendorId
	 */
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTranId() {
		return tranId;
	}
	
	/**
	 * 
	 * @param tranId
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	
	/**
	 * 
	 * @return
	 */
	public Identity getIdentity() {
		return identity;
	}
	
	/**
	 * 
	 * @return
	 */
	public Contact getContact() {
		return contact;
	}
	
	/**
	 * 
	 * @return
	 */
	public Certificate getCertificate() {
		return certificate;
	}
	
	/**
	 * 
	 * @return
	 */
	public CompanyType getCompanyType() {
		return companyType;
	}
	
	/**
	 * 
	 * @return
	 */
	public TradeCondition getTradeCondition() {
		return tradeCondition;
	}
	
	/**
	 * 
	 * @return
	 */
	public BusinessInformation getBusinessInformation() {
		return businessInformation;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getSpecialStatement() {
		return specialStatement;
	}
	
	/**
	 * 
	 * @param specialStatement
	 */
	public void setSpecialStatement(String specialStatement) {
		this.specialStatement = specialStatement;
	}
	
	/**
	 * 
	 * @return
	 */
	public BankInformation getBankInformation() {
		return bankInformation;
	}
	
	/**
	 */
	public Accounting getAccounting() {
		return accounting;
	}
	
	/**
	 * 
	 * @param errors
	 */
	public void validate(Errors errors, IVendorService vendorService, long vendorId) {
		identity.validate(errors, vendorService, vendorId, companyType.getSuperCompanyType());
		contact.validate(errors);
		certificate.validate(errors);
		tradeCondition.validate(errors);
		companyType.validate(errors, vendorService);
		if(Utils.isRoleHD()){
			accounting.validate(errors);
		}
	}
	
	/**
	 * 
	 * @return
	 */
	Map<String, Object> toRowMap() {
		Map<String, Object> rowMap = new ParamMap();
		identity.toMap(rowMap);
		contact.toMap(rowMap);
		certificate.toMap(rowMap);
		companyType.toMap(rowMap);
		tradeCondition.toMap(rowMap);
		businessInformation.toMap(rowMap);
		accounting.toMap(rowMap);
		
		rowMap.put("tran_id", tranId);
		rowMap.put("special_statement", specialStatement);
		rowMap.put("status", status);
		rowMap.put("registered_date", registeredDate);
		
		rowMap.put("action_time", actionTime);
		rowMap.put("applicant", applicant);
		rowMap.put("applicant_name", applicantName);
		rowMap.put("freeze", freeze);
		return rowMap;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasEvaluation() {
		return hasEvaluation;
	}
	
	/**
	 * 
	 * @param hasEvaluation
	 */
	public void setHasEvaluation(boolean hasEvaluation) {
		this.hasEvaluation = hasEvaluation;
	}
	
	/**
	 * 
	 * @param status
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}
	
	/**
	 * 
	 * @return
	 */
	public Date getRegisteredDate() {
		return registeredDate;
	}
	
	/**
	 * 
	 * @param registeredDate
	 */
	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}

	public String getFreeze() {
		return freeze;
	}

	public void setActionTime(Date actionTime) {
		this.actionTime = actionTime;
	}

	public Date getActionTime() {
		return actionTime;
	}

	public void setApplicant(String applicant) {
		this.applicant = applicant;
	}

	public String getApplicant() {
		return applicant;
	}

	public void setApplicantName(String applicantName) {
		this.applicantName = applicantName;
	}

	public String getApplicantName() {
		return applicantName;
	}

	public String[] getTest() {
		return test;
	}

	public void setTest(String[] test) {
		this.test = test;
	}
}
