package cn.sscs.vendormanagement.vendor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.web.multipart.MultipartFile;

public class Attachment {
	
	private long id = -1;
	
	private String tranId = null;
	
	private MultipartFile file = null;

	private String name = null;
	
	private long size = -1;
	
	private String contentType = null;
	
	private byte[] data = null;
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getTranId() {
		return tranId;
	}
	
	/**
	 * 
	 * @param tranId
	 */
	public void setTranId(String tranId) {
		this.tranId = tranId;
	}
	
	/**
	 * @return the file
	 */
	public MultipartFile getFile() {
		return file;
	}

	/**
	 * @param file the file to set
	 */
	public void setFile(MultipartFile file) {
		this.file = file;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name == null ? file.getOriginalFilename() : name;
	}
	
	/**
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getSize() {
		return size == -1 ? file.getSize() : size;
	}
	
	/**
	 * 
	 * @param size
	 */
	public void setSize(long size) {
		this.size = size;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getContentType() {
		return contentType == null ? file.getContentType() : contentType;
	}
	
	/**
	 * 
	 * @param contentType
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
	/**
	 * 
	 * @return
	 */
	public byte[] getData() {
		return data;
	}
	
	/**
	 * 
	 * @param data
	 */
	public void setData(byte[] data) {
		this.data = data;
	}
	
	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("id", id);
		map.put("name", name);
		map.put("content_type", contentType);
		map.put("size", size);
		map.put("data", data);
		
		return map;
	}
}
