package cn.sscs.vendormanagement.vendor;

import java.util.List;
import java.util.Map;

public interface IVendorService {

	/**
	 * 
	 * @return
	 */
	List<Country> getCountries();

	/**
	 * 
	 * @param country
	 * @return
	 */
	List<Region> getRegion(String country);

	/**
	 * 
	 * @param vendorBasicInfo
	 * @param accounts
	 * @return
	 */
	int storeVendorBasicInfo(VendorBasicInfo vendorBasicInfo, List<BankAccount> accounts);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	Identity getIdentity(long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @return
	 */
	VendorBasicInfo findVendorBasicInfoByStatus(long vendorId, int status);

	/**
	 * 
	 * @Title:       findVendorBasicInfoByStatus 
	 * @Description: 多个状态查询 
	 * @param        @param vendorId
	 * @param        @param status
	 * @param        @return     
	 * @return       VendorBasicInfo     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	VendorBasicInfo findVendorBasicInfoByStatus(long vendorId, List<Integer> status);
	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	VendorBasicInfo findAvailableVendorBasicInfo(long vendorId);
	
	/**
	 * 
	 * @param status
	 * @param vendorId
	 */
	void updateStatus(int status, long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @param accounts
	 */
	void storeBankAccount(long vendorId, List<BankAccount> accounts);

	/**
	 * 
	 * @param code
	 * @return
	 */
	String getCountryName(String code);

	/**
	 * 
	 * @param countryCode
	 * @param regionCode
	 * @return
	 */
	String getRegionName(String countryCode, String regionCode);

	/**
	 * 
	 * @param vendorId
	 * @param performances
	 */
	void storeBizInfoPerformance(long vendorId,
			List<BizInfoPerformance> performances);

	/**
	 * 
	 * @param attachment
	 */
	long storeAttachment(Map<String, Object> attachment);

	/**
	 * 
	 * @param tranId
	 * @return
	 */
	List<Attachment> findAttachment(String tranId);

	/**
	 * 
	 * @param tranId
	 * @param vendorId
	 */
	void fixAttachmentData(String tranId, long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	List<Attachment> findAttachment(long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @param performances
	 */
	void updateOrInsertBizInfoPerformance(long vendorId, List<BizInfoPerformance> performances);

	/**
	 * 
	 * @param accounts
	 */
	void updateOrInsertBankAccount(long vendorId, List<BankAccount> accounts);

	/**
	 * 
	 * @param vendorBasicInfo
	 * @param accounts
	 */
	void updateVendorBasicInfo(VendorBasicInfo vendorBasicInfo, List<BankAccount> accounts);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	int searchBankMasterCount(BankSearchCriteria criteria);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	List<BankMaster> searchBankMaster(BankSearchCriteria criteria);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	int searchCnapsCount(BankSearchCriteria criteria);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	List<Cnap> searchCnaps(BankSearchCriteria criteria);

	/**
	 * 
	 * @param name
	 * @return
	 */
	boolean isValidBankName(String name);

	/**
	 * 
	 * @param name
	 * @return
	 */
	boolean isBankOfChina(String name);

	/**
	 * 
	 * @param name
	 * @param institutionNo
	 * @return
	 */
	boolean isValidInstitutionNo(String name, String jointCode);

	/**
	 * 
	 * @param name
	 * @param jointCode
	 * @return
	 */
	boolean isValidJointCode(String name, String jointCode);

	/**
	 * 
	 * @param name
	 * @param cnaps
	 * @return
	 */
	boolean isValidCnaps(String name, String cnaps);

	/**
	 * 
	 * @param id
	 * @return
	 */
	Attachment findAttachmentData(long id);

	/**
	 * 
	 * @param id
	 * @param tranId
	 */
	void deleteAttachmentData(long id, String tranId);

	/**
	 * 
	 * @param id
	 */
	void deleteBankAccount(long id);

	/**
	 * 
	 * @param id
	 */
	void deleteBizInfoPerformance(long id);

	/**
	 * 
	 * @param vendorId
	 * @param status
	 * @return
	 */
	List<BankAccount> findBankAccount(long vendorId, int status);

	/**
	 * 
	 * @param id
	 * @return
	 */
	BankAccount findBankAccountById(long id);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	int getBusiessType(long vendorId);

	/**
	 * 
	 * @param id
	 * @param vendorCode
	 * @param number 
	 */
	void updateVendorCode(long id, String vendorCode);

	/**
	 * 
	 * @param vendorCode
	 * @param superCompanyType 
	 * @return
	 */
	boolean exists(String vendorCode, String superCompanyType);

	/**
	 * 
	 * @param bankAccountID
	 * @return
	 */
	long getVendorIDByBankAccountID(long bankAccountID);

	/**
	 * 
	 * @param name
	 * @param jointCode
	 * @param institutionNo
	 * @return
	 */
	boolean isValidJCodeINo(String name, String jointCode, String institutionNo);
	
	/**
	 * 
	 * @param chineseName
	 * @param vendorId
	 * @param superCompanyType 
	 * @return
	 */
	boolean isValidIdentityCName(String chineseName, long vendorId, String superCompanyType);

	/**
	 * 
	 * @param englishName
	 * @param vendorId
	 * @param superCompanyType 
	 * @return
	 */
	boolean isValidIdentityEName(String englishName, long vendorId, String superCompanyType);
	
	/**
	 * 
	 * @param vendorId
	 */
	void copyVendorInfo(long vendorId);

	/**
	 * 
	 * @param vendorId
	 */
	void restoreVendorInfo(long vendorId);

	/**
	 * 
	 * @param id
	 */
	void deleteBankAccountPhysically(long id);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	int getVendorStatus(long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	boolean alreadyHasCopy(long vendorId);

	/**
	 * 
	 * @param tranId
	 * @param vendorId
	 */
	void fixAttachmentDataWorkflow(String tranId, long vendorId);

	/**
	 * 
	 * @param vendorId
	 * @return
	 */
	List<Attachment> findAttachmentWorkflow(long vendorId);
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	Attachment findAttachmentDataWorkflow(long id);

	/**
	 * 
	 * @param tranId
	 * @return
	 */
	List<Attachment> findAttachmentWorkflow(String tranId);
	/**
	 * 
	 * @param criteria
	 * @return
	 */
	
	/* Start 由于添加了新方法，在供应商查询时不需要调用 ，若其他条件下均无调用 ，由此作废   郑同 2012年7月10日9:55:31*/
	/**
	 * 以供应商编码为查询条件查询供应商总数 
	 * @param criteria
	 * @return
	 */
	int searchVendorCountByNumber(VendorSearchCriteria criteria);
	/**
	 * 以供应商名为查询条件查询供应商总数 
	 * @param criteria
	 * @return
	 */
	int searchVendorCountByName(VendorSearchCriteria criteria);
	/**
	 * 以供应商编号为查询条件查询供应商总数
	 * @param criteria
	 * @return
	 */
	int searchVendorCountByCode(VendorSearchCriteria criteria);
	/**
	 * 以供应商编码为查询条件查询供应商
	 * @param criteria
	 * @return
	 */
	List<SimpleVendorInfo> searchVendorInfoByNumber(
			VendorSearchCriteria criteria);
	/**
	 *  以供应商编号为查询条件查询供应商
	 * @param criteria
	 * @return
	 */
	List<SimpleVendorInfo> searchVendorInfoByCode(VendorSearchCriteria criteria);

	/**
	 * 以供应商名为查询条件查询供应商
	 * @param criteria
	 * @return
	 */
	List<SimpleVendorInfo> searchVendorInfoByName(VendorSearchCriteria criteria);
	
	/**
	 * 供应商维护画面 以供应商名为查询条件 查询供应商方法 
	 * @param criteria
	 * @return
	 */
	List<VendorBasicInfo> searchFullVendorInfoByName(
			VendorSearchCriteria criteria);
	
	/**
	 * 供应商维护画面  以供应商编号为查询条件查询供应商总数
	 * @param criteria
	 * @return
	 */
	List<VendorBasicInfo> searchFullVendorInfoByCode(
			VendorSearchCriteria criteria);

	/**
	 * 
	 * @Title:       searchFullVendorInfoByNumber 
	 * @Description: ( 供应商维护画面 以供应商编号为查询条件查询供应商  ) 
	 * @param        @param criteria
	 * @param        @return     
	 * @return       List<VendorBasicInfo>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	List<VendorBasicInfo> searchFullVendorInfoByNumber(
			VendorSearchCriteria criteria);
	
	/* end 由于添加了新方法，在供应商查询时不需要调用 ，若其他条件下均无调用 ，由此作废   郑同 2012年7月10日9:55:31*/
	
	
	/**
	 * 
	 * @Title:       searchVendorCount 
	 * @Description: ( 供应商查询  配合查询总条数  条件同供应商查询一致  ) 
	 * @param        @param criteria
	 * @param        @return     
	 * @return       int     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	int searchVendorCountForAll(String sql);
	/**
	 * 
	 * @Title:       searchVendorInfo 
	 * @Description: ( 供应商查询   ) 
	 * @param        @param criteria
	 * @param        @return     
	 * @return       List<SimpleVendorInfo>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	List<SimpleVendorInfo> searchVendorInfoForAll(
			String sql);


	/**
	 * 
	 * @Title:       searchFullVendorInfoByName 
	 * @Description: 供应商查询 
	 * @param        @param criteria
	 * @param        @return     
	 * @return       List<VendorBasicInfo>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 10, 2012
	 */
	List<VendorBasicInfo> searchFullVendorInfoForAll(
			String sql);

	
	void deleteAttachmentWorkFlowData(long id, String tranId);

	void updateVendorNumber(long vendorId, String number);

	String getVendorNumber(long vendorId);

	VendorBasicInfo findVendorBasicInfo(long vendorId);

	void tempSaveVendorInfo(long vendorId, String number);

	void updateStatusAndNumber(int status, long vendorId, String number);

	void updateForFreeze(long vendorId);

	void updateForUnfreeze(long vendorId);

	void updateApplicant(String applicant, long vendorId);

	List<VendorBasicInfo> findAvailableVendorBasicInfos(int category);

	void deleteTmp(long vendorId);

	String findVendorCodeByid(long vendorId);

	void updateVendorSubCode(Map<String, Object> map);

    boolean isHasAbilityToChangeBusinessType(long vendorId);

    boolean validate(String superCompanyType, String username);

}
