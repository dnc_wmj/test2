package cn.sscs.vendormanagement.vendor;

import java.util.Locale;

import org.springframework.context.i18n.LocaleContextHolder;

public class SimpleVendorInfo {

	public static final String FREEZE_YES = "1"; // 冻结装
	
	private String id = null;
	
	private String chineseName = null;
	
	private String englishName = null;
	/*郑同 2012年7月13日15:17:30 add  冻结解冻（是否有效）*/
	private String freeze = null;
	
	

	private String vendorCode = null;
	private String vendorNumber = null;

	private int status = -1;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the chineseName
	 */
	public String getChineseName() {
		return chineseName;
	}

	/**
	 * @param chineseName the chineseName to set
	 */
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	/**
	 * @return the englishName
	 */
	public String getEnglishName() {
		return englishName;
	}

	/**
	 * @param englishName the englishName to set
	 */
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

	/**
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * @param vendorCode the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVendorNumber() {
		return vendorNumber;
	}
	
	/**
	 * 
	 * @param vendorNumber
	 */
	public void setVendorNumber(String vendorNumber) {
		this.vendorNumber = vendorNumber;
	}
	/**
	 * 
	 * @return
	 */
	public String getName() {
		Locale locale = LocaleContextHolder.getLocale();
		if ("zh".equals(locale.getLanguage())) {
			return chineseName != null && !chineseName.equals("") ? chineseName : englishName;
		} else if ("en".equals(locale.getLanguage())){
			return englishName != null && !englishName.equals("") ? englishName : chineseName;
		} else {
			return chineseName != null ? chineseName : englishName;
		}
	}
	/**
	 * 
	 * @Title:       getFreeze 
	 * @Description: 获取是否被冻结   为1的状态为冻结状态
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 13, 2012
	 */
	public String getFreeze() {
		return FREEZE_YES.equals(freeze) ? "1":"0";
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}
}
