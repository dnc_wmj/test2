package cn.sscs.vendormanagement.vendor;

import java.util.Map;

import org.springframework.validation.Errors;

import cn.sscs.vendormanagement.Utils;

/**
 * @author kazzy
 */
public class CompanyType {
    
    public static final int BUSINESS_TYPE_NON_TRADE = 0;
    
    public static final int BUSINESS_TYPE_TRADE = 1;
    
    private int domesticInvestment = 0;
    
    private int foreignInvestment = 5;
    
    private String foreignInvestmentOther = null;
    
    private String jointVentureInvestmentRatio = null;
    
    private int businessType = 0;
    
    private String production = null;
    
    private String businessRelation = null;
    
    /**
     * 1: SSCS
     * 2: SSGE
     * 3: SSV
     */
    private String superCompanyType = "0";
    
    /**
	 * 
	 */
    CompanyType() {
    }
    
    /**
     * @param rowMap
     */
    CompanyType(Map<String, Object> rowMap) {
        this.domesticInvestment = (Integer) rowMap.get("domestic_investment");
        this.foreignInvestment = (Integer) rowMap.get("foreign_investment");
        this.foreignInvestmentOther = (String) rowMap
                .get("foreign_investment_other");
        this.jointVentureInvestmentRatio = (String) rowMap
                .get("joint_venture_investment_ratio");
        this.businessType = (Integer) rowMap.get("business_type");
        this.production = (String) rowMap.get("production");
        this.businessRelation = (String) rowMap.get("business_relation");
        this.superCompanyType = (String) rowMap.get("super_company");
    }
    
    /**
     * @return
     */
    public int getDomesticInvestment() {
        return domesticInvestment;
    }
    
    /**
     * @param domesticInvestment
     */
    public void setDomesticInvestment(int domesticInvestment) {
        this.domesticInvestment = domesticInvestment;
    }
    
    /**
     * @return
     */
    public int getForeignInvestment() {
        return foreignInvestment;
    }
    
    /**
     * @param foreignInvestment
     */
    public void setForeignInvestment(int foreignInvestment) {
        this.foreignInvestment = foreignInvestment;
    }
    
    /**
     * @return
     */
    public String getForeignInvestmentOther() {
        return foreignInvestmentOther;
    }
    
    /**
     * @param foreignInvestmentOther
     */
    public void setForeignInvestmentOther(String foreignInvestmentOther) {
        this.foreignInvestmentOther = foreignInvestmentOther;
    }
    
    /**
     * @return
     */
    public String getJointVentureInvestmentRatio() {
        return jointVentureInvestmentRatio;
    }
    
    /**
     * @param jointVentureInvestmentRatio
     */
    public void setJointVentureInvestmentRatio(
            String jointVentureInvestmentRatio) {
        this.jointVentureInvestmentRatio = jointVentureInvestmentRatio;
    }
    
    /**
     * @return
     */
    public int getBusinessType() {
        return businessType;
    }
    
    /**
     * @param businessType
     */
    public void setBusinessType(int businessType) {
        this.businessType = businessType;
    }
    
    /**
     * @param rowMap
     */
    void toMap(Map<String, Object> rowMap) {
        rowMap.put("domestic_investment", domesticInvestment);
        rowMap.put("foreign_investment", foreignInvestment);
        rowMap.put("foreign_investment_other", foreignInvestmentOther);
        rowMap.put("joint_venture_investment_ratio",
                jointVentureInvestmentRatio);
        rowMap.put("business_type", businessType);
        rowMap.put("production", production);
        rowMap.put("business_relation", businessRelation);
        rowMap.put("super_company", superCompanyType);
    }
    
    public String getProduction() {
        return production;
    }
    
    public void setProduction(String production) {
        this.production = production;
    }
    
    public String getBusinessRelation() {
        return businessRelation;
    }
    
    public void setBusinessRelation(String businessRelation) {
        this.businessRelation = businessRelation;
    }
    
    public void setSuperCompanyType(String superCompanyType) {
        this.superCompanyType = superCompanyType;
    }
    
    public String getSuperCompanyType() {
        return superCompanyType;
    }
    
    public void validate(Errors errors, IVendorService vendorService) {
        if (superCompanyType == null || "0".equals(superCompanyType)
                || "".equals(superCompanyType)) {
            
            errors.rejectValue("companyType.superCompanyType", "error.required");
            
        } else if (!vendorService.validate(superCompanyType, Utils.getUser()
                .getUsername())) {
            
            errors.rejectValue("companyType.superCompanyType", "error.required");
        }
        
    }
}
