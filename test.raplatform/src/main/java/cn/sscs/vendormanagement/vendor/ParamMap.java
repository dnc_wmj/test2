package cn.sscs.vendormanagement.vendor;

import java.util.HashMap;

public class ParamMap extends HashMap<String, Object> {

	private static final long serialVersionUID = -5238594434820607751L;

	@Override
	public Object put(String key, Object value) {
		if (value != null && value instanceof String) {
			if (((String)value).equals("")) {
				value = null;
			}
		} 
		return super.put(key, value);
	}
	
	
}
