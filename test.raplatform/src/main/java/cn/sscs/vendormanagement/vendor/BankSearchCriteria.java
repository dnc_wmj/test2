package cn.sscs.vendormanagement.vendor;

public class BankSearchCriteria {

	static final int TYPE_BANK_NO = 0;
	
	static final int TYPE_CNAPS = 1;
	
	private String name = null;
	
	private int type = 0;

	private int page = 0;
	
	@SuppressWarnings("unused")
	private int recordFrom = 0;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * 
	 * @return
	 */
	public String getAmbiguousName() {
		return "%" + name + "%";
	}
	
	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getPage() {
		return page;
	}
	
	/**
	 * 
	 * @param page
	 */
	public void setPage(int page) {
		this.page = page;
	}

	public int getRecordFrom() {
		return this.page * 20;
	}

	public void setRecordFrom(int recordFrom) {
		this.recordFrom = recordFrom;
	}
	
}
