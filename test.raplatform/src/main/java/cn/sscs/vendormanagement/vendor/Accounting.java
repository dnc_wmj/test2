package cn.sscs.vendormanagement.vendor;

import java.util.Map;

import org.springframework.validation.Errors;

/**
 * 
 * @author kazzy
 *
 */
public class Accounting {

	private String companyCode = "SSH1";
	
	private String vendorAccountType = "vendor";
	
	private String vendorGroup = "Z001";
	
	private String groupKey = null;
	
	private String reconAccount = "21001000";
	
	private boolean chkDoubleInv = true;
	
	private int paymentTerm = 8;
	
	/**
	 * 
	 */
	Accounting() {}
	
	/**
	 * 
	 * @param rowMap
	 */
	Accounting(Map<String, Object> rowMap) {
		companyCode = (String) rowMap.get("company_code");
		vendorAccountType = (String) rowMap.get("vendor_account_type");
		vendorGroup = (String) rowMap.get("vendor_group");
		groupKey = (String) rowMap.get("group_key");
		reconAccount = (String) rowMap.get("recon_account");
		paymentTerm = (Integer)rowMap.get("payment_term_for_accounting");
		chkDoubleInv = (Boolean)rowMap.get("chk_double_inv");
	}
	
	/**
	 * 
	 * @return
	 */
	public String getCompanyCode() {
		return companyCode;
	}

	/**
	 * 
	 * @param companyCode
	 */
	public void setCompanyCode(String companyCode) {
		this.companyCode = companyCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVendorAccountType() {
		return vendorAccountType;
	}

	/**
	 * 
	 * @param vendorAccountType
	 */
	public void setVendorAccountType(String vendorAccountType) {
		this.vendorAccountType = vendorAccountType;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVendorGroup() {
		return vendorGroup;
	}
	
	/**
	 * 
	 * @param vendorGroup
	 */
	public void setVendorGroup(String vendorGroup) {
		this.vendorGroup = vendorGroup;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getGroupKey() {
		return groupKey;
	}
	
	/**
	 * 
	 * @param groupKey
	 */
	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getReconAccount() {
		return reconAccount;
	}
	
	/**
	 * 
	 * @param reconAccount
	 */
	public void setReconAccount(String reconAccount) {
		this.reconAccount = reconAccount;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getChkDoubleInv() {
		return chkDoubleInv;
	}
	
	/**
	 * 
	 * @param chkDoubleInv
	 */
	public void setChkDoubleInv(boolean chkDoubleInv) {
		this.chkDoubleInv = chkDoubleInv;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getPaymentTerm() {
		return paymentTerm;
	}
	
	/**
	 * 
	 * @param paymentTerm
	 */
	public void setPaymentTerm(int paymentTerm) {
		this.paymentTerm = paymentTerm;
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("company_code", companyCode);
		rowMap.put("vendor_account_type", vendorAccountType);
		rowMap.put("vendor_group", vendorGroup);
		rowMap.put("group_key", groupKey);
		rowMap.put("recon_account", reconAccount);
		rowMap.put("payment_term_for_accounting", paymentTerm);
		rowMap.put("chk_double_inv", chkDoubleInv);
	}
	
	public void validate(Errors errors) {
		if (groupKey == null || "".equals(groupKey)) {
			errors.rejectValue("accounting.groupKey", "error.required");
		}
	}
}
