package cn.sscs.vendormanagement.vendor;

import java.util.Map;

import org.springframework.validation.Errors;

/**
 * 
 * @author kazzy
 *
 */
public class TradeCondition {
	
	private String paymentTerm = "8";
	
	private String leadTime = null;
	
	private String minimumLot = null;
	
	private int deliveryType = 0;

	private String otherValue = null;// delivery_types_other_value
	/**
	 * 
	 */
	TradeCondition() {}
	
	/**
	 * 
	 * @param rowMap
	 */
	TradeCondition(Map<String, Object> rowMap) {
		this.paymentTerm = (String) rowMap.get("payment_term");
		this.leadTime = (String) rowMap.get("lead_time");
		this.minimumLot = (String) rowMap.get("minimum_lot");
		this.deliveryType = (Integer)rowMap.get("delivery_types");
		this.otherValue = (String)rowMap.get("delivery_types_other_value");
	}
	
	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm() {
		return paymentTerm;
	}

	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 * @return the leadTime
	 */
	public String getLeadTime() {
		return leadTime;
	}

	/**
	 * @param leadTime the leadTime to set
	 */
	public void setLeadTime(String leadTime) {
		this.leadTime = leadTime;
	}

	/**
	 * @return the minimumLot
	 */
	public String getMinimumLot() {
		return minimumLot;
	}

	/**
	 * @param minimumLot the minimumLot to set
	 */
	public void setMinimumLot(String minimumLot) {
		this.minimumLot = minimumLot;
	}

	/**
	 * @return the deliveryType
	 */
	public int getDeliveryType() {
		return deliveryType;
	}

	/**
	 * @param deliveryType the deliveryType to set
	 */
	public void setDeliveryType(int deliveryType) {
		this.deliveryType = deliveryType;
	}
	
	/**
	 * 
	 * @param rowMap
	 */
	void toMap(Map<String, Object> rowMap) {
		rowMap.put("payment_term", paymentTerm);
		rowMap.put("lead_time", leadTime);
		rowMap.put("minimum_lot", minimumLot);
		rowMap.put("delivery_types", deliveryType);
		rowMap.put("delivery_types_other_value", otherValue);
	}
	
	public void validate(Errors errors) {
		
		if ("".equals(paymentTerm)) {
			errors.rejectValue("tradeCondition.paymentTerm", "error.required");
		}
		
//		if ("".equals(leadTime)) {
//			errors.rejectValue("tradeCondition.leadTime", "error.required");
//		}
//		
//		if ("".equals(minimumLot)) {
//			errors.rejectValue("tradeCondition.minimumLot", "error.required");
//		}
//		
//		if ("".equals(deliveryType)) {
//			errors.rejectValue("tradeCondition.deliveryType", "error.required");
//		}
		
	}

	public String getOtherValue() {
		return otherValue;
	}

	public void setOtherValue(String otherValue) {
		this.otherValue = otherValue;
	}
}
