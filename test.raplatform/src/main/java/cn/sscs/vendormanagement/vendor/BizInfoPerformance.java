package cn.sscs.vendormanagement.vendor;

import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author kazzy
 *
 */
public class BizInfoPerformance {

	private long bizInfoId = -1;
	
	private String year = null;
	
	private String salesAmount = null;
	
	private String profit = null;
	
	private String profitability = null;

	public BizInfoPerformance() {}
	
	/**
	 * 
	 * @param bizInfoIs
	 * @param year
	 * @param salesAmount
	 * @param profit
	 * @param profitability
	 */
	BizInfoPerformance(long bizInfoId, String year, String salesAmount, String profit, String profitability) {
		if ("".equals(year)) {
			year = null;
		}
		if ("".equals(salesAmount)) {
			salesAmount = null;
		}
		if ("".equals(profit)) {
			profit = null;
		}
		if ("".equals(profitability)) {
			profitability = null;
		}

		this.bizInfoId = bizInfoId;
		this.year = year;
		this.salesAmount = salesAmount;
		this.profit = profit;
		this.profitability = profitability;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getBizInfoId() {
		return bizInfoId;
	}
	
	/**
	 * 
	 * @param bizInfoId
	 */
	public void setBizInfoId(long bizInfoId) {
		this.bizInfoId = bizInfoId;
	}
	
	/**
	 * @return the year
	 */
	public String getYear() {
		return year;
	}

	/**
	 * @param year the year to set
	 */
	public void setYear(String year) {
		this.year = year;
	}

	/**
	 * @return the salesAmount
	 */
	public String getSalesAmount() {
		return salesAmount;
	}

	/**
	 * @param salesAmount the salesAmount to set
	 */
	public void setSalesAmount(String salesAmount) {
		this.salesAmount = salesAmount;
	}

	/**
	 * @return the profit
	 */
	public String getProfit() {
		return profit;
	}

	/**
	 * @param profit the profit to set
	 */
	public void setProfit(String profit) {
		this.profit = profit;
	}

	/**
	 * @return the profitability
	 */
	public String getProfitability() {
		return profitability;
	}

	/**
	 * @param profitability the profitability to set
	 */
	public void setProfitability(String profitability) {
		this.profitability = profitability;
	}

	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("year", year);
		map.put("sales_amount", salesAmount);
		map.put("profit", profit);
		map.put("profitability", profitability);
		
		return map;
	}

	/**
	 * 
	 * @return
	 */
	boolean hasContents() {
		return year != null || salesAmount != null || profit != null || profitability != null;
	}
	
}
