package cn.sscs.vendormanagement.vendor;

import java.util.HashMap;
import java.util.Map;

import org.springframework.validation.BindingResult;

public class BankAccount {

	public static final int STATUS_ALIVE = 0;
	
	public static final int STATUS_DELETED = 10;
	
	private long bankAccountId = -1;
	
	private String vendorSubcode = null;
	
	private boolean hasRequiredErrorOnVendorSubcode = false;
	
	private String provinceAndRegion = null;
	
	private boolean hasRequiredErrorOnProvinceAndRegion = false;
	
	private String swiftCode = null;
	
	private boolean hasRequiredErrorOnSwiftCode = false;
	
	private String name = null;
	
	private boolean hasRequiredErrorOnName = false;
	
	private boolean hasNoExistErrorOnName = false;
	
	private String account = null;
	
	private boolean hasRequiredErrorOnAccount = false;
	
	private int city = -1;
	
	private String currency = null;
	
	private int type = -1;
	
	private String institutionNo = null;
	
	private boolean hasRequiredErrorOnInstitutionNo = false;
	
	private boolean hasNoExistErrorOnInstitutionNo = false;
	
	private String jointCode = null;
	
	private boolean hasRequiredErrorOnJointCode = false;
	
	private boolean hasNoExistErrorOnJointCode = false;
	
	private String cnaps = null;

	private boolean hasRequiredErrorOnCnaps = false;
	
	private boolean hasNoExistErrorOnCnaps = false;
	
	private int status = STATUS_ALIVE;
	
	public BankAccount() {}
	
	/**
	 * 
	 * @param bankAccountId
	 * @param vendorSubcode
	 * @param provinceAndRegion
	 * @param swiftCode
	 * @param name
	 * @param account
	 * @param city
	 * @param currency
	 * @param type
	 * @param institutionNo
	 * @param jointCode
	 * @param cnaps
	 * @param status
	 */
	BankAccount(long bankAccountId, 
			String vendorSubcode,
			String provinceAndRegion,
			String swiftCode,
			String name,
			String account,
			int city,
			String currency,
			int type,
			String institutionNo,
			String jointCode,
			String cnaps,
			int status) 
	{
		this.bankAccountId = bankAccountId;
		this.vendorSubcode = vendorSubcode;
		this.provinceAndRegion = provinceAndRegion;
		this.swiftCode = swiftCode;
		this.name = name;
		this.account = account;
		this.city = city;
		this.currency = currency;
		this.type = type;
		this.institutionNo = institutionNo;
		this.jointCode = jointCode;
		this.cnaps = cnaps;
		this.status = status;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getBankAccountId() {
		return bankAccountId;
	}
	
	/**
	 * 
	 * @param bankAccountId
	 */
	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	/**
	 * @return the vendorSubcode
	 */
	public String getVendorSubcode() {
		return vendorSubcode;
	}
	
	public boolean getHasRequiredErrorOnVendorSubcode() {
		return hasRequiredErrorOnVendorSubcode;
	}

	/**
	 * @param vendorSubcode the vendorSubcode to set
	 */
	public void setVendorSubcode(String vendorSubcode) {
		this.vendorSubcode = vendorSubcode;
	}

	/**
	 * @return the provinceAndRegion
	 */
	public String getProvinceAndRegion() {
		return provinceAndRegion;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnProvinceAndRegion() {
		return hasRequiredErrorOnProvinceAndRegion;
	}
	
	/**
	 * @param provinceAndRegion the provinceAndRegion to set
	 */
	public void setProvinceAndRegion(String provinceAndRegion) {
		this.provinceAndRegion = provinceAndRegion;
	}

	/**
	 * @return the swiftCode
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnSwiftCode() {
		return hasRequiredErrorOnSwiftCode;
	}
	
	/**
	 * @param swiftCode the swiftCode to set
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnName() {
		return hasRequiredErrorOnName;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getHasNoExistErrorOnName() {
		return hasNoExistErrorOnName;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the account
	 */
	public String getAccount() {
		return account;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnAccount() {
		return hasRequiredErrorOnAccount;
	}
	
	/**
	 * @param account the account to set
	 */
	public void setAccount(String account) {
		this.account = account;
	}

	/**
	 * @return the city
	 */
	public int getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(int city) {
		this.city = city;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the type
	 */
	public int getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(int type) {
		this.type = type;
	}

	/**
	 * @return the institutionNo
	 */
	public String getInstitutionNo() {
		return institutionNo;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnInstitutionNo() {
		return hasRequiredErrorOnInstitutionNo;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getHasNoExistErrorOnInstitutionNo() {
		return hasNoExistErrorOnInstitutionNo;
	}
	
	/**
	 * @param institutionNo the institutionNo to set
	 */
	public void setInstitutionNo(String institutionNo) {
		this.institutionNo = institutionNo;
	}

	/**
	 * @return the jointCode
	 */
	public String getJointCode() {
		return jointCode;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnJointCode() {
		return hasRequiredErrorOnJointCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getHasNoExistErrorOnJointCode() {
		return hasNoExistErrorOnJointCode;
	}
	
	/**
	 * @param jointCode the jointCode to set
	 */
	public void setJointCode(String jointCode) {
		this.jointCode = jointCode;
	}

	/**
	 * @return the cnaps
	 */
	public String getCnaps() {
		return cnaps;
	}

	/**
	 * 
	 * @return
	 */
	public boolean getHasRequiredErrorOnCnaps() {
		return hasRequiredErrorOnCnaps;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean getHasNoExistErrorOnCnaps() {
		return hasNoExistErrorOnCnaps;
	}
	
	/**
	 * @param cnaps the cnaps to set
	 */
	public void setCnaps(String cnaps) {
		this.cnaps = cnaps;
	}

	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		// 供应商子编号
		map.put("vendor_sub_code", vendorSubcode);
		// 银行国家及地区
		map.put("province_and_region", provinceAndRegion);
		// 银行代码Swift Code
		map.put("swift_code", swiftCode);
		// 银行名称
		map.put("name", name);
		// 银行账号
		map.put("account", account);
		// 银行开户地
		map.put("city", city);
		// 币种
		map.put("currency", currency);
		// 银行类别
		map.put("type", type);
		// 机构号
		map.put("institution_no", institutionNo);
		// 联行号
		map.put("joint_code", jointCode);
		// CNAPS
		map.put("cnaps", cnaps);
		
		map.put("status", status);
		
		return map;
	}
	
	
	/**
	 * 
	 * @Title:       validate 
	 * @Description: 银行信息验证方法
	 * @param        @param result
	 * @param        @param vendorService
	 * @param        @return     
	 * @return       boolean     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 5, 2012
	 */
	boolean validate(BindingResult result, IVendorService vendorService) {
		
		boolean retval = false;
		// RMB
		if("RMB".equals(currency)){
			if (vendorSubcode == null || "".equals(vendorSubcode)) {
				hasRequiredErrorOnVendorSubcode = true;
				retval |= true;
			}
			if (provinceAndRegion == null || "".equals(provinceAndRegion)) {
				hasRequiredErrorOnProvinceAndRegion = true;
				retval |= true;
			}
			if (name == null || "".equals(name)) {
				hasRequiredErrorOnName = true;
				retval |= true;
			}
			
			if (account == null || "".equals(account)) {
				hasRequiredErrorOnAccount = true;
				retval |= true;
			}
			// 银行类别 中行
			if(type == 0){
				if (institutionNo == null || "".equals(institutionNo)) {
					hasRequiredErrorOnInstitutionNo = true;
					retval |= true;
				}
				
				if (jointCode == null || "".equals(jointCode)) {
					hasRequiredErrorOnJointCode = true;
					retval |= true;
				}
				
				if(!hasRequiredErrorOnName && !hasRequiredErrorOnInstitutionNo && !hasRequiredErrorOnJointCode){
					if (!vendorService.isValidJCodeINo(name, jointCode, institutionNo)) {
						hasNoExistErrorOnName = true;
						retval |= true;
					}
				}
			// 银行类别 非中行	
			}else if(type == 1){
				if (cnaps == null || "".equals(cnaps)) {
						hasRequiredErrorOnCnaps = true;
						retval |= true;
				} else {
					if (!vendorService.isValidCnaps(name, cnaps)) {
						hasNoExistErrorOnName = true;
						retval |= true;
					}
				}
			}
		}else{
			if (vendorSubcode == null || "".equals(vendorSubcode)) {
				hasRequiredErrorOnVendorSubcode = true;
				retval |= true;
			}
			
			if (swiftCode == null || "".equals(swiftCode)) {
				hasRequiredErrorOnSwiftCode = true;
				retval |= true;
			}
			
			if (provinceAndRegion == null || "".equals(provinceAndRegion)) {
				hasRequiredErrorOnProvinceAndRegion = true;
				retval |= true;
			}
			if (name == null || "".equals(name)) {
				hasRequiredErrorOnName = true;
				retval |= true;
			}
			
			if (account == null || "".equals(account)) {
				hasRequiredErrorOnAccount = true;
				retval |= true;
			}
		}
		
//		boolean retval = false;
//		if (vendorSubcode == null || "".equals(vendorSubcode)) {
//			hasRequiredErrorOnVendorSubcode = true;
//			retval |= true;
//		}
//		if (provinceAndRegion == null || "".equals(provinceAndRegion)) {
//			hasRequiredErrorOnProvinceAndRegion = true;
//			retval |= true;
//		}
//		
//		boolean isBankOfChina = false;
//		if (name == null || "".equals(name)) {
//			hasRequiredErrorOnName = true;
//			retval |= true;
//		} else {
//			if (!vendorService.isValidBankName(name)) {
//				hasNoExistErrorOnName = true;
//				retval |= true;
//			} else {
//				isBankOfChina = vendorService.isBankOfChina(name);
//
//				if (institutionNo == null || "".equals(institutionNo)) {
//					if ("RMB".equals(currency) && isBankOfChina) {
//						hasRequiredErrorOnInstitutionNo = true;
//						retval |= true;
//					}
//				} else {
//					if (!vendorService.isValidInstitutionNo(name, institutionNo)) {
//						hasNoExistErrorOnInstitutionNo = true;
//						retval |= true;
//					}
//				}
//					
//				if (jointCode == null || "".equals(jointCode)) {
//					if ("RMB".equals(currency) && isBankOfChina) {
//						hasRequiredErrorOnJointCode = true;
//						retval |= true;
//					}
//				} else {
//					if (!vendorService.isValidJointCode(name, jointCode)) {
//						hasNoExistErrorOnJointCode = true;
//						retval |= true;
//					}
//				}
//				
//				
//				if (cnaps == null || "".equals(cnaps)) {
//					if ("RMB".equals(currency) && !isBankOfChina) {
//						hasRequiredErrorOnCnaps = true;
//						retval |= true;
//					}
//				} else {
//					if (!vendorService.isValidCnaps(name, cnaps)) {
//						hasNoExistErrorOnCnaps = true;
//						retval |= true;
//					}
//				}
//
//			}
//		}
//		
//		if (account == null || "".equals(account)) {
//			hasRequiredErrorOnAccount = true;
//			retval |= true;
//		}
//		
//		if (!"RMB".equals(currency)) {
//			if (swiftCode == null || "".equals(swiftCode)) {
//				hasRequiredErrorOnSwiftCode = true;
//				retval |= true;
//			}
//		} 
		
		return retval;
	}
}
