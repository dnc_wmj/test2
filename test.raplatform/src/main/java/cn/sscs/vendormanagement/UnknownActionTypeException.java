package cn.sscs.vendormanagement;

public class UnknownActionTypeException extends Exception{
    /**
     * 
     */
    private static final long serialVersionUID = -2529083248351772735L;
    
    public UnknownActionTypeException () {}
    
    public UnknownActionTypeException (String message) {
        super(message);
    }
}
