package cn.sscs.vendormanagement.test;

import java.util.List;

public class ContactForm {
	
	private String[] test;
	private List<Contact> contacts;
	
	public List<Contact> getContacts() {
		return contacts;
	}
	
	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	public String[] getTest() {
		return test;
	}
	
	public void setTest(String[] test) {
		this.test = test;
	}
}
