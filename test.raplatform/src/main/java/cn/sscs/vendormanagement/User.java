package cn.sscs.vendormanagement;

import java.util.List;

import cn.sscs.vendormanagement.certificate.Certificate;

public class User {
    private final static String SSCS = "SSCS";
    private final static String SSGE = "SSGE";
    private final static String SSV = "SSV";
    private final static String SEW = "SEW";
    private final static String SDPW = "SDPW";
    private final static String SPDH = "SPDH";
    private final static String SEH = "SEH";
    
	private String id = null;
	
	private String name = null;
	
	private String password = null;
	
	private String email = null;
	
	private int division = 0;
	
//	private String authority = null;
	
//	private boolean valid = true;
	
	private String userCompany = null;
	
	private Certificate certificate = null;
    
    private List<UserInfo> userInfos = null;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

//	public void setValid(boolean valid) {
//		this.valid = valid;
//	}
//
//	public boolean isValid() {
//		return valid;
//	}
//
//	public void setAuthority(String authority) {
//		this.authority = authority;
//	}
//
//	public String getAuthority() {
//		return authority;
//	}

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany;
    }
    
    public int getUserInfosLength(){
        return userInfos.size();
    }

    public int getDivision() {
        return division;
    }

    public void setDivision(int division) {
        this.division = division;
    }

    public Certificate getCertificate() {
        return certificate;
    }

    public void setCertificate(Certificate certificate) {
        this.certificate = certificate;
    }

    public List<UserInfo> getUserInfos() {
        return userInfos;
    }

    public void setUserInfos(List<UserInfo> userInfos) {
        this.userInfos = userInfos;
    }
    
    public String getAllDivisionInfo(){
        String allDivisionInfo = "";
        
        for(UserInfo userInfo : userInfos){
            if(!"".equals(allDivisionInfo)
                    && !"".equals(userInfo.getDivisionInfo())){
                allDivisionInfo += ", ";
            }
            allDivisionInfo += userInfo.getDivisionInfo();
        }
        return allDivisionInfo;
    }
    
    public String getMainEmail(){
        for(UserInfo userInfo : userInfos){
            if(userCompany.equals(userInfo.getUserCompany()))
                return userInfo.getEmail();
        }
        return "";
    }
    
    public String getMainValid(){
        for(UserInfo userInfo : userInfos){
            if(userCompany.equals(userInfo.getUserCompany()))
                return userInfo.isValid() ? "true" : "false";
        }
        return "false";
    }
    
    public String getUserCompanyName() {
        if("1".equals(userCompany)){
            return SSCS;
        }
        if("2".equals(userCompany)){
            return SSGE;
        }
        if("3".equals(userCompany)){
            return SSV;
        }
        if("4".equals(userCompany)){
            return SEW;
        }
        if("5".equals(userCompany)){
            return SDPW;
        }
        if("6".equals(userCompany)){
            return SPDH;
        }
        if("7".equals(userCompany)){
            return SEH;
        }
        return userCompany;
    }
}
