package cn.sscs.vendormanagement.data;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.DuplicateKeyException;

import cn.sscs.vendormanagement.User;

public class ExcelParser {

	/**
	 * 
	 * @author kazzy
	 * 
	 */
	public static enum Type {
		String, Number, Date, Boolean, Formulra
	}

	private Map<String, CellConfiguration> configurationMap = new HashMap<String, CellConfiguration>();

	private List<CellConfiguration> configurations = new ArrayList<CellConfiguration>();

	/**
	 * 
	 * @param cellName
	 * @param columnName
	 * @param type
	 * @param isKey
	 */
	public void configure(String cellName, String columnName, Type type,
			boolean isKey) {
		configure(cellName, columnName, type, -1, isKey);
	}

	/**
	 * 
	 * @param cellName
	 * @param columnName
	 * @param converter
	 * @param isKey
	 */
	public void configure(String cellName, String columnName,
			DataConverter converter, boolean isKey) {
		configurationMap.put(cellName, new CellConfiguration(cellName,
				columnName, null, -1, isKey, converter, true));
	}

	/**
	 * 
	 * @param cellName
	 * @param columnName
	 * @param type
	 * @param length
	 * @param isKey
	 */
	public void configure(String cellName, String columnName, Type type,
			int length, boolean isKey) {
		configurationMap.put(cellName, new CellConfiguration(cellName,
				columnName, type, length, isKey, null, true));
	}

	/**
	 * 
	 * @param cellName
	 * @param columnName
	 * @param type
	 * @param length
	 * @param isKey
	 * @param isNotNull
	 */
	public void configure(String cellName, String columnName, Type type,
			int length, boolean isKey, boolean isNotNull) {
		configurationMap.put(cellName, new CellConfiguration(cellName,
				columnName, type, length, isKey, null, isNotNull));
	}

	/**
	 * 
	 * @param upload 
	 * @Title:       parse 
	 * @Description: 解析 excel 函数
	 * @param        @param excelInputStream
	 * @param        @param handler
	 * @param        @param headerRow     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
	public List<Map<String, Object>> parse(Upload upload, InputStream excelInputStream,
			ExcelParserRowHandler handler,
			int headerRow) {
	    List<Map<String, Object>> mailinfos = new ArrayList<Map<String,Object>>();
	    
		HSSFWorkbook workBook = null;
		try {
			workBook = new HSSFWorkbook(excelInputStream);
		} catch (IOException e) {
			throw new ExcelParserException("Invalid work book",
					"error.invalid.workbook", e);
		}

		HSSFSheet sheet = workBook.getSheetAt(0);
		if (sheet == null) {
			throw new ExcelParserException("No first sheet",
					"error.no.first.sheet");
		}

		if (sheet.getLastRowNum() < headerRow) {
			throw new ExcelParserException("No header row",
					"error.no.header.row");
		}

		HSSFRow header = sheet.getRow(headerRow);

		for (int i = 0; i < header.getLastCellNum(); i++) {
			String cellname = header.getCell(i).getRichStringCellValue()
					.getString().trim();
			if (cellname == null) {
				continue;
			}

			cellname = cellname.trim();

			CellConfiguration cellconfig = configurationMap.remove(cellname);
			if (cellconfig == null) {
				continue;
			}

			cellconfig.setNum(i);
			configurations.add(cellconfig);
		}

		if (configurationMap.size() > 0) {
			Iterator<String> names = configurationMap.keySet().iterator();
			String message = "{" + names.next();
			while (names.hasNext()) {
				message += ", " + names.next();
			}
			message += "}";
			throw new ExcelParserException("Undefined header: " + message,
					"error.no.configured.header");
		}
		Map<String, Object> rowMap;
		
		// validate user company
		for (int i = headerRow + 1; i <= sheet.getLastRowNum(); i++) {
            HSSFRow row = sheet.getRow(i);
            rowMap = new HashMap<String, Object>();
            for (CellConfiguration cellconf : configurations) {
                try {
                    HSSFCell cell = row.getCell(cellconf.getNum());
    
                    Object value = getCellValue(cell, cellconf);
    
                    if (value == null) {
                        value = handler.handleNullCellValue(cell, cellconf, i);
                    }
                    if (value == null && cellconf.isNotNull()) {
                        throw new ExcelParserException("Invalid record at row "
                                + (i + 1) + ". cell name: "
                                + cellconf.getName()
                                + " is required, but empty.", "error.invalid.row");
                    } else if (value == null && !cellconf.isNotNull()) {
                        rowMap.put(cellconf.getColumnName(), value);
                        continue;
                    }
    
                    rowMap.put(cellconf.getColumnName(), value);
                } catch (ConvertException e) {
                    throw new ExcelParserException("Invalid record at row "
                            + (i + 1) + ": " + e.getMessage(), "error.invalid.row");                    
                } catch (ExcelParserException e) {
                    throw e;
                } catch (Throwable t) {
                    throw new ExcelParserException("Invalid record at row "
                            + (i + 1), "error.invalid.row", t);
                }
            }
            String usercompanyBySelected = upload == null ? "" : upload.getUserCompany();
            
            if(!handler.validationRow(i, rowMap, usercompanyBySelected)){
                throw new ExcelParserException(null, null);
            }
        }
		for (int i = headerRow + 1; i <= sheet.getLastRowNum(); i++) {
			HSSFRow row = sheet.getRow(i);
			rowMap = new HashMap<String, Object>();
			for (CellConfiguration cellconf : configurations) {
				try {
					HSSFCell cell = row.getCell(cellconf.getNum());
	
					Object value = getCellValue(cell, cellconf);
	
					if (value == null) {
						rowMap.put(cellconf.getColumnName(), value);
						continue;
					}
	
					rowMap.put(cellconf.getColumnName(), value);
				} catch (ConvertException e) {
					throw new ExcelParserException("Invalid record at row "
							+ (i + 1) + ": " + e.getMessage(), "error.invalid.row");					
				} catch (ExcelParserException e) {
					throw e;
				} catch (Throwable t) {
					throw new ExcelParserException("Invalid record at row "
							+ (i + 1), "error.invalid.row", t);
				}
			}
			try {
			    Map<String, Object> mailinfo = handler.handleRow(i, rowMap);
			    
			    if(mailinfo != null){
	                mailinfos.add(mailinfo);
	            }
			} catch (DuplicateKeyException e) {
				throw new ExcelParserException("Duplicated record at row "
						+ (i + 1) + ", " + rowMap, "error.duplicated.invoice");
			} catch (DataIntegrityViolationException e) {
				throw new ExcelParserException("Invalid record at row "
						+ (i + 1)  + getReadableMessage(e.getMessage(), ':') + " " + rowMap, "error.invalid.row");
			} catch (Throwable t) {
				throw new ExcelParserException("Invalid record at row "
						+ (i + 1), "error.invalid.row", t);
			}
			
		}
		return mailinfos;
	}

	private String getReadableMessage(String message, char separator) {
		int indexOfSeparator = message.indexOf(separator);
		if (indexOfSeparator > 0) {
			message = message.substring(indexOfSeparator);
		}
		
		return message;
	}
	
	/**
	 * 
	 * @param cell
	 * @return
	 * @throws ConvertException 
	 */
	private Object getCellValue(HSSFCell cell, CellConfiguration cellconf) throws ConvertException {
		
		if (cell == null) {
			return null;
		}
		Object value = null;
		
		switch (cell.getCellType()) {
		case HSSFCell.CELL_TYPE_STRING:
			value = cell.getRichStringCellValue().getString().trim();
			break;
		case HSSFCell.CELL_TYPE_NUMERIC:
			value = cell.getNumericCellValue();
			break;
		case HSSFCell.CELL_TYPE_BOOLEAN:
			value = cell.getBooleanCellValue();
			break;
		}

		if (cellconf.getType() == Type.Date) {
			try {
				value = cell.getDateCellValue();
			} catch (NumberFormatException e) {
				throw new ConvertException("Cell: " + cellconf.getName() + " should be a format Date, but it's not one.");
			}
		} else if (cellconf.getType() == Type.Formulra) {
			value = cell.getCellFormula();
		} else {
			value = convert(value, cellconf);
		}

		if (cellconf.getConverter() != null) {
			value = cellconf.getConverter().convert(value);
		}

		return value;
	}

	/**
	 * 
	 * @param value
	 * @param cellconf
	 * @return
	 */
	private Object convert(Object value, CellConfiguration cellconf) {
		if (cellconf.getType() == Type.String) {
			if (value instanceof Double) {
				value = String.valueOf(((Double) value).longValue());
			}
		}

		return value;
	}

	public void configure(String string, String string2, Type b) {
		
		configurationMap.put(string, new CellConfiguration(string,
				string2, b, 1, false, null, true));
		
	}
}
