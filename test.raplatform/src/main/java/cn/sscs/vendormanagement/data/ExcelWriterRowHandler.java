package cn.sscs.vendormanagement.data;

import org.apache.poi.hssf.usermodel.HSSFRow;

public interface ExcelWriterRowHandler<T> {

	void handleRow(int num, T row, HSSFRow hssfRow);
	
}
