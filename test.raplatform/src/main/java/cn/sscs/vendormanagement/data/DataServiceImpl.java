package cn.sscs.vendormanagement.data;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.security.InvalidKeyException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Security;
import java.security.SignatureException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.UUID;
import java.util.Vector;

import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.sql.DataSource;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.VelocityException;
import org.bouncycastle.asn1.ASN1InputStream;
import org.bouncycastle.asn1.DERBMPString;
import org.bouncycastle.asn1.DERConstructedSequence;
import org.bouncycastle.asn1.DERInputStream;
import org.bouncycastle.asn1.DERObjectIdentifier;
import org.bouncycastle.asn1.DERSequence;
import org.bouncycastle.asn1.pkcs.PKCSObjectIdentifiers;
import org.bouncycastle.asn1.x509.AuthorityKeyIdentifier;
import org.bouncycastle.asn1.x509.SubjectPublicKeyInfo;
import org.bouncycastle.asn1.x509.X509Extensions;
import org.bouncycastle.asn1.x509.X509Name;
import org.bouncycastle.jce.interfaces.PKCS12BagAttributeCarrier;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openssl.PEMReader;
import org.bouncycastle.openssl.PasswordFinder;
import org.bouncycastle.x509.X509V3CertificateGenerator;
import org.bouncycastle.x509.extension.SubjectKeyIdentifierStructure;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.simple.ParameterizedBeanPropertyRowMapper;
import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.velocity.VelocityEngineFactoryBean;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.springframework.web.multipart.MultipartFile;

import cn.sscs.vendormanagement.Authority;
import cn.sscs.vendormanagement.ExtendedJdbcUserDetailsManager;
import cn.sscs.vendormanagement.PasswordUtil;
import cn.sscs.vendormanagement.User;
import cn.sscs.vendormanagement.UserInfo;
import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.data.ExcelParser.Type;
import cn.sscs.vendormanagement.vendor.VendorSearchCriteria;

import com.sun.mail.smtp.SMTPMessage;
@Service("dataService")
@Transactional
public class DataServiceImpl implements IDataService {

	private SimpleJdbcTemplate jdbcTemplate = null;
	
	private SimpleJdbcInsert insertUser = null;
	
	private SimpleJdbcInsert insertAuthority = null;

	private SimpleJdbcInsert insertModificationHistory = null;
	
	private SimpleJdbcInsert insertInterfaceFileHistory = null;
	
//	private SimpleJdbcInsert insertUserCostcenter = null;
//	
//	private SimpleJdbcInsert insertUserCostcenterL1 = null;
	
	private SimpleJdbcInsert insertCertificate = null;
	
//	private SimpleJdbcInsert insertUserCompany = null;
    
	private SimpleJdbcInsert insertUserInfo = null;
	
	@Autowired
	private JavaMailSender mailSender = null;
	
	@Autowired
	ExtendedJdbcUserDetailsManager userService = null;

	
	@Autowired
	public void init(DataSource dataSource) {
		jdbcTemplate = new SimpleJdbcTemplate(dataSource);
		
		insertUser = new SimpleJdbcInsert(dataSource)
			.withTableName("user");

		insertAuthority = new SimpleJdbcInsert(dataSource)
			.withTableName("authorities");

		insertModificationHistory = new SimpleJdbcInsert(dataSource)
			.withTableName("modification_history");

		insertInterfaceFileHistory = new SimpleJdbcInsert(dataSource)
			.withTableName("interface_file_history")
			.usingGeneratedKeyColumns("id");
			
//		insertUserCostcenter = new SimpleJdbcInsert(dataSource)
//		    .withTableName("user_costcenter");
//		
//		insertUserCostcenterL1 = new SimpleJdbcInsert(dataSource)
//		    .withTableName("user_costcenter_l1");
		
		insertCertificate = new SimpleJdbcInsert(dataSource)
            .withTableName("certificates");
		
//		insertUserCompany = new SimpleJdbcInsert(dataSource)
//            .withTableName("user_company");
		
		insertUserInfo = new SimpleJdbcInsert(dataSource)
            .withTableName("user_info");
	}
		
	/**
	 * 
	 * @param id
	 * @return
	 */
	@SuppressWarnings("unused")
    private boolean existsUser(String id) {
		return jdbcTemplate.queryForInt("SELECT count(*) FROM user WHERE id=?", id) > 0;
	}
	
    @Override
    public void registerUsers(Upload upload) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException,
            CertificateException, KeyStoreException, SignatureException {
        if(upload.getFile() != null && !upload.getFile().isEmpty()){
			//oldUserUpload(upload.getFile());
		}
		/*上载用户*/
		if(upload.getUserfile() != null && !upload.getUserfile().isEmpty()){
			newUserUpload(upload);
		}
		/*上载成本中心*/
		if(upload.getCostcenterfile() != null && !upload.getCostcenterfile().isEmpty()){
			costcenterUpload(upload);
		}
	}
	
	/**
	 * 
	 * @Title:       costcenterUpload 
	 * @Description:  ( 解析上载成本中心信息excel  ) 
	 * @param        @param costcenterfile     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	private void costcenterUpload(final Upload upload) {
	    MultipartFile costcenterfile = upload.getCostcenterfile();
		ExcelParser parser = new ExcelParser();
		parser.configure("成本中心", "division", Type.String, 50, false);
		parser.configure("公司编号", "usercompany", Type.String, 50, false);
		parser.configure("中文名称", "zh_name", Type.String, 100, false);
		parser.configure("英文名称", "en_name", Type.String, 100, false);
		parser.configure("是否有效（成本中心）", "c_valid", Type.String, 100, false);
		parser.configure("审批级别", "level", Type.String, 100, false);
		parser.configure("员工编号", "id", Type.String, 100, false);
		parser.configure("域账号", "g_id", Type.String, 100, false);
		parser.configure("中文名", "name", Type.String, 100, false);
		parser.configure("英文名", "e_name", Type.String, 100, false);
		parser.configure("邮件地址", "email", Type.String, 100, false, false);
		parser.configure("是否有效（人员）", "u_valid", Type.String, 100, false);
		
		//delete all authority
        jdbcTemplate.update("DELETE FROM authorities WHERE (authority = 'ROLE_L1' OR authority = 'ROLE_L2') AND usercompany = " + upload.getUserCompany());		
		try {
			parser.parse(upload, costcenterfile.getInputStream(), 
					new ExcelParserRowHandler(){

						/**
						 * (非 Javadoc) 
						 * <p>Title: handleRow</p> 
						 * <p>Description: 解析成本中心excel将值插入DB</p> 
						 * @param num
						 * @param row 
						 * @see cn.sscs.vendormanagement.data.ExcelParserRowHandler#handleRow(int, java.util.Map)
						 */
						@Override
						public Map<String, Object> handleRow(int num, Map<String, Object> row) {
							
							if("Y".equals(row.get("u_valid").toString().toUpperCase())){//authority valid
								int level = Integer.parseInt(String.valueOf(row.get("level")));
								switch(level){
//									case 1: uploadL1(row);break;
//									default: uploadL2(row);
									case 1: uploadAuthority(row, "ROLE_L1");break;
									default: uploadAuthority(row, "ROLE_L2");
								}
							}
							
							return null;
						}
						private void uploadAuthority(Map<String, Object> row, String authority) {
							String id = String.valueOf(row.get("id")).toUpperCase();
							String userCompany = row.get("usercompany").toString().toUpperCase();
							row.put("username", id);
							row.put("authority", authority);
							
							if("SSCS".equals(userCompany)){
                                userCompany = "1";
                            }else if("SSGE".equals(userCompany)){
                                userCompany = "2";
                            }else if("SSV".equals(userCompany)){
                                userCompany = "3";
                            }else if("SEW".equals(userCompany)){
                                userCompany = "4";
                            }else if("SDPW".equals(userCompany)){
                                userCompany = "5";
                            }else if("SPDH".equals(userCompany)){
                                userCompany = "6";
                            }else if("SEH".equals(userCompany)){
                                userCompany = "7";
                            }
							row.put("usercompany", userCompany);
//							if(!hasAuthorityRole(id, authority)){
//							    insertAuthority.execute(row);
//							}
							/* 
							 * 成本中心有效 && it's not repeat record
							 */
							if("Y".equals(row.get("c_valid").toString().toUpperCase())){//&& divisionNotInUser(row)
								
//								if(authority.equals("ROLE_L1")){
//									insertUserCostcenterL1.execute(row);
//								}else{
//									//L2
//									insertUserCostcenter.execute(row);
//								} 
							    // 已经存在的不操作
							    if(!existsAuthority(row)){
							        insertAuthority.execute(row);
							    }
							    
							}
						}
//						private boolean hasAuthorityRole(String id, String authority) {
//							return jdbcTemplate.queryForInt("SELECT count(*) FROM authorities WHERE username=? AND authority = ?", id, authority) > 0;
//						}
						/**
						 * 
						 * @Title:       uploadL2 
						 * @Description:  ( 分角色插入authorities表信息 ： ROLE_L2  ) 
						 * @param        @param row     
						 * @return       void     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 7, 2012
						 * @Deprecated   Role l1 support multiple division, look at @method uploadAuthority
						 */
//						@SuppressWarnings("unused")
//                        @Deprecated
//						private void uploadL2(Map<String, Object> row) {
//							String id = String.valueOf(row.get("id")).toUpperCase();
//							row.put("username", id);
//							row.put("authority", "ROLE_L2");
//							if(!hasL2Role(id)){
//								insertAuthority.execute(row);
//							}
//							/* 
//							 * 成本中心有效并且成本中心不是user表中的，就往usercostcenter表中插入
//							 */
//							if("Y".equals(row.get("c_valid").toString().toUpperCase()) && divisionNotInUser(row)){
////								insertUserCostcenter.execute(row);
//							}
//						}

						/**
						 * 
						 * @Title:       divisionNotInUser 
						 * @Description:  ( 判断组织机构是否相符合  ) 
						 * @param        @param row
						 * @param        @return     
						 * @return       boolean     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 7, 2012
						 */
//						private boolean divisionNotInUser(Map<String, Object> row) {
//							
//							ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>() {
//								@Override
//								public String mapRow(ResultSet rs, int rowNum) throws SQLException {
//									return rs.getString("division");
//								}
//							};
//							@SuppressWarnings("deprecation")
//							List<String> divisionInUser = jdbcTemplate.query(
//									"SELECT division FROM user WHERE id =?", 
//									mapper, 
//									row.get("id").toString());
////							Object _divisionInUser = jdbcTemplate.queryForObject(
////									"SELECT division FROM user WHERE id =?", Object.class, row.get("id").toString());
////							String divisionInUser = jdbcTemplate.queryForObject(
////									"SELECT division FROM user WHERE id =?",
////									String.class, row.get("id").toString());
//									
//							return divisionInUser == null || divisionInUser.size() == 0 
//										? false 
//										: !divisionInUser.get(0).equals(row.get("division").toString());
//							//return _divisionInUser == null ? true : _divisionInUser.toString().equals(row.get("division").toString());
//						}

						private boolean existsAuthority(Map<String, Object> row) {
						    return jdbcTemplate.queryForInt(
						            "SELECT count(*) FROM authorities WHERE " +
						                    "username=:username AND " +
						                    "authority =:authority AND " +
						                    "division =:division AND " +
						                    "usercompany =:usercompany", row) > 0;
                        }
                        /**
						 * 
						 * @Title:       uploadL1 
						 * @Description:  ( 分角色插入authorities表信息 ： ROLE_L1) 
						 * @param        @param row     
						 * @return       void     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 7, 2012
						 * @Deprecated   Role l1 support multiple division, look at @method uploadAuthority
						 */
						@SuppressWarnings("unused")
                        @Deprecated
						private void uploadL1(Map<String, Object> row) {
							String id = String.valueOf(row.get("id")).toUpperCase();
							row.put("username", id);
							row.put("authority", "ROLE_L1");
							
							if(!l1RoleAleadyexist(id)){
								insertAuthority.execute(row);
							}
						}

						/**
						 * 
						 * @Title:       l1RoleAleadyexist 
						 * @Description:  (  判断是否重复  ) 
						 * @param        @param id
						 * @param        @return     
						 * @return       boolean     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 7, 2012
						 */
						private boolean l1RoleAleadyexist(String id) {
							return jdbcTemplate.queryForInt("SELECT count(*) FROM authorities WHERE username=? AND authority = 'ROLE_L1'", id) > 0;
						}

						@Override
						public Object handleNullCellValue(HSSFCell cell,
								CellConfiguration cellconf, int row) {
							return null;
						}
                        @Override
                        public boolean validationRow(int num,
                                Map<String, Object> row, String clientUsercompany) {
                            String userCompany = row.get("usercompany").toString().toUpperCase();
                            if("SSCS".equals(userCompany)){
                                userCompany = "1";
                            }else if("SSGE".equals(userCompany)){
                                userCompany = "2";
                            }else if("SSV".equals(userCompany)){
                                userCompany = "3";
                            }else if("SEW".equals(userCompany)){
                                userCompany = "4";
                            }else if("SDPW".equals(userCompany)){
                                userCompany = "5";
                            }else if("SPDH".equals(userCompany)){
                                userCompany = "6";
                            }else if("SEH".equals(userCompany)){
                                userCompany = "7";
                            }
                            // upload.getUserCompany() 为页面选择的所属公司
                            if(!clientUsercompany.equals(userCompany)){
                                throw new ExcelParserException("It is different super company from your selected to the upload flie at row "
                                        + (num + 1), "error.invalid.row", null);
                            }
                            
                            return true;
                        }}, 
					1);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @Title:       newUserUpload 
	 * @Description:  ( 解析上载员工信息excel ) 
	 * @param        @param file     
	 * @return       void     
	 * @throws SignatureException 
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
	private void newUserUpload(final Upload upload) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, KeyStoreException, SignatureException {
	    MultipartFile file = upload.getUserfile();
	    ExcelParser parser = new ExcelParser();
		parser.configure("员工编号", "id", Type.String, 10, true);
		parser.configure("域账号", "g_id", Type.String, 50, false, false);
		parser.configure("中文名", "name", Type.String, 50, false);
		parser.configure("英文名", "e_name", Type.String, 50, false);
		parser.configure("成本中心", "division", Type.String, 100, false);
		parser.configure("邮件地址", "email", Type.String, 100, false);
		parser.configure("是否有效", "valid", Type.Boolean);
		parser.configure("公司编号", "usercompany", Type.String, 100, false);
		final SystemSetting setting = getSystemSetting();
		try {
		    List<Map<String, Object>> mailinfos = 
		            parser.parse(upload, file.getInputStream(), new ExcelParserRowHandler(){
						/**
						 * (非 Javadoc) 
						 * <p>Title: handleRow</p> 
						 * <p>Description: 解析用户excel函数插入DB </p> 
						 * @param num
						 * @param row 
						 * @throws IOException 
						 * @throws SignatureException 
						 * @throws KeyStoreException 
						 * @throws CertificateException 
						 * @throws NoSuchProviderException 
						 * @throws NoSuchAlgorithmException 
						 * @throws InvalidKeyException 
						 * @see cn.sscs.vendormanagement.data.ExcelParserRowHandler#handleRow(int, java.util.Map)
						 */
        				@Override
                        public Map<String, Object> handleRow(int num, Map<String, Object> row){
							// check email's format
        				    String email = row.get("email").toString();
							if(!Utils.isMail(email)){
								throw new DataIntegrityViolationException(". Email format error. ");
							}
							
							String id = String.valueOf(row.get("id")).toUpperCase();
							String password = PasswordUtil.getInstance().newPassword();
							String division = ((String) row.get("division")).replace("，", ",");
							String userCompany = row.get("usercompany").toString().toUpperCase();
							if("SSCS".equals(userCompany)){
							    userCompany = "1";
							}else if("SSGE".equals(userCompany)){
							    userCompany = "2";
							}else if("SSV".equals(userCompany)){
                                userCompany = "3";
                            }else if("SEW".equals(userCompany)){
                                userCompany = "4";
                            }else if("SDPW".equals(userCompany)){
                                userCompany = "5";
                            }else if("SPDH".equals(userCompany)){
                                userCompany = "6";
                            }else if("SEH".equals(userCompany)){
                                userCompany = "7";
                            }
							
							boolean valid = new Boolean(row.get("valid").toString());
							row.put("username", id);
							row.put("id", id);
							row.put("password", password);
                            row.put("authority", "ROLE_USER");
                            row.put("last_change_passwd_time", new Date());
                            row.put("usercompany", userCompany);
                            row.put("valid", valid ? 1 : 0 );
							if(division.indexOf(",") != -1){
								row.put("division", Integer.parseInt(division.split(",")[0].trim()));
							}
							
							// usercompany
							if(userExistCompany(id, userCompany)){
	                            // 
	                            jdbcTemplate.update(
	                                      "UPDATE user_info SET " +
	                                        " email=:email, valid=:valid " +
	                                        "WHERE id=:id and usercompany=:usercompany", 
	                                        row);
                            }else{
                                insertUserInfo.execute(row);
                            }
							
							/* 查找已存在用户 替换其信息以excel 
							 * 若原存在用户无效
							 * 则发送Email
							 * */
							List<User> userList = getExistsUser(id);
							if (userList.size() > 0) 
							{
								jdbcTemplate.update(
										"UPDATE user SET " +
										"  name=:name, " +
										"  division=:division, " +
										"  usercompany=:usercompany " +
										"WHERE id=:id", 
										row);
								insertAuthority(row, division);
								// if the situation is that invalid in DB and valid in file, send mail for notice password.
								if(valid && isInValidUser(userList.get(0).getId())){
								    row.put("password", userList.get(0).getPassword());
									return row;
									//file.delete();
								}else{
								    return null;
								}
							} else {
								insertUser.execute(row);
								insertAuthority(row, division);
                                if(valid){
									return row;
								}else{
								    return null;
								}
							}
						}

        				private void insertAuthority(final Map<String, Object> row,
                                String division) {
        				    if(division.indexOf(",") != -1){
                                for(String d : division.split(",")){
                                    String username = row.get("username").toString();
                                    String authority = "ROLE_USER";
                                    String usercompany = row.get("usercompany").toString();
                                    Map<String, Object> authorityRow = new HashMap<String, Object>();
                                    authorityRow.put("division", d);
                                    authorityRow.put("username", username);
                                    authorityRow.put("authority", authority);
                                    authorityRow.put("usercompany", usercompany);
                                    insertAuthority(authorityRow);
                                }
                            }else{
                                insertAuthority(row);
                            }
                        }
        				
        				private void insertAuthority(final Map<String, Object> row) {
                            jdbcTemplate.update(
                                    "DELETE FROM authorities " +
                                    "WHERE " +
                                    " username=:username AND " +
                                    " authority =:authority AND " +
                                    " division=:division AND " +
                                    " usercompany=:usercompany", row);
                            insertAuthority.execute(row);
                        }
        				
                        private boolean isInValidUser(String id) {
        				    
                            return jdbcTemplate.queryForInt(
                                    "select count(*) from user_info where id = ? and valid = 1", 
                                    id) == 0;
                        }
                        
        				@SuppressWarnings("deprecation")
                        private boolean isUserCompanyChanged(String id, String userCompany) {
                            ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>() {
                                @Override
                                public String mapRow(ResultSet rs, int rowNum)
                                        throws SQLException {
                                    return rs.getString("usercompany");
                                }
                            };
        				    
                            List<String> userCompanyList = jdbcTemplate.query(
                                    "select "+
                                        "u.usercompany "+
                                    "from  "+
                                        "workflow_rule w, user u "+
                                    "where  "+
                                        "u.id = ? and "+
                                        "(w.applicant=? or w.l1=? or w.l2 =? or w.ceo =? or w.hd =? or w.fm =? or w.dm =?) and "+ 
                                        "(w.status = 1 OR w.status = 3)", 
                                        mapper, id, id, id, id, id, id, id, id);
                            for(String userCompanyInWorkflow : userCompanyList){
                                if(!userCompany.equals(userCompanyInWorkflow)){
                                    return true;
                                }
                            }
                            return false;
                        }

                        /**
        				 * 
        				 * @param id
        				 * @param upperCase
        				 * @return
        				 */
                        private boolean userExistCompany(String id,
                                String userCompany) {
                            return jdbcTemplate.queryForInt(
                                    "SELECT COUNT(*) FROM user_info WHERE id =? and usercompany=?", 
                                    id, userCompany) > 0;
                        }

                        /**
						 * 
						 * @Title:       getExistsUser 
						 * @Description:  ( 查找已存在用户  ) 
						 * @param        @param id
						 * @param        @return     
						 * @return       List<User>     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 6, 2012
						 */
						@SuppressWarnings("deprecation")
						private List<User> getExistsUser(String id) {
							List<User> users = jdbcTemplate.query(
									"SELECT * FROM user WHERE id =?",
									ParameterizedBeanPropertyRowMapper.newInstance(User.class), 
									id);
//							for(VndMgmntUser user : users){
//							    
//							}
							return users;
						}

						/**
						 * 
						 * @Title:       applyingWorkflowExistsUser 
						 * @Description:  ( 判断用户是否有申请信息  ) 
						 * @param        @param id
						 * @param        @return     
						 * @return       boolean     
						 * @throws 
						 * User:         dnc
						 * DataTime:     Jul 6, 2012
						 */
						private boolean workflowExistsUser(String id) {
							int count = jdbcTemplate.queryForInt(
									"select " +
										"count(*) " +
									"from " +
										"workflow_rule " +
									"where " +
										"(applicant=? or l1=? or l2 =? or ceo =? or hd =? or fm =? or dm =?) and " +
										"(status = 1 OR status = 3)", 
										id, id, id, id, id, id, id);
							if(count > 0){
								return false;
							}else{
								return true;
							}
						}

						/**
						 * (非 Javadoc) 
						 * <p>Title: handleNullCellValue</p> 
						 * <p>Description: </p> 
						 * @param cell
						 * @param cellconf
						 * @param row
						 * @return 
						 * @see cn.sscs.vendormanagement.data.ExcelParserRowHandler#handleNullCellValue(org.apache.poi.hssf.usermodel.HSSFCell, cn.sscs.vendormanagement.data.CellConfiguration, int)
						 */
						@Override
						public Object handleNullCellValue(HSSFCell cell,
								CellConfiguration cellconf, int row) {
							return null;
						}

                        @Override
                        public boolean validationRow(int num,
                                Map<String, Object> row, String clientUsercompany) {
                            
                            String id = row.get("id").toString();
                            boolean userInWorkflow = isInWorkflow(id);
                           
                            // 1 在申请中的用户不能设置为无效
                            boolean valid = new Boolean(row.get("valid").toString());
                            if (!valid) {
                                if (!workflowExistsUser(id)) {
                                    throw new DataIntegrityViolationException(". User in the workflow. User can't be invalid. Row "
                                            + (num + 1));
                                }
                            }
                            // 2 在申请中的用户不能修改所属公司
                            // IsDifferentUserCompanyFromWorkflowToExcel
                            String userCompany = row.get("usercompany").toString().toUpperCase();
                            if("SSCS".equals(userCompany)){
                                userCompany = "1";
                            }else if("SSGE".equals(userCompany)){
                                userCompany = "2";
                            }else if("SSV".equals(userCompany)){
                                userCompany = "3";
                            }else if("SEW".equals(userCompany)){
                                userCompany = "4";
                            }else if("SDPW".equals(userCompany)){
                                userCompany = "5";
                            }else if("SPDH".equals(userCompany)){
                                userCompany = "6";
                            }else if("SEH".equals(userCompany)){
                                userCompany = "7";
                            }
                            
                            if (isUserCompanyChanged(id, userCompany)) {
                                throw new DataIntegrityViolationException(". User in the workflow. User company can't change. Row "
                                        + (num + 1));
                            }
                            
                            // upload.getUserCompany() 为页面选择的所属公司
                            if(!clientUsercompany.equals(userCompany)){
                                throw new ExcelParserException("It is different super company from your selected to the upload flie at row "
                                        + (num + 1), "error.invalid.row", null);
                            }
                            
                            // Division shouldn't be changed when user in workflow.
                            boolean divisionCanChange = false;
                            if(userInWorkflow){
                                User dbUser = getUser(id);
                                String division = ((String) row.get("division")).replace("，", ",");
                                if(division.indexOf(",") != -1){
                                    for(String dString : division.split(",")){
                                        if(dbUser.getDivision() == Integer.parseInt(dString.trim())){
                                            divisionCanChange = true;
                                            break;
                                        }
                                    }
                                }else{
                                    if(dbUser.getDivision() == Integer.parseInt(division)){
                                        divisionCanChange = true;
                                    }
                                }
                            }
                            
                            if(divisionCanChange){
                                throw new DataIntegrityViolationException(". User in the workflow. User division can't change. Row "
                                        + (num + 1));
                            }
                            return true;
                        }

                        private boolean isInWorkflow(String id) {
                            return jdbcTemplate.queryForInt(
                                    "select "+
                                        "count(*) "+
                                    "from  "+
                                        "workflow_rule w "+
                                    "where  "+
                                        "(w.applicant=? or w.l1=? or w.l2 =? or w.ceo =? or w.hd =? or w.fm =? or w.dm =?) and "+ 
                                        "(w.status = 1 OR w.status = 3)", 
                                         id, id, id, id, id, id, id) > 0;
                            
                        }
                    },
					0);// the method will parse excel from first line.
		    
		    // sendmail
		    for (Map<String, Object> row : mailinfos){
		        String id = row.get("id").toString();
		        String password = row.get("password").toString();
		        String userCompany = row.get("usercompany").toString().toUpperCase();
                if("1".equals(userCompany)){
                    userCompany = "SSCS";
                }else if("2".equals(userCompany)){
                    userCompany = "SSGE";
                }else if("3".equals(userCompany)){
                    userCompany = "SSV";
                }else if("4".equals(userCompany)){
                    userCompany = "SEW";
                }else if("5".equals(userCompany)){
                    userCompany = "SDPW";
                }else if("6".equals(userCompany)){
                    userCompany = "SPDH";
                }else if("7".equals(userCompany)){
                    userCompany = "SEH";
                }
                row.put("usercompany", userCompany);
		        // create PKCS12
                ByteArrayOutputStream pkcs12OutputStream = createPKCS12(row);
                // create file
                File attmentFile = new File( id+".p12" ); 
                DataOutputStream to = new DataOutputStream(new FileOutputStream(attmentFile)); 
                pkcs12OutputStream.writeTo(to); 
                
		        Map<String, String> templateModel = new HashMap<String, String>();
                templateModel.put("id", id);
                templateModel.put("password", password);
                sendMail(id, 
                        setting.getPasswordNotificationMailSubject(), 
                        setting.getPasswordNotificationMailTemplate(), 
                        templateModel, null);
                sendMail(id, 
                        setting.getCertattMailSubject(), 
                        setting.getCertattMailTemplate(), 
                        templateModel, attmentFile);
                pkcs12OutputStream.close();
                to.close();
                attmentFile.delete();
		    }
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	/**
	 * @Title:       oldUserUpload 
	 * @Description:  ( 无调用  ) 
	 * @param        @param file     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
//	public void oldUserUpload(MultipartFile file){
//		ExcelParser parser = new ExcelParser();
//		parser.configure("ID", "id", Type.String, 10, true);
//		parser.configure("NAME", "name", Type.String, 50, false);
//		parser.configure("EMAIL", "email", Type.String, 100, false);
//		parser.configure("DIVISION", "division", Type.String, 100, false);
//		parser.configure("AUTHORITY", "authority", Type.String, 20, false);
//		parser.configure("VALID", "valid", Type.Boolean);
//
//		final SystemSetting setting = getSystemSetting();
//
//		try {
//			parser.parse(file.getInputStream(), 
//					new ExcelParserRowHandler(){
//
//						@Override
//						public void handleRow(int num, Map<String, Object> row) {
//							// 在申请中的用户不能设置为false
//							boolean valid = new Boolean(row.get("valid").toString());
//							if(!valid){
//								if(!applyingWorkflowExistsUser(Integer.parseInt(row.get("id").toString()))){
//									throw new DataIntegrityViolationException(". User in the workflow. ");
//								}
//							}
//							row.put("valid", valid ? 1 : 0 );
//							String password = PasswordUtil.getInstance().newPassword();
//							String id = (String) row.get("id");
//							String division = ((String) row.get("division")).replace("，", ",");
//							if(division.indexOf(",") != -1){
//								row.put("division", Integer.parseInt(division.split(",")[0].trim()));
//							}
//							row.put("password", password);
//							row.put("username", id);
//							String authoritys = row.get("authority").toString().replace("，", ",");
//							if (existsUser(id)) 
//							{
//								jdbcTemplate.update(
//										"UPDATE user SET " +
//										"  name=:name," +
//										"  email=:email," +
//										"  division=:division, " +
//										"  valid=:valid " +
//										"WHERE id=:id", 
//										row);
//								if(authoritys.indexOf(",") == -1){
//									jdbcTemplate.update("DELETE FROM authorities WHERE username=:username", row);
//									insertAuthority.execute(row);
//								}else{
//									jdbcTemplate.update("DELETE FROM authorities WHERE username=:username", row);
//									Map<String, Object> authorityRow = new HashMap<String, Object>();
//									for(String authority : authoritys.split(",")){
//										authorityRow.put("authority", authority.trim());
//										authorityRow.put("username", id);
//										insertAuthority.execute(authorityRow);
//										authorityRow.clear();
//									}
//								}
//								
//								
//								if(isL2(row) && division.indexOf(",") != -1){
//									mutiCostcenterL2(row, division);
//								}
//							} else {
//								insertUser.execute(row);
//								if(authoritys.indexOf(",") == -1){
//									jdbcTemplate.update("DELETE FROM authorities WHERE username=:username", row);
//									insertAuthority.execute(row);
//								}else{
//									jdbcTemplate.update("DELETE FROM authorities WHERE username=:username", row);
//									Map<String, Object> authorityRow = new HashMap<String, Object>();
//									for(String authority : authoritys.split(",")){
//										authorityRow.put("authority", authority.trim());
//										authorityRow.put("username", id);
//										insertAuthority.execute(authorityRow);
//										authorityRow.clear();
//									}
//								}
//								
//								
//								if(isL2(row) && division.indexOf(",") != -1){
//									mutiCostcenterL2(row, division);
//								}
//								Map<String, String> templateModel = new HashMap<String, String>();
//								templateModel.put("id", id);
//								templateModel.put("password", password);
//								sendMail(id, 
//										setting.getPasswordNotificationMailSubject(), 
//										setting.getPasswordNotificationMailTemplate(), 
//										templateModel, null);
//							
//							}
//						}
//
//						private boolean applyingWorkflowExistsUser(int id) {
//							int count = jdbcTemplate.queryForInt(
//									"select " +
//										"count(*) " +
//									"from " +
//										"workflow_rule " +
//									"where " +
//										"(l1=? or l2 =? or ceo =? or hd =? or fm =? or dm =?) and " +
//										"(status = 1 OR status = 3)", 
//										id,id,id,id,id,id);
//							if(count > 0){
//								return false;
//							}else{
//								return true;
//							}
//							
//						}
//
//						private void mutiCostcenterL2(Map<String, Object> row, String division) {
//							jdbcTemplate.update("DELETE FROM user_costcenter WHERE id=:id", row);
//							String[] costcenters = division.split(",");
//							Map<String, Object> costcenterRow = new HashMap<String, Object>();
//							for(String costcenter : costcenters){
//								costcenterRow.clear();
//								costcenterRow.put("id", (String) row.get("id"));
//								costcenterRow.put("division", costcenter);
//								insertUserCostcenter.execute(costcenterRow);
//							}
//						}
//
//						private boolean isL2(Map<String, Object> row) {
//							String authorities = String.valueOf(row.get("authority")).replace("，", ",");
//							if(authorities.indexOf(",") == -1){
//								return Utils.ROLE_L2.equals(authorities);
//							}else{
//								boolean isL2Flag = false;
//								for(String authority : authorities.split(",")){
//									if(Utils.ROLE_L2.equals(authority)){
//										isL2Flag = true;
//										break;
//									}
//								}
//								return isL2Flag;
//							}
//							
//						}
//
//						@Override
//						public Object handleNullCellValue(HSSFCell cell,
//								CellConfiguration cellconf, int row) {
//							return null;
//						}}, 
//					0);
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
//	private boolean hasL2Role(String id) {
//		return jdbcTemplate.queryForInt("SELECT count(*) FROM authorities WHERE username=? AND authority = 'ROLE_L2'", id) > 0;
//	}
	
//	private boolean hasL1Role(String id) {
//		return jdbcTemplate.queryForInt("SELECT count(*) FROM authorities WHERE username=? AND authority = 'ROLE_L1'", id) > 0;
//	}
	
	@Override
	public int searchUserCount(UserSearchCriteria criteria) {
		return jdbcTemplate.queryForInt(
				"SELECT count(*) FROM user u WHERE (u.name LIKE :ambiguousName OR u.id LIKE :ambiguousName)", 
				new BeanPropertySqlParameterSource(criteria));
	}

	@Override
	@SuppressWarnings("deprecation")
	public List<User> searchUsers(UserSearchCriteria criteria) {
		List<User> users = jdbcTemplate.query(
				"SELECT * " +
				"FROM user u "+
				"WHERE  " +
				"(u.name LIKE :ambiguousName OR u.id LIKE :ambiguousName) ORDER BY id LIMIT :recordFrom, 20",
				ParameterizedBeanPropertyRowMapper.newInstance(User.class), 
				new BeanPropertySqlParameterSource(criteria));
		
		for(User user : users){
		    List<UserInfo> userInfos = jdbcTemplate.query(
                    "SELECT * " +
                    "FROM user_info ui "+
                    "WHERE " +
                    "ui.id = ?",
                    ParameterizedBeanPropertyRowMapper.newInstance(UserInfo.class),
                    user.getId());
		    for (UserInfo userInfo : userInfos) {
		        List<Authority> authorities = jdbcTemplate.query(
	                    "SELECT * " +
	                    "FROM authorities a "+
	                    "WHERE " +
    	                    "a.username = ? AND " +
    	                    "a.usercompany = ?",
	                    ParameterizedBeanPropertyRowMapper.newInstance(Authority.class),
	                    userInfo.getId(), userInfo.getUserCompany());
		        userInfo.setAuthorityList(authorities);
            }
		    user.setUserInfos(userInfos);
//			
//			if(hasL2Role(user.getId())){
//				List<String> divisions = getL2Divisions(user.getId());
//				if(divisions != null && divisions.size() !=0 ){
//					String divisionsToExcle = "";
//					boolean costcenterHasL2Division = false;
//					for(String division : divisions){
//						divisionsToExcle += division+",";
//						if(division.equals(user.getDivision())){
//							costcenterHasL2Division = true;
//						}
//					}
//					if(costcenterHasL2Division){
//						divisionsToExcle = divisionsToExcle.substring(0,divisionsToExcle.length() -1);
//					}else{
//						divisionsToExcle += user.getDivision();
//					}
//					user.setDivision(divisionsToExcle);
//				}
//			}else if(hasL1Role(user.getId())){//没处理员工下载的情况，
//				List<String> divisions = getL1Divisions(user.getId());
//				if(divisions != null && divisions.size() !=0 ){
//					String divisionsToExcle = "";
//					boolean costcenterHasL1Division = false;
//					for(String division : divisions){
//						divisionsToExcle += division+",";
//						if(division.equals(user.getDivision())){
//							costcenterHasL1Division = true;
//						}
//					}
//					if(costcenterHasL1Division){
//						divisionsToExcle = divisionsToExcle.substring(0,divisionsToExcle.length() -1);
//					}else{
//						divisionsToExcle += user.getDivision();
//					}
//					user.setDivision(divisionsToExcle);
//				}
//			}
		}
		return users;
	}
	
	@Override
	public void changePassword(String username, String newPassword) {
		jdbcTemplate.update(
				"UPDATE user SET password=?,last_change_passwd_time = NOW() WHERE id=?", 
				newPassword, username);
	}

	@Override
	public void storeModificationHistory(ModificationHistory modificationHistory) {
		modificationHistory.setRegisteredDate(new Date());
		insertModificationHistory.execute(modificationHistory.toMap());
	}
	/**
	 *  (非 Javadoc) 
	 * <p>Title: getModificationHistories</p> 
	 * <p>Description: 已处理变更数据 列表</p> 
	 * @return 
	 * @see cn.sscs.vendormanagement.data.IDataService#getModificationHistories()
	 */
	@Override
	@SuppressWarnings("deprecation")
	public List<ModificationHistory> getModificationHistories() {
		return jdbcTemplate.query(
				"SELECT m.id as id, " +
				"  v.vendor_code as vendorCode," +
				"  m.vendor_id as vendorId, " +
				"  m.bank_account_id as bankAccountId, " +
				"  m.classification, " +
				"  m.registered_date as registeredDate " +
				"FROM modification_history m, vendor_basic_info v " +
				"WHERE v.id = m.vendor_id " +
				// 只查找所属公司是sscs的变更履历
				"AND v.super_company = 1 " +
				"ORDER BY m.id", 
				ParameterizedBeanPropertyRowMapper.newInstance(ModificationHistory.class));
	}
	
	@Override
	@SuppressWarnings("deprecation")
	public List<ModificationHistory> getBankAccountDeletedModificationHistories(long vendorId) {
		return jdbcTemplate.query(
				"SELECT id, " +
				"  vendor_id as vendorId, " +
				"  bank_account_id as bankAccountId, " +
				"  classification, " +
				"  registered_date as registeredDate " +
				"FROM modification_history " +
				"WHERE vendor_id = ? AND classification = 40 ", 
				ParameterizedBeanPropertyRowMapper.newInstance(ModificationHistory.class),
				vendorId);
	}
	@Override
	public void clearModificationHistory(long unneededMaxId) {
		jdbcTemplate.update("DELETE FROM modification_history WHERE id <= ?", unneededMaxId);
	}
	
	@Override
	public long storeInterfaceFile(byte[] data, Date createdDate) {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("data", data);
		map.put("created_date", createdDate);
		
		return insertInterfaceFileHistory.executeAndReturnKey(map).longValue();
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<InterfaceFile> getInterfaceFiles() {
		return jdbcTemplate.query(
				"SELECT id, created_date as createdDate FROM interface_file_history ORDER BY created_date DESC", 
				ParameterizedBeanPropertyRowMapper.newInstance(InterfaceFile.class));
	}
	
	@Override
	public byte[] getInterfaceFileData(long id) {
		return jdbcTemplate.queryForObject(
				"SELECT data FROM interface_file_history WHERE id = ?",
				byte[].class, 
				id);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public SystemSetting getSystemSetting() {
		return jdbcTemplate.queryForObject(
				"SELECT re_eval_month as reEvalMonth, " +
				" approval_request_mail_subject as approvalRequestMailSubject, " +
				" freeze_day as freezeDay, " +
				" approval_request_mail_template as approvalRequestMailTemplate, " +
				" denied_mail_subject as deniedMailSubject, " +
				" denied_mail_template as deniedMailTemplate, " +
				" cancel_mail_subject as cancelMailSubject, " +
				" cancel_mail_template as cancelMailTemplate, " +
				" reject_mail_subject as rejectMailSubject, " +
				" reject_mail_template as rejectMailTemplate, " +
				" freeze_applicant_valid_subject as freezeValidMailSubject," +
				" freeze_applicant_invalid_subject as freezeInvalidMailSubject," +
				" freeze_applicant_valid_template as freezeValidMailTemplate," +
				" freeze_applicant_invalid_template as freezeInvalidMailTemplate," +
				" application_finished_mail_subject as applicationFinishedMailSubject, " +
				" application_finished_mail_template as applicationFinishedMailTemplate, " +
				" re_evaluation_request_mail_subject as reEvaluationRequestMailSubject, " +
				" re_evaluation_request_mail_template as reEvaluationRequestMailTemplate, " +
				" password_notification_mail_subject as passwordNotificationMailSubject, " +
				" password_notification_mail_template as passwordNotificationMailTemplate, " +
				"  certatt_subject as certattMailSubject, " +
                "  certatt_template as certattMailTemplate " +
				"FROM system_setting " +
				"WHERE id = 1", 
				ParameterizedBeanPropertyRowMapper.newInstance(SystemSetting.class));
	}
	
	@Override
	public void updateSystemSetting(SystemSetting systemSetting) {
		jdbcTemplate.update(
				"UPDATE system_setting SET " +
				"  re_eval_month = :reEvalMonth," +
				"  freeze_day = :freezeDay," +
				"  approval_request_mail_subject = :approvalRequestMailSubject," +
				"  approval_request_mail_template = :approvalRequestMailTemplate," +
				"  denied_mail_subject = :deniedMailSubject," +
				"  denied_mail_template = :deniedMailTemplate," +
				"  reject_mail_subject = :rejectMailSubject," +
				"  reject_mail_template = :rejectMailTemplate," +
				"  cancel_mail_subject = :cancelMailSubject," +
				"  cancel_mail_template = :cancelMailTemplate," +
				"  freeze_applicant_valid_subject = :freezeValidMailSubject," +
				"  freeze_applicant_invalid_subject = :freezeInvalidMailSubject," +
				"  application_finished_mail_subject = :applicationFinishedMailSubject," +
				"  application_finished_mail_template = :applicationFinishedMailTemplate," +
				"  re_evaluation_request_mail_subject = :reEvaluationRequestMailSubject," +
				"  re_evaluation_request_mail_template = :reEvaluationRequestMailTemplate, " +
				"  password_notification_mail_subject = :passwordNotificationMailSubject," +
				"  password_notification_mail_template = :passwordNotificationMailTemplate, " +
				"  freeze_applicant_valid_template = :freezeValidMailTemplate, " +
				"  freeze_applicant_invalid_template = :freezeInvalidMailTemplate, " +
				"  certatt_subject = :certattMailSubject, " +
                "  certatt_template = :certattMailTemplate " +
				"WHERE id = 1", 
				new BeanPropertySqlParameterSource(systemSetting));
	}
	
	
	/**
	 * 
	 * @param subject
	 * @param address
	 * @param template
	 * @param templateModel 
	 * @throws MessagingException 
	 */
	@Override
	public void sendMail(String receiverUserId,
			final String subject,  
			final String template, 
			final Map<String, String> templateModel,
			final File file) {
		
		//final VndMgmntUser user = (VndMgmntUser) userService.loadUserByUsername(receiverUserId);
	    final User user = getUser(receiverUserId);
	    // init Velocity
		Properties props = System.getProperties();
		props.put("resource.loader", "class");
		props.put("class.resource.loader.class",
				"org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
		// unused
		props.put("output.encoding", "UTF-8");
		props.put("input.encoding", "UTF-8");
		VelocityEngineFactoryBean v = new VelocityEngineFactoryBean();
		v.setVelocityProperties(props);
		String content = merge(template, templateModel);
		Map<String, Object> toHtml = new HashMap<String, Object>();
		toHtml.put("HTML", content);
		try {
			MimeMessage msg = mailSender.createMimeMessage();
			// to repair language garbled
			MimeMessageHelper helper = new MimeMessageHelper(msg, true,
					"UTF-8");
			msg.setHeader("Content-Transfer-Encoding", "7bit");

			InternetAddress from = new InternetAddress("support@sscs-apps.com", "SSCS Vendor Selection Notification");
			helper.setFrom(from);
			helper.setSubject(subject);
			helper.setTo(user.getEmail());
			VelocityEngine velocityEngine = v.createVelocityEngine();
			final String emailText = VelocityEngineUtils.mergeTemplateIntoString(
						velocityEngine, "mailTemplate.vm", toHtml);
			helper.setText(emailText, true);
			// add attachment
			if(file != null){
			    helper.addAttachment(file.getName(), file);
			}
			SMTPMessage smtpMessage = new SMTPMessage(msg);
			smtpMessage.setEnvelopeFrom("wangmingjian.compass@gmail.com");
			/*
			 * 扩展为支持添加附件的
			 */
			mailSender.send(smtpMessage);
		} catch (VelocityException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}
	
	private static final RowMapper<InterfaceData> interfaceDataRowMapper =
		new RowMapper<InterfaceData>(){

		@Override
		public InterfaceData mapRow(ResultSet result, int row)
				throws SQLException {
			InterfaceData data = new InterfaceData();
			data.setVendorId(result.getLong("id"));
			data.setVendorCode(result.getString("vendor_code"));
			data.setVendorSubCode(result.getString("vendor_sub_code"));
			
			String name = result.getString("vendor_chinese_name");
			if (name == null || "".equals(name.trim())) {
				name = result.getString("vendor_english_name");
			}
			data.setName(name);
			
			String chineseName = result.getString("vendor_chinese_name");
			if (chineseName == null || "".equals(chineseName)) {
				chineseName = result.getString("vendor_english_name");
			}
			data.setChineseName(chineseName);
			
			String englishName = result.getString("vendor_english_name");
			if (englishName == null || "".equals(englishName)) {
				englishName = result.getString("vendor_chinese_name");
			}
			data.setEnglishName(englishName);
			
			data.setFax(result.getString("fax"));
			data.setBankName(result.getString("name"));
			data.setBankAccount(result.getString("account"));
			data.setBankCityType(result.getInt("city"));
			data.setVendorType(result.getInt("vendor_type"));
			data.setBusinessType(result.getInt("business_type"));
			data.setVendorGroup(result.getString("vendor_group"));
			data.setTel(result.getString("tel"));
			data.setAddress(result.getString("address"));
			data.setContactPerson(result.getString("person"));
			data.setSwiftCode(result.getString("swift_code"));
			data.setPaymentTerm(result.getInt("payment_term_for_accounting"));
			data.setBankProvinceAndRegion(result.getString("province_and_region"));
			//增加业务类型  、 所属公司
			data.setSuperCompany(result.getInt("super_company"));
			
			//data.setBankAddress(result.getString("province_and_region"));
			
			data.setCurrency(result.getString("currency"));
			data.setBankType(result.getInt("type"));
			data.setInstitutionNo(result.getString("institution_no"));
			data.setJointCode(result.getString("joint_code"));
			data.setCnaps(result.getString("cnaps"));
			data.setBankAccountId(result.getLong("bankAccountId"));
			//省市和地区
			//城市
			//组代码
			//申请人
			data.setRegion(result.getString("region_name"));
			data.setCity(result.getString("vendorCity"));
			data.setGroupKey(result.getString("group_key"));
			data.setApplient_name(result.getString("applicant_name"));
			
			data.setBankStatus(result.getInt("bankStatus"));
			data.setFreeze(result.getString("freeze"));
			//是否有效
			//data.setEffective(result.getInt("vendorStatus"));
			// 是否有效
			int effective = 1;
			if(data.getFreeze() != null && "1".equals(data.getFreeze())){
				effective = 0;
			}
			data.setEffective(effective);
			data.setVendorAccountType(result.getString("vendor_account_type"));
			return data;
		}
	};
	private static final RowMapper<InterfaceData> interfaceDataRowMapperByDownload =
			new RowMapper<InterfaceData>(){

			@Override
			public InterfaceData mapRow(ResultSet result, int row)
					throws SQLException {
				InterfaceData data = new InterfaceData();
				data.setVendorId(result.getLong("id"));
				data.setVendorCode(result.getString("vendor_code"));
				data.setVendorSubCode(result.getString("vendor_sub_code"));
				
				String name = result.getString("vendor_chinese_name");
				if (name == null || "".equals(name.trim())) {
					name = result.getString("vendor_english_name");
				}
				data.setName(name);
				
				String chineseName = result.getString("vendor_chinese_name");
				if (chineseName == null || "".equals(chineseName)) {
					chineseName = result.getString("vendor_english_name");
				}
				data.setChineseName(chineseName);
				String englishName = result.getString("vendor_english_name");
				if (englishName == null || "".equals(englishName)) {
					englishName = result.getString("vendor_chinese_name");
				}
				data.setEnglishName(englishName);
				data.setFax(result.getString("fax"));
				data.setBankName(result.getString("name"));
				data.setBankAccount(result.getString("account"));
				data.setBankCityType(result.getInt("city"));
				data.setVendorType(result.getInt("vendor_type"));
				data.setBusinessType(result.getInt("business_type"));
				data.setVendorGroup(result.getString("vendor_group"));
				data.setTel(result.getString("tel"));
				data.setAddress(result.getString("address"));
				data.setContactPerson(result.getString("person"));
				data.setSwiftCode(result.getString("swift_code"));
				data.setPaymentTerm(result.getInt("payment_term_for_accounting"));
				data.setEffective(result.getInt("vendorStatus"));
				data.setBankProvinceAndRegion(result.getString("province_and_region"));
				//增加业务类型  、 所属公司
				data.setSuperCompany(result.getInt("super_company"));
				
				//data.setBankAddress(result.getString("province_and_region"));
				
				data.setCurrency(result.getString("currency"));
				data.setBankType(result.getInt("type"));
				data.setInstitutionNo(result.getString("institution_no"));
				data.setJointCode(result.getString("joint_code"));
				data.setCnaps(result.getString("cnaps"));
				data.setBankAccountId(result.getLong("bankAccountId"));
				//省市和地区
				//城市
				//组代码
				//申请人
				data.setRegion(result.getString("region_name"));
				data.setCity(result.getString("vendorCity"));
				data.setGroupKey(result.getString("group_key"));
				data.setApplient_name(result.getString("applicant_name"));
				
				data.setBankStatus(result.getInt("bankStatus"));
				data.setFreeze(result.getString("freeze"));
				data.setFreeze(result.getString("freeze"));
				data.setVendorAccountType(result.getString("vendor_account_type"));
				return data;
			}
		};
	@Override
	public List<InterfaceData> findInterfaceDataByVendorId(long id) {
		return jdbcTemplate.query(
				"SELECT "+
					"v.id, "+
					"v.vendor_code, "+ 
					"b.vendor_sub_code, "+
					"v.vendor_chinese_name, "+
					"v.vendor_english_name, "+
					"v.fax, "+
					"b.name, "+
					"b.account, "+
					"b.city, "+
					"v.vendor_type, "+
					"v.business_type, "+
					"v.vendor_group, "+
					"v.tel, "+
					"v.address, "+ 
					"v.person, "+
					"b.swift_code, "+
					"v.payment_term_for_accounting, "+
					"v.status as vendorStatus, "+
					"b.province_and_region, "+
					"b.currency, "+
					"b.type, "+
					"b.institution_no, "+
					"b.joint_code, "+
					"b.cnaps, "+
					"b.id as bankAccountId, "+
					"r.name as region_name, "+
					"v.city as vendorCity, "+
					"v.group_key, "+
					"v.applicant_name, "+
					"v.freeze, "+
					"v.vendor_account_type, " +
					"b.status as bankStatus, " +
					"v.super_company "+ 
				"FROM "+
					"vendor_basic_info v LEFT JOIN regions r ON v.country = r.country AND v.region = r.region_code, bank_account b "+ 
				"WHERE "+
					"b.vendor_id = v.id AND "+
					"b.vendor_id = ? ",
				interfaceDataRowMapper, 
				id);
	}
	
	
	/**
	 * 
	 * @param template
	 * @param templateModel
	 * @return
	 */
	private String merge(String template, Map<String, String> templateModel) {
		for (Map.Entry<String, String> entry : templateModel.entrySet()) {
			template = template.replace("${" + entry.getKey() + "}", entry.getValue());
		}
		
		return template;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<String> findWorkflowUsers(String l1, String l2, String ceo, String hd, String fm, String dm, String applicant) {
		String sql ="";
		if(ceo != null){
			sql = " select  name  from user where id = '"+applicant+"'"+
				  " union all " +
				  " select  name  from user where id = '"+l1+"'"+
				  " union all " +
				  " select  name  from user where id = '"+l2+"'"+
				  " union all " +
				  " select  name  from user where id = '"+ceo+"'"+
				  " union all " +
				  " select  name  from user where id = '"+hd+"'"+
				  " union all " +
				  " select  name  from user where id = '"+fm+"'"+
				  " union all " +
				  " select  name  from user where id = '"+dm+"' ;";
		}else{
			sql = " select  name  from user where id = '"+applicant+"'"+
				  " union all " +
				  " select  name  from user where id = '"+l1+"'"+
				  " union all " +
				  " select  name  from user where id = '"+l2+"'"+
				  " union all " +
				  " select  name  from user where id = '"+hd+"'"+
				  " union all " +
				  " select  name  from user where id = '"+fm+"'"+
				  " union all " +
				  " select  name  from user where id = '"+dm+"' ;";
		}
		ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			    String name = rs.getString("name");
			    return name;
			}
		};
		if(l1 == null || "".equals(l1)){
			List<String> name = jdbcTemplate.query(sql, mapper);
			name.add(1, "");
			return name;
		}else{
			return jdbcTemplate.query(sql, mapper);
		}
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<User> getAllUserInfo() {
		List<User> users = jdbcTemplate.query(
				"SELECT u.id as id, u.name as name, ui.email, u.division "+ //, if(ui.valid=1,'true','false') as valid
				"FROM user u, user_info ui " +
				"where u.id = ui.id AND " +
				"u.usercompany = ui.usercompany",
				ParameterizedBeanPropertyRowMapper.newInstance(User.class));
		
//		ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>(){
//			@Override
//			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
//			    String name = rs.getString("authority");
//			    return name;
//			}
//		};
//		for(VndMgmntUser user : users){
//			List<String> authoritys = jdbcTemplate.query(
//					"SELECT authority from authorities where username='"+user.getId()+"'", mapper);
//			
//			String authoritysStr = "";
//			int i = 0;
//			for(String authority : authoritys){
//				if(i>0) authoritysStr += ",";
//				authoritysStr += authority;
//				i++;
//			}
//			user.setAuthority(authoritysStr);
//		}
		return users;
	}

	@SuppressWarnings("deprecation")
	@Override
	public List<String> getL2Divisions(String id) {
		String sql = 
			"SELECT division "+ 
				"FROM user_costcenter u "+
				"WHERE u.id = '"+id+"'";
		ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			    String name = rs.getString("division");
			    return name;
			}
		};
		return jdbcTemplate.query(sql, mapper);
	}
	
	@SuppressWarnings("deprecation")
	@Override
	public List<String> getL1Divisions(String id) {
		String sql = 
			"SELECT division "+ 
				"FROM user_costcenter_l1 u "+
				"WHERE u.id = '"+id+"'";
		ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>(){
			@Override
			public String mapRow(ResultSet rs, int rowNum) throws SQLException {
			    String name = rs.getString("division");
			    return name;
			}
		};
		return jdbcTemplate.query(sql, mapper);
	}
	/**
	 * (非 Javadoc) 
	 * <p>Title: findInterfaceData</p> 
	 * <p>Description: </p> 
	 * @param criteria
	 * @return 
	 * @see cn.sscs.vendormanagement.data.IDataService#findInterfaceData(int)
	 */
	@Override
	public List<InterfaceData> findInterfaceData(VendorSearchCriteria criteria) {
		String type = "";
		if(!"".equals(criteria.getText().trim())){
			if(VendorSearchCriteria.TYPE_CODE.equals(criteria.getType())){
				type = " v.vendor_code = '"+criteria.getText().trim()+"' AND ";
			}else if(VendorSearchCriteria.TYPE_NAME.equals(criteria.getType())){
				type = " (v.vendor_chinese_name like '%"+criteria.getText().trim()+"%' OR v.vendor_english_name like '%"+criteria.getText().trim()+"%') AND ";
			}else if(VendorSearchCriteria.TYPE_NUMBER.equals(criteria.getType())){
				type = " v.number = '"+criteria.getText().trim()+"' AND ";
			}
		}
		
		String category = "";
		if(VendorSearchCriteria.CATEGORY_ALL != criteria.getCategory()){
			category = " v.vendor_type  = "+criteria.getCategory()+" AND ";
		}
		
		String super_company = "";
		if(VendorSearchCriteria.COMPANYTYPE_ALL != criteria.getSuperCompanyType()){
			super_company = " v.super_company = " + criteria.getSuperCompanyType() + " AND ";
		}
		return jdbcTemplate.query(
				"SELECT "+
					"v.id, "+
					"v.vendor_code, "+ 
					"b.vendor_sub_code, "+
					"v.vendor_chinese_name, "+
					"v.vendor_english_name, "+
					"v.fax, "+
					"b.name, "+
					"b.account, "+
					"b.city, "+
					"v.vendor_type, "+
					"v.business_type, "+
					"v.vendor_group, "+
					"v.tel, "+
					"v.address, "+ 
					"v.person, "+
					"b.swift_code, "+
					"v.payment_term_for_accounting, "+
					"v.status as vendorStatus, "+
					"b.province_and_region, "+
					"b.currency, "+
					"b.type, "+
					"b.institution_no, "+
					"b.joint_code, "+
					"b.cnaps, "+
					"b.id as bankAccountId, "+
					"r.name as region_name, "+
					"v.city as vendorCity, "+
					"v.group_key, "+
					"v.applicant_name, "+
					"v.freeze, "+
					"v.vendor_account_type, " +
					"b.status as bankStatus, "+
					"v.super_company "+ //所属公司
				"FROM "+
					"vendor_basic_info v, bank_account b, regions r "+ 
				"WHERE "+
					"b.vendor_id = v.id AND "+
					"v.country = r.country AND "+
					"v.region = r.region_code AND "+
					type +
					category +
					super_company +
					"v.status = 40 " + //args +
				/**
				 * 供应商是初期导入的情况下，region可能为空，上面的SQL就不能全部都搜出来了。
				 */
				" UNION "+
				"SELECT "+
					"v.id, "+
					"v.vendor_code, "+ 
					"b.vendor_sub_code, "+
					"v.vendor_chinese_name, "+
					"v.vendor_english_name, "+
					"v.fax, "+
					"b.name, "+
					"b.account, "+
					"b.city, "+
					"v.vendor_type, "+
					"v.business_type, "+
					"v.vendor_group, "+
					"v.tel, "+
					"v.address, "+ 
					"v.person, "+
					"b.swift_code, "+
					"v.payment_term_for_accounting, "+
					"v.status as vendorStatus, "+
					"b.province_and_region, "+
					"b.currency, "+
					"b.type, "+
					"b.institution_no, "+
					"b.joint_code, "+
					"b.cnaps, "+
					"b.id as bankAccountId, "+
					"'' as region_name, "+
					"v.city as vendorCity, "+
					"v.group_key, "+
					"v.applicant_name, "+
					"v.freeze, "+
					"v.vendor_account_type, " +
					"b.status as bankStatus, "+ 
					"v.super_company "+ //所属公司
				"FROM "+
					"vendor_basic_info v, bank_account b "+ 
				"WHERE "+
					"b.vendor_id = v.id AND "+
					"(v.country is NULL OR v.region is NULL OR v.country = '' OR v.region='') AND "+
					type +
					category +
					super_company +
					"v.status = 40 " ,//+ args,
					interfaceDataRowMapperByDownload);
	}
	
	@Override
    public ByteArrayOutputStream createPKCS12(final Map<String, Object> row)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            CertificateException, IOException, KeyStoreException,
            InvalidKeyException, SignatureException {
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("Start create PKCS12.");
        }
        
        Security.addProvider(new BouncyCastleProvider());
        // create keypar
        String keyType = "RSA";
        KeyPairGenerator keyPairGen = null;
        
        keyPairGen = KeyPairGenerator.getInstance(keyType, "BC");
        
        keyPairGen.initialize(1024);
        KeyPair keypair = keyPairGen.genKeyPair();
        PrivateKey privateKey = keypair.getPrivate();
        PublicKey publicKey = keypair.getPublic();
        
        // create subjectDN
        Vector<DERObjectIdentifier> oids = new Vector<DERObjectIdentifier>();
        Vector<String> attributes = new Vector<String>();
        oids.addElement(X509Name.O);// organization
        oids.addElement(X509Name.OU);// organizational unit name
        oids.addElement(X509Name.CN);// common name
        attributes.addElement(row.get("usercompany").toString());
        attributes.addElement(row.get("division").toString());
        attributes.addElement(row.get("id").toString());
        X509Name subjectDN = new X509Name(oids, attributes);
        
        // get ca.crt from path
        InputStream certBIS;
        X509Certificate caCert = null;
        certBIS = // getClass().getClassLoader().getResourceAsStream("root_certificate/ca.crt");
        Thread.currentThread().getContextClassLoader()
                .getResourceAsStream("root_certificate/ca.crt");
        CertificateFactory certCF = CertificateFactory.getInstance("X.509");
        caCert = (X509Certificate) certCF.generateCertificate(certBIS);
        
        //
        X509V3CertificateGenerator certGen = new X509V3CertificateGenerator();
        ASN1InputStream dnStream = new ASN1InputStream(
                new ByteArrayInputStream(caCert.getSubjectX500Principal()
                        .getEncoded()));
        DERSequence dnSequence = null;
        dnSequence = (DERSequence) dnStream.readObject();
        certGen.setIssuerDN(new X509Name(dnSequence));
        certGen.setSubjectDN(subjectDN);
        certGen.addExtension(X509Extensions.SubjectKeyIdentifier, false,
                new SubjectKeyIdentifierStructure(publicKey));
        certGen.addExtension(X509Extensions.AuthorityKeyIdentifier, false,
                createAuthorityKeyId(caCert.getPublicKey()));
        Date startDate = new Date(System.currentTimeMillis());
        certGen.setNotBefore(startDate);
        Date endDate = Utils.getAfterOneYearDate(startDate);
        certGen.setNotAfter(endDate);// 1 years
        
        BigInteger serialNumber = new BigInteger(UUID.randomUUID().toString()
                .replaceAll("-", ""), 16);
        certGen.setSerialNumber(serialNumber);
        certGen.setSignatureAlgorithm("MD5withRSA");
        certGen.setPublicKey(publicKey);
        
        // get ca.key from path
        InputStream ca_key_fis = null;
        ca_key_fis = // getClass().getClassLoader()
                Thread.currentThread().getContextClassLoader()
                    .getResourceAsStream("root_certificate/ca.key");
        PasswordFinder pswd = new PasswordFinder() {
            @Override
            public char[] getPassword() {
                return "dodopipe".toCharArray();
            }
        };
        
        // key to keyPair
        PEMReader preader = new PEMReader(new InputStreamReader(ca_key_fis),
                pswd);
        Object o = null;
        
        o = preader.readObject();
        
        KeyPair pair = null;
        if (o instanceof KeyPair) {
            pair = (KeyPair) o;
        }
        X509Certificate cert = null;
        cert = certGen.generate(pair.getPrivate(), "BC");
        cert.verify(caCert.getPublicKey());
        
        //
        // add the friendly name for the private key
        //
        
        PKCS12BagAttributeCarrier bagAttr = (PKCS12BagAttributeCarrier) privateKey;
        Certificate[] chain = new Certificate[1];
        chain[0] = cert;
        
        //
        // this is also optional - in the sense that if you leave this
        // out the keystore will add it automatically, note though that
        // for the browser to recognise which certificate the private key
        // is associated with you should at least use the pkcs_9_localKeyId
        // OID and set it to the same as you do for the private key's
        // corresponding certificate.
        //
        
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_friendlyName,
                new DERBMPString(row.get("id").toString()));
        
        bagAttr.setBagAttribute(PKCSObjectIdentifiers.pkcs_9_at_localKeyId,
                new SubjectKeyIdentifierStructure(publicKey));
        
        //
        // store the key and the certificate chain
        //
        KeyStore store = null;
        store = KeyStore.getInstance("PKCS12", "BC");
        store.load(null, null);
        
        //
        // if you haven't set the friendly name and local key id above
        // the name below will be the name of the key
        //
        
        store.setKeyEntry(row.get("id").toString(), privateKey,
                row.get("password").toString().toCharArray(), chain);
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        char[] passwd = row.get("password").toString().toCharArray();
        store.store(out, passwd);
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("End create PKCS12.");
            Utils.logger.debug("Start Save PKCS12.");
        }
        // save PKCS12
        Map<String, Object> pkcs12Row = new HashMap<String, Object>();
        pkcs12Row.put("userid", row.get("id").toString());
        pkcs12Row.put("data", out.toByteArray());
        pkcs12Row.put("serial_number", serialNumber.toString());
//        Date startDate = new Date(System.currentTimeMillis());
//        Date endDate = Utils.getAfterOneYearDate(startDate);
        pkcs12Row.put("date", endDate);
        saveCert(pkcs12Row);
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("End Save PKCS12.");
        }
        
        certBIS.close();
        ca_key_fis.close();
        preader.close();
        dnStream.close();
        return out;
    }
    
    // 这里是上面用到的生成签发者公钥标识的函数:
    protected static AuthorityKeyIdentifier createAuthorityKeyId(PublicKey pubKey) throws IOException  {
        AuthorityKeyIdentifier authKeyId = null;
        ByteArrayInputStream bIn = new ByteArrayInputStream(
                pubKey.getEncoded());
        SubjectPublicKeyInfo info = new SubjectPublicKeyInfo(
                (DERConstructedSequence) new DERInputStream(bIn)
                        .readObject());
        authKeyId = new AuthorityKeyIdentifier(info);
       
        return authKeyId;
    }
    
    @Override
    public void saveCert(final Map<String, Object> row){
        jdbcTemplate.update("DELETE FROM certificates WHERE userid=:userid", row);
        insertCertificate.execute(row);
    }
    
    @Override
    public User getUser(String id) {
        @SuppressWarnings("deprecation")
        User user = jdbcTemplate.queryForObject(
                "SELECT u.id, u.password, u.name, ui.email, u.usercompany, u.division " +
                "FROM user u, user_info ui " +
                "WHERE u.id = ui.id and u.usercompany = ui.usercompany and u.id =?",
                ParameterizedBeanPropertyRowMapper.newInstance(User.class), 
                id);
        return user;
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public List<User> getAllValidUsersList() {
        return jdbcTemplate.query(
                "SELECT DISTINCT(u.id) as id " +
                "FROM user u, user_info ui where u.id = ui.id and ui.valid = 1",
                ParameterizedBeanPropertyRowMapper.newInstance(User.class));
    }
}
