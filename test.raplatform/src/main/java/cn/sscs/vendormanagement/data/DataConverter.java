package cn.sscs.vendormanagement.data;

public interface DataConverter {

	Object convert(Object value) throws ConvertException;
}
