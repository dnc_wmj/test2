package cn.sscs.vendormanagement.data;

public class ExcelParserException extends RuntimeException {

	private static final long serialVersionUID = 3377289659400022705L;

	private String messageKey = null;
	
	/**
	 * 
	 * @param message
	 */
	ExcelParserException(String message, String messageKey) {
		super(message);
		this.messageKey = messageKey;
	}
	
	/**
	 * 
	 * @param message
	 * @param messageKey
	 * @param cause
	 */
	ExcelParserException(String message, String messageKey, Throwable cause) {
		super(message, cause);
		this.messageKey = messageKey;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getMessageKey() {
		return messageKey;
	}
}
