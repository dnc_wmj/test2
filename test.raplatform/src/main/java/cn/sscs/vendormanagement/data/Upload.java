package cn.sscs.vendormanagement.data;

import org.springframework.web.multipart.MultipartFile;

public class Upload {
    public static final String SSCS ="SSCS";
    public static final String SSGE ="SSGE";
    public static final String SSV ="SEV";
    public static final String SEW ="SEW";
    public static final String SDPW ="SDPW";
    public static final String SPDH ="SPDH";
    public static final String SEH ="SEH";
    
    private MultipartFile file = null;
    private MultipartFile userfile = null;
    private MultipartFile costcenterfile = null;
    private String userCompany = "1";
    
    /**
     * @return the file
     */
    public MultipartFile getFile() {
        return file;
    }
    
    /**
     * @param file
     *            the file to set
     */
    public void setFile(MultipartFile file) {
        this.file = file;
    }
    
    public MultipartFile getUserfile() {
        return userfile;
    }
    
    public void setUserfile(MultipartFile userfile) {
        this.userfile = userfile;
    }
    
    public MultipartFile getCostcenterfile() {
        return costcenterfile;
    }
    
    public void setCostcenterfile(MultipartFile costcenterfile) {
        this.costcenterfile = costcenterfile;
    }

    public String getUserCompany() {
        return userCompany;
    }

    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany;
    }
    
}
