package cn.sscs.vendormanagement.data;

public class UserSearchCriteria {

	private String name = null;
	
	private int page = 0;

	@SuppressWarnings("unused")
	private int recordFrom = 0;
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * 
	 * @return
	 */
	public String getAmbiguousName() {
		return "%" + name + "%";
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the page
	 */
	public int getPage() {
		return page;
	}

	/**
	 * @param page the page to set
	 */
	public void setPage(int page) {
		this.page = page;
	}

	public int getRecordFrom() {
		return this.page * 20;
	}

	public void setRecordFrom(int recordFrom) {
		this.recordFrom = recordFrom;
	}
	
	
	
}
