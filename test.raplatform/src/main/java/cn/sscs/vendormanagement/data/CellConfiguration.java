package cn.sscs.vendormanagement.data;

import cn.sscs.vendormanagement.data.ExcelParser.Type;

public class CellConfiguration {

private String name = null;
	
	private String columnName = null;
	
	private Type type = null;
	
	private int num = -1;
	
	@SuppressWarnings("unused")
	private int length = -1;
	
	private boolean isKey = false;
	
	private boolean isNotNull = false;
	
	private DataConverter converter = null;
	
	/**
	 * 
	 * @param name
	 * @param columnName
	 * @param type
	 * @param length
	 * @param isKey
	 * @param converter
	 * @param isNotNull
	 */
	public CellConfiguration(String name, 
			String columnName, 
			Type type, 
			int length, 
			boolean isKey, 
			DataConverter converter,
			boolean isNotNull) {
		this.name = name;
		this.columnName = columnName;
		this.type = type;
		this.length = length;
		this.isKey = isKey;
		this.converter = converter;
		this.isNotNull = isNotNull;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getColumnName() {
		return columnName;
	}
	
	/**
	 * 
	 * @return
	 */
	public Type getType() {
		return type;
	}

	/**
	 * 
	 * @return
	 */
	public int getNum() {
		return num;
	}
	
	/**
	 * 
	 * @param num
	 */
	public void setNum(int num) {
		this.num = num;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isKey() {
		return isKey;
	}
	
	/**
	 * 
	 * @return
	 */
	public DataConverter getConverter() {
		return converter;
	}
	
	/**
	 * 
	 * @return
	 */
	public boolean isNotNull() {
		return isNotNull;
	}
}
