package cn.sscs.vendormanagement.data;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class ExcelWriter<T> {

	private List<T> rows = null;
	
	private InputStream excelTemplate = null;
	
	private String sheetName = null;
	
	/**
	 * 
	 * @param rows
	 * @param excelTemplate
	 * @param sheetName
	 */
	public ExcelWriter(List<T> rows, InputStream excelTemplate, String sheetName) {
		this.rows = rows;
		this.excelTemplate = excelTemplate;
		this.sheetName = sheetName;
	}

	/**
	 * 
	 * @param out
	 * @param handler
	 * @throws IOException 
	 */
	public void write(OutputStream out, ExcelWriterRowHandler<T> handler) 
		throws IOException 
	{
		
		HSSFWorkbook workBook = new HSSFWorkbook(excelTemplate);
		workBook.setSheetName(0, sheetName);
		HSSFSheet sheet = workBook.getSheetAt(0);
		
		
		
		for (int i=0; i<rows.size(); i++) {
			HSSFRow hssfRow = sheet.createRow(i + 1);
			
			handler.handleRow(i, rows.get(i), hssfRow);
		}
		
		workBook.write(out);
	}
}
