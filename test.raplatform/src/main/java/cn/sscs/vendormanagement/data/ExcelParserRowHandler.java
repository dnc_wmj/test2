package cn.sscs.vendormanagement.data;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCell;

public interface ExcelParserRowHandler {

	/**
	 * Handles a row data. The row data includes variables that you configured as needed.
	 * 
	 * @param num
	 * @param row
	 * @throws IOException 
	 * @throws SignatureException 
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
    Map<String, Object> handleRow(int num, Map<String, Object> row) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchProviderException, CertificateException, KeyStoreException, SignatureException, IOException;

    /**
	 * 
	 * @param cell
	 * @param cellconf
	 * @param row
	 */
	Object handleNullCellValue(HSSFCell cell, CellConfiguration cellconf, int row);
	
	/**
	 * 
	 * @param num
	 * @param row
	 * @param usercompanyBySelected 
	 * @return
	 */
	boolean validationRow(int num, Map<String, Object> row, String usercompanyBySelected);
}
