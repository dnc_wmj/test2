package cn.sscs.vendormanagement.data;

public class InterfaceData {
	
	private long vendorId = -1;
	
	private String vendorCode = null;
	
	private String vendorSubCode = null;
	
	private String name = null;
	
	private String chineseName = null;
	
	private String englishName = null;
	
	private String fax = null;
	
	private String bankName = null;
	
	private String bankAccount = null;
	
	private int bankCityType = -1;
	
	private int vendorType = -1;
	
	private int businessType = -1;
	
	/**
	 * 增加所属公司类型
	 */
	private int superCompany = -1;
	
	private String tel = null;
	
	private String address = null;
	
	private String contactPerson = null;
	
	private String bankAddress = null;
	
	private String swiftCode = null;
	
	private String bankRoutingCode = null;
	
	private int paymentTerm = -1;
	
	private int effective = -1;

	
	private String bankProvinceAndRegion = null;

	private String currency = null;
	
	private int bankType = -1;
	
	private String institutionNo = null;
	
	private String jointCode = null;
	
	private String cnaps = null;
	
	private long bankAccountId = -1;
	
	private int bankStatus = -1;
	
	private String vendorGroup = null;
	
	//省市和地区
	private String region_name = null;
	//城市
	private String city = null;
	//组代码
	private String groupKey = null;
	//申请人
	private String applient_name = null;
	
	private String freeze = null;
	
	private String vendorAccountType = "vendor";
	/**
	 * @return the vendorCode
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * @param vendorCode the vendorCode to set
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}

	/**
	 * 
	 * @return
	 */
	public String getVendorSubCode() {
		return vendorSubCode;
	}
	
	/**
	 * 
	 * @param vendorSubCode
	 */
	public void setVendorSubCode(String vendorSubCode) {
		this.vendorSubCode = vendorSubCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}


	/**
	 * 
	 * @param nameame
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the fax
	 */
	public String getFax() {
		return fax;
	}

	/**
	 * @param fax the fax to set
	 */
	public void setFax(String fax) {
		this.fax = fax;
	}

	/**
	 * @return the bankName
	 */
	public String getBankName() {
		return bankName;
	}

	/**
	 * @param bankName the bankName to set
	 */
	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	/**
	 * @return the bankAccount
	 */
	public String getBankAccount() {
		return bankAccount;
	}

	/**
	 * @param bankAccount the bankAccount to set
	 */
	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	/**
	 * @return the bankCityType
	 */
	public int getBankCityType() {
		return bankCityType;
	}

	/**
	 * @param bankCityType the bankCityType to set
	 */
	public void setBankCityType(int bankCityType) {
		this.bankCityType = bankCityType;
	}

	/**
	 * 
	 * @return
	 */
	public int getBusinessType() {
		return businessType;
	}
	
	/**
	 * 
	 * @param businessType
	 */
	public void setBusinessType(int businessType) {
		this.businessType = businessType;
	}
	
	/**
	 * @return the vendorType
	 */
	public String getVendorType() {
		String retval = null;
		
		switch (vendorType) {
		case 0:
			retval = "Exter";
			break;
		case 1:
			retval = "Inter";
			break;
		case 2:
			retval = "SSCS Group";
		}
		
		return retval;
	}

	/**
	 * @param vendorType the vendorType to set
	 */
	public void setVendorType(int vendorType) {
		this.vendorType = vendorType;
	}

	/**
	 * @return the tel
	 */
	public String getTel() {
		return tel;
	}

	/**
	 * @param tel the tel to set
	 */
	public void setTel(String tel) {
		this.tel = tel;
	}

	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}

	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the contactPerson
	 */
	public String getContactPerson() {
		return contactPerson;
	}

	/**
	 * @param contactPerson the contactPerson to set
	 */
	public void setContactPerson(String contactPerson) {
		this.contactPerson = contactPerson;
	}

	/**
	 * @return the bankAddress
	 */
	public String getBankAddress() {
		return bankAddress;
	}

	/**
	 * @param bankAddress the bankAddress to set
	 */
	public void setBankAddress(String bankAddress) {
		this.bankAddress = bankAddress;
	}

	/**
	 * @return the swiftCode
	 */
	public String getSwiftCode() {
		return swiftCode;
	}

	/**
	 * @param swiftCode the swiftCode to set
	 */
	public void setSwiftCode(String swiftCode) {
		this.swiftCode = swiftCode;
	}

	/**
	 * @return the bankRoutingCode
	 */
	public String getBankRoutingCode() {
		return bankRoutingCode;
	}

	/**
	 * @param bankRoutingCode the bankRoutingCode to set
	 */
	public void setBankRoutingCode(String bankRoutingCode) {
		this.bankRoutingCode = bankRoutingCode;
	}

	/**
	 * @return the paymentTerm
	 */
	public String getPaymentTerm() {
		String retval = null;
		switch (paymentTerm) {
		case 3:
			retval = "S015";
			break;
		case 4:
			retval = "S075";
			break;
		case 5:
			retval = "S090";
			break;
		case 6:
			retval = "S115";
			break;
		case 7:
			retval = "S125";
			break;
		case 8:
			retval = "S215";
			break;
		case 9:
			retval = "S225";
			break;
		case 10:
			retval = "S315";
			break;
		case 11:
			retval = "S325";
			break;

		}
		return retval;
	}

	/**
	 * @param paymentTerm the paymentTerm to set
	 */
	public void setPaymentTerm(int paymentTerm) {
		this.paymentTerm = paymentTerm;
	}

	/**
	 * @return the effective
	 */
	public int getEffective() {
		return effective;
	}

	/**
	 * @param effective the effective to set
	 */
	public void setEffective(int effective) {
		this.effective = effective;
	}

	/**
	 * @return the bankProvinceAndRegion
	 */
	public String getBankProvinceAndRegion() {
		return bankProvinceAndRegion;
	}

	/**
	 * @param bankProvinceAndRegion the bankProvinceAndRegion to set
	 */
	public void setBankProvinceAndRegion(String bankProvinceAndRegion) {
		this.bankProvinceAndRegion = bankProvinceAndRegion;
	}

	/**
	 * @return the currency
	 */
	public String getCurrency() {
		return currency;
	}

	/**
	 * @param currency the currency to set
	 */
	public void setCurrency(String currency) {
		this.currency = currency;
	}

	/**
	 * @return the bankType
	 */
	public int getBankType() {
		return bankType;
	}

	/**
	 * @param bankType the bankType to set
	 */
	public void setBankType(int bankType) {
		this.bankType = bankType;
	}

	/**
	 * @return the institutionNo
	 */
	public String getInstitutionNo() {
		return institutionNo;
	}

	/**
	 * @param institutionNo the institutionNo to set
	 */
	public void setInstitutionNo(String institutionNo) {
		this.institutionNo = institutionNo;
	}

	/**
	 * @return the jointCode
	 */
	public String getJointCode() {
		return jointCode;
	}

	/**
	 * @param jointCode the jointCode to set
	 */
	public void setJointCode(String jointCode) {
		this.jointCode = jointCode;
	}

	/**
	 * @return the cnaps
	 */
	public String getCnaps() {
		return cnaps;
	}

	/**
	 * @param cnaps the cnaps to set
	 */
	public void setCnaps(String cnaps) {
		this.cnaps = cnaps;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getBankAccountId() {
		return bankAccountId;
	}
	
	/**
	 * 
	 * @param bankAccountId
	 */
	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getBankStatus() {
		return bankStatus;
	}
	
	/**
	 * 
	 * @param bankStatus
	 */
	public void setBankStatus(int bankStatus) {
		this.bankStatus = bankStatus;
	}

	/**
	 * @return the vendorGroup
	 */
	public String getVendorGroup() {
		return vendorGroup;
	}

	/**
	 * @param vendorGroup the vendorGroup to set
	 */
	public void setVendorGroup(String vendorGroup) {
		this.vendorGroup = vendorGroup;
	}

	public String getRegion() {
		return region_name;
	}

	public void setRegion(String region) {
		this.region_name = region;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getGroupKey() {
		return groupKey;
	}

	public void setGroupKey(String groupKey) {
		this.groupKey = groupKey;
	}

	public String getApplient_name() {
		return applient_name;
	}

	public void setApplient_name(String applient_name) {
		this.applient_name = applient_name;
	}

	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}

	public long getVendorId() {
		return vendorId;
	}

	public void setFreeze(String freeze) {
		this.freeze = freeze;
	}

	public String getFreeze() {
		return freeze;
	}

	public void setVendorAccountType(String vendorAccountType) {
		this.vendorAccountType = vendorAccountType;
	}

	public String getVendorAccountType() {
		return vendorAccountType;
	}
	
	
	public int getSuperCompany() {
		return superCompany;
	}

	public void setSuperCompany(int superCompany) {
		this.superCompany = superCompany;
	}

	public String getChineseName() {
		return chineseName;
	}

	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}

	public String getEnglishName() {
		return englishName;
	}

	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}

}
