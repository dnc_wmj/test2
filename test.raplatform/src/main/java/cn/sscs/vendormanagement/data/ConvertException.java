package cn.sscs.vendormanagement.data;

public class ConvertException extends Exception {

	private static final long serialVersionUID = 5516571595439964853L;

	/**
	 * 
	 * @param message
	 */
	public ConvertException(String message) {
		super(message);
	}
}
