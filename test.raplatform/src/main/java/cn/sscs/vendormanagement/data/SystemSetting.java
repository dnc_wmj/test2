package cn.sscs.vendormanagement.data;

import java.util.regex.Pattern;

import org.springframework.validation.Errors;

public class SystemSetting {

	private int reEvalMonth = -1;

	private String approvalRequestMailSubject = null;

	private String approvalRequestMailTemplate = null;

	private String deniedMailSubject = null;

	private String deniedMailTemplate = null;

	private String rejectMailSubject = null;

	private String rejectMailTemplate = null;

	private String cancelMailSubject = null;

	private String cancelMailTemplate = null;

	private String applicationFinishedMailSubject = null;

	private String applicationFinishedMailTemplate = null;

	private String reEvaluationRequestMailSubject = null;

	private String reEvaluationRequestMailTemplate = null;

	private String passwordNotificationMailSubject = null;

	private String passwordNotificationMailTemplate = null;

	private String freezeDay = null;

	private String freezeValidMailSubject = null;

	private String freezeValidMailTemplate = null;

	private String freezeInvalidMailSubject = null;

	private String freezeInvalidMailTemplate = null;

	private String certattMailSubject = null;

    private String certattMailTemplate = null;
	/**
	 * 
	 * @return
	 */
	public int getReEvalMonth() {
		return reEvalMonth;
	}

	/**
	 * 
	 * @param reEvalMonth
	 */
	public void setReEvalMonth(int reEvalMonth) {
		this.reEvalMonth = reEvalMonth;
	}

	/**
	 * @return the approvalRequestMailSubject
	 */
	public String getApprovalRequestMailSubject() {
		return approvalRequestMailSubject;
	}

	/**
	 * @param approvalRequestMailSubject
	 *            the approvalRequestMailSubject to set
	 */
	public void setApprovalRequestMailSubject(String approvalRequestMailSubject) {
		this.approvalRequestMailSubject = approvalRequestMailSubject;
	}

	/**
	 * @return the approvalRequestMailTemplate
	 */
	public String getApprovalRequestMailTemplate() {
		return approvalRequestMailTemplate;
	}

	/**
	 * @param approvalRequestMailTemplate
	 *            the approvalRequestMailTemplate to set
	 */
	public void setApprovalRequestMailTemplate(String approvalRequestMailTemplate) {
		this.approvalRequestMailTemplate = approvalRequestMailTemplate;
	}

	/**
	 * @return the deniedMailSubject
	 */
	public String getDeniedMailSubject() {
		return deniedMailSubject;
	}

	/**
	 * @param deniedMailSubject
	 *            the deniedMailSubject to set
	 */
	public void setDeniedMailSubject(String deniedMailSubject) {
		this.deniedMailSubject = deniedMailSubject;
	}

	/**
	 * @return the deniedMailTemplate
	 */
	public String getDeniedMailTemplate() {
		return deniedMailTemplate;
	}

	/**
	 * @param deniedMailTemplate
	 *            the deniedMailTemplate to set
	 */
	public void setDeniedMailTemplate(String deniedMailTemplate) {
		this.deniedMailTemplate = deniedMailTemplate;
	}

	/**
	 * @return the applicationFinishedMailSubject
	 */
	public String getApplicationFinishedMailSubject() {
		return applicationFinishedMailSubject;
	}

	/**
	 * @param applicationFinishedMailSubject
	 *            the applicationFinishedMailSubject to set
	 */
	public void setApplicationFinishedMailSubject(String applicationFinishedMailSubject) {
		this.applicationFinishedMailSubject = applicationFinishedMailSubject;
	}

	/**
	 * @return the applicationFinishedMailTemplate
	 */
	public String getApplicationFinishedMailTemplate() {
		return applicationFinishedMailTemplate;
	}

	/**
	 * @param applicationFinishedMailTemplate
	 *            the applicationFinishedMailTemplate to set
	 */
	public void setApplicationFinishedMailTemplate(String applicationFinishedMailTemplate) {
		this.applicationFinishedMailTemplate = applicationFinishedMailTemplate;
	}

	/**
	 * @return the reEvaluationRequestMailSubject
	 */
	public String getReEvaluationRequestMailSubject() {
		return reEvaluationRequestMailSubject;
	}

	/**
	 * @param reEvaluationRequestMailSubject
	 *            the reEvaluationRequestMailSubject to set
	 */
	public void setReEvaluationRequestMailSubject(String reEvaluationRequestMailSubject) {
		this.reEvaluationRequestMailSubject = reEvaluationRequestMailSubject;
	}

	/**
	 * @return the reEvaluationRequestMailTemplate
	 */
	public String getReEvaluationRequestMailTemplate() {
		return reEvaluationRequestMailTemplate;
	}

	/**
	 * @param reEvaluationRequestMailTemplate
	 *            the reEvaluationRequestMailTemplate to set
	 */
	public void setReEvaluationRequestMailTemplate(String reEvaluationRequestMailTemplate) {
		this.reEvaluationRequestMailTemplate = reEvaluationRequestMailTemplate;
	}

	/**
	 * 
	 * @return
	 */
	public String getPasswordNotificationMailSubject() {
		return passwordNotificationMailSubject;
	}

	/**
	 * 
	 * @param passwordNotificationMailSubject
	 */
	public void setPasswordNotificationMailSubject(String passwordNotificationMailSubject) {
		this.passwordNotificationMailSubject = passwordNotificationMailSubject;
	}

	/**
	 * 
	 * @param passwordNotificationMailTemplate
	 */
	public void setPasswordNotificationMailTemplate(String passwordNotificationMailTemplate) {
		this.passwordNotificationMailTemplate = passwordNotificationMailTemplate;
	}

	/**
	 * 
	 * @return
	 */
	public String getPasswordNotificationMailTemplate() {
		return passwordNotificationMailTemplate;
	}

	public String getRejectMailSubject() {
		return rejectMailSubject;
	}

	public void setRejectMailSubject(String rejectMailSubject) {
		this.rejectMailSubject = rejectMailSubject;
	}

	public String getRejectMailTemplate() {
		return rejectMailTemplate;
	}

	public void setRejectMailTemplate(String rejectMailTemplate) {
		this.rejectMailTemplate = rejectMailTemplate;
	}

	public String getCancelMailSubject() {
		return cancelMailSubject;
	}

	public void setCancelMailSubject(String cancelMailSubject) {
		this.cancelMailSubject = cancelMailSubject;
	}

	public String getCancelMailTemplate() {
		return cancelMailTemplate;
	}

	public void setCancelMailTemplate(String cancelMailTemplate) {
		this.cancelMailTemplate = cancelMailTemplate;
	}

	public void setFreezeDay(String freezeDay) {
		this.freezeDay = freezeDay;
	}

	public String getFreezeDay() {
		return freezeDay;
	}
	
	public String getFreezeValidMailSubject() {
		return freezeValidMailSubject;
	}

	public void setFreezeValidMailSubject(String freezeValidMailSubject) {
		this.freezeValidMailSubject = freezeValidMailSubject;
	}

	public String getFreezeValidMailTemplate() {
		return freezeValidMailTemplate;
	}

	public void setFreezeValidMailTemplate(String freezeValidMailTemplate) {
		this.freezeValidMailTemplate = freezeValidMailTemplate;
	}
	
	public String getFreezeInvalidMailSubject() {
		return freezeInvalidMailSubject;
	}

	public void setFreezeInvalidMailSubject(String freezeInvalidMailSubject) {
		this.freezeInvalidMailSubject = freezeInvalidMailSubject;
	}

	public String getFreezeInvalidMailTemplate() {
		return freezeInvalidMailTemplate;
	}

	public void setFreezeInvalidMailTemplate(String freezeInvalidMailTemplate) {
		this.freezeInvalidMailTemplate = freezeInvalidMailTemplate;
	}

	public void validate(Errors errors) {
		if (freezeDay != null && !"".equals(freezeDay)) {
			Pattern pattern = Pattern.compile("[0-9]*");
			if (!pattern.matcher(freezeDay).matches()) {
				errors.rejectValue("freezeDay", "error.isnotNumber");
			}
		}

	}

    public String getCertattMailSubject() {
        return certattMailSubject;
    }

    public void setCertattMailSubject(String certattMailSubject) {
        this.certattMailSubject = certattMailSubject;
    }

    public String getCertattMailTemplate() {
        return certattMailTemplate;
    }

    public void setCertattMailTemplate(String certattMailTemplate) {
        this.certattMailTemplate = certattMailTemplate;
    }
	
}
