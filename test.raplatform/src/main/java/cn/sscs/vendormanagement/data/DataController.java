package cn.sscs.vendormanagement.data;

import java.beans.PropertyEditorSupport;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.sscs.vendormanagement.User;
import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.VndMgmntUser;
import cn.sscs.vendormanagement.evaluation.Evaluation;
import cn.sscs.vendormanagement.evaluation.IEvaluationService;
import cn.sscs.vendormanagement.vendor.BankAccount;
import cn.sscs.vendormanagement.vendor.IVendorService;
import cn.sscs.vendormanagement.vendor.VendorBasicInfo;
import cn.sscs.vendormanagement.vendor.VendorSearchCriteria;

@Transactional
@Controller
@RequestMapping(value="/data")
public class DataController {

	@Autowired
	private IDataService dataService = null;
	
	@Autowired
	private IVendorService vendorService = null;

	@Autowired
	private IEvaluationService evaluationService = null;

	@InitBinder  
	public void InitBinder(WebDataBinder dataBinder) {  
	    dataBinder.registerCustomEditor(Date.class, new PropertyEditorSupport() {  
	        public void setAsText(String value) {  
	            try {  
	                setValue(new SimpleDateFormat("yyyy/MM/dd").parse(value));  
	            } catch(ParseException e) {  
	                setValue(null);  
	            }
	        }  
	        public String getAsText() {
	        	return (getValue() == null) ? null : new SimpleDateFormat("yyyy/MM/dd").format((Date) getValue());
	        }          
	  
	    });  
	}
	
	@RequestMapping(value="/menu", method=RequestMethod.GET)
	public String getMenu() {
		return "data/menu";
	}
	/**
	 * 
	 * @Title:       goGetUserManagement 
	 * @Description:  ( 用户管理  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
	@RequestMapping(value="/user", method=RequestMethod.GET)
	public String goGetUserManagement(UserSearchCriteria criteria, Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("upload", new Upload());
		model.addAttribute("criteria", criteria);
		
		return "data/user";
	}
	/**
	 * 
	 * @Title:       getUserManagement 
	 * @Description:  ( 用户管理界面 - 响应查询按钮 提交的请求  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 * 
	 * 以页面录入条件和page信息 查询 user信息
	 *		SELECT * FROM USER U WHERE (U.NAME LIKE :AMBIGUOUSNAME OR U.ID LIKE :AMBIGUOUSNAME) ORDER BY ID LIMIT :RECORDFROM, 20
	 * 
	 */
	@RequestMapping(value="/user", method=RequestMethod.POST)
	public String getUserManagement(UserSearchCriteria criteria, Model model) {
		Utils.checkHasRoleHD();
		/* 保存查询条件*/
		model.addAttribute("upload", new Upload());
		model.addAttribute("criteria", criteria);
		if (criteria.getName() != null) {
			
			List<User> users = dataService.searchUsers(criteria);
			int pages = users.size() > 0 ? dataService.searchUserCount(criteria)/20 : 0;
			
			model.addAttribute("pages", pages);
			model.addAttribute("users", users);
		} 
		
		return "data/user";
	}
	/**
	 * 
	 * @Title:       upload 
	 * @Description:  ( 用户管理界面 - 响应上传按钮响应 ) 
	 * @param        @param upload
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws SignatureException 
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 6, 2012
	 */
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public String upload(Upload upload, Model model)
            throws InvalidKeyException, NoSuchAlgorithmException,
            NoSuchProviderException, CertificateException, KeyStoreException,
            SignatureException {
        Utils.checkHasRoleHD();
		
		dataService.registerUsers(upload);
		
		return "redirect:/data/user";
	}
	
	@RequestMapping(value="/change/password", method=RequestMethod.GET)
	public String getChangePassword(Model model) {
		model.addAttribute("changePassword", new ChangePassword());
		
		return "data/change_password";
	}
	
	@RequestMapping(value="/change/password", method=RequestMethod.POST)
	public String changePassword(ChangePassword changePassword, BindingResult result, Model model) {
		
		changePassword.validate(result);
		
		if (result.hasErrors()) {
			model.addAttribute("changePassword", changePassword);
			return "data/change_password";
		}
		
		dataService.changePassword(Utils.getUser().getUsername(), changePassword.getNewPassword());
		
		return "redirect:/workflow/myapplications";
	}

	@RequestMapping(value="/system/setting", method=RequestMethod.GET)
	public String getSystemSetting(Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("setting", dataService.getSystemSetting());
		
		return "data/system_setting";
		
	}
	/**
	 * 
	 * @Title:       getInterface 
	 * @Description:  ( 数据管理 - 下载数据变更  ) 
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 3, 2012
	 */
	@RequestMapping(value="/interface", method=RequestMethod.GET)
	public String getInterface(Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("dataHistories", dataService.getModificationHistories());
		model.addAttribute("fileHistories", dataService.getInterfaceFiles());
		
		return "data/interface";
	}

	/**
	 * 
	 * @Title:       requestInterfaceFile 
	 * @Description:  ( 数据管理 - 下载数据变更   响应下载按钮 ) 
	 * @param        @param model
	 * @param        @return
	 * @param        @throws IOException     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 3, 2012
	 */
	@RequestMapping(value="/interface", method=RequestMethod.POST)
	public synchronized String requestInterfaceFile(Model model) throws IOException {
		Utils.checkHasRoleHD();
		
		List<ModificationHistory> histories = dataService.getModificationHistories();
		if (histories.size() == 0) {
			model.addAttribute("dataHistories", dataService.getModificationHistories());
			model.addAttribute("fileHistories", dataService.getInterfaceFiles());
			return "data/interface";
		}
		

		HSSFWorkbook workBook = new HSSFWorkbook(
				getClass().getClassLoader().getResourceAsStream("VendorCodeImportTemplate.xls"));
		workBook.setSheetName(0, "Vendor");
		HSSFSheet sheet = workBook.getSheetAt(0);
		
		
		int row = 1;
		for (ModificationHistory history : histories) {
			switch(history.getClassification()) {
			case ModificationHistory.CLASS_INSERT:
			case ModificationHistory.CLASS_UPDATE:
			case ModificationHistory.CLASS_DELETE:
			case ModificationHistory.CLASS_FREEZE:
			case ModificationHistory.CLASS_UNFREEZE:
				for (InterfaceData data : dataService.findInterfaceDataByVendorId(history.getVendorId())) {
					write(history, data, sheet.createRow(row++));
				}
				break;
			}
			
		}
		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		workBook.write(out);
		
		long id = dataService.storeInterfaceFile(out.toByteArray(), new Date());
		
		if (histories.size() > 0) {
			dataService.clearModificationHistory(histories.get(histories.size() -1 ).getId());	
		}
		
		
		model.addAttribute("interfaceFileId", id);
		model.addAttribute("fileHistories", dataService.getInterfaceFiles());
		
		return "data/interface";
	}

	/**
	 * 
	 * @Title:       requestUserInfo 
	 * @Description:  ( 数据管理 - 下载数据变更 已处理变更记录的下载功能) 
	 * @param        @param model
	 * @param        @param response
	 * @param        @throws IOException     
	 * @return       void     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 3, 2012
	 */
	@RequestMapping(value="/interface/down/userinfo", method=RequestMethod.POST)
	public void requestUserInfo(Model model,//UserSearchCriteria criteria, 
			HttpServletResponse response) throws IOException {
//		Utils.checkHasRoleHD();
//		
//		List<VndMgmntUser> users = dataService.getAllUserInfo();
//		HSSFWorkbook workBook = new HSSFWorkbook(getClass().getClassLoader().getResourceAsStream("users.xls"));
//		workBook.setSheetName(0, "Users");
//		HSSFSheet sheet = workBook.getSheetAt(0);
//		
//		int row = 1;
//		for (VndMgmntUser user : users) {
//		    // line 464
//			write(user, sheet.createRow(row++));
//		}
//		
//		response.setContentType("application/vnd.ms-excel");
//		response.setHeader(
//		        "Content-Disposition",
//		        "filename=\"" + "users.xls" + "\"");
//		
//		
//		OutputStream out = response.getOutputStream();
//		workBook.write(out);
//		out.flush();
//		out.close();
		
	}
	
	@SuppressWarnings("deprecation")
	private void write(ModificationHistory history, InterfaceData data, HSSFRow hssfRow) {
		String vendorCode = (data.getVendorCode()==null || "".equals(data.getVendorCode()))
								? "" 
								: data.getVendorCode();
		String vendorSubCode = "00".equals(data.getVendorSubCode())?"":"-" + data.getVendorSubCode();
		hssfRow.createCell((short)0).setCellValue(
				new HSSFRichTextString( vendorCode + vendorSubCode));
		
//		hssfRow.createCell((short)1).setCellValue(
//				new HSSFRichTextString(data.getName()));
		/*
		 * 名称
		 * 
		 * 如果供应商变更数据下载表格里，供应商的名称，根据银行账户币种，抓取不同的名称
		 * USD,JPY,HKD账户抓取英文名称， RMB账户抓取 中文名称
		 * 比方说  		313800   	索尼物流       币种：RMB，       	 中国银行
		 *              313800-01   SSCS      币种：USD       花旗银行
		 *              313800-02   SSCS      币种：JPY       东三菱银行"		
		 *  
		 * 注：	USD,JPY,HKD时，优先抓英文名称，如果为空，则抓中文名
		 *  	RMB时，优先抓中文名称，如果为空，则抓英文名
		 *		币种为空时，优先抓中文（根据实际情况分析后的结果）
		 */
		if((data.getCurrency() != null // 币种不为空
				&& ("USD".equals(data.getCurrency()) || "JPY".equals(data.getCurrency()) || "HKD".equals(data.getCurrency()) // 币种为3种之一
						)) && data.getEnglishName() != null //英文名不为空
						){
			hssfRow.createCell((short)1).setCellValue(
					new HSSFRichTextString(data.getEnglishName()));
		}else{
			// 币种为空优先中文名， 当中文名为空是则英文名
			// 货币为RMB时，优先抓中文名称，如果为空，则抓英文名
			// 英文名称，如果为空，则抓中文名
			hssfRow.createCell((short)1).setCellValue(
					new HSSFRichTextString(data.getName()));
		}
		
		hssfRow.createCell((short)2).setCellValue(
				new HSSFRichTextString(data.getFax()));
		hssfRow.createCell((short)3).setCellValue(
				new HSSFRichTextString(data.getBankName()));
		hssfRow.createCell((short)4).setCellValue(
				new HSSFRichTextString(data.getBankAccount()));
		String bankCityType = "FALSE";
		if(data.getBankCityType() == 1){
			bankCityType = "TRUE";
		}
		hssfRow.createCell((short)5).setCellValue(new HSSFRichTextString(bankCityType));
		
		hssfRow.createCell((short)6).setCellValue(new HSSFRichTextString(data.getVendorType()));
		
		//hssfRow.createCell((short)7).setCellValue(data.getBusinessType());
		hssfRow.createCell((short)7).setCellValue(new HSSFRichTextString(data.getVendorGroup()));
		
		hssfRow.createCell((short)8).setCellValue(
				new HSSFRichTextString(data.getTel()));
		hssfRow.createCell((short)9).setCellValue(
				new HSSFRichTextString(data.getAddress()));
		hssfRow.createCell((short)10).setCellValue(
				new HSSFRichTextString(data.getContactPerson()));
		
		hssfRow.createCell((short)11).setCellValue(
				new HSSFRichTextString(data.getBankAddress()));
		
		hssfRow.createCell((short)12).setCellValue(
				new HSSFRichTextString(data.getSwiftCode()));
		
		hssfRow.createCell((short)14).setCellValue(new HSSFRichTextString(data.getPaymentTerm()));
		
		String effective = "TRUE";
		if(data.getEffective() == 0){
			effective = "FALSE";
		}
		hssfRow.createCell((short)15).setCellValue(new HSSFRichTextString(effective));

		hssfRow.createCell((short)17).setCellValue(
				new HSSFRichTextString(data.getBankProvinceAndRegion()));
		hssfRow.createCell((short)18).setCellValue(
				new HSSFRichTextString(data.getCurrency() == null ? "" :(data.getCurrency().equals("RMB") ? "CNY" : data.getCurrency())));
		
		String bankType = "TRUE";
		if(data.getBankType() == 1){
			bankType = "FALSE";
		}
		hssfRow.createCell((short)19).setCellValue(new HSSFRichTextString(bankType));
		hssfRow.createCell((short)20).setCellValue(
				new HSSFRichTextString(data.getJointCode()));
		hssfRow.createCell((short)21).setCellValue(
				new HSSFRichTextString(data.getInstitutionNo()));
		hssfRow.createCell((short)22).setCellValue(
				new HSSFRichTextString(data.getCnaps()));
		//银行是否有效
		String bankValid = "true";
		if(data.getBankStatus() != 0){
			bankValid = "false";
		}
		hssfRow.createCell((short)23).setCellValue(
				new HSSFRichTextString(bankValid));
		//省市和地区
		hssfRow.createCell((short)24).setCellValue(
				new HSSFRichTextString(data.getRegion()));
		//城市
		hssfRow.createCell((short)25).setCellValue(
				new HSSFRichTextString(data.getCity()));
		//组代码
		hssfRow.createCell((short)26).setCellValue(
				new HSSFRichTextString(data.getGroupKey()));
		//申请人
		hssfRow.createCell((short)27).setCellValue(
				new HSSFRichTextString(data.getApplient_name()));
		String flag = null;
		switch (history.getClassification()) {
		case ModificationHistory.CLASS_INSERT:
			flag = "INSERT";
			break;
		case ModificationHistory.CLASS_UPDATE:
			if (data.getBankStatus() == BankAccount.STATUS_ALIVE) {
				flag = "UPDATE";
			} else if (data.getBankStatus() == BankAccount.STATUS_DELETED) {
				flag = "DELETE";
				vendorService.deleteBankAccountPhysically(data.getBankAccountId());
			}
			break;
		case ModificationHistory.CLASS_DELETE:
			flag = "DELETE";
			break;
		case ModificationHistory.CLASS_FREEZE:
			flag = "FREEZE";
			break;
		case ModificationHistory.CLASS_UNFREEZE:
			flag = "UNFREEZE";
			break;
		}
		// 状态
//		hssfRow.createCell((short)27).setCellValue(
//				new HSSFRichTextString(flag));
	}
	
//	@SuppressWarnings("deprecation")
//	private void write(User user, HSSFRow hssfRow) {
//		hssfRow.createCell((short)0).setCellValue(
//				new HSSFRichTextString( user.getId()));
//		hssfRow.createCell((short)1).setCellValue(
//				new HSSFRichTextString(user.getName()));
//		hssfRow.createCell((short)2).setCellValue(
//				new HSSFRichTextString(user.getEmail()));
//		if(Utils.ROLE_L2.equals(user.getAuthority())){
//			List<String> divisions = dataService.getL2Divisions(user.getId());
//			if(divisions != null && divisions.size() != 0 ){
//				String divisionsToExcle = "";
//				boolean costcenterHasL2Division = false;
//				for(String division : divisions){
//					divisionsToExcle += division+",";
//					if(division.equals(user.getDivision())){
//						costcenterHasL2Division = true;
//					}
//				}
//				if(costcenterHasL2Division){
//					divisionsToExcle = divisionsToExcle.substring(0,divisionsToExcle.length() -1);
//				}else{
//					divisionsToExcle += user.getDivision();
//				}
//				hssfRow.createCell((short)3).setCellValue(
//						new HSSFRichTextString(divisionsToExcle));
//			}else{
//				hssfRow.createCell((short)3).setCellValue(
//						new HSSFRichTextString(user.getDivision()));
//			}
//		}else{
//			hssfRow.createCell((short)3).setCellValue(
//					new HSSFRichTextString(user.getDivision()));
//		}
//		hssfRow.createCell((short)4).setCellValue(
//				new HSSFRichTextString(user.getAuthority()));
//		hssfRow.createCell((short)5).setCellValue(
//				new HSSFRichTextString(String.valueOf(user.isValid())));
//	}
	
	@RequestMapping(value="/interface/download/{id}", method=RequestMethod.GET)
	public void downloadAttachment(@PathVariable("id") long id, 
			HttpServletResponse response) {
		Utils.checkHasRoleHD();
		
		response.setContentType("application/vnd.ms-excel");
		response.setHeader(
		        "Content-Disposition",
		        "filename=\"" + "VendorModification_"+DateFormat.getDateInstance().format(new Date())+".xls" + "\"");
		
		try {
			OutputStream out = response.getOutputStream();
			out.write(dataService.getInterfaceFileData(id));
			out.flush();
			out.close();
		} catch (IOException e) {
			throw new IllegalStateException(e);
		}		
		
	}
	
	@RequestMapping(value="/system/setting", method=RequestMethod.POST)
	public String updateSystemSetting(SystemSetting setting,  BindingResult result, Model model) {
		Utils.checkHasRoleHD();
		
		setting.validate(result);
		
		if(result.hasErrors()){
			model.addAttribute("setting", setting);
			model.addAttribute("errorSpan", true);
			return "data/system_setting";
		}else{
			dataService.updateSystemSetting(setting);
			return "redirect:/data/system/setting";
		}
	}

	/**
	 * 
	 * @Title:       goSearchVendorForMaintainnance 
	 * @Description:  ( 响应数据管理页面  供应商数据维护按钮 ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/vendor/search", method=RequestMethod.GET)
	public String goSearchVendorForMaintainnance(VendorSearchCriteria criteria, Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("criteria", criteria);
		
		return "vendor/search_vendor_form_for_maintainance";
	}
	
	/**
	 * 
	 * @Title:       goDownloadVendor 
	 * @Description:  ( 数据管理-供应商数据下载按钮响应 ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/vendor/download", method=RequestMethod.GET)
	public String goDownloadVendor(VendorSearchCriteria criteria, Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("criteria", criteria);
		
		return "vendor/download_vendor";
	}
	
	/**
	 * 
	 * @Title:       downloadVendor 
	 * @Description:  ( 响应供应商数据下载页面下载按钮下载功能  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @param response
	 * @param        @throws IOException     
	 * @return       void     
	 * @throws 
	 * @Deprecated:	第二次变更，下载功能移至供应商查询里，replace by <code>VendorController</code><method></method>
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@Deprecated
	@RequestMapping(value="/vendor/download", method=RequestMethod.POST)
	public void downloadVendor(VendorSearchCriteria criteria, Model model, HttpServletResponse response) throws IOException {
		//全员都可以下载
		//Utils.checkHasRoleHD();
		// 下载所有登记状态供应商
		List<InterfaceData> datas = dataService.findInterfaceData(criteria);
		//使用模板
		HSSFWorkbook workBook = new HSSFWorkbook(getClass().getClassLoader().getResourceAsStream("vendors.xls"));
		workBook.setSheetName(0, "Vendors");
		HSSFSheet sheet = workBook.getSheetAt(0);
		// follow the template at the third line begin.
		int row = 2;
		
		for (InterfaceData data : datas){
			String date = evaluationService.getLatestEval(data.getVendorId()); //
			write(date, data, sheet.createRow(row++));
		}
		response.setContentType("application/vnd.ms-excel");
		response.setHeader(
		        "Content-Disposition",
		        "filename=\"" + "vendors.xls" + "\"");
		
		OutputStream out = response.getOutputStream();
		workBook.write(out);
		out.flush();
		out.close();
	}
	
	@SuppressWarnings("deprecation")
	private void write(String date, InterfaceData data, HSSFRow hssfRow) {
		String vendorCode = (data.getVendorCode() == null || "".equals(data.getVendorCode())) 
								? "" 
								: data.getVendorCode();
		String vendorSubCode = "00".equals(data.getVendorSubCode()) ? "" : "-" + data.getVendorSubCode();
		//郑同 2012年7月12日15:38:08 modif 插入所属公司0、业务类型3、是否评估4、是否review字段
		//所属公司
		String superCompany = "";
		switch (data.getSuperCompany()) {
    		case 1:
    			superCompany = "SSCS";		break;
    		case 2:
    			superCompany = "SSGE";		break;
    		case 3:
    			superCompany = "SSV";		break;
    		case 4:
                superCompany = "SEW";       break;
            case 5:
                superCompany = "SDPW";       break;
            case 6:
                superCompany = "SPDH";       break;
            case 7:
                superCompany = "SEH";       break;
            default:
                superCompany = "SSCS";
		}
		hssfRow.createCell((short)0).setCellValue(
				new HSSFRichTextString( superCompany ));		
		// 供应商编号
		hssfRow.createCell((short)1).setCellValue(
				new HSSFRichTextString( vendorCode + vendorSubCode));
		// 名称
		hssfRow.createCell((short)2).setCellValue(
				new HSSFRichTextString(data.getName()));
		//业务类型
		String businessType = "";
		switch (data.getBusinessType()) {
		case 10:
			businessType = "非贸（仓库）";		break;
		case 11:
			businessType = "非贸（运输）";		break;
		case 12:
			businessType = "非贸（仓库&运输）";break;
		case 13:
			businessType = "非贸（海关）";		break;
		case 14:
			businessType = "非贸（仓库&海关）";break;
		case 15:
			businessType = "非贸（运输&海关）";break;
		case 16:
			businessType = "非贸（仓库&运输&海关）";break;
		case 1:
			businessType = "贸易";		break;
		case 2:
			businessType = "共通";		break;
		default:
			break;
		}
		hssfRow.createCell((short)3).setCellValue(
						new HSSFRichTextString(businessType ));
		//获取供应商的有效评估信息数量
		int evaCount = evaluationService.hasEvals(data.getVendorId(),Evaluation.STATUS_IS_EFFECTIVE);
		//是否评估	
		hssfRow.createCell((short)4).setCellValue(
				new HSSFRichTextString( evaCount >= 1?"是":"否" ));		
		//是否Review
		hssfRow.createCell((short)5).setCellValue(
				new HSSFRichTextString( evaCount >= 2?"是":"否"));		

		// 传真
		hssfRow.createCell((short)6).setCellValue(
				new HSSFRichTextString(data.getFax()));
		// 银行
		hssfRow.createCell((short)7).setCellValue(
				new HSSFRichTextString(data.getBankName()));
		// 帐号
		hssfRow.createCell((short)8).setCellValue(
				new HSSFRichTextString(data.getBankAccount()));
		
		// 是否本地银行
		String bankCityType = "FALSE";
		if(data.getBankCityType() == 1){
			bankCityType = "TRUE";
		}
		hssfRow.createCell((short)9).setCellValue(new HSSFRichTextString(bankCityType));
		// 分组
		hssfRow.createCell((short)10).setCellValue(new HSSFRichTextString(data.getVendorType()));
		// 类别
		hssfRow.createCell((short)11).setCellValue(new HSSFRichTextString(data.getVendorGroup()));
		// 联系电话
		hssfRow.createCell((short)12).setCellValue(
				new HSSFRichTextString(data.getTel()));
		// 联系地址
		hssfRow.createCell((short)13).setCellValue(
				new HSSFRichTextString(data.getAddress()));
		// 联系人
		hssfRow.createCell((short)14).setCellValue(
				new HSSFRichTextString(data.getContactPerson()));
		// 银行地址	
		hssfRow.createCell((short)15).setCellValue(
				new HSSFRichTextString(data.getBankAddress()));
		// Swift代码
		hssfRow.createCell((short)16).setCellValue(
				new HSSFRichTextString(data.getSwiftCode()));
		// 第17列为银行Routing代码
		// 下载变更数据时没有，所以不对应
		// 收款期限
		hssfRow.createCell((short)18).setCellValue(new HSSFRichTextString(data.getPaymentTerm()));
		// 是否有效
		String effective = "TRUE";
		if(data.getFreeze() != null && "1".equals(data.getFreeze())){
			effective = "FALSE";
		}
		hssfRow.createCell((short)19).setCellValue(new HSSFRichTextString(effective));
		// 账户类型
		String vendorAccountType = data.getVendorAccountType();
		vendorAccountType = vendorAccountType == null || vendorAccountType.equals("vendor") ? "" : vendorAccountType;
		hssfRow.createCell((short)20).setCellValue(new HSSFRichTextString(vendorAccountType));
		// 银行国家	
		hssfRow.createCell((short)21).setCellValue(
				new HSSFRichTextString(data.getBankProvinceAndRegion()));
		// 省市和地区
		hssfRow.createCell((short)22).setCellValue(
				new HSSFRichTextString(data.getRegion()));
		// 城市
		hssfRow.createCell((short)23).setCellValue(
				new HSSFRichTextString(data.getCity()));
		// 组代码
		hssfRow.createCell((short)24).setCellValue(
				new HSSFRichTextString(data.getGroupKey()));
		// 申请人
		hssfRow.createCell((short)25).setCellValue(
				new HSSFRichTextString(data.getApplient_name()));
		// 最新评估时间列
		hssfRow.createCell((short)26).setCellValue(
				new HSSFRichTextString(date));
	}

	/**
	 * 
	 * @Title:       searchVendorForMaintainnance 
	 * @Description:  ( 响应供应商数据维护页面查询功能 （查询按钮和page分页查询）  ) 
	 * @param        @param criteria
	 * @param        @param model
	 * @param        @return     
	 * @return       String     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 7, 2012
	 */
	@RequestMapping(value="/vendor/search", method=RequestMethod.POST)
	public String searchVendorForMaintainnance(VendorSearchCriteria criteria, Model model) {
		Utils.checkHasRoleHD();
		
		model.addAttribute("criteria", criteria);
		
		if (criteria.getText() == null) {
			model.addAttribute("criteria", criteria);
			return "vendor/search_vendor_form_for_maintainance";
		}
		
		model.addAttribute("editable", true);
		
		int pages = -1;
		List<VendorBasicInfo> vendors = null;

		//设置sql条件
		criteria.sb = new StringBuffer();
		criteria.sb.append("WHERE (status = 5 OR status = 20 OR status = 30 OR status = 40) " );
		
		/*拼接的是sql写一个方法终结*/
		if(criteria.getText() != null && !"".equals(criteria.getText().trim())){
			if (criteria.getType().equals(VendorSearchCriteria.TYPE_NAME)) {
				/* 拼接sql  => name */
				criteria.and().Left()
				 		 .vendorChineseNameLike(criteria.getText().trim()).
				 		 or()
				 		 .vendorEnglishNameLike(criteria.getText().trim())
				 		 			.Right();
				
			} else if (criteria.getType().equals(VendorSearchCriteria.TYPE_CODE)) {//
				criteria.and().vendorCodeEqual(criteria.getText().trim());
//				criteria.and().vendorTypeEqual(criteria.getText().trim());
			} else if (criteria.getType().equals(VendorSearchCriteria.TYPE_NUMBER)){
				criteria.and().numberEqual(criteria.getText().trim());
			}
		}
		
		/* 拼接sql  => vendor_type = category */
		if(3 != criteria.getCategory()){
			criteria.and().vendorTypeEqual(criteria.getCategory());
		}
		
		/* 拼接sql  => super_company = superCompany */
		if(0 != criteria.getSuperCompanyType()){
			criteria.and().superCompanyEqual(criteria.getSuperCompanyType());
		}
		//分页查询
		int searchVendorCount = vendorService.searchVendorCountForAll(criteria.sb.toString());

		criteria.sb.append("  ORDER BY vendor_code " );
		criteria.sb.append("  LIMIT "+criteria.getRecordFrom()+", " + 20 );
		
		vendors = vendorService.searchFullVendorInfoForAll(criteria.sb.toString());
		pages = vendors.size() > 0 ? (searchVendorCount% 20==0?searchVendorCount/ 20-1:searchVendorCount/ 20) : 0;

		for (VendorBasicInfo vbi : vendors) {
			vbi.setHasEvaluation(evaluationService.hasLatestEvaluationByVendorId(vbi.getVendorId()));
		}

		if (vendors != null && vendors.size() > 0) {
			model.addAttribute("vendors", vendors);
			model.addAttribute("pages", pages);
		}
		
		return "vendor/search_vendor_form_for_maintainance";
	}
	
    @RequestMapping(value = "/down/cert", method = RequestMethod.GET)
    public void downloadAttachment(HttpServletResponse response,
            HttpServletRequest request) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException,
            FileNotFoundException, CertificateException, KeyStoreException,
            SignatureException, IOException {
        
        VndMgmntUser user = Utils.getUser();
        Map<String, Object> row = new HashMap<String, Object>();
        row.put("usercompany", user.getUserCompany());
        row.put("division", user.getDivision());
        row.put("id", user.getUsername());
        row.put("password", user.getPassword());
        ByteArrayOutputStream outputStream = dataService.createPKCS12(row);
        // save PKCS12
//        Map<String, Object> pkcs12Row = new HashMap<String, Object>();
//        pkcs12Row.put("userid", user.getUsername());
//        pkcs12Row.put("data", outputStream.toByteArray());
//        Date startDate = new Date(System.currentTimeMillis());
//        Date endDate = Utils.getAfterOneYearDate(startDate);
//        pkcs12Row.put("date", endDate);
//        dataService.saveCert(pkcs12Row);
        
        response.setContentType("application/x-pkcs12");
        response.setHeader("Content-Disposition",
                "filename=\"" + user.getUsername() + ".p12\"");
        
        try {
            OutputStream out = response.getOutputStream();
            out.write(outputStream.toByteArray());
            out.flush();
            out.close();
        } catch (IOException e) {
            throw new IllegalStateException(e);
        } finally {
//            user.setCanDownCert(false);
        }
        
    }

    @RequestMapping(value = "/user/reset/{id}", method = RequestMethod.GET)
    public void resetUserbyId(@PathVariable("id") String id,
            HttpServletResponse response,
            HttpServletRequest request) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException,
            FileNotFoundException, CertificateException, KeyStoreException,
            SignatureException, IOException {
        Utils.checkHasRoleHD();
        User user = dataService.getUser(id);
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("-------------------");
            Utils.logger.debug("UserId: " + user.getId());
        }
        //final VndMgmntUser user = (VndMgmntUser) userService.loadUserByUsername(receiverUserId);
        Map<String, Object> row = new HashMap<String, Object>();
        row.put("usercompany", user.getUserCompanyName());
        row.put("division", user.getDivision());
        row.put("id", user.getId());
        row.put("password", user.getPassword());
        
        ByteArrayOutputStream outputStream = dataService.createPKCS12(row);
        
        // create file
        File file = new File( id+".p12" ); 
        final SystemSetting setting = dataService.getSystemSetting();
        
        DataOutputStream to = new DataOutputStream(new FileOutputStream(file)); 
        outputStream.writeTo(to); 
        Map<String, String> templateModel = new HashMap<String, String>();
        templateModel.put("id", id);
        templateModel.put("password", user.getPassword());
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("Start send mail.");
            Utils.logger.debug("Start send ID&PASSWORD mail.");
        }
        
        dataService.sendMail(id, 
                setting.getPasswordNotificationMailSubject(), 
                setting.getPasswordNotificationMailTemplate(), 
                templateModel, null);
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("Start send P12 mail.");
        }
        
        dataService.sendMail(id, 
                setting.getCertattMailSubject(), 
                setting.getCertattMailTemplate(), 
                templateModel, file);
        
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("End send mail.");
        }
        
        outputStream.close();
        to.close();
        file.delete();
    }
    
    @RequestMapping(value = "/user/reset", method = RequestMethod.GET)
    public void resetUser(
            HttpServletResponse response,
            HttpServletRequest request) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException,
            FileNotFoundException, CertificateException, KeyStoreException,
            SignatureException, IOException {
        Utils.checkHasRoleHD();
        List<User> users = dataService.getAllValidUsersList();
        if(Utils.logger.isDebugEnabled()){
            Utils.logger.debug("Reset users' NO.: "+users.size());
        }
        for(User user : users){
            resetUserbyId(user.getId(), response, request);
        }
    }
}
