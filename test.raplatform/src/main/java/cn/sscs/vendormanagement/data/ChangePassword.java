package cn.sscs.vendormanagement.data;

import java.util.regex.Pattern;

import org.springframework.validation.BindingResult;

import cn.sscs.vendormanagement.Utils;
import cn.sscs.vendormanagement.VndMgmntUser;

/**
 * 
 * @author kazzy
 *
 */
public class ChangePassword {

	private String newPassword = null;
	
	private String passwordConfirmation = null;

	/**
	 * @return the newPassword
	 */
	public String getNewPassword() {
		return newPassword;
	}

	/**
	 * @param newPassword the newPassword to set
	 */
	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	/**
	 * @return the passwordConfirmation
	 */
	public String getPasswordConfirmation() {
		return passwordConfirmation;
	}

	/**
	 * @param passwordConfirmation the passwordConfirmation to set
	 */
	public void setPasswordConfirmation(String passwordConfirmation) {
		this.passwordConfirmation = passwordConfirmation;
	}

	/**
	 * 
	 * @param result
	 */
	void validate(BindingResult result) {
		if (newPassword == null || passwordConfirmation == null
				|| "".equals(newPassword) || "".equals(passwordConfirmation)) {
			result.rejectValue("newPassword", "error.not.a.same");
		}

		
		if (!newPassword.equals(passwordConfirmation)) {
			result.rejectValue("newPassword", "error.not.a.same");
		}
		
		String regex1 = "^[a-zA-Z0-9\\D]{6,50}$";
		// have char
		String regex2 = "[a-zA-Z]+";
		// have number
		String regex3 = "[0-9]+";
		// have character
		String regex4 = "[\\D]+";
		// 23 24 34
		boolean match1 = Pattern.compile(regex2).matcher(newPassword).find() && Pattern.compile(regex3).matcher(newPassword).find();
		boolean match2 = Pattern.compile(regex2).matcher(newPassword).find() && Pattern.compile(regex4).matcher(newPassword).find();
		boolean match3 = Pattern.compile(regex3).matcher(newPassword).find() && Pattern.compile(regex4).matcher(newPassword).find();
		
		if(!Pattern.compile(regex1).matcher(newPassword).find()){
			result.rejectValue("newPassword", "error.invalid.info");
		}else if(!(match1 && match2 && match3)){
			result.rejectValue("newPassword", "error.invalid.info");
		}
        
//		if(!Pattern.compile(regex1).matcher(newPassword).find() 
//        		|| !Pattern.compile(regex2).matcher(newPassword).find()){
//        	result.rejectValue("newPassword", "error.invalid.info");
//        }
		VndMgmntUser user = Utils.getUser();
        //not same to name & not same to id
		if(user.getName().equals(newPassword) || user.getUsername().equals(newPassword)){
			result.rejectValue("newPassword", "error.sameto.nameorid");
		}
		
	}
	
}
