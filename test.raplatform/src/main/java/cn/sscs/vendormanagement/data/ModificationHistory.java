package cn.sscs.vendormanagement.data;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TimeZone;

public class ModificationHistory {

	public static final int CLASS_INSERT = 10;
	
	public static final int CLASS_UPDATE = 20;
	
	public static final int CLASS_DELETE = 30;
	public static final int CLASS_BANK_ACCOUNT_DELETE = 40;
	public static final int CLASS_FREEZE = 50;
	public static final int CLASS_UNFREEZE = 60;
	
	
	private long id = -1;
	
	private long vendorId = -1;
	
	private String vendorCode = null;
	
	private long bankAccountId = -1;
	
	private int classification = -1;
	
	private Date registeredDate = null;

	/**
	 * 
	 */
	public ModificationHistory(){}
	
	
	/**
	 * 
	 * @param vendorId
	 * @param bankAccountId
	 * @param classification
	 */
	public ModificationHistory(long vendorId, long bankAccountId, int classification){
		this.vendorId = vendorId;
		this.bankAccountId = bankAccountId;
		this.classification = classification;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}
	
	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}
	
	/**
	 * @return the vendorId
	 */
	public long getVendorId() {
		return vendorId;
	}

	/**
	 * @param vendorId the vendorId to set
	 */
	public void setVendorId(long vendorId) {
		this.vendorId = vendorId;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getVendorCode() {
		return vendorCode;
	}

	/**
	 * 
	 * @param vendorCode
	 */
	public void setVendorCode(String vendorCode) {
		this.vendorCode = vendorCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public long getBankAccountId() {
		return bankAccountId;
	}
	
	/**
	 * 
	 * @param bankAccountId
	 */
	public void setBankAccountId(long bankAccountId) {
		this.bankAccountId = bankAccountId;
	}
	
	/**
	 * @return the classification
	 */
	public int getClassification() {
		return classification;
	}

	/**
	 * @param classification the classification to set
	 */
	public void setClassification(int classification) {
		this.classification = classification;
	}

	/**
	 * 
	 * @return
	 */
	public Date getRegisteredDate() {
		
		return registeredDate;
	}
	
	/**
	 * 
	 * @param registeredDate
	 */
	public void setRegisteredDate(Date registeredDate) {
		this.registeredDate = registeredDate;
	}
	
	/**
	 * 
	 * @return
	 */
	Map<String, Object> toMap() {
		Map<String, Object> map = new HashMap<String, Object>();
		
		map.put("vendor_id", vendorId);
		map.put("bank_account_id", bankAccountId);
		map.put("classification", classification);
		map.put("registered_date", registeredDate);
		
		return map;
	}
	public String getShanghaiRegisteredDate(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd kk:mm:ss ");
		sdf.setTimeZone(TimeZone.getTimeZone("Asia/Shanghai"));
		return sdf.format(registeredDate);
	}
}
