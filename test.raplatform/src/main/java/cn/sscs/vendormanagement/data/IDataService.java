package cn.sscs.vendormanagement.data;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SignatureException;
import java.security.cert.CertificateException;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.mail.MessagingException;

import cn.sscs.vendormanagement.User;
import cn.sscs.vendormanagement.vendor.VendorSearchCriteria;


/**
 * 
 * @author kazzy
 *
 */
public interface IDataService {

	/**
	 * 
	 * @param upload
	 * @throws SignatureException 
	 * @throws KeyStoreException 
	 * @throws CertificateException 
	 * @throws NoSuchProviderException 
	 * @throws NoSuchAlgorithmException 
	 * @throws InvalidKeyException 
	 */
    void registerUsers(Upload upload) throws InvalidKeyException,
            NoSuchAlgorithmException, NoSuchProviderException,
            CertificateException, KeyStoreException, SignatureException;

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	int searchUserCount(UserSearchCriteria criteria);

	/**
	 * 
	 * @param criteria
	 * @return
	 */
	List<User> searchUsers(UserSearchCriteria criteria);

	/**
	 * 
	 * @param username
	 * @param newPassword
	 */
	void changePassword(String username, String newPassword);

	/**
	 * 
	 * @param modificationHistory
	 */
	void storeModificationHistory(ModificationHistory modificationHistory);

	/**
	 * 
	 * @return
	 */
	List<ModificationHistory> getModificationHistories();

	/**
	 * 
	 * @param unneededMaxId
	 */
	void clearModificationHistory(long unneededMaxId);

	/**
	 * 
	 * @param data
	 * @param createdDate
	 */
	long storeInterfaceFile(byte[] data, Date createdDate);

	/**
	 * 
	 * @return
	 */
	List<InterfaceFile> getInterfaceFiles();

	/**
	 * 
	 * @param id
	 * @return
	 */
	byte[] getInterfaceFileData(long id);

	/**
	 * 
	 * @return
	 */
	SystemSetting getSystemSetting();

	/**
	 * 
	 * @param systemSetting
	 */
	void updateSystemSetting(SystemSetting systemSetting);

	/**
	 * 
	 * @param receiverUserId
	 * @param subject
	 * @param template
	 * @param templateModel
	 * @throws MessagingException 
	 */
	void sendMail(String receiverUserId, String subject, String template,
			Map<String, String> templateModel, File file);

	/**
	 * 
	 * @param id
	 * @return
	 */
	List<InterfaceData> findInterfaceDataByVendorId(long id);

	/**
	 * 
	 * @return
	 */
	List<ModificationHistory> getBankAccountDeletedModificationHistories(long vendorId);

	List<String> findWorkflowUsers(String l1, String l2, String ceo, String hd, String fm, String dm, String processor);

	List<User> getAllUserInfo();

	List<String> getL2Divisions(String id);

	/**
	 * 
	 * @Title:       findInterfaceData 
	 * @Description: 调整方法内部 增加查询字段   此方法是下载供应商数据查询方法 
	 * @param        @param category
	 * @param        @return     
	 * @return       List<InterfaceData>     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 12, 2012
	 */
	List<InterfaceData> findInterfaceData(VendorSearchCriteria criteria);

	List<String> getL1Divisions(String id);

    ByteArrayOutputStream createPKCS12(Map<String, Object> row)
            throws NoSuchAlgorithmException, NoSuchProviderException,
            FileNotFoundException, CertificateException, IOException, KeyStoreException, InvalidKeyException, SignatureException;

    void saveCert(Map<String, Object> row);

    User getUser(String id);

    List<User> getAllValidUsersList();
}
