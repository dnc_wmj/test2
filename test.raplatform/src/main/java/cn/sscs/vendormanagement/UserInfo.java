package cn.sscs.vendormanagement;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * 
 * @author mingjian
 *
 */
public class UserInfo implements Serializable{
    
    private static final long serialVersionUID = -7207706145586487247L;
    private String id;
    private String userCompany;
    private String email;
    private boolean valid;
    private List<Authority> authorityList = null;
    
    public String getId() {
        return id;
    }
    
    public void setId(String id) {
        this.id = id;
    }
    
    public String getUserCompany() {
        return userCompany;
    }
    
    public void setUserCompany(String userCompany) {
        this.userCompany = userCompany;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }
    
    public boolean isValid() {
        return valid;
    }
    
    public void setValid(boolean valid) {
        this.valid = valid;
    }
    
    public List<Authority> getAuthorityList() {
        return authorityList;
    }
    
    public void setAuthorityList(List<Authority> authorityList) {
        this.authorityList = authorityList;
    }
    public String getDivisionInfo(){
        String dInfo = "";
        List<Integer> divisionList = new ArrayList<Integer>();
        for(Authority authority : authorityList){
            int dbDivision = authority.getDivision();
            boolean repeatFlag = false;
            for (int division : divisionList) {
                if(dbDivision == division){
                    repeatFlag = true;
                    break;
                }
            }
            if(!repeatFlag){
                if(divisionList.size() != 0){
                    dInfo += ", ";
                }
                divisionList.add(dbDivision);
                dInfo += dbDivision +"";
            }
        }
        return dInfo;
    }
}
