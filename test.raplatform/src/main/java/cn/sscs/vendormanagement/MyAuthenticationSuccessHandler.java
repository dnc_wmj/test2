package cn.sscs.vendormanagement;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.security.web.util.RedirectUrlBuilder;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;
/**
 * This class implements AuthenticationSuccessHandler and override onAuthenticationSuccess method.
 * The method named onAuthenticationSuccess will be called when the authentication is success.
 * 
 * @author mingjian
 *
 */
public class MyAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth) throws IOException,
			ServletException {
		VndMgmntUser user = (VndMgmntUser) auth.getPrincipal();
		WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext());
		DataSource dataSource = (DataSource) webApplicationContext.getBean("dataSource");
		SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
		String sql = "update user set error_times = 0 where id = ?";
		template.update(sql, user.getUsername());
//		FilterChain chain = (FilterChain)request.getAttribute("filterChain");
//		chain.doFilter(request, response);
//		String url = (String)request.getSession().getAttribute("goto");  
//        request.getSession().removeAttribute("goto");  
        String returnUrl = buildHttpReturnUrlForRequest(request);
		response.sendRedirect(returnUrl);
		
	}
	protected String buildHttpReturnUrlForRequest(HttpServletRequest request)  
            throws IOException, ServletException {  
  
  
        RedirectUrlBuilder urlBuilder = new RedirectUrlBuilder();  
        urlBuilder.setScheme("http");  
        urlBuilder.setServerName(request.getServerName());  
        urlBuilder.setPort(request.getServerPort());  
        urlBuilder.setContextPath(request.getContextPath());  
        urlBuilder.setServletPath(request.getServletPath());  
        urlBuilder.setPathInfo(request.getPathInfo());  
        urlBuilder.setQuery(request.getQueryString());  
  
        return urlBuilder.getUrl();  
    } 
}
