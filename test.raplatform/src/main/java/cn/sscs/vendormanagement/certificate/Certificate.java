package cn.sscs.vendormanagement.certificate;


public class Certificate {
//    private String attrCN = "CN";               // 用户名 common name
//    private String attrOU = "OU";               // 组织 organizational unit name
//    private String attrO = "O";                 // 公司 organization
//    private String attrC = "C";                 // 国家 country code
//    private String attrL = "L";                 // 市 locality name
//    private String attrST = "ST";               // 省市 state, or province name
//    private String attrEMAIL = "EMAILADDRESS";  // email
    private int attrSerialNumber = -1;          // 序列号
    
//    public String getAttrCN() {
//        return attrCN;
//    }
//    
//    public void setAttrCN(String attrCN) {
//        this.attrCN = attrCN;
//    }
//    
//    public String getAttrOU() {
//        return attrOU;
//    }
//    
//    public void setAttrOU(String attrOU) {
//        this.attrOU = attrOU;
//    }
//    
//    public String getAttrO() {
//        return attrO;
//    }
//    
//    public void setAttrO(String attrO) {
//        this.attrO = attrO;
//    }
//    
//    public String getAttrC() {
//        return attrC;
//    }
//    
//    public void setAttrC(String attrC) {
//        this.attrC = attrC;
//    }
//    
//    public String getAttrL() {
//        return attrL;
//    }
//    
//    public void setAttrL(String attrL) {
//        this.attrL = attrL;
//    }
//    
//    public String getAttrST() {
//        return attrST;
//    }
//    
//    public void setAttrST(String attrST) {
//        this.attrST = attrST;
//    }
//    
//    public String getAttrEMAIL() {
//        return attrEMAIL;
//    }
//    
//    public void setAttrEMAIL(String attrEMAIL) {
//        this.attrEMAIL = attrEMAIL;
//    }
    
    public int getAttrSerialNumber() {
        return attrSerialNumber;
    }
    
    public void setAttrSerialNumber(int attrSerialNumber) {
        this.attrSerialNumber = attrSerialNumber;
    }
}
