package cn.sscs.vendormanagement;

import java.awt.BasicStroke;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.imageio.ImageIO;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import cn.sscs.vendormanagement.workflow.WorkflowRule;

@Controller
public class SealController {
	
	@Autowired
	ExtendedJdbcUserDetailsManager userService = null;
	
	/**
	 * 
	 * @param workflowId
	 * @param response
	 * @return
	 * @throws IOException 
	 * @throws FileNotFoundException 
	 */
	@RequestMapping(value="/seal/{userId}/{date}/{result}", method=RequestMethod.GET)
	public void getSeal(@PathVariable("userId") String userId,
			@PathVariable("date") long date,
			@PathVariable("result") int result,
			HttpServletResponse response) throws IOException {
		
		BufferedImage im = new BufferedImage(88,88, BufferedImage.TYPE_INT_RGB);
		Graphics2D g = im.createGraphics();
		
		g.setFont(new Font("宋体", Font.PLAIN, 12));
		
		Rectangle2D rect = new Rectangle2D.Double(0, 0, 88, 88);
		g.setColor(Color.WHITE);
		g.fill(rect);
		g.draw(rect);
		
		Ellipse2D ellipse = new Ellipse2D.Double(4, 4, 80, 80);
		g.fill(ellipse);
		g.setColor(Color.RED);
		g.setStroke(new BasicStroke(4));
		g.draw(ellipse);
		
		g.setStroke(new BasicStroke(3));
		Line2D line = new Line2D.Double(10, 30, 78, 30);
		g.setColor(Color.RED);
		g.fill(line);
		g.draw(line);

		line = new Line2D.Double(10, 58, 78, 58);
		g.setColor(Color.RED);
		g.fill(line);
		g.draw(line);
		
		VndMgmntUser user = (VndMgmntUser) userService.loadUserByUsername(userId);

		int x = -1;
		switch(user.getName().length()) {
		case 2:
			x = 32;
			break;
		case 3:
			x = 27;
			break;
		case 4:
			x = 22;
			break;
		}
		
		g.drawString(user.getName(), x, 23);
		
		g.drawString(new SimpleDateFormat("yyyy/MM/dd").format(new Date(date)), 8, 48);
		
		String label = null;
		int leftPadding = -1;
		if (result == WorkflowRule.RESULT_APPROVED) {
			label = "Approved";
			leftPadding = 14;
		} else {
			label = "Denied";
			leftPadding = 22;
		}
		g.drawString(label, leftPadding, 72);
		
        g.dispose();
        
        response.setContentType("image/jpeg");
        
        ImageOutputStream ios =
                ImageIO.createImageOutputStream(response.getOutputStream());
        ImageWriter iw = (ImageWriter)ImageIO.
                getImageWritersByFormatName("jpeg").next();
       
        
        iw.setOutput(ios);
        iw.write(im);   
		
        
	}
	
}
