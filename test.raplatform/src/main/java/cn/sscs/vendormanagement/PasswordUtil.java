package cn.sscs.vendormanagement;

import java.io.StringWriter;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

public class PasswordUtil {

private static PasswordUtil instance = null;
	
	static final char[] chars =  new char[('z' - 'a') + ('Z' - 'A') + 2];
	
	private SecureRandom random = null;
	
	private byte[] seed = null;
	
	public static PasswordUtil getInstance() {
		if (instance == null) {
			instance = new PasswordUtil();
		}
		
		return instance;
	}
	
	private PasswordUtil() {
		try {
			random = SecureRandom.getInstance("SHA1PRNG");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException(e);
		}
		seed = random.generateSeed(10);
		
		for (char i=0; i<='z'-'a'; i++ ) {
			chars[i] = (char) ('a' + i);
		}
		
		for (char i=0; i<='Z'-'A'; i++ ) {
			chars[i+26] = (char) ('A' + i);
		}

	}
	
	public String newPassword() {
		StringWriter writer = new StringWriter();
		random.nextBytes(seed);
		for (byte b : seed) {
			writer.append(chars[ (b & 0x7F) % 51]);
		}
		return writer.toString();
	}
}
