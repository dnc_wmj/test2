package cn.sscs.vendormanagement;

import java.io.IOException;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 * This class implements AuthenticationSuccessHandler and override onAuthenticationFailure method.
 * The method named onAuthenticationFailure will be called when the authentication is false.
 * 
 * @author mingjian
 *
 */
public class MyAuthenticationFailureHandler implements AuthenticationFailureHandler{

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response, AuthenticationException exception)
			throws IOException, ServletException {
		HttpServletResponse rsp = (HttpServletResponse) response;
		String errorInfo = "hasSigninError";
		if(exception.getAuthentication() != null){
			String username = exception.getAuthentication().getPrincipal().toString();
			WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(((HttpServletRequest) request).getSession().getServletContext());
			DataSource dataSource = (DataSource) webApplicationContext.getBean("dataSource");
			SystemBean systemBean = (SystemBean) webApplicationContext.getBean("systemBean");
			SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
			String sql_for_check_unlocked = "select count(*) from user where id = ? and valid = 1 ";
			
			int count = template.queryForInt(sql_for_check_unlocked, username);
			int times;
			if(count == 0){
				times = -1;
			}else{
				String sql_for_query_error_times = "select error_times from user where id = ? and valid = 1 ";
				times = template.queryForInt(sql_for_query_error_times, username);
			}
			if(times == -1){
				//errorInfo = "hasSigninError";
			}else if(times >= systemBean.getUpperLimitErrorLoginTimes()){
				String sql_for_update_lock_user = "update user set lock_time = ?, valid = 2 where id = ?";
				template.update(sql_for_update_lock_user, new Date(), username);
				errorInfo = "userIsLocked";
			}else{
				String sql_for_update_error_times = "update user set error_times = ? where id = ?";
				template.update(sql_for_update_error_times, ++times, username);
			}
			
			
		}
		rsp.sendRedirect("/vndmanagement/signin?"+errorInfo+"=true");
	}
	
}
