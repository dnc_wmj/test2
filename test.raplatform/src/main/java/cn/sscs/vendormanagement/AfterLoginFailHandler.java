package cn.sscs.vendormanagement;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import org.springframework.jdbc.core.simple.ParameterizedRowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcTemplate;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class AfterLoginFailHandler extends
        SimpleUrlAuthenticationFailureHandler {
    
    private String defaultFailureUrl;
    private final static String TRY_MAX_COUNT = "_TRY_MAX_COUNT";
    
    private static final String ATTR_CER = "javax.servlet.request.X509Certificate";
    private static final String CONTENT_TYPE = "text/plain;charset=UTF-8";
    private static final String DEFAULT_ENCODING = "UTF-8";
    private static final String SCHEME_HTTPS = "https";
    
    public void setDefaultFailureUrl(String defaultFailureUrl) {
        this.defaultFailureUrl = defaultFailureUrl;
    }
    
    @SuppressWarnings("deprecation")
    @Override
    public void onAuthenticationFailure(HttpServletRequest request,
            HttpServletResponse response, AuthenticationException exception)
            throws IOException, ServletException {
        // ---------------------------------------------------------
        // TODO 1检查客户浏览器是否存在dodopipe发布的证书
        // TODO 2依次匹配信任列表中的证书
        // TODO 3如果有匹配的则跳转到密码验证页面（用户名已知）
        // TODO 3.1多个证书的情况
        // TODO 3.2密码读入成功
        // TODO 3.3密码读入失败
        // ---------------------------------------------------------
        // -----------------------------------------------------------------
        
        // ------------------------------------------------------------------
        // HttpServletResponse rsp = (HttpServletResponse) response;
        String errorInfo = "hasSigninError";
        if (exception.getAuthentication() != null) {
            WebApplicationContext webApplicationContext = WebApplicationContextUtils
                    .getWebApplicationContext(((HttpServletRequest) request)
                            .getSession().getServletContext());
            DataSource dataSource = (DataSource) webApplicationContext
                    .getBean("dataSource");
            SimpleJdbcTemplate template = new SimpleJdbcTemplate(dataSource);
            String username = exception.getAuthentication().getPrincipal()
                    .toString();
            // 先判断存不存在该用户和该用户是否是有效用户
            String sql_getuser = "select valid from user_info where id = ?";
            ParameterizedRowMapper<String> mapper = new ParameterizedRowMapper<String>() {
                @Override
                public String mapRow(ResultSet rs, int rowNum)
                        throws SQLException {
                    return rs.getString("valid");
                }
            };
            List<String> valids = template.query(sql_getuser, mapper, username);
            boolean valid = false;
            for(String v : valids){
                if("1".equals(v)){
                    valid = true;
                    break;
                }
            }
            if (valid) {
                
                HttpSession session = request.getSession();
                Integer tryCount = (Integer) session.getAttribute(username
                        + TRY_MAX_COUNT);
                if (tryCount == null) {
                    session.setAttribute(username + TRY_MAX_COUNT, 1);// 增加失败次数
                } else {
                    tryCount = tryCount + 1;
                    SystemBean systemBean = (SystemBean) webApplicationContext
                            .getBean("systemBean");
                    if (tryCount > systemBean.getUpperLimitErrorLoginTimes() - 1) {
                        // 超过上限，锁定账户
                        String sql_for_update_lock_user = "update user set lock_time = ? where id = ?";
                        String sql_for_update_lock_user2 = "update user_info set valid = 2 where id = ? and valid = 1";
                        template.update(sql_for_update_lock_user, new Date(), username);
                        template.update(sql_for_update_lock_user2, username);
                        errorInfo = "userIsLocked";
                    } else {
                        session.setAttribute(username + TRY_MAX_COUNT, tryCount);
                    }
                }
            }
        }
        // rsp.sendRedirect("/ereconcile/signin?" + errorInfo + "=true");
        super.setDefaultFailureUrl("/signin?" + errorInfo + "=true");
        super.onAuthenticationFailure(request, response, exception);
    }
    
}
