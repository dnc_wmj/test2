package cn.sscs.vendormanagement;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

public class Utils {
    
    public static Log logger = LogFactory.getLog(SigninController.class);
    
	private static final String ROLE_HD = "ROLE_HD";
	private static final String ROLE_FM = "ROLE_FM";
	private static final String ROLE_DM = "ROLE_HD";
	public static final String ROLE_L2 = "ROLE_L2";

	/**
	 * 
	 * @return
	 */
	public static VndMgmntUser getUser() {
		return (VndMgmntUser) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
	}

	/**
	 * 
	 * @return
	 */
	public static boolean isRoleHD() {
		for (GrantedAuthority grantedAuthority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (grantedAuthority.getAuthority().equals(ROLE_HD)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isRoleFM() {
		for (GrantedAuthority grantedAuthority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (grantedAuthority.getAuthority().equals(ROLE_FM)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isRoleDM() {
		for (GrantedAuthority grantedAuthority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
			if (grantedAuthority.getAuthority().equals(ROLE_DM)) {
				return true;
			}
		}
		return false;
	}

	public static Map<String, String> newMailTemplateModel(HttpServletRequest request) {
		Map<String, String> model = new HashMap<String, String>();
		model.put("host", request.getServerName());
		model.put(":port", ":" + request.getLocalPort());
		return model;
	}

	public static void checkHasRoleHD() {
		if (!isRoleHD()) {
			throw new AccessDeniedException();
		}
	}

	public static String encodeFileName(HttpServletRequest request, String fileName) {
		String agent = request.getHeader("USER-AGENT");
		String encodeFileName = fileName;
		try {
			if (fileName != null) {
				char[] charFileName = fileName.toCharArray();
				StringBuffer sb = new StringBuffer();
				for (int i = 0; i < charFileName.length; i++) {
					if (charFileName[i] == ' ') {
						sb.append(charFileName[i]);
					} else {
						sb.append(URLEncoder.encode("" + charFileName[i], "UTF-8"));
					}
				}
				fileName = sb.toString();
			}

			if (agent != null) {
				if (agent.indexOf("MSIE 7") != -1 || agent.indexOf("MSIE 8") != -1 || agent.indexOf("MSIE 9") != -1) {
					// IE
					char[] charFileName = fileName.toCharArray();
					String ab = "";
					for (int i = 0; i < charFileName.length; i++) {
						StringBuffer sb = new StringBuffer();
						char a = charFileName[i];
						sb.append(a);
						byte[] b = sb.toString().getBytes("Windows-31J");
						// 　1 ブランクの場合
						if (a == ' ') {
							ab += "%20";
						}
						// 2 漢字の場合
						else if (b.length > 1) {
							// IE7BugのFix：５C、７Cの場合、IE７は５F、７Fになる
							// だから、５C、７Cの場合、URLEncodingを使う
							if (b[b.length - 1] == 0X5C || b[b.length - 1] == 0X7C) {
								ab += URLEncoder.encode(sb.toString(), "UTF-8");
							}
							// 普通の漢字
							else {
								ab += new String(sb.toString().getBytes("Windows-31J"), "ISO-8859-1");
							}
						}
						// 3 別の英数字の場合
						else {
							ab += new String(sb.toString().getBytes("Windows-31J"), "ISO-8859-1");
						}
					}
					encodeFileName = ab;
				} else if (agent.indexOf("MSIE 6") != -1) {
					encodeFileName = new String(fileName.getBytes("Windows-31J"), "ISO-8859-1");
				} else if (agent.indexOf("Firefox") != -1) {
					// FF
					encodeFileName = new String(fileName.getBytes("UTF-8"), "ISO-8859-1");
				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return encodeFileName;
	}

	/**
	 * 
	 * @Title:       isNonTrade 
	 * @Description: 传递business_type判断是否为非贸 
	 * @param        @param bussynessType
	 * @param        @return     
	 * @return       boolean     
	 * @throws 
	 * User:         dnc
	 * DataTime:     Jul 5, 2012
	 */
	public static boolean isNonTrade(int bussynessType) {
		return bussynessType >= 10 && bussynessType <= 16;
	}
	
	/***
	 * Check mail address is right
	 * 
	 * @param address
	 * @return
	 */
	public static boolean isMail(String mailAddress) {
		if (mailAddress == null) {
			return false;
		}

		if (!mailAddress.contains("@")) {
			return false;
		}

		if (countSubstr(mailAddress, "@") != 1) {
			return false;
		}

		if (containMultibyte(mailAddress)) {
			return false;
		}

		String[] partArray = mailAddress.split("@");
		String localPart = partArray[0];
		String domainPart = partArray[1];

		if (localPart.length() == 0) {
			return false;
		}

		Pattern p = Pattern.compile("[^A-Za-z0-9\\x21\\x23\\x24\\x25\\x26\\x27\\x2A\\x2B\\x2D\\x2F\\x3D\\x3F\\x5E\\x5F\\x60\\x7B\\x7C\\x7D\\x7E.]");
		Matcher m = p.matcher(localPart);
		if (m.find()) {
			return false;
		}

		p = Pattern.compile("[^A-Za-z0-9\\x21\\x23\\x24\\x25\\x26\\x27\\x2A\\x2B\\x2D\\x2F\\x3D\\x3F\\x5E\\x5F\\x60\\x7B\\x7C\\x7D\\x7E\\x5B\\x5D.]");
		m = p.matcher(domainPart);
		if (m.find()) {
			return false;
		}

		if (countSubstr(domainPart, "\\.") == 0) {
			return false;
		}

		p = Pattern.compile("(^\\..+)|(.+\\.$)");
		m = p.matcher(domainPart);
		if (m.matches()) {
			return false;
		}

		if (countSubstr(domainPart, "\\.\\.") > 0) {
			return false;
		}

		if (countSubstr(domainPart, "\\[") > 0 || countSubstr(domainPart, "\\]") > 0) {
			if (!Pattern.matches("^\\[.+\\]$", domainPart)) {
				return false;
			}
		}
		return true;

	}
	
	public static int countSubstr(String source, String sub) {
		int count = 0;
		String[] sourceArray = source.split(sub, -1);
		count = sourceArray.length - 1;
		return count;
	}

	public static boolean containMultibyte(String source) {
		return !(source.getBytes().length == source.length());
	}

	public static boolean isOldNonTrade(int businessType) {
		return (3 <= businessType && businessType <= 9) || businessType == 0;
	}
	
	/**
	 * The vendor's businessType is trade.
	 * @param businessType
	 * @return
	 */
	public static boolean isTrade(int businessType) {
		return businessType == 1;
	}
	
	public static boolean parentCompanyIsSSCS(String superCompanyType){
        if("1".equals(superCompanyType)){
            return true;
        }else{
            return false;
        }
    }

    public static Date getAfterOneYearDate(Date startDate) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);
        cal.add(Calendar.YEAR, 1);
        return cal.getTime();
    }
}
