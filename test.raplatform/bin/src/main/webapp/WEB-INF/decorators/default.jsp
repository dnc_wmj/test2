<%@ include file="/WEB-INF/views/includes.jsp" %>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<title>
		<decorator:title />
	</title>
	<link rel="stylesheet" href="<c:url value="/styles/base.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
</head>
<body>
<div id="container">
  <div id="header">
    <h2 class="alt">SSCSCN Vendor Management</h2>
  </div>
  <hr>
  <div id="body">
    <decorator:body/>
  </div>
 
  <hr>
  
  <div id="footer" class="alt">
    Sony Supply Chain Solutions (China) Limited
  </div>
</div>
</body>
</html>
