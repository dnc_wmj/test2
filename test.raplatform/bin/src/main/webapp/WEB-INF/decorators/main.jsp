<%@page import="cn.sscs.vendormanagement.Utils"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>
<%@page pageEncoding="UTF-8"%>
<%@ taglib prefix="decorator" uri="http://www.opensymphony.com/sitemesh/decorator" %>
<html>
<head>
	<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
	<title>
		<decorator:title />
	</title>
	<link rel="stylesheet" href="<c:url value="/styles/base.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
    <!-- 
    <script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.2.min.js" />" charset="UTF-8"></script>
     -->
    <decorator:head />
</head>
<body>
<div id="container">
  <div id="header">
    <h2 class="alt" style="color:green;">SSCSCN Vendor Management</h2>
  </div>
  <div style="text-align:right;">
    <%=Utils.getUser().getName() %>(<%=Utils.getUser().getUsername() %>) | <a href="<c:url value="/signout"/>">Sign out</a>
    |
    <% if (request.getMethod().equals("GET")) { 	%>
		<a href="?locale=en_us"><fmt:message key="header.locale.en"/></a> |
		<a href="?locale=zh_cn"><fmt:message key="header.locale.cn"/></a> 
	<% } %>
  </div>
  <br>
  <div id="menu">
  	<%@ include file="/WEB-INF/decorators/menu.jsp" %>
  </div>
  <hr>
  
  <div id="body">
    <decorator:body/>
  </div>
  <hr>
  
  <div id="footer" class="alt">
    Sony Supply Chain Solutions (China) Limited
  </div>
</div>
</body>
</html>
