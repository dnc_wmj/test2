<%@ include file="/WEB-INF/views/includes.jsp"%>
<html>
<head>
<title><fmt:message key="access.deny.title" /></title>
</head>
<body>
	<link rel="stylesheet" href="<c:url value="/styles/signin.css" />" type="text/css" media="screen, projection"
		charset="utf-8" />
	<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection"
		charset="utf-8" />
	<div class="mainContainer">
		<div id="signin" style="text-align: left;">
			<br>
			<h1 style="color: red; text-align: center;">
				<fmt:message key="access.deny.title" />
			</h1>
			<span style="position: relative; top: 5px;">
				<fmt:message key="access.deny.info.description" />
			</span>
			<br>
&nbsp;&nbsp;&nbsp;&nbsp;<span style="position: relative; top: 5px;"><fmt:message key="access.deny.info.a" /></span> 
			<br>
&nbsp;&nbsp;&nbsp;&nbsp;<span style="position: relative; top: 5px;"><fmt:message key="access.deny.info.b" /></span>
			<hr>
			<span style="position: relative; top: 5px; text-align: right; color: blue;">
				<fmt:message key="access.deny.help" />
			</span>
			<div class="spacer"></div>
		</div>
	</div>
</body>
</html>