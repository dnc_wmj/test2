<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="workflow.detail"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
});
</script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="workflow.detail"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>&nbsp;</p>
<table class="basic_form_table">
<tbody>
<%@ include file="/WEB-INF/views/workflow/approval_table.jsp" %>
<%@ include file="/WEB-INF/views/workflow/approval_contents.jsp" %>
<c:if test="${workflow.status != 2}">
	<c:if test="${editable}">
	  <tr>
	    <td style="border:none;"><fmt:message key="evaluation.comment"/> : </th>
	  </tr>
	  <tr>
	    <td><textarea id="comment" name="comment" style="font-size:12px;height:5em;"></textarea></td>
	  </tr>  
	  <tr>
	    <td style="border:none;white-space:nowrap;">
	    <table>
	    <tr>
	      <td style="border:none">
	    	<spring:url value="/workflow/resume/${workflow.id}" var="formUrl"/>
	    	<form:form action="${fn:escapeXml(formUrl)}" method="post" onsubmit="$('#commentHidden').val($('#comment').val());">
		    <input type="hidden" name="comment" id="commentHidden">
	      	<button name="action" value="reapplication" type="submit" style="margin-left:20px;">
	        	<fmt:message key="button.reapplication"/>
	      	</button>
	    	</form:form>
	   
	</c:if>
			<spring:url value="/workflow/cancel/${workflow.id}" var="formUrl"/>
			<form:form action="${fn:escapeXml(formUrl)}" method="post">
	  		<c:if test="${!editable}">
			  <table style="padding-left:40px;">
			  <tr>
			  </c:if>
			    <td style="border:none;">
			      <button name="action" value="cancel" type="submit" style="margin-left:20px;">
			        <fmt:message key="button.cancel"/>
			      </button>
			</form:form>
	    	  </table>
	    </td>
	  </tr>
</c:if>
</tbody>
</table>
<hr>
<%@ include file="/WEB-INF/views/workflow/detail_header.jsp" %>
<br>
<div id="ui-tab">
  <ul>
    <c:if test="${hasEvaluation == true}">
      <li><a href="#fragment-1"><span><fmt:message key="evaluation.title"/></span></a></li>
    </c:if>
    <li><a href="#fragment-2"><span><fmt:message key="basicinfo.detail"/></span></a></li>
  </ul>

  <c:if test="${hasEvaluation == true}">
    <div id="fragment-1">
      <c:if test="${evaluation.businessType < 3}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
      </c:if>
      <c:if test="${evaluation.businessType >= 3 && evaluation.businessType <= 9}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part.jsp" %>
      </c:if>
    </div>
  </c:if>
  <div id="fragment-2">
    <%@ include file="/WEB-INF/views/vendor/basic_information_part.jsp" %>
  </div>
</div>
</div>
</div>
</body>
</html>