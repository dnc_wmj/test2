<%@ include file="/WEB-INF/views/includes.jsp" %>
  <tr>
    <th style="text-align:left;width:100%;">
      <span style="color:darkred;font-size:1.5em;">  
        <fmt:message key="workflow.type"/> : 
        <c:if test="${workflow.modifiedDataType == 10}">
          <fmt:message key="workflow.type.new"/>
        </c:if>
        <c:if test="${workflow.modifiedDataType == 20}">
          <fmt:message key="workflow.type.edit"/>
        </c:if>       
        <c:if test="${workflow.modifiedDataType == 30}"> 
          <fmt:message key="workflow.type.reeval"/>
        </c:if>
        <c:if test="${workflow.modifiedDataType == 40}">
          <fmt:message key="workflow.type.delete"/>
        </c:if>
		<c:if test="${workflow.modifiedDataType == 60}">
          <fmt:message key="workflow.type.freeze"/>
        </c:if>
        <c:if test="${workflow.modifiedDataType == 70}">
          <fmt:message key="workflow.type.unfreeze"/>
        </c:if>
        
      </span>
      <br>
      <br>
      <table class="workflowWord">
      	<tr>
	      <c:if test="${workflow.workflowType == 'full'}">
      		<th>
	      		<c:if test="${workflow.stage == 0}">
	      			<span style="color:blue;">
	      		</c:if>
		        <fmt:message key="workflow.applicant"/>
		        <c:if test="${workflow.stage == 0}"></span></c:if> 
	        </th>
	        <c:if test="${userNames[1] != ''}">
	      		<th rowspan="2">
		        	<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	      		</th>
	      		<th>
	      			<c:if test="${workflow.stage == 10}"><span style="color:blue;"></c:if>
		        	<div><fmt:message key="workflow.l1"/></div>
		        	<c:if test="${workflow.stage == 10}"></span></c:if> 
	      		</th>
      		</c:if>
      		<th rowspan="2">
      			<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
      		</th>
      		<th>
      			<c:if test="${workflow.stage == 20}"><span style="color:blue;"></c:if>
		        <div><fmt:message key="workflow.l2"/></div>
		        <c:if test="${workflow.stage == 20}"></span></c:if>
      		</th>
					<c:if test="${isSSCS}">
      		<th rowspan="2">
      			<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
      		</th>
      		<c:if test="${evaluation != null && evaluation.totalPointForCompare <= 89}">
	      		<th width="100px">
	      			<c:if test="${workflow.stage == 30}"><span style="color:blue;"></c:if>
		            <div>[CEO]&nbsp;</div>
		            <c:if test="${workflow.stage == 30}"></span></c:if>
	      		</th>
	      		<th rowspan="2">
	      			<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	      		</th>
      		</c:if>
      		</c:if>
	      </c:if>
	      	<c:if test="${isSSCS}">
	      		<th>
		      		<c:if test="${workflow.stage == 40}"><span style="color:blue;"></c:if>
			        <div><fmt:message key="workflow.hd"/></div>
			        <c:if test="${workflow.stage == 40}"></span></c:if>
	      		</th>
	      		<th rowspan="2">
	      			<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	      		</th>
	      		<th>
		      		<c:if test="${workflow.stage == 50}"><span style="color:blue;"></c:if>
			        <div><fmt:message key="workflow.fm"/></div>
			        <c:if test="${workflow.stage == 50}"></span></c:if>
	      		</th>
	      		<th rowspan="2">
	      			<img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
	      		</th>
	      		<th>
	      			<c:if test="${workflow.stage == 60}"><span style="color:blue;"></c:if>
			        <div><fmt:message key="workflow.dm"/></div>
			        <c:if test="${workflow.stage == 60}"></span></c:if>
	      		</th>
					</c:if>
	   	</tr>
     	<tr>
    		<th style="font-size: 10px">
    			<c:if test="${workflow.stage == 0}"><span style="color:blue;"></c:if>
    			(${userNames[0] })
    			<c:if test="${workflow.stage == 0}"></span></c:if>
    		</th>
    		<c:if test="${userNames[1] != ''}">
      		<th style="font-size: 10px">
      			<c:if test="${workflow.stage == 10}"><span style="color:blue;"></c:if>
      			(${userNames[1] })
      			<c:if test="${workflow.stage == 10}"></span></c:if>
      		</th>
    		</c:if>
    		<c:if test="${userNames[2] != ''}">
      		<th style="font-size: 10px">
      			<c:if test="${workflow.stage == 20}"><span style="color:blue;"></c:if>
      			(${userNames[2] })
      			<c:if test="${workflow.stage == 20}"></span></c:if>
      		</th>
    		</c:if>
    		<c:if test="${isSSCS}">		
		      <c:choose> 
		      	
					  <c:when test="${evaluation != null && evaluation.totalPointForCompare <= 89}">   
					    <th  style="font-size: 10px">
					    	<c:if test="${workflow.stage == 30}"><span style="color:blue;"></c:if>
			      			(${userNames[3] })
			      			<c:if test="${workflow.stage == 30}"></span></c:if>
			      		</th>
			      		<th style="font-size: 10px">
			      			<c:if test="${workflow.stage == 40}"><span style="color:blue;"></c:if>
			      			(${userNames[4] })
			      			<c:if test="${workflow.stage == 40}"></span></c:if>
			      		</th>
			      		<th style="font-size: 10px">
			      			<c:if test="${workflow.stage == 50}"><span style="color:blue;"></c:if>
			      			(${userNames[5] })
			      			<c:if test="${workflow.stage == 50}"></span></c:if>
			      		</th>
			      		<th style="font-size: 10px">
			      			<c:if test="${workflow.stage == 60}"><span style="color:blue;"></c:if>
			      			(${userNames[6] })
			      			<c:if test="${workflow.stage == 60}"></span></c:if>
			      		</th>
					  </c:when> 
					  <c:otherwise>   
					    <th style="font-size: 10px">
					    	<c:if test="${workflow.stage == 40}"><span style="color:blue;"></c:if>
			      			(${userNames[3] })
			      			<c:if test="${workflow.stage == 40}"></span></c:if>
			      		</th>
			      		<th style="font-size: 10px">
			      			<c:if test="${workflow.stage == 50}"><span style="color:blue;"></c:if>
			      			(${userNames[4] })
			      			<c:if test="${workflow.stage == 50}"></span></c:if>
			      		</th>
			      		
			      		<th style="font-size: 10px">
			      			<c:if test="${workflow.stage == 60}"><span style="color:blue;"></c:if>
			      			(${userNames[5] })
			      			<c:if test="${workflow.stage == 60}"></span></c:if>
			      		</th>
					  </c:otherwise> 
					</c:choose>
				</c:if>
  		 </tr>
	 		
	   <c:if test="${not empty workflow_results_for_seal}">
	   <tr>
	   	<td></td>
	   	<td></td>
	      <c:forEach var="result" items="${workflow_results_for_seal}">
			  <c:if test="${result.stage != 0 && result.result == 1}">   
			  	<td style="border:1px solid;text-align:center;font-size:0.8em;color:darkred;">
		          <img src="<c:url value="/seal/${result.userId}/${result.processedDate.time}/${result.result}" />"/><br>
		          <c:if test="${result.stage == 10}">
		            <fmt:message key="workflow.l1"/>
		          </c:if>
		          <c:if test="${result.stage == 20}">
		            <fmt:message key="workflow.l2"/>
		          </c:if>
		          <c:if test="${result.stage == 30}">
		            CEO
		          </c:if>
		          <c:if test="${result.stage == 40}">
		            <fmt:message key="workflow.hd"/>
		          </c:if>
		          <c:if test="${result.stage == 50}">
		            <fmt:message key="workflow.fm"/>
		          </c:if>
		          <c:if test="${result.stage == 60}">
		            <fmt:message key="workflow.dm"/>
		          </c:if>
		        </td>
		        <td></td>
			  </c:if> 
	      </c:forEach> 
      </tr>
      </c:if>
	 </table>
    </th>
  </tr>
