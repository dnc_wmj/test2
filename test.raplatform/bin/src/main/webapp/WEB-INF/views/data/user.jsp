<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="data.menu.user"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />" charset="UTF-8"></script>
<script type="text/javascript">
function reset(_link, _url) {
	var $this = $(_link);
	$this.hide();
	$parent = $this.parents(".button").eq(0);
	var $wait=$parent.children().eq(1);
	var $success=$parent.children().eq(2);
	$wait.show();
	$.ajax(
		{
			url: _url,
			cache: false,
			success: function() {
		    	$wait.hide();
				$success.show();
	  	}
	  })
}
function allreset() {
	if(confirm('<fmt:message key="user.reset.all.confirm"/>')){
		var $form= $('#form');
		var $this = $('.allreset', $form);
		$this.hide();
		var $wait=$('.wait', $form);
		var $success=$('.allsuccess', $form);
		$wait.show();
		$.ajax(
			{
				url: '<c:url value="/data/user/reset"/>',
				cache: false,
				success: function() {
				  $wait.hide();
				  $success.show();
			  }
		 	})
	}
	
}
</script>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="data.menu.user"/></h1>
<p>&nbsp;</p>
<h1 style="font-size:1.1em;" class="alt"><fmt:message key="data.menu.upload"/></h1>

<form:form modelAttribute="upload" action="upload" method="post" enctype="multipart/form-data">
<table style="font-size: 12px;">
<tr>
	<td><fmt:message key="data.upload.userFile"/></td>
	<td><input style="width:250px;" type="file" name="userfile"/></td>
</tr>
<tr>
	<td><fmt:message key="data.upload.costcenterFile"/></td>
	<td><input style="width:250px;" type="file" name="costcenterfile"/></td>
</tr>
<tr>
	<td>
		<fmt:message key="basicinfo.form.company_type.parent_company"/> <span style = "font-weight: bolder;">:</span>
	</td>
	<td>
		<select name="userCompany" style="width:auto;">
		   <option value="1"><fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/></option>
		   <option value="2"><fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/></option>
		   <option value="3"><fmt:message key="basicinfo.form.company_type.parent_company.SSV"/></option>
		   <option value="4"><fmt:message key="basicinfo.form.company_type.parent_company.SEW"/></option>
		   <option value="5"><fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/></option>
		   <option value="6"><fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/></option>
		   <option value="7"><fmt:message key="basicinfo.form.company_type.parent_company.SEH"/></option>
		</select>
	</td>
</tr>
<tr>
	<td></td>
	<td><input type="submit" value="<fmt:message key="data.upload.button"/>"></td>
</tr>
</table>
 <!--
 <br>
 <br>
 <a href="<c:url value="/samples/users.xls"/>"><fmt:message key="data.upload.template"/></a>
 <br>
  -->
 <br>
</form:form>
<!-- 
<hr>
<h1 style="font-size:1.1em;" class="alt"><fmt:message key="data.down.userinfo"/></h1>
<br>
 <spring:url value="/data/interface/down/userinfo" var="formUrl"/>
<form:form action="${fn:escapeXml(formUrl)}" method="post" >
 <input type="submit" value="<fmt:message key="data.down.userinfo"/>">
</form:form>
<br><br>
 -->
<hr>
<h1 style="font-size:1.1em;" class="alt"><fmt:message key="search.user"/></h1>
<form:form modelAttribute="criteria" action="user" method="post" id="form">
 <fmt:message key="search.user.IDorName"/> :  
 <form:input style="width:300px;" path="name" />
 <input type="submit" value="<fmt:message key="button.search"/>">
  	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
 <input class="allreset" onclick="allreset()" type="button" value="<fmt:message key="search.user.all.reset"/>">
 <span class="wait" style="display:none;">
	<img src="<c:url value="/images/wait.gif"/>" alt="waitting" />
 </span>
 <span class = "allsuccess" style="color: blue; display: none;"><fmt:message key="user.reset.all.success"/></span><br>
</form:form>

<c:if test="${users != null}">
<hr>
<span style="font-size:0.8em;margin-left:20px;">
  <c:if test="${criteria.page != 0}">
    <c:url var="prev" value="/data/user">
      <c:param name="page" value="${(criteria.page - 1)}" />
    </c:url>
    <a href="#" onclick="$('#form').attr('action','<c:out value="${prev}"/>');$('#form').submit();">&lt;-&nbsp;</a>
  </c:if>
  <c:out value="${(criteria.page + 1)}" />/<c:out value="${(pages + 1)}"/>
  <c:if test="${criteria.page < pages}">
  <c:url var="next" value="/data/user">
    <c:param name="page" value="${(criteria.page + 1)}" />
  </c:url>
  <a href="#" onclick="$('#form').attr('action','<c:out value="${next}"/>');$('#form').submit();">&nbsp;-&gt;</a>
  </c:if>
</span>
<table class="default_table" style="width:800px;">
  <tr>
    <th style="width: 60px;"><fmt:message key="search.user.id"/></th>  
    <th><fmt:message key="search.user.name"/></th>
    <th><fmt:message key="search.user.password"/></th>
    <th style="width: 150px;"><fmt:message key="search.user.email"/></th>
    <th><fmt:message key="search.user.division"/></th>
    <th><fmt:message key="search.user.valid"/></th>
    <th style="width: 66px;">&nbsp;</th>
    <th style="display:none"></th>
  </tr>
<c:if test="${rowspan}">
	<c:forEach var="user" items="${users}" >
	  <c:forEach var="userinfo" items="${user.userInfos}" varStatus="userStatus">
	  <tr>
	  	<c:if test="${userStatus.count == 1}">
	    <td rowspan="<c:out value="${user.userInfosLength}"/>"><c:out value="${user.id}" /></td>
	    <td rowspan="<c:out value="${user.userInfosLength}"/>"><c:out value="${user.name}" /></td>
	    <td rowspan="<c:out value="${user.userInfosLength}"/>"><c:out value="${user.password}" /></td>
		</c:if>
	    <td><c:out value="${userinfo.email}" /></td>
	    <td><c:out value="${userinfo.divisionInfo}" /></td>
	    <td style="text-align: center;"><c:out value="${userinfo.valid}" /></td>
		<c:if test="${userStatus.count == 1}">
	    <td rowspan="<c:out value="${user.userInfosLength}"/>" style="width: 80px; text-align: center;">
	    	<div class="button">
	    		<a  href="javascript:void(0)" onclick="reset(this, '<c:url value="/data/user/reset/${user.id}"/>')"><fmt:message key="search.user.reset"/></a>
	    		<div class="wait" style="display:none;">
	    			<img src="<c:url value="/images/wait.gif"/>" alt="waitting" />
	    	    </div>
	    	    <span class = "success" style="color: blue;display:none;"><fmt:message key="user.reset.success"/></span>
	    	</div>
	    </td>
	    </c:if>
	  </tr>
	  </c:forEach>
	</c:forEach>
</c:if>
	<c:forEach var="user" items="${users}" >
	  <tr>
	    <td ><c:out value="${user.id}" /></td>
	    <td ><c:out value="${user.name}" /></td>
	    <td ><c:out value="${user.password}" /></td>
	    <td><c:out value="${user.mainEmail}" /></td>
	    <td><c:out value="${user.allDivisionInfo}" /></td>
	    <td style="text-align: center;"><c:out value="${user.mainValid}" /></td>
	    <td style="width: 80px; text-align: center;">
	    	<div class="button">
	    		<a  href="javascript:void(0)" onclick="reset(this, '<c:url value="/data/user/reset/"/>'+'${user.id}')"><fmt:message key="search.user.reset"/></a>
	    		<div class="wait" style="display:none;">
	    			<img src="<c:url value="/images/wait.gif"/>" alt="waitting" />
	    	    </div>
	    	    <span class = "success" style="color: blue;display:none;"><fmt:message key="user.reset.success"/></span>
	    	</div>
	    </td>
	  </tr>
	</c:forEach>
</table>
</c:if>
</div>
</div>
</body>
</html>