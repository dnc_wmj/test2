<%@ include file="/WEB-INF/views/includes.jsp"%>
<html>
	<head>
		<title>Maintainance Menu</title>
	</head>
	<body>
		<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8" />
		<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8" />
		<div class="mainContainer">
			<div class="basic_form contextContainer">
				<h1 class="alt">
					Maintainance Menu
				</h1>
				<br>
				<table>
					<tr>
						<td>
							<form action="<c:url value="/data/vendor/search" />" method="get">
								<button type="submit">
									<fmt:message key="data.menu.maintainance" />
								</button>
							</form>
						</td>
						<c:if test="${showDown}">
						<!-- 第二次修改后隐藏 -->
						<td>
							<form action="<c:url value="/data/vendor/download" />"
								method="get">
								<button type="submit" style="width: 200px;">
									<fmt:message key="data.menu.vendor.download" />
								</button>
							</form>
						</td>
						</c:if>
						<td>
							<form action="<c:url value="/data/user" />" method="get">
								<button type="submit">
									<fmt:message key="data.menu.user" />
								</button>
							</form>
						</td>
						<td>
							<form action="<c:url value="/data/interface" />" method="get">
								<button type="submit" style="width: 200px;">
									<fmt:message key="data.menu.download" />
								</button>
							</form>
						</td>

					</tr>
				</table>
			</div>
		</div>
		</div>
	</body>
</html>