<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="evaluation.title"/></title>
</head>
<body onload="init()">

<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
<link rel="stylesheet" href="<c:url value="/styles/basic_non_trade_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
		<script type="text/javascript">
$(document).ready(function(){
	$('.tranCond1').click(function(){
		if($(this).val() == 0){
			$(".setVendorCode").css('display','none');
			$("#vendorCode").val("");
		}
	});
	
	$('.tranCond2').click(function(){
		if($(this).val() == 1)
			$(".setVendorCode").css('display','');
	});
	$('.priority', '.default_non_trade_table').change(function(){
		var priorityVal = $(this).val();
		var tr = $(this).parents('tr').eq(0);
		var yesNoVal = $('.yesNo', tr).val();
		var scoreYesNo = $('.score_yesno',tr);
		var topScore = $('.top_score',tr);
		switch(priorityVal){
		  	case "1": 
		  		switch(yesNoVal){
		  				case "1": scoreYesNo.text("0").addClass('errorScore');
		  						$('.errorComment').css('display','block');
		  						break;
		  				case "2": scoreYesNo.text("25").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
		  						break;
		  				default: scoreYesNo.text("").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
					};
				topScore.text("25");
				break;
			case "2": 
		  		switch(yesNoVal){
		  				case "1": scoreYesNo.text("3").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
		  						break;
		  				case "2": scoreYesNo.text("15").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
		  						break;
		  				default: scoreYesNo.text("").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
					};
				topScore.text("15");
				break;
			case "3": 
		  		switch(yesNoVal){
		  				case "1": scoreYesNo.text("1").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
		  						break;
		  				case "2": scoreYesNo.text("5").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
		  						break;
		  				default: scoreYesNo.text("").removeClass('errorScore');
		  						$('.errorComment').css('display','none');
					};
				topScore.text("5");
				break;
		  }
		$('.yesNo', tr).trigger('change');
		topScore = null;
		scoreYesNo = null;
		tr = null;
  });
  $('.yesNo','.default_non_trade_table').change(function(){
  	var thisVal = $(this).val();
  	var tr = $(this).parents('tr').eq(0);
  	var priorityVal = $('.priority', tr).val();
	var scoreYesNo = $('.score_yesno',tr);
	var $non_point = $('.non_point',tr);
	switch(thisVal){
		  	case "1": 
		  		switch(priorityVal){
		  				case "1": scoreYesNo.text("25").removeClass('errorScore');
		  							$('.errorComment').css('display','none');
		  						  break;
		  				case "2": scoreYesNo.text("15").removeClass('errorScore');
								  	$('.errorComment').css('display','none');
								  	break;
		  				case "3": scoreYesNo.text("5").removeClass('errorScore');
								  	$('.errorComment').css('display','none');
								  	break;
					};
				$non_point.val(0).attr('disabled','disabled');
				break;
			case "0": 
		  		switch(priorityVal){
		  				case "1": scoreYesNo.text("0").addClass('errorScore');
								  $('.errorComment').css('display','block');
								  break;
		  				case "2": scoreYesNo.text("3").removeClass('errorScore');
								  $('.errorComment').css('display','none');
								  break;
		  				case "3": scoreYesNo.text("1").removeClass('errorScore');
								  $('.errorComment').css('display','none');
								  break;
					};
				$non_point.val(0).attr('disabled','disabled');
				break;
			default : 
		  		scoreYesNo.text("").removeClass('errorScore');
				$('.errorComment').css('display','none');
				$non_point.attr('disabled','');
	}
	$('.non_point', tr).trigger('change');
	scoreYesNo = null;
	tr = null;
  });
  $('.non_point','.default_non_trade_table').change(function(){
	  var $tr = $(this).parents('tr').eq(0);
	  var nonPoint = $(this).val();
	  var priorityVal = $('.priority',$tr).val();
	  var scorePoint = $('.score_point', $tr);
	  var $yesNo = $('.yesNo', $tr);
	  var $scoreYesNo = $('.score_yesno', $tr);
	  switch(nonPoint){
	  	case "1": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("5");
	  						break;
	  				case "2": scorePoint.text("3");
	  						break;
	  				case "3": scorePoint.text("1");
	  						break;
				};
			$yesNo.val(2).attr('disabled','disabled');
			$scoreYesNo.text("").removeClass('errorScore');
			break;
		case "2": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("10");
	  						break;
	  				case "2": scorePoint.text("6");
	  						break;
	  				case "3": scorePoint.text("2");
	  						break;
				};
			$yesNo.val(2).attr('disabled','disabled');
			$scoreYesNo.text("").removeClass('errorScore');
			break;
		case "3": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("15");
	  						break;
	  				case "2": scorePoint.text("9");
	  						break;
	  				case "3": scorePoint.text("3");
	  						break;
				};
			$yesNo.val(2).attr('disabled','disabled');
			$scoreYesNo.text("").removeClass('errorScore');
			break;
		case "4": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("20");
	  						break;
	  				case "2": scorePoint.text("12");
	  						break;
	  				case "3": scorePoint.text("4");
	  						break;
				};
			$yesNo.val(2).attr('disabled','disabled');
			$scoreYesNo.text("").removeClass('errorScore');
			break;
		case "5": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("25");
	  						break;
	  				case "2": scorePoint.text("15");
	  						break;
	  				case "3": scorePoint.text("5");
	  						break;
				};
			$yesNo.val(2).attr('disabled','disabled');
			$scoreYesNo.text("").removeClass('errorScore');
			break;
		default : 
			scorePoint.text("");
			$yesNo.attr('disabled','');
	  }
	  scorePoint = null;
  });

  $('.btn_submit').click(function(){
  	var validate = true;
  	var temp = true;
  	$('.mustCheck').each(function(){
  		var _tr = $(this).parents('tr').eq(0);
  		// Essential
  		if($('.mustCheckValue', this).val() == '1'){
  			if($('.yesNo', _tr).val() == '2' && $('.non_point',_tr).val() == '0'){
  				$(this).addClass('errorScore');
  				validate = false;
  				if(temp){
					$(this).get(0).scrollIntoView();
  					temp = false;
  				}
  			}else{
  				$(this).removeClass('errorScore');;
  			}
  		}
  		// Priority : High && Yes/No : no
  		if(_tr.find('.score_yesno').find('.errorScore').length != 0){
  			validate = false;
  		}
  		_tr = null;
  	});
  	
  	if($('#tranCond2').get(0).checked == true)
  		if($('#vendorCode').val() == ''){
  			$('#vendorCode').addClass('errorScore');
  			validate = false;
  		}
  			
  	
  	if(validate){
  		$('.yesNo','.default_non_trade_table').attr('disabled','');
  		$('.non_point','.default_non_trade_table').attr('disabled','');
  		$('.form').submit();
  	}else{
  		$('.errorStart').remove();
  		return false;
  	}
  	
  });
});
function init(){
	$('.yesNo','.default_non_trade_table').each(function(){
	var $yesNo = $(this);
	var thisVal = $yesNo.val();
  	var tr = $(this).parents('tr').eq(0);
  	var priorityVal = $('.priority', tr).val();
	var scoreYesNo = $('.score_yesno',tr);
	var topScore = $('.top_score',tr);
	var $nonPoint = $('.non_point',tr);
	var nonPoint = $nonPoint.val();
	var priorityVal = $('.priority',tr).val();
	var scorePoint = $('.score_point',tr);
	switch(thisVal){
	  	case "1": 
	  		switch(priorityVal){
	  				case "1": scoreYesNo.text("25").removeClass('errorScore');
							  	topScore.text("25");
							  	$('.errorComment').css('display','none');
	  						  break;
	  				case "2": scoreYesNo.text("15").removeClass('errorScore');
	  						  topScore.text("15");
	  						  $('.errorComment').css('display','none');
							  	break;
	  				case "3": scoreYesNo.text("5").removeClass('errorScore');
	  						  topScore.text("5");
	  						  $('.errorComment').css('display','none');
							  	break;
				};
			$non_point.val(0).attr('disabled','disabled');
			break;
		case "0": 
	  		switch(priorityVal){
	  				case "1": scoreYesNo.text("0").addClass('errorScore');
	  						  topScore.text("25");
	  						  $('.errorComment').css('display','block');
							  	break;
	  				case "2": scoreYesNo.text("3").removeClass('errorScore');
	  						  topScore.text("15");
	  						  $('.errorComment').css('display','none');
									break;
	  				case "3": scoreYesNo.text("1").removeClass('errorScore');
							  	topScore.text("5");
							  	$('.errorComment').css('display','none');
							  	break;
				};
			$non_point.val(0).attr('disabled','disabled');
			break;
		default : 
			switch(priorityVal){
	  				case "1": topScore.text("25");
							  break;
	  				case "2": topScore.text("15");
							  break;
	  				case "3": topScore.text("5");
							  break;
				};
	  		scoreYesNo.text("").removeClass('errorScore');
	  		$non_point.attr('disabled','');
	}
	
	
    switch(nonPoint){
	  	case "1": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("5");
	  						break;
	  				case "2": scorePoint.text("3");
	  						break;
	  				case "3": scorePoint.text("1");
	  						break;
				};
				$yesNo.val(2).attr('disabled','disabled');
			break;
		case "2": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("10");
	  						break;
	  				case "2": scorePoint.text("6");
	  						break;
	  				case "3": scorePoint.text("2");
	  						break;
				};
				$yesNo.val(2).attr('disabled','disabled');
			break;
		case "3": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("15");
	  						break;
	  				case "2": scorePoint.text("9");
	  						break;
	  				case "3": scorePoint.text("3");
	  						break;
				};
				$yesNo.val(2).attr('disabled','disabled');
			break;
		case "4": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("20");
	  						break;
	  				case "2": scorePoint.text("12");
	  						break;
	  				case "3": scorePoint.text("4");
	  						break;
				};
				$yesNo.val(2).attr('disabled','disabled');
			break;
		case "5": 
	  		switch(priorityVal){
	  				case "1": scorePoint.text("25");
	  						break;
	  				case "2": scorePoint.text("15");
	  						break;
	  				case "3": scorePoint.text("5");
	  						break;
				};
				$yesNo.val(2).attr('disabled','disabled');
			break;
		default : 
			scorePoint.text("");
			$yesNo.attr('disabled','');
    }
	scorePoint = null;
	scoreYesNo = null;
	topScore = null;
	tr = null;
	});
}
</script>
<div class="mainContainer">
<div class="basic_non_trade_form contextContainer">
<h1 class="alt"><fmt:message key="evaluation.title"/> - <c:out value="${identity.name}"/></h1>

<c:if test="${tmpRuleId != null}">
	<c:if test="${!reeval}">
		<spring:url value="/evaluation/register/${vendor_id}/${tmpRuleId}" var="formUrl" />
	</c:if>
	
	<c:if test="${reeval}">
		<!-- 临时保存的一定不是再评估 -->
		<spring:url value="/evaluation/reeval/${vendor_id}/${tmpRuleId}" var="formUrl" />
	</c:if>
</c:if>
<c:if test="${tmpRuleId == null}">
	<c:if test="${!reeval}">
		<spring:url value="/evaluation/register/${vendor_id}" var="formUrl" />
	</c:if>
	<c:if test="${reeval}">
		<spring:url value="/evaluation/reeval/${vendor_id}" var="formUrl" />
	</c:if>
</c:if>
<form:form class="form" action="${fn:escapeXml(formUrl)}"
	modelAttribute="evaluation" 
	method="post">

<div class="index" style="color:green;"><fmt:message key="evaluation.title.1"/></div>
<table class="basic_non_trade_form_table">
<tbody>
  <tr>
    <th><fmt:message key="evaluation.company_short_name"/> : </th>
    <td style="width:390px;"><form:input path="shortName" style="width:390px;"/></td>
    <td style="color:green;"><fmt:message key="evaluation.company_short_name.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.server_name" /> : </th>
    <td><form:input path="serviceName" style="width:390px;"/></td>
    <td style="color:green;"><fmt:message key="evaluation.server_name.description"/></td>
  </tr>  
  <tr>
	<th><fmt:message key="evaluation.service2_category"/> : </th>
    <td><form:input path="service2Category" style="width:390px;"/></td>
    <td style="color:green;"><fmt:message key="evaluation.service2_category.description"/></td>
  </tr>

  
  <tr>
	<th><fmt:message key="evaluation.transaction_condition"/> : </th>
    <td>
      <table>
        <tr>
          <td style="border:none;">
            <form:radiobutton value="0" path="tranCond" class="tranCond1"/> 
          	<fmt:message key="evaluation.transaction_condition.new"/>
          </td>
          <td style="border:none;">
            <form:radiobutton value="1" path="tranCond" class="tranCond2"/>
            <fmt:message key="evaluation.transaction_condition.existing"/>
          </td>
          <td style="border:none;">
            <div class="setVendorCode" style="display:none;">
            	&nbsp;&nbsp;<span style="color:red;">* </span>
            	<fmt:message key="basicinfo.form.vendor_code"/> : 
            	<input type="text" name="vendorCode" style="width: 110px;" <c:if test="${reeval}">disabled="true"</c:if> MAXLENGTH="10" value="${evaluation.vendorCode}"/><form:errors path="vendorCode" cssClass="errors"/>
            </div>
          </td>
        </tr>
      </table>
    </td>
    <td style="color:green;"><fmt:message key="evaluation.transaction_condition.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.selection_reason"/> : </th>
    <td><form:textarea path="selectionReason" style="font-size:12px;width:390px;height:3em;"></form:textarea></td>
    <td style="color:green;"><fmt:message key="evaluation.selection_reason.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.comment"/> : </th>
    <td><form:textarea path="comment" style="font-size:12px;width:390px;height:3em;"></form:textarea></td>
    <td style="color:green;"><fmt:message key="evaluation.comment.description"/></td>
  </tr>  
  <tr>
	<th><fmt:message key="evaluation.applicant"/> : </th>
    <td><form:input path="applicant" style="width:390px;"/></td>
    <td style="color:green;"><fmt:message key="evaluation.applicant.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.date2"/> : </th>
    <td><form:input path="date2" style="width:390px;"/></td>
    <td style="color:green;"><fmt:message key="evaluation.date2.description"/></td>
  </tr>
</tbody>
</table>

<div class="index" style="color:green;"><fmt:message key="evaluation.title.2"/></div>
<table class="basic_non_trade_form_table">
	<tr style="text-align: center;">
		<td>
		<!-- å¦ææ¯åè¯ä¼°åæ´æç¤ºæè¨   modfi éå  2012å¹´7æ6æ¥17:21:40 -->
		<c:if test="${isReeval}">
			<div style="text-align: center;font-variant: normal;font-style: normal;border: solid;border-width: 1px;width: 260px;margin: 0 auto;background-color: red;color: white;display: none;font-size: 15px; border-color: black;" class="index errorComment">
				<fmt:message key="evaluation.nontrade.reeval.comment"/>
			</div>
			<div style="text-align: center;font-variant: normal;font-style: normal;width: 100%;margin: 0 auto;color: red;font-size: 15px;" >
				<fmt:message key="evaluation.nontrade.reeval.comment2"/>
			</div>
		</c:if>
		<c:if test="${!isReeval}">
			<div style="text-align: center;font-variant: normal;font-style: normal;border: solid;border-width: 1px;width: 260px;margin: 0 auto;background-color: red;color: white;display: none;font-size: 15px; border-color: black;" class="index errorComment"><fmt:message key="evaluation.nontrade.comment"/></div>
			<div style="text-align: center;font-variant: normal;font-style: normal;width: 100%;margin: 0 auto;color: red;font-size: 15px;" ><fmt:message key="evaluation.nontrade.comment2"/></div>
		</c:if>
		<!-- modfi end  éå  2012å¹´7æ6æ¥17:21:40 -->
		</td>
	</tr>
</table>


<%
	int index = 0;
	Evaluation eval = ((Evaluation)pageContext.findAttribute("evaluation"));
	boolean status = eval.isAfterValidte();
	boolean mResult = true;
    String priority = "1";
    String yesNo = "2";
    String non_point = "0";
	
%>
<c:forEach var="form" items="${nonTradeEvaluationForm}" varStatus="formStatus">
	<div class="index">
		<c:out value="${formStatus.count}"/>. <c:out value="${form.content}"/>
	</div>
	<table class="default_non_trade_table">
	<tbody>
	<!-- 2012-7-6 15:51:08 zhengtong modif -->
	<!-- 
	  <tr> 
	    <th>Category</th> åç±» 
	    <th>Sub-Category</th>å­	åç±» 
	    <th>Items</th>é¡¹ç®
	    <th>Detail Items (English version)</th>å·ä½
	    
	    <th>Essential</th>éè¦é¡¹ç®
	    <th>Priority</th>éè¦
	    <th>Yes/No</th>æ¯å¦
	    <th>Score</th>åæ°
	    
	    <th>Point</th>ç¹æ°
	    <th>Score</th>åæ°
	    <th>Top Score</th>æé«åæ°
	  </tr>
	 -->
	  <tr>
	    <th><fmt:message key="evaluation.form.head.Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Sub-Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Items"/></th>
	    <th><fmt:message key="evaluation.form.head.Detail-Items"/></th>
	    
	    <th><fmt:message key="evaluation.form.head.Essential"/></th>
	    <th><fmt:message key="evaluation.form.head.Priority"/></th>
	    <th><fmt:message key="evaluation.form.head.Yes-No"/></th>
	    <th><fmt:message key="evaluation.form.head.Score"/></th>
	    
	    <th><fmt:message key="evaluation.form.head.Point"/></th>
	    <th><fmt:message key="evaluation.form.head.Max-Score"/></th>
	    <th><fmt:message key="evaluation.form.head.Top-Score"/></th>
	  </tr>
	 <!-- 2012-7-6 15:51:08 zhengtong modif end  -->
	  <c:forEach var="category" items="${form.categoryList}" varStatus="categoryStatus">
	  	<c:forEach var="subCategory" items="${category.subCategorysList}" varStatus="subCategoryStatus">
	  		<c:forEach var="items" items="${subCategory.nonTradeItems}" varStatus="itemsStatus">
	  			<c:forEach var="detailItems" items="${items.detailItemsList}" varStatus="detailItemsStatus">
			  		<tr>     
				      <c:if test="${subCategoryStatus.count == 1 && itemsStatus.count == 1 && detailItemsStatus.count ==1}">
					      <td style="width:130px;" rowspan="<c:out value="${category.categoryLength}"/>">
					          <c:out escapeXml="false" value="${category.content}"></c:out>
					      </td>
				      </c:if>
				      <c:if test="${itemsStatus.count == 1 && detailItemsStatus.count ==1}">
					      <td style="width:150px;" rowspan="<c:out value="${subCategory.subCategorysLength}"/>">
					          <c:out escapeXml="false" value="${subCategory.content}"></c:out>
					      </td>
				      </c:if>
				      <c:if test="${detailItemsStatus.count ==1}">
					      <td style="width:200px;" rowspan="<c:out value="${items.nonTradeItemsLength}"/>">
					          <c:out escapeXml="false" value="${items.content}"></c:out>
					      </td>
				      </c:if>
				      
				      <td style="width:250px;">
				          <c:out escapeXml="false" value="${detailItems.content}"></c:out>
				      </td>
				      <% 
				           if(status){
				          	  mResult = eval.getmResult()[index];
				          	  priority = eval.getPriority()[index];
				          	  yesNo = eval.getYesNo()[index];
					      	  non_point = eval.getNon_point()[index];
				           }
				       %>
				      <td class="mustCheck <%=mResult ? "" : " errorScore"%>" style="width:50px;text-align:center;" >
				      	  <!-- éå modif  2012å¹´7æ6æ¥16:41:13 -->
				      	  <!-- 
				          <c:if test="${detailItems.mustCheck == 1}">M</c:if>
				          <c:if test="${detailItems.mustCheck == 0}">-</c:if>
				      	   -->
				           <c:if test="${detailItems.mustCheck == 1}"><fmt:message key="evaluation.form.detail.M"/></c:if>
				           <c:if test="${detailItems.mustCheck == 0}"><fmt:message key="evaluation.form.detail._"/></c:if>
				          <!-- éå modif end 2012å¹´7æ6æ¥16:41:37				           -->
				          <input name="formId" type="hidden" value="${form.formId}">
				          <input name="categoryId" type="hidden" value="${category.categoryId}">
				          <input name="subCategoryId" type="hidden" value="${subCategory.subCategoryId}">
				          <input name="itemsId" type="hidden" value="${items.itemsId}">
				          <input name="detailItemsId" type="hidden" value="${detailItems.detailItemsId}">
				          <input name="weight" type="hidden" value="${form.weight}">
				          <input name="mustCheck" class ="mustCheckValue" type="hidden" value="${detailItems.mustCheck}">
				          
				      </td>
				      <td style="width:50px;text-align:center;">
				      
				        <form:select path="priority" class="priority"  style="border:solid 1px #aacfe4;" multiple="false">
				          <!-- éå modif  2012å¹´7æ6æ¥16:41:13 -->
				      	  <!-- 
				          <option value="1" <%="1".equals(priority)?"selected=\"selected\"":""%>>High</option>
				          <option value="2" <%="2".equals(priority)?"selected=\"selected\"":""%>>Middle</option>
				          <option value="3" <%="3".equals(priority)?"selected=\"selected\"":""%>>Low</option>
				          -->
				          <option value="1" <%="1".equals(priority)?"selected=\"selected\"":""%>><fmt:message key="evaluation.form.detail.High"/></option>
				          <option value="2" <%="2".equals(priority)?"selected=\"selected\"":""%>><fmt:message key="evaluation.form.detail.Middle"/></option>
				          <option value="3" <%="3".equals(priority)?"selected=\"selected\"":""%>><fmt:message key="evaluation.form.detail.Low"/></option>
				          <!-- éå modif end  2012å¹´7æ6æ¥16:41:13 -->
				        </form:select>
				      </td>
				      <td style="width:50px;text-align:center;">
				        <form:select path="yesNo" class="yesNo"  style="border:solid 1px #aacfe4;" multiple="false">
				          <option value="2" <%="2".equals(yesNo)?"selected=\"selected\"":""%>></option>
				          <!-- éå modif  2012å¹´7æ6æ¥16:41:13 -->
				      	  <!-- 
				          <option value="1" <%="1".equals(yesNo)?"selected=\"selected\"":""%>>Yes</option>
				          <option value="0" <%="0".equals(yesNo)?"selected=\"selected\"":""%>>No</option>
				          -->
				          <option value="1" <%="1".equals(yesNo)?"selected=\"selected\"":""%>><fmt:message key="evaluation.form.detail.Yes"/></option>
				          <option value="0" <%="0".equals(yesNo)?"selected=\"selected\"":""%>><fmt:message key="evaluation.form.detail.No"/></option>
				          <!-- éå modif  2012å¹´7æ6æ¥16:41:13 --> 
				        </form:select>
				      </td>
				      <td style="width:50px;text-align:center;" class="score_yesno">
				      </td>
				      <td style="width:50px;text-align:center;">
				        <form:select path="non_point" class="non_point"  style="border:solid 1px #aacfe4;" multiple="false">
				          <option value="0" <%="0".equals(non_point)?"selected=\"selected\"":""%>></option>
				          <option value="5" <%="5".equals(non_point)?"selected=\"selected\"":""%>>5</option>
				          <option value="4" <%="4".equals(non_point)?"selected=\"selected\"":""%>>4</option>
				          <option value="3" <%="3".equals(non_point)?"selected=\"selected\"":""%>>3</option>
				          <option value="2" <%="2".equals(non_point)?"selected=\"selected\"":""%>>2</option>
				          <option value="1" <%="1".equals(non_point)?"selected=\"selected\"":""%>>1</option>
				        </form:select>
				      </td>
				      <td style="width:50px;text-align:center;" class="score_point"></td>
				      <td style="width:50px;text-align:center;" class="top_score">25</td>
				    </tr>
				    <% index++; %>
	  			</c:forEach>
	  		</c:forEach>
	  	</c:forEach>
	  </c:forEach>
	</tbody>
	</table>
</c:forEach>

<hr>

<button type="button" class="btn_submit" style="margin-left:20px;"><fmt:message key="button.submit"/></button>
</form:form>
</div>
</div>
</body>
</html>