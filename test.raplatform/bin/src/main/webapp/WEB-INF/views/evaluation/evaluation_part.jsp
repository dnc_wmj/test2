<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>
<br>
<c:if test="${editable}">
<div class="button">
<c:if test="${workflow == null && !isReeval}">
	<c:if test="${tmpRuleId != null}">
		<a href="<c:url value="/evaluation/tmpsave/updateform/${eval_id}/${tmpRuleId}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<a href="<c:url value="/evaluation/updateform/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
</c:if>
<c:if test="${workflow == null && isReeval}">
<a href="<c:url value="/evaluation/updateform/reeval/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
<c:if test="${workflow != null}">
<a href="<c:url value="/evaluation/updateform/${workflow.evalId}/${workflow.id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
</div>
<br>
</c:if>
<table style="border:none;" class="basic_form_table">
<tbody>
  <tr>
    <th colspan="2" style="border:none;text-align:left;">
    <c:if test="${evaluation.totalPoint != -1}">
      <table>
      <tr>
        <td style="border:none;"><span style="font-size:1.7em;color:red;nowrap;white-space:nowrap;"><fmt:message key="confirm.total_point"/> : <c:out value="${evaluation.totalPoint}" /> Points</span></td>
        <td style="border:1px solid;background:#cccccc;">
          <span style="font-size:1.7em;white-space:nowrap;">
          <c:if test="${evaluation.totalPoint >= 90}">Preferred</c:if>
          <c:if test="${evaluation.totalPoint < 90 && evaluation.totalPoint >= 70}">Acceptable</c:if>
          <c:if test="${evaluation.totalPoint < 70 && evaluation.totalPoint >= 50}">Restricted</c:if>
          <c:if test="${evaluation.totalPoint < 50}">Unqualified</c:if>
          </span>
        </td>
        <td style="border:none;"><img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" /></td>
        <td style="border:1px solid;background:lemonchiffon;">
          <c:if test="${evaluation.totalPoint >= 90}">
            <span style="font-size:1.7em;white-space:nowrap;">Best Choice</span>
          </c:if>
          <c:if test="${evaluation.totalPoint < 90 && evaluation.totalPoint >= 50}">
            <span style="font-size:1.7em;color:blue;white-space:nowrap;">Vendor Development(Potential Vendor)</span>
          </c:if>          
          <c:if test="${evaluation.totalPoint < 50}">
            <span style="font-size:1.7em;color:redwhite-space:nowrap;">Eliminated</span>
          </c:if>
          
        </td>
      </tr>
      </table>
    </c:if>
    </th>
  </tr>
  <tr>
    <th colspan="2" style="padding-left:8px;border:none;text-align:left;">
      <fmt:message key="evaluation.date"/> : <c:out value="${evaluation.registeredDate}" />
    </th>
  </tr>
</tbody>
</table>
<!-- common evaluation needn't evaluation1 -->
<c:if test="${evaluation.businessType != 2}">
  <div class="index" style="color:green;"><fmt:message key="evaluation.title.1"/></div>
	<table class="basic_form_table">
	<tbody>
	  <tr>
	    <th><fmt:message key="evaluation.company_short_name"/> : </th>
	    <td><c:out value="${evaluation.shortName}" /></td>
	  </tr>
	  
	  <c:if test="${evaluation.businessType == 1}">
	  <tr>
		<th><fmt:message key="evaluation.product_name"/> : </th>
	    <td><c:out value="${evaluation.productName}" /></td>
	  </tr>  
	  <tr>
		<th><fmt:message key="evaluation.product_category"/> : </th>
	    <td><c:out value="${evaluation.productCategory}" /></td>
	  </tr>
	  </c:if>
	  
	  <c:if test="${evaluation.businessType == 0}">
	  <tr>
		<th><fmt:message key="evaluation.service_category"/> : </th>
	    <td>
	        <c:if test="${evaluation.serviceCategory == '0'}"><fmt:message key="evaluation.service_category.aircargo"/></c:if>
	        <c:if test="${evaluation.serviceCategory == '1'}"><fmt:message key="evaluation.service_category.seacargo"/></c:if>
	        <c:if test="${evaluation.serviceCategory == '2'}"><fmt:message key="evaluation.service_category.warehouse"/></c:if>
	        <c:if test="${evaluation.serviceCategory == '3'}"><fmt:message key="evaluation.service_category.importing.exporting"/></c:if>
	        <c:if test="${evaluation.serviceCategory == '4'}"><fmt:message key="evaluation.service_category.track.transportation"/></c:if>
	        <c:if test="${evaluation.serviceCategory == '5'}"><fmt:message key="evaluation.service_category.other"/></c:if>
	    
	    </td>
	  </tr>  
	  <tr>
		<th><fmt:message key="evaluation.service_industry"/> : </th>
	    <td><c:out value="${evaluation.serviceIndustry}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.service_scope"/> : </th>
	    <td><c:out value="${evaluation.serviceScope}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.major_accounts"/> : </th>
	    <td><c:out value="${evaluation.majorAccounts}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.volume_per_year"/> : </th>
	    <td><c:out value="${evaluation.volumePerYear}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.num_of_biz_sites_nationwide"/> : </th>
	    <td><c:out value="${evaluation.numOfBizSitesNationwide}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.forwarding_company"/> : </th>
	    <td><c:out value="${evaluation.forwardingCompany}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.info_fix_assets_nationwide"/> : </th>
	    <td><c:out value="${evaluation.infoFixAssetsNationwide}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.num_of_empl_nationwide"/> : </th>
	    <td><c:out value="${evaluation.numOfEmplNationwide}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.cooperation_condition"/> : </th>
	    <td><c:out value="${evaluation.cooperationCondition}" /></td>
	  </tr>
	  </c:if>
	    
	  <tr>
		<th><fmt:message key="evaluation.transaction_condition"/> : </th>
	    <td>
	      <c:if test="${evaluation.tranCond == 0}">
	        <fmt:message key="evaluation.transaction_condition.new"/>
	      </c:if>
	      <c:if test="${evaluation.tranCond == 1}">
	        <fmt:message key="evaluation.transaction_condition.existing"/>
	      </c:if>
	    </td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.selection_reason"/> : </th>
	    <td><pre><c:out value="${evaluation.selectionReason}" /></pre></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.comment"/> : </th>
	    <td><pre><c:out value="${evaluation.comment}" /></pre></td>
	  </tr>  
	  <tr>
		<th><fmt:message key="evaluation.applicant"/> : </th>
	    <td><c:out value="${evaluation.applicant}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.date2"/> : </th>
	    <td><c:out value="${evaluation.date2}" /></td>
	  </tr> 
	</tbody>
	</table>
	
	<div class="index" style="color:green;"><fmt:message key="evaluation.title.2"/></div>
</c:if>
<%int index = 0; %>

<c:forEach var="category" items="${evaluationCategories}" varStatus="ctgrStatus">
<div class="index">
<c:out value="${ctgrStatus.count}"/>. <c:out value="${category.name}"/> &lt;MAX <c:out value="${category.maxPoint}"/> Points &gt;
</div>
<table class="default_table">
<tbody>
  <!-- 2012-7-6 15:51:08 zhengtong modif -->
	<!-- 
	   <tr>
	    <th>Detail Content</th> 具体内容
	    <th>Evaluation Standards</th>评估标准
	    <th>Standards</th>打分标准
	    <th>Points</th>分数
	    
	    <th>Remark</th> 说明
	  </tr>
	 -->
	  <tr>
	    <th><fmt:message key="evaluation.form.trade.head.Detail-Content"/></th>
	    <th><fmt:message key="evaluation.form.trade.head.Evaluation-Standards"/></th>
	    <th><fmt:message key="evaluation.form.trade.head.Standards"/></th>
	    <th><fmt:message key="evaluation.form.trade.head.Points"/></th>
	    
	    <th><fmt:message key="evaluation.form.trade.head.Remark"/></th>
	  </tr>
 
  <c:forEach var="content" items="${category.evaluationContents}">
    <c:forEach var="item" items="${content.evaluationItems}" varStatus="itemStatus">
    <tr>      
      <c:if test="${itemStatus.count == 1}">
	      <td style="width:130px;" rowspan="<c:out value="${content.evaluationItemLength}"/>">
	          <c:out value="${content.content}"></c:out>
	      </td>
      </c:if>
      <td style="width:200px;"><c:out escapeXml="false" value="${item.evaluationStandard}"></c:out></td>
      <td><c:out escapeXml="false" value="${item.gradingStandard}"></c:out></td>
      <td  style="width:50px;text-align:center;">
        <%=((Evaluation)pageContext.findAttribute("evaluation")).getPoint()[index] %>
      </td>
      <td style="width:150px;">
         <c:set var="reIndex" value="<%= index%>"></c:set>
         <c:out value="${evaluation.remark[reIndex]}"></c:out>
	  </td>
	  <% index++; %>
    </tr>
    </c:forEach>
  </c:forEach>
</tbody>
</table>
</c:forEach>
