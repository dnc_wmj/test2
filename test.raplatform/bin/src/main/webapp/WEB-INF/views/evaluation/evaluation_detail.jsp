<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="confirm"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>

<div class="mainContainer">
<div class="basic_form contextContainer">
	<c:if test="${evaluation.businessType < 3}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
    </c:if>
    <c:if test="${evaluation.businessType >= 3 && evaluation.businessType <= 9}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part.jsp" %>
    </c:if>
    <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part_second.jsp" %>
    </c:if>
</div>
</div>
</body>
</html>