<%@ include file="/WEB-INF/views/includes.jsp" %>
<div class="basic_form" style="width:1164px">
    <table style="" class="default_table">
      <tr>
        <th style="width:100px;">&nbsp;</th>
        <th style="width:150px:">Grand TTL</th>
        <th style="width:150px:">Grand Top TTL</th>
        <th style="width:150px:"><fmt:message key="evaluation.date"/></th>
      </tr>
      <c:forEach var="eval" items="${pastEvaluations}">
        <tr>
          <td style="width:100px;">
            <div class="button">
              <a href="<c:url value="/evaluation/view/${eval.id}"/>"><fmt:message key="workflow.detail"/></a>
            </div>
          </td>  
          <td><c:out value="${eval.grandScore}"/></td>
          <td><c:out value="${eval.grandTopScore}"/></td>
          <td><c:out value="${eval.shanghaiRegisteredDate}"/></td>
        </tr>
      </c:forEach>
    </table>
</div>
