<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="evaluation.title"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<div class="mainContainer">
<div class="basic_form contextContainer">
<h1 class="alt"><fmt:message key="evaluation.title"/> - <c:out value="${identity.name}"/></h1>
<table style="border:none;" class="basic_form_table">
<tbody>
  <tr>
    <th colspan="2" style="border:none;text-align:left;">
    <c:if test="${evaluation.totalPointForCompare != -1}">
      <span style="font-size:1.7em;color:red;"><fmt:message key="confirm.total_point"/> : <c:out value="${evaluation.totalPoint}" /> Points</span><br>
    </c:if>
    </th>
  </tr>
</tbody>
</table>

<c:if test="${wfid == null}">
  <c:if test="${!forMaintainance && !isReeval}">
    <spring:url value="/evaluation/update" var="formUrl"/>
  </c:if>
  <c:if test="${!forMaintainance && isReeval}">
    <spring:url value="/evaluation/reeval/update" var="formUrl"/>
  </c:if>
  <c:if test="${forMaintainance}">
    <spring:url value="/evaluation/update/maintainance" var="formUrl"/>
  </c:if>  
</c:if>
<c:if test="${wfid != null}">
<spring:url value="/evaluation/update/${wfid}" var="formUrl"/>
</c:if>

<form:form action="${fn:escapeXml(formUrl)}"
	modelAttribute="evaluation" 
	method="post">
<form:hidden path="id"/>
<form:hidden path="vendorId"/>
<form:hidden path="businessType"/>
<div class="index" style="color:green;"><fmt:message key="evaluation.title.1"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="evaluation.company_short_name"/> : </th>
    <td style="width:350px;"><form:input path="shortName"/></td>
    <td style="color:green;"><fmt:message key="evaluation.company_short_name.description"/></td>
  </tr>
  

  <tr>
	<th><fmt:message key="evaluation.server_name"/> : </th>
    <td><form:input path="serviceName"/></td>
    <td style="color:green;"><fmt:message key="evaluation.server_name.description"/></td>
  </tr>  
  <tr>
	<th><fmt:message key="evaluation.service2_category"/> : </th>
    <td><form:input path="service2Category"/></td>
    <td style="color:green;"><fmt:message key="evaluation.service2_category.description"/></td>
  </tr>

  
  <tr>
	<th><fmt:message key="evaluation.transaction_condition"/> : </th>
    <td>
      <table>
        <tr>
          <td style="border:none;">
            <form:radiobutton value="0" path="tranCond"/> 
          	<fmt:message key="evaluation.transaction_condition.new"/>
          </td>
          <td style="border:none;">
            <form:radiobutton value="1" path="tranCond"/>
            <fmt:message key="evaluation.transaction_condition.existing"/>
          </td>
        </tr>
      </table>
    </td>
    <td style="color:green;"><fmt:message key="evaluation.transaction_condition.description"/></td>
  </tr>
  
 <!-- 
  <tr>
	<th><fmt:message key="evaluation.service_category"/> : </th>
    <td>
    <form:select path="serviceCategory">
        <form:option value="0"><fmt:message key="evaluation.service_category.aircargo"/></form:option>
        <form:option value="1"><fmt:message key="evaluation.service_category.seacargo"/></form:option>
        <form:option value="2"><fmt:message key="evaluation.service_category.warehouse"/></form:option>
        <form:option value="3"><fmt:message key="evaluation.service_category.importing.exporting"/></form:option>
        <form:option value="4"><fmt:message key="evaluation.service_category.track.transportation"/></form:option>
        <form:option value="5"><fmt:message key="evaluation.service_category.other"/></form:option>
      </form:select>
    </td>
    <td style="color:green;"><fmt:message key="evaluation.service_category.description"/></td>
  </tr>  
  <tr>
	<th><fmt:message key="evaluation.service_industry"/> : </th>
    <td><form:input path="serviceIndustry"/></td>
    <td style="color:green;"><fmt:message key="evaluation.service_industry.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.service_scope"/> : </th>
    <td><form:input path="serviceScope"/></td>
    <td style="color:green;"><fmt:message key="evaluation.service_scope.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.major_accounts"/> : </th>
    <td><form:input path="majorAccounts"/></td>
    <td style="color:green;"><fmt:message key="evaluation.major_accounts.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.volume_per_year"/> : </th>
    <td><form:input path="volumePerYear"/></td>
    <td style="color:green;"><fmt:message key="evaluation.volume_per_year.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.num_of_biz_sites_nationwide"/> : </th>
    <td><form:input path="numOfBizSitesNationwide"/></td>
    <td style="color:green;"><fmt:message key="evaluation.num_of_biz_sites_nationwide.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.forwarding_company"/> : </th>
    <td><form:input path="forwardingCompany"/></td>
    <td style="color:green;"><fmt:message key="evaluation.forwarding_company.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.info_fix_assets_nationwide"/> : </th>
    <td><form:input path="infoFixAssetsNationwide"/></td>
    <td style="color:green;"><fmt:message key="evaluation.info_fix_assets_nationwide.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.num_of_empl_nationwide"/> : </th>
    <td><form:input path="numOfEmplNationwide"/></td>
    <td style="color:green;"><fmt:message key="evaluation.num_of_empl_nationwide.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.cooperation_condition"/> : </th>
    <td><form:textarea path="cooperationCondition" style="font-size:12px;width:350px;height:3em;"></form:textarea></td>
    <td style="color:green;"><fmt:message key="evaluation.cooperation_condition.description"/></td>
  </tr>

 -->	  
  <tr>
	<th><fmt:message key="evaluation.selection_reason"/> : </th>
    <td><form:textarea path="selectionReason" style="font-size:12px;width:350px;height:3em;"></form:textarea></td>
    <td style="color:green;"><fmt:message key="evaluation.selection_reason.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.comment"/> : </th>
    <td><form:textarea path="comment" style="font-size:12px;width:350px;height:3em;"></form:textarea></td>
    <td style="color:green;"><fmt:message key="evaluation.comment.description"/></td>
  </tr>  
  <tr>
	<th><fmt:message key="evaluation.applicant"/> : </th>
    <td><form:input path="applicant"/></td>
    <td style="color:green;"><fmt:message key="evaluation.applicant.description"/></td>
  </tr>
  <tr>
	<th><fmt:message key="evaluation.date2"/> : </th>
    <td><form:input path="date2"/></td>
    <td style="color:green;"><fmt:message key="evaluation.date2.description"/></td>
  </tr>
</tbody>
</table>
<div class="index" style="color:green;"><fmt:message key="evaluation.title.2"/></div>
<div style="text-align: center; font-variant: normal; font-style: normal;" class="index">
	<span style="color: red;">*</span><fmt:message key="evaluation.nontrade.comment"/>
</div>
<%int index = 0; %>
<c:forEach var="category" items="${evaluationCategories}" varStatus="ctgrStatus">
<div class="index">
<c:out value="${ctgrStatus.count}"/>. <c:out value="${category.name}"/> &lt;MAX <c:out value="${category.maxPoint}"/> Points &gt;
</div>
<table class="default_table">
<tbody>
  <tr>
    <th>Detail Content</th>
    <th>Evaluation Standards</th>
    <th>Standards</th>
    <th>Points</th>
    <th>Remark</th>
  </tr>
  <c:forEach var="content" items="${category.evaluationContents}">
    <c:forEach var="item" items="${content.evaluationItems}" varStatus="itemStatus">
	    <c:forEach var="item2" items="${item.evaluationItemItems}" varStatus="item2Status">
	    <tr>      
	      <c:if test="${itemStatus.count == 1 && item2Status.count == 1}">
		      <td style="width:130px;" rowspan="<c:out value="${content.evaluationNonItemLength}"/>">
		          <c:out value="${content.content}"></c:out>
		      </td>
	      </c:if>
	      <c:if test="${item2Status.count == 1}">
		      <td style="width:200px;" rowspan="<c:out value="${item.evaluationItemItemLength}"/>">
		          <c:out escapeXml="false" value="${item.evaluationStandard}"></c:out>
		      </td>
	      </c:if>
	      <td><c:out escapeXml="false" value="${item2.gradingStandard}"></c:out></td>
	      <td  style="width:50px;text-align:center;">
	        <form:select path="point" style="border:solid 1px #aacfe4;" multiple="false">
	          <c:forEach var="point" items="${item2.pointArray}">
	            
	            <%
	            boolean selected = false;
	            if (Integer.parseInt((String)pageContext.getAttribute("point")) ==
	      			((Evaluation)pageContext.findAttribute("evaluation")).getPoint()[index]) {
	            	selected = true;
	            }
	      		%>
	      		<option value="${point}" <%=selected ? "selected" : "" %>>
	            ${point}
	          </c:forEach>
	        </form:select>
	       </td>
	       <td style="width:150px;">
	         <% 
	           String remark = ((Evaluation)pageContext.findAttribute("evaluation")).getRemark()[index];
	           if (remark == null || "".equals(remark)) {
	        	   remark = "";
	           }
	         %>
	         <textarea value="" name="remark" style="font-size:12px;border:solid 1px #aacfe4;width:100%;height:3em;"><%=remark%></textarea>
		   </td>
	    </tr>
	    <% index++; %>
	    </c:forEach>
    </c:forEach>
  </c:forEach>
</tbody>
</table>
</c:forEach>

<hr>

<button type="submit" style="margin-left:20px;"><fmt:message key="button.edit"/></button>
</form:form>
</div>
</div>
</body>
</html>