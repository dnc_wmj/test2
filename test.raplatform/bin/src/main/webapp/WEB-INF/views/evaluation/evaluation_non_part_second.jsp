<%@page import="cn.sscs.vendormanagement.evaluation.Evaluation"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>

<br>
<c:if test="${editable}">
<div class="button">
<c:if test="${workflow == null && !isReeval}">
	<c:if test="${tmpRuleId != null}">
		<a href="<c:url value="/evaluation/tmpsave/updateform/${eval_id}/${tmpRuleId}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<a href="<c:url value="/evaluation/updateform/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
	</c:if>
</c:if>
<c:if test="${workflow == null && isReeval}">
<a href="<c:url value="/evaluation/updateform/reeval/${eval_id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
<c:if test="${workflow != null}">
<a href="<c:url value="/evaluation/updateform/${workflow.evalId}/${workflow.id}" />"><fmt:message key="evaluation.edit"/></a>
</c:if>
</div>
<br>
</c:if>
<!-- 
<table style="border:none;" class="basic_non_trade_form_table">
<tbody>
  <tr>
    <th colspan="2" style="border:none;text-align:left;">
    <c:if test="${evaluation.totalPointForCompare != -1}">
      <table>
      <tr>
        <td style="border:none;"><span style="font-size:1.7em;color:red;nowrap;white-space:nowrap;"><fmt:message key="confirm.total_point"/> : <c:out value="${evaluation.totalPoint}" /> Points</span></td>
        <td style="border:1px solid;background:#cccccc;">
          <span style="font-size:1.7em;white-space:nowrap;">
          <c:if test="${evaluation.totalPointForCompare >= 90}">Preferred</c:if>
          <c:if test="${evaluation.totalPointForCompare < 90 && evaluation.totalPointForCompare >= 70}">Acceptable</c:if>
          <c:if test="${evaluation.totalPointForCompare < 70 && evaluation.totalPointForCompare >= 50}">Restricted</c:if>
          <c:if test="${evaluation.totalPointForCompare < 50}">Unqualified</c:if>
          </span>
        </td>
        <td style="border:none;"><img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" /></td>
        <td style="border:1px solid;background:lemonchiffon;">
          <c:if test="${evaluation.totalPointForCompare >= 90}">
            <span style="font-size:1.7em;white-space:nowrap;">Best Choice</span>
          </c:if>
          <c:if test="${evaluation.totalPointForCompare < 90 && evaluation.totalPointForCompare >= 50}">
            <span style="font-size:1.7em;color:blue;white-space:nowrap;">Vendor Development(Potential Vendor)</span>
          </c:if>          
          <c:if test="${evaluation.totalPointForCompare < 50}">
            <span style="font-size:1.7em;color:redwhite-space:nowrap;">Eliminated</span>
          </c:if>
          
        </td>
      </tr>
      </table>
    </c:if>
    </th>
  </tr>
  <tr>
    <th colspan="2" style="padding-left:8px;border:none;text-align:left;">
      <fmt:message key="evaluation.date"/> : <c:out value="${evaluation.registeredDate}" />
    </th>
  </tr>
</tbody>
</table>
 -->
  <div class="index" style="color:green;"><fmt:message key="evaluation.title.1"/></div>
	<table class="basic_non_trade_form_table">
	<tbody>
	  <tr>
	    <th><fmt:message key="evaluation.company_short_name"/> : </th>
	    <td><c:out value="${evaluation.shortName}" /></td>
	  </tr>
	  
	  <tr>
		<th><fmt:message key="evaluation.server_name"/> : </th>
	    <td><c:out value="${evaluation.serviceName}" /></td>
	  </tr>  
	  <tr>
		<th><fmt:message key="evaluation.service2_category"/> : </th>
	    <td><c:out value="${evaluation.service2Category}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.transaction_condition"/> : </th>
	    <td>
	      <c:if test="${evaluation.tranCond == 0}">
	        <fmt:message key="evaluation.transaction_condition.new"/>
	      </c:if>
	      <c:if test="${evaluation.tranCond == 1}">
	        <fmt:message key="evaluation.transaction_condition.existing"/>
	      </c:if>
	    </td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.selection_reason"/> : </th>
	    <td><pre><c:out value="${evaluation.selectionReason}" /></pre></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.comment"/> : </th>
	    <td><pre><c:out value="${evaluation.comment}" /></pre></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.applicant"/> : </th>
	    <td><c:out value="${evaluation.applicant}" /></td>
	  </tr>
	  <tr>
		<th><fmt:message key="evaluation.date2"/> : </th>
	    <td><c:out value="${evaluation.date2}" /></td>
	  </tr>  
	</tbody>
	</table>
	
	<div class="index" style="color:green;"><fmt:message key="evaluation.title.2"/></div>
<c:forEach var="form" items="${nonTradeEvaluationForm}" varStatus="formStatus">
	<div class="index">
		<c:out value="${formStatus.count}"/>. <c:out value="${form.content}"/>
	</div>
	<table class="default_non_trade_table">
	<tbody>
	 <!-- 2012-7-6 15:51:08 zhengtong modif -->
	<!-- 
	  <tr> 
	    <th>Category</th> 分类 
	    <th>Sub-Category</th>子	分类 
	    <th>Items</th>项目
	    <th>Detail Items (English version)</th>具体
	    
	    <th>Essential</th>重要项目
	    <th>Priority</th>重要
	    <th>Yes/No</th>是否
	    <th>Score</th>分数
	    
	    <th>Point</th>点数
	    <th>Score</th>分数
	    <th>Top Score</th>最高分数
	  </tr>
	 -->
	  <tr>
	    <th><fmt:message key="evaluation.form.head.Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Sub-Category"/></th>
	    <th><fmt:message key="evaluation.form.head.Items"/></th>
	    <th><fmt:message key="evaluation.form.head.Detail-Items"/></th>
	    
	    <th><fmt:message key="evaluation.form.head.Essential"/></th>
	    <th><fmt:message key="evaluation.form.head.Priority"/></th>
	    <th><fmt:message key="evaluation.form.head.Yes-No"/></th>
	    <th><fmt:message key="evaluation.form.head.Score"/></th>
	    
	    <th><fmt:message key="evaluation.form.head.Point"/></th>
	    <th><fmt:message key="evaluation.form.head.Max-Score"/></th>
	    <th><fmt:message key="evaluation.form.head.Top-Score"/></th>
	  </tr>
	  <c:forEach var="category" items="${form.categoryList}" varStatus="categoryStatus">
	  	<c:forEach var="subCategory" items="${category.subCategorysList}" varStatus="subCategoryStatus">
	  		<c:forEach var="items" items="${subCategory.nonTradeItems}" varStatus="itemsStatus">
	  			<c:forEach var="detailItems" items="${items.detailItemsList}" varStatus="detailItemsStatus">
			  		<tr>      
				      <c:if test="${subCategoryStatus.count == 1 && itemsStatus.count == 1 && detailItemsStatus.count ==1}">
					      <td style="width:130px;" rowspan="<c:out value="${category.categoryLength}"/>">
					          <c:out escapeXml="false" value="${category.content}"></c:out>
					      </td>
				      </c:if>
				      <c:if test="${itemsStatus.count == 1 && detailItemsStatus.count ==1}">
					      <td style="width:150px;" rowspan="<c:out value="${subCategory.subCategorysLength}"/>">
					          <c:out escapeXml="false" value="${subCategory.content}"></c:out>
					      </td>
				      </c:if>
				      <c:if test="${detailItemsStatus.count ==1}">
					      <td style="width:200px;" rowspan="<c:out value="${items.nonTradeItemsLength}"/>">
					          <c:out escapeXml="false" value="${items.content}"></c:out>
					      </td>
				      </c:if>
				      
				      <td style="width:250px;">
				          <c:out escapeXml="false" value="${detailItems.content}"></c:out>
				      </td>
				      
				      <td style="width:50px;text-align:center;">
				          <c:if test="${detailItems.mustCheck == 1}"><fmt:message key="evaluation.form.detail.M"/></c:if>
				          <c:if test="${detailItems.mustCheck == 0}"><fmt:message key="evaluation.form.detail._"/></c:if>
				          <input name="formId" type="hidden" value="${form.formId}"/>
				          <input name="categoryId" type="hidden" value="${category.categoryId}"/>
				          <input name="subCategoryId" type="hidden" value="${subCategory.subCategoryId}"/>
				          <input name="itemsId" type="hidden" value="${items.itemsId}"/>
				          <input name="detailItemsId" type="hidden" value="${detailItems.detailItemsId}"/>
				          <input name="weight" type="hidden" value="${form.weight}"/>
				      </td>
				      <td style="width:50px;text-align:center;">
				          <c:if test="${detailItems.priority == 1}"><fmt:message key="evaluation.form.detail.High"/></c:if>
				          <c:if test="${detailItems.priority == 2}"><fmt:message key="evaluation.form.detail.Middle"/></c:if>
				          <c:if test="${detailItems.priority == 3}"><fmt:message key="evaluation.form.detail.Low"/></c:if>
				      </td>
				      <td style="width:50px;text-align:center;">
				        <c:if test="${detailItems.yesNo == 2}"></c:if>
				        <c:if test="${detailItems.yesNo == 1}"><fmt:message key="evaluation.form.detail.Yes"/></c:if>
				        <c:if test="${detailItems.yesNo == 0}"><fmt:message key="evaluation.form.detail.No"/></c:if>
				      </td>
				      <td style="width:50px;text-align:center;" class="score_yesno">
				      	<c:out escapeXml="false" value="${detailItems.scoreYesno}"></c:out>
				      </td>
				      <td style="width:50px;text-align:center;">
				      	<c:if test="${detailItems.point != 0}">
				        	<c:out escapeXml="false" value="${detailItems.point}"></c:out>
				        </c:if>
				      </td>
				      <td style="width:50px;text-align:center;" class="score_point">
				        <c:out escapeXml="false" value="${detailItems.scorePoint}"></c:out>
				      </td>
				      <td style="width:50px;text-align:center;" class="top_score">
					  	  <c:out escapeXml="false" value="${detailItems.topPoint}"></c:out>
					  </td>
				    </tr>
	  			</c:forEach>
	  		</c:forEach>
	  	</c:forEach>
	  </c:forEach>
	</tbody>
	</table>
</c:forEach>
