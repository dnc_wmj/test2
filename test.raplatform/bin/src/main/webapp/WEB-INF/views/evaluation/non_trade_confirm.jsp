<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="confirm"/></title>
</head>
<body>
<style type="text/css">
.errorScore {
	background-color: red;
}

.basic_non_trade_form {
	border: solid 2px green;
	margin: 0 auto;
	width: 1200px;
	padding: 14px;
}

.basic_non_trade_form h1 {
	font-size: 18px;
	font-weight: bold;
	margin-bottom: 8px;
	color: green;
}

.basic_non_trade_form p {
	font-size: 11px;
	color: #666666;
	margin-bottom: 20px;
	border-bottom: solid 1px green;
	padding-bottom: 10px;
}

.basic_non_trade_form button {
	cursor: hand;
	clear: both;
	margin-top: 5px;
	width: 150px;
	height: 30px;
	background: no-repeat #323232;
	text-align: center;
	color: #FFFFFF;
	font-size: 13px;
	font-weight: bold;
	border: 3px outset #ABABAB;
}

.mainContainer {
	text-align: center;
}

.contextContainer {
	text-align: left;
}

.input_error {
	background-color: khaki;
	border: solid 1px #aacfe4;
}

.checkbox_error {
	background-color: khaki;
	border: solid 1px #aacfe4;
}

.index {
	color: black;
	font-size: 16px;
	font-family: "Warnock Pro", "Goudy Old Style", "Palatino",
		"Book Antiqua", Georgia, serif;
	font-style: italic;
	font-weight: normal;
	padding-left: 20px;
	padding-top: 5px;
}

.basic_non_trade_form_table {
	border-top: solid 2px black;
	margin: 0 20px;
	width: 1150px;
	padding: 5;
	font-size: 12px;
	font-family: "Lucida Grande", "Lucida Sans Unicode", Verdana, Arial,
		Helvetica, sans-serif;
	color: #666666;
	border-collapse: collapse;
}


.basic_non_trade_form_table td {
	margin: 0;
	padding: 2px;
	font-size: 12px;
	border-bottom: dashed 1px silver;
}

.basic_non_trade_form_table th {
	text-align: right;
	border-bottom: dashed 1px silver;
	width: 300px;
}

.basic_non_trade_form_table input[type=text] {
	border: solid 1px #aacfe4;
	width: 100%;
}

.basic_non_trade_form_table input[type=password] {
	border: solid 1px #aacfe4;
	width: 100%;
}

.basic_non_trade_form_table textarea {
	border: solid 1px #aacfe4;
	font-size: 14px;
	width: 100%;
}

.basic_non_trade_form_table select {
	width: 100%;
}

.basic_non_trade_form_table input[type=radio] {
	font-size: 12px;
}

.workflowWord {
	font-size: 1.3em;
	color: green;
}

.workflowWord th {
	text-align: center;
	vertical-align: middle;
	border-bottom-width: 0px;
	width: auto;
}
</style>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.upload-1.0.2.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
  <spring:hasBindErrors name="submit">
  $('#ui-tab > ul').tabs("select","fragment-2");
  </spring:hasBindErrors>
	
    $("#attachment").change(function() {
	   $(this).upload('<c:url value="/evaluation/attachment/${basic_info.tranId}" />', function(res) {
			if (res.error != undefined && res.error) {
				alert("Faild to upload.");
				return false;
			}
			if (res != undefined && res == 'maxUploadSizeFailure') {
				alert("The uploaded file exceeds the maximum limit.");
				return false;
			}
			$(res).insertAfter(this);
		}, 'html');
	});
    $("#costcenter").change(function() {
    	$this = $(this);
    	var costcenter = $this.val();
        //var urltol1 = '<c:url value="/evaluation/changeCostcenterL1/${supercompany}/" />' + costcenter;
        //var urltol2 = '<c:url value="/evaluation/changeCostcenterL2/${supercompany}/" />' + costcenter;
      	var supercompany = '${supercompany}';
    	//var urltol1 = '<c:url value="/evaluation/changeCostcenterL1/${supercompany}" />' +'/'+ costcenter;
   		//var urltol2 = '<c:url value="/evaluation/changeCostcenterL2/${supercompany}" />' +'/'+ costcenter;
    
  		$("#l1").load('<c:url value="/evaluation/changeCostcenterL1/" />' +'/'+ supercompany +'/'+ costcenter);
	  	$("#l2").load('<c:url value="/evaluation/changeCostcenterL2/" />' +'/'+ supercompany +'/'+ costcenter);
    	//$("#l1").load(urltol1);
 	    //$("#l2").load(urltol2);
 	});
});
function delAttachment(attachment, aid, tid) {
	$.ajax({
		type: 'POST',
	    url: '<c:url value="/evaluation/attachment_workflow/delete"/>/' + aid +'/' + tid + '?_='+new Date(),
	    cache: false,
	    success : function(data){
	    	$("#" + $(attachment).attr('class')).remove();
	    }
	});
	$('#attachment').val("");
	return false;
} 
</script>

<div class="mainContainer">
<div class="basic_non_trade_form contextContainer">
<h1 class="alt"><fmt:message key="confirm"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>&nbsp;</p>
<c:if test="${basicInfoUpdate == null && basicInfoDelete == null && basicInfoFreeze == null && basicInfoUnfreeze == null}">
	<c:if test="${tmpRuleId != null}">
		<spring:url value="/evaluation/submit/${vendor_id}/${eval_id}/${tmpRuleId}" var="formUrl"/>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<spring:url value="/evaluation/submit/${vendor_id}/${eval_id}" var="formUrl"/>
	</c:if>
</c:if>
<c:if test="${basicInfoDelete != null}">
	<spring:url value="/vendor/delete/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoUpdate != null}">
	<spring:url value="/vendor/edit/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoFreeze != null}">
	<spring:url value="/vendor/freeze/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${basicInfoUnfreeze != null}">
	<spring:url value="/vendor/unfreeze/submit/${vendor_id}" var="formUrl"/>
</c:if>
<c:if test="${tmpRuleId != null}">
<spring:url value="/evaluation/retmpsave/${vendor_id}/${eval_id}/${tmpRuleId} " var="tempsvaeUrl"/>
</c:if>
<c:if test="${tmpRuleId == null}">
<spring:url value="/evaluation/tmpsave/${vendor_id}/${eval_id} " var="tempsvaeUrl"/>
</c:if>
<form:form action="${fn:escapeXml(formUrl)}"
	modelAttribute="submit" 
	method="post" cssClass="contextContainer">
	<input type="hidden" name="result" value="${evaluation.result}"/>
<c:if test="${evaluation.result == 0 || evaluation.result == 3}">
<table class="basic_non_trade_form_table">
  <tr>
    <td style="text-align:left;">
      <span style="font-size:1.3em;color:green;">
        <fmt:message key="workflow.applicant"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l1"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.l2"/> 
        <c:if test="${isSSCS}">
        <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <c:if test="${evaluation.result == 0}">[CEO] <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" /></c:if>
        <fmt:message key="workflow.hd"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.fm"/> <img style="vertical-align:bottom;" src="<c:url value="/images/arrow_next.png" />" />
        <fmt:message key="workflow.dm"/>
        </c:if>
      </span>
    </td>
  </tr>
</table>
<table class="basic_non_trade_form_table" style="border-top:none">    
<tbody>
  <tr>
    <th><fmt:message key="workflow.costcenter"/> : </th>
    <td>
      <form:select path="costcenter" multiple="false" style="width: 300px;" id="costcenter">
        <c:forEach var="costcenter" items="${costcenter}">
          <option value="${costcenter}" <c:if test="${selectedDivision == costcenter}">selected="true"</c:if>>${costcenter}
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="workflow.l1"/> : </th>
    <td>
      <form:select path="l1" multiple="false" style="width: 300px;" id="l1">
      	<option value="">
        <c:forEach var="l1" items="${l1s}">
          <option value="${l1.id}">${l1.name}(${l1.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
  <tr style="<c:if test="${l2NullErrer}">background-color: red;</c:if>">
    <th><fmt:message key="workflow.l2"/> : </th>
    <td>
      <form:select path="l2" multiple="false" style="width: 300px;" id="l2">
        <c:forEach var="l2" items="${l2s}">
          <option value="${l2.id}">${l2.name}(${l2.id})
        </c:forEach>
      </form:select>
    </td>
  </tr>
	 <c:if test="${isSSCS}">  
	  <c:if test="${evaluation.result == 0}">
		  <tr>
		    <th>CEO : </th>
		    <td>
		      <form:select path="ceo" multiple="false" style="width: 300px;">
		        <c:forEach var="ceo" items="${ceos}">
		          <option value="${ceo.id}">${ceo.name}(${ceo.id})
		        </c:forEach>
		      </form:select>
		    </td>
		  </tr>  
	  </c:if>
	  <tr>
	    <th><fmt:message key="workflow.hd"/> : </th>
	    <td>
	      <form:select path="hd" multiple="false" style="width: 300px;">
	        <c:forEach var="hd" items="${hds}">
	          <option value="${hd.id}">${hd.name}(${hd.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.fm"/> : </th>
	    <td>
	      <form:select path="fm" multiple="false" style="width: 300px;">
	        <c:forEach var="fm" items="${fms}">
	          <option value="${fm.id}">${fm.name}(${fm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	  <tr>
	    <th><fmt:message key="workflow.dm"/> : </th>
	    <td>
	      <form:select path="dm" multiple="false" style="width: 300px;">
	        <c:forEach var="dm" items="${dms}">
	          <option value="${dm.id}">${dm.name}(${dm.id})
	        </c:forEach>
	      </form:select>
	    </td>
	  </tr>
	</c:if>  

<c:if test="${evaluation.totalPointForCompare >= 70}">
  <tr>
    <td style="border:none;">
      <button type="submit" style="margin-left:90px;"><fmt:message key="button.submit"/></button>
    </td>
  		<c:if test="${tmpRuleId == null && basicInfoUpdate == null && basicInfoDelete == null && basicInfoFreeze == null && basicInfoUnfreeze == null}">
		    <td style="border:none;">
		      <button type="button" onclick="window.location='${fn:escapeXml(tempsvaeUrl)}'"style="margin-left:0px;"><fmt:message key="button.save"/></button>
		    </td>
	    </c:if>
  </tr>
</c:if>
</tbody>
</table>
</c:if>
<table style="width:600px;" class="default_table">
<tr>
  <th style="width:100px;"><fmt:message key="basicinfo.form.attachment"/></th>
  <td>
  <c:forEach var="attachment" items="${attachments}">
    <a href="<c:url value="/vendor/attachment/download/${attachment.id}"/>"><c:out value="${attachment.name}"/></a><br>
  </c:forEach>
  </td>
</tr>
</table>

<div class="index"><span style="color:red;">* </span><fmt:message key="evaluation.attachment.title"/>
	<c:if test="${attachmentsIsRequired}"><span class="errors" style="font-size: 12px;"><fmt:message key="error.required"/></span></c:if>
</div>
<table class="basic_non_trade_form_table"">
<tbody>
  <tr>
    <th style="text-align:left;">
    	<div style="margin-top:2px;">
    	<a href="<c:url value="/samples/VendorCompare.xls"/>"><fmt:message key="evaluation.attachment.template"/></a>
    	</div>
    	<input id="attachment" name="file" type="file" class="<c:if test='${attachmentsIsRequired}'>input_error</c:if>"/>
    	
      <c:forEach var="attachment2" items="${attachment_workflow}">
	  <div id="a-<c:out value="${attachment2.id}"/>">
        <table>
          <tr><td rowspan="4" style="border:none;">
            <a class="a-<c:out value="${attachment2.id}"/>" onClick="delAttachment(this, '<c:out value='${attachment2.id}'/>', '<c:out value='${attachment2.tranId}'/>');return false;"><img src="<c:url value="/images/action_delete.png"/>"/></a>
          </td></tr>
          <tr><td style="border:none;">Content Type : <c:out value="${attachment2.contentType}" /></td></tr>
          <tr><td style="border:none;">Name : <c:out value="${attachment2.name}" /><br></td></tr>
          <tr><td style="border:none;">Size : <c:out value="${attachment2.size}" /><br></td></tr>
        </table>
        <hr>
       </div>      
     </c:forEach>
    </th>
  </tr>
</tbody>
</table>

<hr>
<div id="ui-tab">
  <ul>
    <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
    <li><a href="#fragment-1"><span><fmt:message key="evaluation.score_summary"/></span></a></li>
    </c:if>
    <c:if test="${internal == false}">
    <li><a href="#fragment-3"><span><fmt:message key="evaluation.title"/></span></a></li>
    </c:if>
    <li><a href="#fragment-2"><span><fmt:message key="basicinfo.detail"/></span></a></li>
  </ul>

  <c:if test="${internal == false}">
  <div id="fragment-1">
  	<c:if test="${evaluation.businessType < 3}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part.jsp" %>
    </c:if>
    <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
    	<%@ include file="/WEB-INF/views/evaluation/evaluation_part_score_summary.jsp" %>
    </c:if>
  </div>  
  </c:if>
  <c:if test="${evaluation.businessType >= 10 && evaluation.businessType <= 16}">
	  <div id="fragment-3">
	    	<%@ include file="/WEB-INF/views/evaluation/evaluation_non_part_second.jsp" %>
	  </div>  
  </c:if>
  <div id="fragment-2">
    <%@ include file="/WEB-INF/views/vendor/basic_information_part_score_summary.jsp" %>
  </div>

</div>
</form:form>
</div>
</div>
</body>
</html>