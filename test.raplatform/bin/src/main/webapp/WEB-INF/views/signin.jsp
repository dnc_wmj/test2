<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
  <title><fmt:message key="signin.title"/></title>
</head>
<body>
  <link rel="stylesheet" href="<c:url value="/styles/signin.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
  <link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
  <div class="mainContainer">
  <div id="signin" >
    <form id="form" name="form" method="post" action="<c:url value="/signinprocess" />">
      <h1 class="contextContainer"><fmt:message key="signin.title"/></h1>
      <c:if test="${not empty param.hasSigninError}">
		<div class="errors">
			<fmt:message key="error.signin"/><br>
		</div>
	  </c:if>
	  
	  <c:if test="${not empty param.userIsLocked}">
		<div class="errors">
			<fmt:message key="error.user.locked"/><br>
		</div>
	  </c:if>
      <label style="position: relative; top: 5px;"><fmt:message key="signin.id"/></label>
      <input type="text" name="j_username" id="name" value="${username}" 
      		style="border-color:#FAFAD2; background-color:#FAFAD2;position: relative; top: -2px"
      		readonly="readonly"/>
	    
      <label  style="position: relative; top: 5px;"><fmt:message key="signin.password"/></label>
      <input type="password" name="j_password" id="password" />

      <button type="submit"><fmt:message key="signin.button"/></button>
      <div class="spacer">
    </div>
  </form>
  </div>
  </div>
</body>
</html>