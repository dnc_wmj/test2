<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="documents.title"/></title>
<base target="_self">
</head>
<body>
<br>
<br>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<div class="mainContainer">
<div class="basic_form contextContainer" style="width:600px;">
<h3 ><fmt:message key="documents.title"/></h3>
<hr>
<fmt:message key="documents.info.a"/>
</div>
</div>