<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="search.detail"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_non_trade_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/table.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>
<script type="text/javascript">
$(function() {
  $('#ui-tab > ul').tabs();
});
</script>

<div class="mainContainer">
<div class="basic_non_trade_form contextContainer">
<h1 class="alt"><fmt:message key="search.detail"/> - <c:out value="${basic_info.identity.name}" /></h1>
<p>
<span style="padding-left:10px;font-size:1.4em;font-style:italic;">
  STATUS : 
      <c:if test="${basic_info.status == 5}">
        <fmt:message key="search.status.5"/>
      </c:if>
      <c:if test="${basic_info.status == 10}">
        <fmt:message key="search.status.10"/>
      </c:if>
	  <c:if test="${basic_info.status == 20}">
        <fmt:message key="search.status.20"/>
      </c:if>
	  <c:if test="${basic_info.status == 30}">
        <fmt:message key="search.status.30"/>
      </c:if>
	  <c:if test="${basic_info.status == 40}">
        <fmt:message key="search.status.40"/>
      </c:if>
      <c:if test="${basic_info.status == 50}">
        <fmt:message key="search.status.50"/>
      </c:if>	  
</span>
</p>
<table>
<tr>
<c:if test="${basic_info.status != 5 && basic_info.status != 50}">
	<c:if test="${hasEvaluation == true || basic_info.identity.vendorType == 0}">
		<td>
			<form action="<c:url value="/evaluation/reeval/${basic_info.vendorId}" />" method="get">
			  <button type="submit"><fmt:message key="evaluation.reeval"/></button>
			</form>
		</td>
	</c:if>
	<c:if test="${!freeze}">
		<td>
			<form action="<c:url value="/vendor/updateform/edit/basic/${basic_info.vendorId}" />" method="get">
			  <button type="submit"><fmt:message key="basicinfo.edit"/></button>
			</form>
		</td>
	</c:if>
	<c:if test="${delete}">
		<td>
			<form action="<c:url value="/vendor/delete/confirm/${basic_info.vendorId}" />" method="get">
			  <button type="submit"><fmt:message key="basicinfo.delete"/></button>
			</form>
		</td>
	</c:if>
	<c:if test="${!freeze}">
		<td>
			<form action="<c:url value="/vendor/freeze/confirm/${basic_info.vendorId}" />" method="get">
			  <button type="submit"><fmt:message key="basicinfo.freeze"/></button>
			</form>
		</td>
	</c:if>
	<c:if test="${basic_info.identity.vendorType != 0 && freeze}">
		<td>
			<form action="<c:url value="/vendor/unfreeze/confirm/${basic_info.vendorId}" />" method="get">
			  <button type="submit"><fmt:message key="basicinfo.unfreeze"/></button>
			</form>
		</td>
	</c:if>
</c:if>

<c:if test="${basic_info.status == 5}">
<td>
<form action="<c:url value="/vendor/updateform/edit/basic/${basic_info.vendorId}" />" method="get">
  <button type="submit"><fmt:message key="basicinfo.continueto.edit"/></button>
</form>
</td>
<td>
<form action="<c:url value="/vendor/edit/cancel/basic/${basic_info.vendorId}" />" method="get">
  <button type="submit"><fmt:message key="basicinfo.cancel.edit"/></button>
</form>
</td>

</c:if>
</tr>
</table>
<br>
<table style="width:600px;" class="default_non_trade_table">
<tr>
  <th style="width:100px;"><fmt:message key="basicinfo.form.attachment"/></th>
  <td>
  <c:forEach var="attachment" items="${attachments}">
    <a href="<c:url value="/vendor/attachment/download/${attachment.id}"/>"><c:out value="${attachment.name}"/></a><br>
  </c:forEach>
  </td>
</tr>
</table>
<hr>
<div id="ui-tab">
  <ul>
    <li><a href="#fragment-1"><span><fmt:message key="basicinfo.detail"/></span></a></li>
    <c:if test="${hasEvaluation == true}">
    	<li><a href="#fragment-4"><span><fmt:message key="evaluation.score_summary"/></span></a></li>
    	<li><a href="#fragment-2"><span><fmt:message key="evaluation.latest"/></span></a></li>
    	<li><a href="#fragment-3"><span><fmt:message key="evaluation.past"/></span></a></li>
    </c:if>
  </ul>

  <div id="fragment-1">
    <%@ include file="/WEB-INF/views/vendor/basic_information_part_score_summary.jsp" %>
  </div>
  <c:if test="${hasEvaluation == true}">
  	
    <div id="fragment-4">
      <%@ include file="/WEB-INF/views/evaluation/evaluation_part_score_summary.jsp" %>
    </div>
    <div id="fragment-2">
	  <%@ include file="/WEB-INF/views/evaluation/evaluation_non_part_second.jsp" %>
	</div> 
    <div id="fragment-3">
      <%@ include file="/WEB-INF/views/evaluation/evaluation_past_list_non_trade.jsp" %>
    </div>
    
  </c:if>

</div>
</div>
</div>
</body>
</html>