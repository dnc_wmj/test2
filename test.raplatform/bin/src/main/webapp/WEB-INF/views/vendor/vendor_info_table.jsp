<%@ include file="/WEB-INF/views/includes.jsp" %>

<div style="overflow:scroll;width:900px;">
<table style="width:1024px;" class="default_table">
  <tr>
    <c:if test="${editable}">
      <th><fmt:message key="vendorInfo.table.action"/></th>
    </c:if>
    <th><fmt:message key="vendorInfo.table.name"/></th>  
    <th><fmt:message key="vendorInfo.table.vendor_code"/></th>
    <th><fmt:message key="search.vendor_number"/></th>
    <th><fmt:message key="vendorInfo.table.address"/></th>
    <th><fmt:message key="vendorInfo.table.tel"/></th>    
    <th><fmt:message key="vendorInfo.table.bank"/></th>
    <th><fmt:message key="vendorInfo.table.bank_account"/></th>
  </tr>
<c:forEach var="vendor" items="${vendors}">
  <c:set var="rowSpan" value="${vendor.bankInformation.staticBankAccountCount}"/>
  <tr>
    <c:if test="${editable}">
    <td rowSpan="<c:out value="${rowSpan}" />" style="white-space:nowrap;text-align:center;width:280px;">
      <div class="button" style="width:100%;white-space:nowrap;">
        <a href="<c:url value="/vendor/updateform/maintainance/basic/${vendor.vendorId}" />"><fmt:message key="vendorInfo.table.edit.vbi"/></a>
        <c:if test="${vendor.hasEvaluation}">
          <a href="<c:url value="/evaluation/updateform/maintainance/${vendor.vendorId}" />"><fmt:message key="vendorInfo.table.edit.eval"/></a>
        </c:if>
        <c:if test="${delete}">
       	  <a href="<c:url value="/vendor/delete/${vendor.vendorId}" />"><fmt:message key="button.delete"/></a>
     	</c:if>
     	<c:if test="${vendor.freeze != 1}">
       	  <a href="<c:url value="/vendor/freeze/${vendor.vendorId}" />"><fmt:message key="button.freeze"/></a>
     	</c:if>
     	<c:if test="${vendor.freeze == 1}">
       	  <a href="<c:url value="/vendor/unfreeze/${vendor.vendorId}" />"><fmt:message key="button.unfreeze"/></a>
     	</c:if>
      </div>
    </td>
    </c:if>
    <td rowSpan="<c:out value="${rowSpan}" />"><c:out value="${vendor.identity.name}" /></td>
    <td rowSpan="<c:out value="${rowSpan}" />" style="text-align:center;width:60px;">
      <c:out value="${vendor.identity.vendorCode}" />
    </td>
    <td rowSpan="<c:out value="${rowSpan}" />" style="text-align:center;width:90px;">
      <c:out value="${vendor.identity.vendorNumber}" />
    </td>
    <td rowSpan="<c:out value="${rowSpan}" />" style="width:90px;"><c:out value="${vendor.contact.address}" /></td>
    <td rowSpan="<c:out value="${rowSpan}" />" style="width:80px;"><c:out value="${vendor.contact.tel}" /></td>
  <c:forEach var="bank" items="${vendor.bankInformation.staticBankAccounts}" varStatus="status">
  <c:if test="${status.index != 0}">
  <tr>
  </c:if>
    <td style="text-align:center;width:100px;">
      <c:out value="${bank.name}" />
    </td>
    <td style="text-align:center;width:100px;">
      <c:out value="${bank.account}" />
    </td>
  </tr>
  </c:forEach>
</c:forEach>
</table>
</div>