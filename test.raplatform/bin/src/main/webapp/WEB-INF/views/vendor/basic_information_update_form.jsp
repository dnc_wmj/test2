<%@page import="cn.sscs.vendormanagement.Utils"%>
<%@ include file="/WEB-INF/views/includes.jsp" %>
<html>
<head>
<title><fmt:message key="basicinfo.form.title.update"/></title>
</head>
<body>
<link rel="stylesheet" href="<c:url value="/styles/basic_form.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/ui.tabs.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<link rel="stylesheet" href="<c:url value="/styles/jquery-ui-1.8.8.custom.css" />" type="text/css" media="screen, projection" charset="utf-8"/>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-1.4.4.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.upload-1.0.2.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery-ui-1.8.8.custom.min.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/jquery.ui.datepicker-zh-CN.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.core.js" />"></script>
<script type="text/javascript" src="<c:url value="/scripts/lib/ui.tabs.min.js" />"></script>

<script type="text/javascript">
$(function() {

  var id = new Date().getMilliseconds();
  window.initBusinessType = $('.companyTypeBusinessType').val();
  $("#add_performance").click(function(){
	var uid = ++id;
    $("#biz_performance").before(
    	'<tr id="p-' + uid + '">' +
           '<input type="hidden" value="-1" name="businessInformation.bizInfoId">' +
           '<td style="border:none;"><a id="p-' + uid + '" href="#" onClick="delPeformance(this, -1);return false;"><img width="10px" height="10px" src="<c:url value="/images/action_delete.png"/>"/></a></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.year"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.salesAmount"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.profit"></td>' +
           '<td style="border:none;"><input type="text" name="businessInformation.profitability"></td>' +
        '</tr>'
    );
    return false;
  });

  $('#ui-tab > ul').tabs();

  $("#add_bank_info").click(function() {
 var bankInfo = $("#bank_info").clone();

    var uid = ++id;

    bankInfo.attr("id", "b-" + uid);
    var obj = $("table", bankInfo).find('.searchBank').eq(0);
    $(obj).attr("colspan","3");
    $(obj).parent().append('<td style="text-align:right;"><a id="b-' + uid + '" onClick="delBank(this, -1);return false;"><img class="deleteIcon" src="<c:url value="/images/action_delete.png"/>"/><fmt:message key="button.delete"/></a></td>');
    $("input[name='bankInformation.bankAccountId']", bankInfo).attr("value", "-1");
    $("input[name='bankInformation.vendorSubcode']", bankInfo).attr("value", "");
    $("input[name='bankInformation.vendorSubcode']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.provinceAndRegion']", bankInfo).attr("value", "");
    $("input[name='bankInformation.provinceAndRegion']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.swiftCode']", bankInfo).attr("value", "");
    $("input[name='bankInformation.swiftCode']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.name']", bankInfo).attr("value", "");
    $("input[name='bankInformation.name']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.account']", bankInfo).attr("value", "");
    $("input[name='bankInformation.account']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.institutionNo']", bankInfo).attr("value", "");
    $("input[name='bankInformation.institutionNo']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.jointCode']", bankInfo).attr("value", "");
    $("input[name='bankInformation.jointCode']", bankInfo).removeClass("input_error");
    $("input[name='bankInformation.cnaps']", bankInfo).attr("value", "");
    $("input[name='bankInformation.cnaps']", bankInfo).removeClass("input_error");
    
    var banks = $("#banks");
    $(bankInfo).find('.errors').remove();
    banks.append(bankInfo);
    resetVendorSubcode();
    //
    $("input[name='bankInformation.type']", bankInfo).val(1);
    $(".bankInformationType", bankInfo).trigger('change');
    return false;
  });

  $("#country").change(function(evObj){
    var cid = $(this).val();
    $("#region").load('<c:url value="/vendor/regions" />' + '/' + cid);
  });

  $("#attachment").change(function() {
    $(this).upload('<c:url value="/vendor/attachment/${vendorBasicInfo.tranId}" />', function(res) {
        if (res.error != undefined && res.error) {
            alert("Faild to upload.");
            return false;
        }
        if (res != undefined && res == 'maxUploadSizeFailure') {
			alert("The uploaded file exceeds the maximum limit.");
    		return false;
    	}
        $(res).insertAfter(this);
    }, 'html');
  });

  $(".date").datepicker({
		changeMonth: true,
		changeYear: true,
		yearRange: "-40:+20"
	});
  
  $(".delivery_types").click(function(){
		if($(".delivery_types_other").get(0).checked){
				$(".delivery_types_other_value").show();
				$(".delivery_types_other_span").hide();
		}else{
				$(".delivery_types_other_value").hide();
				$(".delivery_types_other_span").show();
		}
  });
	
  $(".delivery_types").trigger("click");
  
  $("#banks").delegate(".bankInformationType","change",function(){
	  	var scope = $(this).parents('.basic_form_table').get(0);
	  	if($(this).val() == '1'){//not in china
	  			$('.institutionJointCode',scope).hide();
	  			$('.institutionNo',scope).val('').attr('readonly','true');
	  			$('.jointCode',scope).val('').attr('readonly','true');
	  			$('.cnaps',scope).removeAttr('readonly');
	  			$('.cnapsTr',scope).show();
	  		}else{//in china
	  			$('.institutionJointCode',scope).show();
	  			$('.cnapsTr',scope).hide();
	  			$('.institutionNo',scope).removeAttr('readonly');
	  			$('.jointCode',scope).removeAttr('readonly');
	  			$('.cnaps',scope).val('').attr('readonly','true');
	  		}
  });
  $(".bankInformationType").trigger('change');
});

function delPeformance(performance, id) {
	$.ajax({
        type: 'POST',
        url: '<c:url value="/vendor/bizinfo/delete"/>/' + id,
        cache: false,
        success : function(data){
            $(performance).parents('tr').eq(0).remove();
        }
    })
        
    return false;
}

function delBank(bank, id) {
	$.ajax({
        type: 'POST',
        url: '<c:url value="/vendor/bank/delete"/>/' + id,
        cache: false,
        success : function(data){
            $("#" + bank.id).empty();
            resetVendorSubcode();
        }
    });
    
    return false;
}

function resetVendorSubcode(){
    var len = $('.vendorSubcode').length;
    for (var i = 0; i < len; i++) {
        $('.vendorSubcode').eq(i).val((i+'').length == 1? "0"+i : i);
    }
}

function delAttachment(attachment, aid, tid) {
	$.ajax({
        type: 'POST',
        url: '<c:url value="/vendor/attachment/delete"/>/' + aid +'/' + tid + '?_='+new Date(),
        cache: false,
        success : function(data){
            $("#" + $(attachment).attr('class')).remove();
        }
    })
        
    return false;
}

function openWindowModalDialog(strHref,pWidth,pHeight,self){
    var strRef="";
    strRef=strRef+"dialogWidth="+pWidth+"px;dialogHeight="+pHeight+"px;";
    strRef=strRef+"dialogLeft="+(screen.availWidth/2-pWidth/2)+"px;dialogTop="+(screen.availHeight/2-pHeight/2)+"px;";
    strRef=strRef+"resizable=no;status=no;help=no";
    var obj = $(self).parents('.bank_area').eq(0);
    var returnValue = window.showModalDialog(strHref,'',strRef);
    if(!returnValue){
        returnValue = window.ReturnValue
    }
    if(returnValue != null && returnValue != undefined){
        if(returnValue.bankType == 'chineseBank'){
            $(obj).find('input[name$="bankInformation.name"]').eq(0).val(returnValue.bankName);
            $(obj).find('input[name$="bankInformation.institutionNo"]').eq(0).removeAttr('readonly').val(returnValue.bankInstitutionNo).parents('tr').eq(0).show();
            $(obj).find('input[name$="bankInformation.jointCode"]').eq(0).removeAttr('readonly').val(returnValue.bankJointCode).parents('tr').eq(0).show();
            $(obj).find('select[name$="bankInformation.type"]')[0].selectedIndex = 0;
            //
            $(obj).find('input[name$="bankInformation.cnaps"]').eq(0).val('').attr('readonly','true').parents('tr').eq(0).hide();
        }else if(returnValue.bankType == 'otherBank'){
            $(obj).find('input[name$="bankInformation.name"]').eq(0).val(returnValue.cnapName);
            $(obj).find('input[name$="bankInformation.cnaps"]').eq(0).removeAttr('readonly').val(returnValue.cnapNo).parents('tr').eq(0).show();
            $(obj).find('select[name$="bankInformation.type"]')[0].selectedIndex = 1;
            //
            $(obj).find('input[name$="bankInformation.institutionNo"]').eq(0).val('').attr('readonly','true').parents('tr').eq(0).hide();
            $(obj).find('input[name$="bankInformation.jointCode"]').eq(0).val('').attr('readonly','true').parents('tr').eq(0).hide();
        }
    }
}

/*
 * 当前台的input，类型是text时，后台对应的field是数组的情况下
 * 前台的input值中含有半角逗号，后台将接受2个值，
 * 所以需要将半角逗号替换，在后台接受值之后再替换回来
 */
function changeValue(){
	
	//银行国家及地区
	//$('.provinceAndRegion').val($('.provinceAndRegion').val().replace(',','->，'));
	//银行代码Swift Code
	
	//$('.swiftCode').val($('.swiftCode').val().replace(',','->，'));
	//银行名称
	
	//$('.bankName').val($('.bankName').val().replace(',','->，'));
	//银行账号
	
	//$('.bankAccount').val($('.bankAccount').val().replace(',','->，'));
	//机构号
	
	//$('.institution_no').val($('.institution_no').val().replace(',','->，'));
	//联行号
	
	//$('.jointCode').val($('.jointCode').val().replace(',','->，'));
	//CNAPS
	
	//$('.cnaps').val($('.cnaps').val().replace(',','->，'));
}

function checkBusinessType(){
	<c:if test='${canChangeBusinessType}'>
	// 能修改business type并且是编辑状态的一定是导入的供应商的基本信息编辑操作， 这时不进行提问
		<c:if test="${importVendorEditInfo == null}">
			if(window.initBusinessType != $('.companyTypeBusinessType').val()){
				if(!confirm('<fmt:message key="basicinfo.form.bussinessType.changed"/>')){
					return false;
				}
			}
		</c:if>
	</c:if>
}
</script>

<c:if test="${wfid == null}">
  <c:if test="${!forMaintainance}">
      
    <c:if test="${tmpRuleId != null}">
    	<!-- tmp save -->
		<spring:url value="/vendor/update/application/basic/${eval_id}/${tmpRuleId}" var="formUrl"/>
	</c:if>
	<c:if test="${tmpRuleId == null}">
		<spring:url value="/vendor/update/application/basic/${eval_id}" var="formUrl"/>
	</c:if>
  </c:if>
  <c:if test="${forMaintainance}">
    <spring:url value="/vendor/update/maintainance/basic/${vendorBasicInfo.tranId}" var="formUrl"/>
  </c:if>
  <c:if test="${forEdit && !isImportExterVendor}">
	<spring:url value="/vendor/update/edit/basic/${vendorBasicInfo.tranId}" var="formUrl"/>
  </c:if>
  <c:if test="${forEdit && isImportExterVendor}">
  	<!-- auto evaluation -->
	<spring:url value="/vendor/reeval/update/edit/basic/${vendorBasicInfo.tranId}" var="formUrl"/>
  </c:if>
</c:if>
<c:if test="${wfid != null && !isRoleHD}">
	<spring:url value="/vendor/update/workflow/basic/${wfid}" var="formUrl"/>
</c:if>
<c:if test="${wfid != null && isRoleHD}">
	<spring:url value="/vendor/update/workflow/hd/basic/${wfid}" var="formUrl"/>
</c:if>

<form:form action="${fn:escapeXml(formUrl)}" 
	modelAttribute="vendorBasicInfo" 
	method="post" cssClass="mainContainer" onsubmit='return checkBusinessType()' >
<div class="basic_form contextContainer">
<form:hidden path="vendorId"/>
<form:hidden path="tranId"/>
<form:hidden path="status"/>
<form:hidden path="identity.vendorNumber"/>
<h1 class="alt"><fmt:message key="basicinfo.form.title.update"/></h1>
<h3 class="alt" style="float: right; top: -30px; position: relative; right: 20px;"><fmt:message key="search.vendor_number"/> : <c:out value="${vendorBasicInfo.identity.vendorNumber}" /></h3>
<p style="color:red;"><fmt:message key="basicinfo.form.description"/></p>
<div class="index"><fmt:message key="basicinfo.form.name"/></div>
<table class="basic_form_table">
<tbody>
   <tr>
     <th>
    	<span style="color:red;">* </span><fmt:message key="basicinfo.form.company_type.parent_company"/> : <br>
    	<form:errors path="companyType.superCompanyType" cssClass="errors"/>
     </th>
     <td colspan="3">
     	<c:if test="${wfid != null}">
     		<!-- HD edit vendor info in workflow, approval employes not allow to change -->
     		<c:if test="${vendorBasicInfo.companyType.superCompanyType == 0}" >
		        &nbsp;
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 1}" >
		        <fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 2}" >
		        <fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 3}" >
		        <fmt:message key="basicinfo.form.company_type.parent_company.SSV"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 4}">
		        <fmt:message key="basicinfo.form.company_type.parent_company.SEW"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 5}">
		        <fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 6}">
		        <fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/>
		    </c:if>
		    <c:if test="${vendorBasicInfo.companyType.superCompanyType == 7}">
		        <fmt:message key="basicinfo.form.company_type.parent_company.SEH"/>
		    </c:if>
		    <form:hidden path="companyType.superCompanyType"/>
     	</c:if>
     	<c:if test="${wfid == null}">
	     	<form:select path="companyType.superCompanyType" style="width:auto;">
		       <c:if test="${!isSSCS}">
				   <c:forEach var="supercompany" items="${supercompany}">
				      <option value="${supercompany.value}">${supercompany.name}
				   </c:forEach>
			   </c:if>
			   <c:if test="${isSSCS}">
				   <form:option value="0">&nbsp;</form:option>
				   <form:option value="1"><fmt:message key="basicinfo.form.company_type.parent_company.SSCS"/></form:option>
				   <form:option value="2"><fmt:message key="basicinfo.form.company_type.parent_company.SSGE"/></form:option>
				   <form:option value="3"><fmt:message key="basicinfo.form.company_type.parent_company.SSV"/></form:option>
				   <form:option value="4"><fmt:message key="basicinfo.form.company_type.parent_company.SEW"/></form:option>
				   <form:option value="5"><fmt:message key="basicinfo.form.company_type.parent_company.SDPW"/></form:option>
				   <form:option value="6"><fmt:message key="basicinfo.form.company_type.parent_company.SPDH"/></form:option>
				   <form:option value="7"><fmt:message key="basicinfo.form.company_type.parent_company.SEH"/></form:option>
			   </c:if>
	       </form:select>
     	</c:if>
    </td>
  </tr>
  <tr>
  <tr>
    <th><fmt:message key="basicinfo.form.vendor_code"/> : <br></th>
    <td>
    <% if (Utils.isRoleHD()) {%>
      <form:input path="identity.vendorCode"/>
    <% } else { %>
      <form:input path="identity.vendorCode" readonly="true"/>
    <% } %>  
    </td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.vendor_type"/> : <br></th>
    <td style="width:240px;">
      <c:if test="${vendorBasicInfo.identity.vendorType == 0}" >
        <fmt:message key="basicinfo.form.vendor_type.external"/>
      </c:if>
      <c:if test="${vendorBasicInfo.identity.vendorType == 1}" >
        <fmt:message key="basicinfo.form.vendor_type.internal"/>
      </c:if>
      <c:if test="${vendorBasicInfo.identity.vendorType == 2}" >
        <fmt:message key="basicinfo.form.vendor_type.other"/>
      </c:if>
      <form:hidden path="identity.vendorType"/>
    </td>
  </tr>
  <tr>
    <th>
    <c:if test="${test}" >
	    <form:input path="test" cssErrorClass="input_error"/>
	    <form:input path="test" cssErrorClass="input_error"/>
	    <form:input path="test" cssErrorClass="input_error"/>
	    <form:input path="test" cssErrorClass="input_error"/>
	    <form:input path="test" cssErrorClass="input_error"/>
    </c:if>
      <fmt:message key="basicinfo.form.chinese_name"/> : <br>
      <form:errors path="identity.chineseName" cssClass="errors"/>
    </th>
    <td><form:input path="identity.chineseName" cssErrorClass="input_error"/></td>
    <th>
      <fmt:message key="basicinfo.form.english_name"/> : <br>
      <form:errors path="identity.englishName" cssClass="errors"/> 
    </th>
    <td><form:input path="identity.englishName" cssErrorClass="input_error"/></td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.contact"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.contact.country"/> : <br> 
    </th>
    <td><form:select id="country" path="contact.country" items="${countries}" itemLabel="name" itemValue="code" /></td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.contact.region"/> : <br></th>
    <td style="width:240px;">
      <form:select id="region" path="contact.region" items="${regions}" itemLabel="name" itemValue="code" style="width:240px;" />
    </td>
  </tr>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.contact.city"/> : <br>
    	<form:errors path="contact.city" cssClass="errors"/>
    </th>
    <td><form:input path="contact.city"  cssErrorClass="input_error"/></td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.contact.address"/> : <br>
      <form:errors path="contact.address" cssClass="errors"/>
    </th>
    <td><form:input path="contact.address" cssErrorClass="input_error"/></td>
  </tr>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.contact.tel"/> : <br>
      <form:errors path="contact.tel" cssClass="errors"/>
    </th>
    <td><form:input path="contact.tel" cssErrorClass="input_error"/></td>
    <th><fmt:message key="basicinfo.form.contact.postal_code"/> : <br></th>
    <td><form:input path="contact.postalCode"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.person"/> : <br></th>
    <td><form:input path="contact.person"/></td>
    <th><fmt:message key="basicinfo.form.contact.homepage"/> : <br></th>
    <td><form:input path="contact.homepage"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.contact.fax"/> : <br></th>
    <td><form:input path="contact.fax"/></td>
    <th colspan="2">&nbsp;</th>
  </tr>

</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.certificate"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.certificate.business_licence_id"/> : <br>
        <form:errors path="certificate.bizLicenceId" cssClass="errors"/>
    </th>
    <td><form:input path="certificate.bizLicenceId" cssErrorClass="input_error"/></td>
    <th><fmt:message key="basicinfo.form.certificate.legal_representative"/> : <br></th>
    <td style="width:240px;"><form:input path="certificate.legalRepresentative"/></td>
  </tr>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.certificate.registration_capital"/> : <br>
      <form:errors path="certificate.regCapital" cssClass="errors"/>
    </th>
    <td><form:input path="certificate.regCapital" cssErrorClass="input_error"/></td>
    <th><fmt:message key="basicinfo.form.certificate.registration_address"/> : <br></th>
    <td><form:input path="certificate.regAddress"/></td>
  </tr>
  <tr>
    <th>
        <fmt:message key="basicinfo.form.certificate.registration_date"/> : <br>
        <form:errors path="certificate.regDate" cssClass="errors"/>
    </th>
    <td><form:input class="date" path="certificate.regDate" cssErrorClass="input_error date"/></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_biz"/> : <br>
    <form:errors path="certificate.bizLicenceExpirationDate" cssClass="errors"/></th>
    <td><form:input class="date" path="certificate.bizLicenceExpirationDate" cssErrorClass="input_error date"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.state_tax_reg_id"/> : <br></th>
    <td><form:input path="certificate.stateTaxRegId"/></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_state"/> : <br>
    <form:errors path="certificate.stateTaxExpirationDate" cssClass="errors"/></th>
    <td><form:input class="date" path="certificate.stateTaxExpirationDate" cssErrorClass="input_error date"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.local_tax_reg_id"/> : <br></th>
    <td><form:input path="certificate.localTaxRegId"/></td>
    <th><fmt:message key="basicinfo.form.certificate.expiration_date_local"/> : <br>
    <form:errors path="certificate.localTaxExpirationDate" cssClass="errors"/></th>
    <td><form:input class="date" path="certificate.localTaxExpirationDate" cssErrorClass="input_error date"/></td>
  </tr>
  <tr>
     <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.certificate.certificate"/> : <br>
        <form:errors path="certificate.certIso9000" cssClass="errors"/>
    </th>
    <td style="padding:0;">
      <table>
        <tr>
          <td style="border:none;">
            <form:checkbox path="certificate.certIso9000" label="ISO9000" cssErrorClass="checkbox_error"/>
          </td>
        </tr>
        <tr>
          <td style="border:none;">
            <form:checkbox path="certificate.certIso14000" label="ISO14000" cssErrorClass="checkbox_error"/>
          </td>
        </tr>
        <tr>  
          <td style="border:none;">
            <form:checkbox path="certificate.certQs9000" label="QS9000" cssErrorClass="checkbox_error"/>
          </td>
        </tr>
      </table>
    </td>
    <th>Others : </th>
    <td><form:input path="certificate.certOther" cssErrorClass="input_error"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.certificate.listed_company"/> : <br></th>
    <td>
      <form:select path="certificate.listedCompany">
        <form:option value="true"><fmt:message key="basicinfo.yes"/></form:option>
        <form:option value="false"><fmt:message key="basicinfo.no"/></form:option>
      </form:select>      
    </td>
    <th><fmt:message key="basicinfo.form.certificate.reg_date_and_address"/> : <br></th>
    <td><form:input path="certificate.regDateAndAddress"/></td>
  </tr>
 <tr>
    <th><fmt:message key="basicinfo.form.certificate.have_transaction"/> : <br></th>
    <td>
      <form:select path="certificate.haveTransaction">
        <form:option value="true"><fmt:message key="basicinfo.yes"/></form:option>
        <form:option value="false"><fmt:message key="basicinfo.no"/></form:option>
      </form:select>
    </td>
    <th><fmt:message key="basicinfo.form.certificate.cooperation_date_period"/> : <br></th>
    <td><form:input path="certificate.cooperationDateOrPeriod"/></td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.company_type"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.domestic_investment"/> : <br></th>
    <td colspan="3">
      <table>
        <tr>
          <td style="border:none;">
            <form:radiobutton value="5" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.domestic_investment.state"/>
          </td>
          <td style="border:none;">
            <form:radiobutton value="6" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.domestic_investment.private"/>  
          </td>
          <td style="border:none;">
            <form:radiobutton value="7" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.domestic_investment.joint"/>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.foreign_investment"/> : <br></th>
    <td>
      <table style="width: 230px;">
        <tr>
          <td style="border:none;width:52px">
            <form:radiobutton value="0" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.foreign_investment.tw"/>
          </td>
          <td style="border:none;width:52px">
            <form:radiobutton value="1" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.foreign_investment.hk"/>
          </td>
          <td style="border:none;width:52px">
            <form:radiobutton value="2" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.foreign_investment.jp"/>
          </td>
          <td style="border:none;width:52px">
            <form:radiobutton value="3" path="companyType.foreignInvestment"/>
            <fmt:message key="basicinfo.form.company_type.foreign_investment.usa"/>
          </td>
        </tr>
      </table>
    </td>
    <th>
     <table>
        <tr>
         <td style="border:none;"><form:radiobutton value="4" path="companyType.foreignInvestment"/></td>
         <td style="border:none;">Others : </td>
       </tr>
     </table>
    </th>
    <td><form:input style="width:230px;" path="companyType.foreignInvestmentOther"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.joint_venture"/> : <br></th>
    <td colspan="3">
      <table>
        <tr>
          <td style="border:none;font-size:12px;"><fmt:message key="basicinfo.form.company_type.joint_venture.investor_and_investment_ratio"/> : </td>
          <td style="border:none;"><form:input path="companyType.jointVentureInvestmentRatio" cssStyle="width:300px"/></td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.company_type.business_type"/> : <br></th>
    <td colspan="3">
      <table style="width:80px;">
        <tr>
        <c:if test="${!canChangeBusinessType}">
	          <td style="border:none;">
	            <c:if test="${vendorBasicInfo.companyType.businessType == 0}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 1}">
			      <fmt:message key="basicinfo.form.company_type.business_type.trade"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 2}">
			      <fmt:message key="basicinfo.form.company_type.business_type.common"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 3}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 4}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 5}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 6}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 7}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 8}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 9}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 15}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 14}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 10}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 11}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 12}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 13}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/>
			    </c:if>
			    <c:if test="${vendorBasicInfo.companyType.businessType == 16}">
			      <fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/>
			    </c:if>
			    <form:hidden path="companyType.businessType"/>
	          </td>
          </c:if>
          <c:if test="${canChangeBusinessType}">
	          <td style="border:none;">
		          <form:select path="companyType.businessType" style="width:auto;" class="companyTypeBusinessType">
		            <!-- 
		              <form:option value="0"><fmt:message key="basicinfo.form.company_type.business_type.nontrade"/></form:option>
		             -->
		              <form:option value="10"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_warehouse"/></form:option>
		              <form:option value="11"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_transport"/></form:option>
		              <form:option value="12"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t"/></form:option>
		              <form:option value="13"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_c"/></form:option>
		              <form:option value="14"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_c"/></form:option>
		              <form:option value="15"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_t_c"/></form:option>
		              <form:option value="16"><fmt:message key="basicinfo.form.company_type.business_type.nontrade_w_t_c"/></form:option>
		              <form:option value="1"><fmt:message key="basicinfo.form.company_type.business_type.trade"/></form:option>
		              <form:option value="2"><fmt:message key="basicinfo.form.company_type.business_type.common"/></form:option>
		          </form:select>
	          </td>
          </c:if>
        </tr>
      </table>
    </td>
  </tr>
    <th><fmt:message key="basicinfo.form.company_type.production"/> : <br></th>
    <td colspan="3">
      <table style="width:700px;">
        <tr>
          <td style="border:none;">
            <form:textarea cssStyle="width:100%;height:5em;" path="companyType.production"></form:textarea>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.company_type.business_relation"/> : <br></th>
    <td colspan="3">
      <table>
        <tr>
          <td style="border:none;">
                <form:input cssStyle="width:230px;" path="companyType.businessRelation"/>&nbsp;<span style="color:red;"><fmt:message key="basicinfo.form.company_type.business_relation.suggest"/></span>
          </td>
        </tr>
      </table>
    </td>
  </tr>
  
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.trade_conditions"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><span style="color:red;"><fmt:message key="basicinfo.form.trade_conditions.payment_term_suggest"/> </span><fmt:message key="basicinfo.form.trade_conditions.payment_term"/> : <br>
        <form:errors path="tradeCondition.paymentTerm" cssClass="errors"/>
    </th>
    <td><form:select path="tradeCondition.paymentTerm">
		<form:option value="12"><fmt:message key="basicinfo.form.accounting.payment_term.12"/></form:option>
        <form:option value="13"><fmt:message key="basicinfo.form.accounting.payment_term.13"/></form:option>
        <form:option value="3"><fmt:message key="basicinfo.form.accounting.payment_term.3"/></form:option>
        <form:option value="14"><fmt:message key="basicinfo.form.accounting.payment_term.14"/></form:option>
        <form:option value="15"><fmt:message key="basicinfo.form.accounting.payment_term.15"/></form:option>
        <form:option value="16"><fmt:message key="basicinfo.form.accounting.payment_term.16"/></form:option>
        <form:option value="4"><fmt:message key="basicinfo.form.accounting.payment_term.4"/></form:option>
        <form:option value="5"><fmt:message key="basicinfo.form.accounting.payment_term.5"/></form:option>
        <form:option value="6"><fmt:message key="basicinfo.form.accounting.payment_term.6"/></form:option>
        <form:option value="7"><fmt:message key="basicinfo.form.accounting.payment_term.7"/></form:option>
        <form:option value="8"><fmt:message key="basicinfo.form.accounting.payment_term.8"/></form:option>
        <form:option value="9"><fmt:message key="basicinfo.form.accounting.payment_term.9"/></form:option>
        <form:option value="10"><fmt:message key="basicinfo.form.accounting.payment_term.10"/></form:option>
        <form:option value="11"><fmt:message key="basicinfo.form.accounting.payment_term.11"/></form:option>
      </form:select>
     </td>
    <th><fmt:message key="basicinfo.form.trade_conditions.lead_time"/> : <br>
        <form:errors path="tradeCondition.leadTime" cssClass="errors"/>
    </th>
    <td style="width:240px;"><form:input path="tradeCondition.leadTime" cssErrorClass="input_error"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.trade_conditions.minimum_lot"/> : <br>
        <form:errors path="tradeCondition.minimumLot" cssClass="errors"/>
    </th>
    <td><form:input path="tradeCondition.minimumLot" cssErrorClass="input_error"/></td>
    <th><fmt:message key="basicinfo.form.trade_conditions.delivery_types"/> : <br>
        <form:errors path="tradeCondition.deliveryType" cssClass="errors"/>
    </th>
    <td>
      <table>
        <tr class="delivery_types">
          <td style="border:none;"><form:radiobutton value="0" path="tradeCondition.deliveryType"/><fmt:message key="basicinfo.form.trade_conditions.delivery_types.CIF"/></td>
          <td style="border:none;"><form:radiobutton value="1" path="tradeCondition.deliveryType"/><fmt:message key="basicinfo.form.trade_conditions.delivery_types.FOB"/></td>
          <td style="border:none;"><form:radiobutton value="2" path="tradeCondition.deliveryType"/><fmt:message key="basicinfo.form.trade_conditions.delivery_types.CFR"/></td>
          <td style="border:none;"><form:radiobutton value="3" path="tradeCondition.deliveryType" class="delivery_types_other"/><span class="delivery_types_other_span"><fmt:message key="basicinfo.form.trade_conditions.delivery_types.OTHER"/></span><form:input path="tradeCondition.otherValue" class="delivery_types_other_value" style="display: none; width:45px;" /></td>
        </tr>
      </table>
    </td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.business_information"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th style="white-space:nowrap;"><fmt:message key="basicinfo.form.business_information.number_of_staffs"/> : <br></th>
    <td>
      <table style="width:100%;">
        <tr>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.date"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.number_of_direct_staffs"/><br><form:errors path="businessInformation.numberOfDirectStaffs" cssClass="errors"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.number_of_indirect_staffs"/><br><form:errors path="businessInformation.numberOfIndirectStaffs" cssClass="errors"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.total_staffs"/><br><form:errors path="businessInformation.totalStaffs" cssClass="errors"/></td>
        </tr>
        <tr>
          <td style="border:none;">
            <form:input path="businessInformation.date"/>
          </td>
          <td style="border:none;">
            <form:input path="businessInformation.numberOfDirectStaffs" cssErrorClass="input_error"/>
          </td>
          <td style="border:none;">
            <form:input path="businessInformation.numberOfIndirectStaffs" cssErrorClass="input_error"/>
          </td>
          <td style="border:none;">
            <form:input path="businessInformation.totalStaffs" cssErrorClass="input_error"/>
          </td>          
        </tr>
      </table>
    </td>
  </tr>
  <tr>
    <th style="vertical-align:middle;"><fmt:message key="basicinfo.form.business_information.performance"/> : <br></th>
    <td>
      <table style="width:100%;">
        <tr>
          <td style="border:none;text-align:center;">&nbsp;</td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.year"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.sales_amount"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.profit"/></td>
          <td style="border-bottom:solid 1px black;text-align:center;"><fmt:message key="basicinfo.form.business_information.profitability"/></td>
        </tr>
        <c:if test="${vendorBasicInfo.businessInformation.performance != null}">
          <c:forEach varStatus="status" var="performance" items="${vendorBasicInfo.businessInformation.performance}">
            <tr id="p-<c:out value="${status.index}" />">
              <input value="<c:out value="${performance.bizInfoId}"/>" type="hidden" name="businessInformation.bizInfoId">
              <c:if test="${status.index == 0}">
                <td style="border:none;width:20px;">
                  &nbsp;
                </td>
              </c:if>
              <c:if test="${status.index != 0}">
                <td style="border:none;width:20px;">
                  <a id="p-<c:out value="${status.index}" />" href="#" onClick="delPeformance(this, <c:out value="${performance.bizInfoId}"/>);return false;"><img width="10px" height="10px" src="<c:url value="/images/action_delete.png"/>"/></a>
                </td>
              </c:if>
              <td style="border:none;"><input value="<c:out value="${performance.year}"/>" type="text" name="businessInformation.year"></td>
              <td style="border:none;"><input value="<c:out value="${performance.salesAmount}"/>" type="text" name="businessInformation.salesAmount"></td>
              <td style="border:none;"><input value="<c:out value="${performance.profit}"/>" type="text" name="businessInformation.profit"></td>
              <td style="border:none;"><input value="<c:out value="${performance.profitability}"/>" type="text" name="businessInformation.profitability"></td>
            </tr>
          </c:forEach>
        </c:if>
        <c:if test="${vendorBasicInfo.businessInformation.performance == null}">
          <tr>
            <input type="hidden" value="-1" name="businessInformation.bizInfoId"/>
            <td style="border:none;width:20px;">&nbsp;</td>
            <td style="border:none;"><input type="text" name="businessInformation.year"></td>
            <td style="border:none;"><input type="text" name="businessInformation.salesAmount"></td>
            <td style="border:none;"><input type="text" name="businessInformation.profit"></td>
            <td style="border:none;"><input type="text" name="businessInformation.profitability"></td>
          </tr>
        </c:if>
        <tr id="biz_performance">
		  <td colspan="4" style="border:none;"><a id="add_performance" href="#"><img width="10" height="10" src="<c:url value="/images/list-add.png"/>"/>&nbsp;<fmt:message key="basicinfo.form.business_information.add_performance"/></a></td>
        </tr>
      </table>
    </td>
  </tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.special_statement"/></div>
<table class="basic_form_table">
<tbody>
<tr>
  <td><form:textarea style="width:100%;height:5em;" path="specialStatement"></form:textarea></td>
</tr>
</tbody>
</table>

<div class="index"><fmt:message key="basicinfo.form.bank_information"/>&nbsp;&nbsp;
<a id="add_bank_info" href="#">
  <img width="10" height="10" src="<c:url value="/images/list-add.png"/>"/>
  <span style="font-size:0.8em;font-style:normal;">&nbsp;<fmt:message key="basicinfo.form.bank_information.add_bank"/></span>
</a>
</div>
<div id="banks">
<c:if test="${bankAccounts != null}">

	<c:forEach varStatus="status" var="account" items="${bankAccounts}">
		<c:if test="${status.index == 0}">
			<div id="bank_info" class="bank_area">
		</c:if>
		<c:if test="${status.index != 0}">
			<div id="b-<c:out value="${status.index}"/>" class="bank_area">
		</c:if>
	
		<table class="basic_form_table">
		<input value="<c:out value="${account.bankAccountId}"/>" type="hidden" name="bankInformation.bankAccountId"/>
		<tbody>
		 <c:if test="${status.index != 0}">
		    <tr>
		        <td colspan="3">
		          <a style="margin-right:15px;" href="javascript:void(0)" onclick="openWindowModalDialog('<c:url value="/vendor/search/bank"/>','680','610',this);">
		                <span><fmt:message key="search.bank.title"/></span>
		          </a>
		      </td>
		      <td style="text-align:right;">
		        <a id="b-<c:out value="${status.index}"/>" onClick="delBank(this, <c:out value="${account.bankAccountId}"/>);return false;"><img class="deleteIcon" src="<c:url value="/images/action_delete.png"/>"/><fmt:message key="button.delete"/></a>
		      </td>
		    </tr>
		  </c:if>
		  <c:if test="${status.index == 0}">
		  <tr>
		      <td class="searchBank" colspan="4">
		    <a style="margin-right:15px;" href="javascript:void(0)" onclick="openWindowModalDialog('<c:url value="/vendor/search/bank"/>','680','610',this);">
		        <span><fmt:message key="search.bank.title"/></span>
		    </a>
		      </td>
		    </tr>
		  </c:if>
		  <tr>
		    <th><fmt:message key="basicinfo.form.bank_information.vendor_subcode"/> : <br>
		    <c:if test="${account.hasRequiredErrorOnVendorSubcode}">
		      <span class="errors"><fmt:message key="error.required"/></span>
		    </c:if>
		    </th>
		    <td><input class='vendorSubcode <c:if test="${account.hasRequiredErrorOnVendorSubcode}">input_error</c:if>' value="<c:out value="${account.vendorSubcode}"/>" type="text" name="bankInformation.vendorSubcode" readonly="true" style="border:0px !important;"/></td>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.province_and_region"/> : <br>
		    <c:if test="${account.hasRequiredErrorOnProvinceAndRegion}">
		      <span class="errors"><fmt:message key="error.required"/></span>
		    </c:if>
		    </th>
		    <td style="width:240px;">
		      <input class="provinceAndRegion" <c:if test="${account.hasRequiredErrorOnProvinceAndRegion}">class="input_error"</c:if> value="<c:out value="${account.provinceAndRegion}"/>" type="text" name="bankInformation.provinceAndRegion"/>
		    </td>
		  </tr>
		
		  <tr>
		    <th><fmt:message key="basicinfo.form.bank_information.swift_code"/> : <br>
		    <c:if test="${account.hasRequiredErrorOnSwiftCode}">
		      <span class="errors"><fmt:message key="error.required"/></span>
		    </c:if>
		    </th>
		    <td><input class="swiftCode" <c:if test="${account.hasRequiredErrorOnSwiftCode}">class="input_error"</c:if> value="<c:out value="${account.swiftCode}"/>" type="text" name="bankInformation.swiftCode"/></td>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.name"/> : <br>
		    <c:if test="${account.hasRequiredErrorOnName}">
		      <span class="errors"><fmt:message key="error.required"/></span><br>
		    </c:if>
		    <c:if test="${account.hasNoExistErrorOnName}">
		      <span class="errors"><fmt:message key="error.noexist"/></span><br>
		    </c:if>
		    
		    </th>
		    <td><input class="bankName" <c:if test="${account.hasRequiredErrorOnName}">class="input_error"</c:if>
		               <c:if test="${account.hasNoExistErrorOnName}">class="input_error"</c:if> 
		            value="<c:out value="${account.name}"/>" type="text" name="bankInformation.name"/></td>
		  </tr>
		  <tr>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.account"/> : <br>
		    <c:if test="${account.hasRequiredErrorOnAccount}">
		      <span class="errors"><fmt:message key="error.required"/></span>
		    </c:if>
		    </th>
		    <td><input class= "bankAccount" <c:if test="${account.hasRequiredErrorOnAccount}">class="input_error"</c:if> value="<c:out value="${account.account}"/>" type="text" name="bankInformation.account"/></td>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.city"/> : <br></th>
		    <td>
		      <select name="bankInformation.city">
		        <option value="0" <c:if test="${account.city == 0}">selected</c:if>><fmt:message key="basicinfo.form.bank_information.city.others"/></option>
		        <option value="1" <c:if test="${account.city == 1}">selected</c:if>><fmt:message key="basicinfo.form.bank_information.city.shanghai"/></option>
		      </select>
		    </td>
		  </tr>
		  <tr>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.currency"/> : <br></th>
		    <td>
		      <select name="bankInformation.currency">
		        <option value="RMB" <c:if test="${account.currency == 'RMB'}">selected</c:if>>CNY</option>
		        <option value="USD" <c:if test="${account.currency == 'USD'}">selected</c:if>>USD</option>
		        <option value="JPY" <c:if test="${account.currency == 'JPY'}">selected</c:if>>JPY</option>
		        <option value="HKD" <c:if test="${account.currency == 'HKD'}">selected</c:if>>HKD</option>
		      </select>
		    </td>
		    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.type"/> : <br></th>
		    <td>
		      <select name="bankInformation.type" class="bankInformationType">
		        <option value="0" <c:if test="${account.type == 0}">selected</c:if>><fmt:message key="basicinfo.form.bank_information.type.bank_of_china"/></option>
		        <option value="1" <c:if test="${account.type == 1}">selected</c:if>><fmt:message key="basicinfo.form.bank_information.type.other"/></option>
		      </select>
		    </td>
		  </tr>
		  <tr class="institutionJointCode">
		    <th><fmt:message key="basicinfo.form.bank_information.institution_no"/> : <br>
		      <c:if test="${account.hasRequiredErrorOnInstitutionNo}">
		        <span class="errors"><fmt:message key="error.required"/></span><br>
		      </c:if>
		      <c:if test="${account.hasNoExistErrorOnInstitutionNo}">
		        <span class="errors"><fmt:message key="error.noexist"/></span>
		      </c:if>
		    </th>
		    <td><input class="institution_no institutionNo" 
		    			<c:if test="${account.hasRequiredErrorOnInstitutionNo}">class="input_error"</c:if>
		    	        <c:if test="${account.hasNoExistErrorOnInstitutionNo}">class="input_error"</c:if>
		    	        <c:if test="${account.type == 1}">readonly </c:if>
		    		     value="<c:out value="${account.institutionNo}"/>" type="text" name="bankInformation.institutionNo"/></td>
		    <th><fmt:message key="basicinfo.form.bank_information.joint_code"/> : <br>
		      <c:if test="${account.hasRequiredErrorOnJointCode}">
		        <span class="errors"><fmt:message key="error.required"/></span><br>
		      </c:if>
		      <c:if test="${account.hasNoExistErrorOnJointCode}">
		        <span class="errors"><fmt:message key="error.noexist"/></span>
		      </c:if>
		    </th>
		    <td><input class="jointCode" 
		    			<c:if test="${account.hasRequiredErrorOnJointCode}">class="input_error"</c:if>
		    	        <c:if test="${account.hasNoExistErrorOnJointCode}">class="input_error"</c:if>
		    	        <c:if test="${account.type == 1}">readonly </c:if>
		    		    value="<c:out value="${account.jointCode}"/>" type="text" name="bankInformation.jointCode"/></td>
		  </tr>
		  <tr class="cnapsTr">
		    <th>CNAPS : <br>
		      <c:if test="${account.hasRequiredErrorOnCnaps}">
		        <span class="errors"><fmt:message key="error.required"/></span><br>
		      </c:if>
		      <c:if test="${account.hasNoExistErrorOnCnaps}">
		        <span class="errors"><fmt:message key="error.noexist"/></span>
		      </c:if>
		    </th>
		    <td><input class="cnaps" 
		    		   <c:if test="${account.hasRequiredErrorOnCnaps}">class="input_error"</c:if>
		    	       <c:if test="${account.hasNoExistErrorOnCnaps}">class="input_error"</c:if> 
		    	       <c:if test="${account.type == 0}">readonly </c:if>
		    			value="<c:out value="${account.cnaps}"/>" type="text" name="bankInformation.cnaps"/></td>
		    <th colspan="2">&nbsp;</th>
		  </tr>  
		</tbody>
		</table>
	</div>
	</c:forEach>
</c:if>
<c:if test="${bankAccounts == null}">
<div id="bank_info" class="bank_area">
<table class="basic_form_table">
<input type="hidden" value="-1" name="bankInformation.bankAccountId"/>
<tbody>
  <tr>
      <td class="searchBank" colspan="4"> 
    <a style="margin-right:15px;" href="javascript:void(0)" onclick="openWindowModalDialog('<c:url value="/vendor/search/bank"/>','680','610',this);">
        <span><fmt:message key="search.bank.title"/></span>
    </a>
      </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.vendor_subcode"/> : <br>    </th>
    <td><form:input path="bankInformation.vendorSubcode" cssClass="vendorSubcode" value="00" readonly="true" style="border:0px !important;"/></td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.province_and_region"/> : <br></th>
    <td style="width:240px;"><form:input path="bankInformation.provinceAndRegion"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.bank_information.swift_code"/> : <br></th>
    <td><form:input path="bankInformation.swiftCode"/></td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.name"/> : <br></th>
    <td><form:input path="bankInformation.name"/></td>
  </tr>
  <tr>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.account"/> : <br></th>
    <td><form:input path="bankInformation.account"/></td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.city"/> : <br></th>
    <td>
      <form:select path="bankInformation.city" multiple="false">
        <form:option value="0"><fmt:message key="basicinfo.form.bank_information.city.others"/></form:option>
        <form:option value="1"><fmt:message key="basicinfo.form.bank_information.city.shanghai"/></form:option>
      </form:select>
    </td>
  </tr>
  <tr>
     <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.currency"/> : <br></th>
    <td>
      <form:select path="bankInformation.currency" multiple="false">
        <form:option value="RMB">CNY</form:option>
        <form:option value="USD">USD</form:option>
        <form:option value="JPY">JPY</form:option>
        <form:option value="HKD">HKD</form:option>
      </form:select>
    </td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.bank_information.type"/> : <br></th>
    <td>
      <form:select path="bankInformation.type" multiple="false">
        <form:option value="0"><fmt:message key="basicinfo.form.bank_information.type.bank_of_china"/></form:option>
        <form:option value="1"><fmt:message key="basicinfo.form.bank_information.type.other"/></form:option>
      </form:select>
    </td>
  </tr>
  <tr class="institutionJointCode">
    <th><fmt:message key="basicinfo.form.bank_information.institution_no"/> : <br></th>
    <td><form:input path="bankInformation.institutionNo" class="institutionNo" /></td>
    <th><fmt:message key="basicinfo.form.bank_information.joint_code"/> : <br></th>
    <td><form:input path="bankInformation.jointCode" class="jointCode" /></td>
  </tr>
  <tr class="cnapsTr">
    <th>CNAPS : </th>
    <td><form:input path="bankInformation.cnaps" readonly="true" class="cnaps" /></td>
    <th colspan="2">&nbsp;</th>
  </tr>
</tbody>
</table>
</div>
</c:if>
</div>

<div class="index"><span style="color:red;">* </span><fmt:message key="basicinfo.form.attachment"/>
	<a style="margin-right:15px;" href="javascript:void(0)" onclick="openWindowModalDialog('<c:url value="/vendor/attachmentInfo"/>','680','610',this);">
	    <span><fmt:message key="basicinfo.form.attachment.info"/></span>
	</a>
<c:if test="${attachmentsIsRequired}"><span class="errors" style="font-size: 12px;"><fmt:message key="error.required"/></span></c:if>
</div>
<table class="basic_form_table"">
<tbody>
  <tr>
    <th style="text-align:left;">
      <input id="attachment" name="file" type="file" class="<c:if test='${attachmentsIsRequired}'>input_error</c:if>"/>
      <c:forEach var="attachment" items="${attachments}">
      <div id="a-<c:out value="${attachment.id}"/>">
        <table>
          <tr><td rowspan="4" style="border:none;">
            <a class="a-<c:out value="${attachment.id}"/>" onClick="delAttachment(this, '<c:out value='${attachment.id}'/>', '<c:out value='${vendorBasicInfo.tranId}'/>');return false;"><img src="<c:url value="/images/action_delete.png"/>"/></a>
          </td></tr>
          <tr><td style="border:none;">Content Type : <c:out value="${attachment.contentType}" /></td></tr>
          <tr><td style="border:none;">Name : <c:out value="${attachment.name}" /><br></td></tr>
          <tr><td style="border:none;">Size : <c:out value="${attachment.size}" /><br></td></tr>
        </table>
        <hr>
       </div>      
     </c:forEach>
    </th>
  </tr>
</tbody>
</table>

<p></p>
<button type="submit"><fmt:message key="button.edit"/></button>
<p></p>

<sec:authorize access="hasRole('ROLE_HD')">
<div class="index"><fmt:message key="basicinfo.form.accounting"/></div>
<table class="basic_form_table">
<tbody>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.company_code"/> : <br></th>
    <td>
      <form:select path="accounting.companyCode">
      	<form:option value="SGD1"><fmt:message key="basicinfo.form.accounting.company_code.sgd1"/></form:option>
      	<form:option value="SJS1"><fmt:message key="basicinfo.form.accounting.company_code.sjs1"/></form:option>
      	<form:option value="SSZ1"><fmt:message key="basicinfo.form.accounting.company_code.ss10"/></form:option>
        <form:option value="SSZ2"><fmt:message key="basicinfo.form.accounting.company_code.ss11"/></form:option>
        <form:option value="SSH1"><fmt:message key="basicinfo.form.accounting.company_code.sscs"/></form:option>
        <form:option value="SSD1"><fmt:message key="basicinfo.form.accounting.company_code.ssd1"/></form:option>
      </form:select>
    </td>
    <th><fmt:message key="basicinfo.form.accounting.vendor_account_type"/> : <br></th>
    <td style="width:240px;">
      <form:select path="accounting.vendorAccountType">
        <form:option value="vendor"><fmt:message key="basicinfo.form.accounting.vendor_account_type.vendor"/></form:option>
      	<form:option value="CompanyCard"><fmt:message key="basicinfo.form.accounting.vendor_account_type.companyCard"/></form:option>
        <form:option value="SalaryCard"><fmt:message key="basicinfo.form.accounting.vendor_account_type.salaryCard"/></form:option>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.vendor_group"/> : <br></th>
    <td>
      <form:select path="accounting.vendorGroup">
        <form:option value="Z001"><fmt:message key="basicinfo.form.accounting.vendor_group.z001"/></form:option>
        <form:option value="ZFGV"><fmt:message key="basicinfo.form.accounting.vendor_group.zfgv"/></form:option>
        <form:option value="ZLAD"><fmt:message key="basicinfo.form.accounting.vendor_group.zlad"/></form:option>
        <form:option value="ZLOV"><fmt:message key="basicinfo.form.accounting.vendor_group.zlov"/></form:option>
        <form:option value="ZLRA"><fmt:message key="basicinfo.form.accounting.vendor_group.zlra"/></form:option>
        <form:option value="ZLRO"><fmt:message key="basicinfo.form.accounting.vendor_group.zlro"/></form:option>
        <form:option value="ZLSV"><fmt:message key="basicinfo.form.accounting.vendor_group.zlsv"/></form:option>
        <form:option value="ZLVO"><fmt:message key="basicinfo.form.accounting.vendor_group.zlvo"/></form:option>
        <form:option value="ZOTV"><fmt:message key="basicinfo.form.accounting.vendor_group.zotv"/></form:option>
        <form:option value="ZSAP"><fmt:message key="basicinfo.form.accounting.vendor_group.zsap"/></form:option>
        <form:option value="ZSCN"><fmt:message key="basicinfo.form.accounting.vendor_group.zscn"/></form:option>
        <form:option value="ZSO1"><fmt:message key="basicinfo.form.accounting.vendor_group.zso1"/></form:option>
        <form:option value="ZSON"><fmt:message key="basicinfo.form.accounting.vendor_group.zson"/></form:option>
      </form:select>
    </td>
    <th><span style="color:red;">* </span><fmt:message key="basicinfo.form.accounting.group_key"/> : <br>
    	<form:errors path="accounting.groupKey" cssClass="errors"/>
    </th>
    <td><form:input path="accounting.groupKey" cssErrorClass="input_error"/></td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.recon_account"/> : <br></th>
    <td>
      <form:select path="accounting.reconAccount">
        <form:option value="21001000"><fmt:message key="basicinfo.form.accounting.recon_account.21001000"/></form:option>
        <form:option value="21899005"><fmt:message key="basicinfo.form.accounting.recon_account.21899005"/></form:option>
      </form:select>
    </td>
    <th><fmt:message key="basicinfo.form.accounting.payment_term"/> : <br></th>
    <td>
      <form:select path="accounting.paymentTerm">
<!--       
        <form:option value="1"><fmt:message key="basicinfo.form.accounting.payment_term.1"/></form:option>
        <form:option value="2"><fmt:message key="basicinfo.form.accounting.payment_term.2"/></form:option>
-->
		<form:option value="12"><fmt:message key="basicinfo.form.accounting.payment_term.12"/></form:option>
        <form:option value="13"><fmt:message key="basicinfo.form.accounting.payment_term.13"/></form:option>
        <form:option value="3"><fmt:message key="basicinfo.form.accounting.payment_term.3"/></form:option>
        <form:option value="14"><fmt:message key="basicinfo.form.accounting.payment_term.14"/></form:option>
        <form:option value="15"><fmt:message key="basicinfo.form.accounting.payment_term.15"/></form:option>
        <form:option value="16"><fmt:message key="basicinfo.form.accounting.payment_term.16"/></form:option>
        <form:option value="4"><fmt:message key="basicinfo.form.accounting.payment_term.4"/></form:option>
        <form:option value="5"><fmt:message key="basicinfo.form.accounting.payment_term.5"/></form:option>
        <form:option value="6"><fmt:message key="basicinfo.form.accounting.payment_term.6"/></form:option>
        <form:option value="7"><fmt:message key="basicinfo.form.accounting.payment_term.7"/></form:option>
        <form:option value="8"><fmt:message key="basicinfo.form.accounting.payment_term.8"/></form:option>
        <form:option value="9"><fmt:message key="basicinfo.form.accounting.payment_term.9"/></form:option>
        <form:option value="10"><fmt:message key="basicinfo.form.accounting.payment_term.10"/></form:option>
        <form:option value="11"><fmt:message key="basicinfo.form.accounting.payment_term.11"/></form:option>
      </form:select>
    </td>
  </tr>
  <tr>
    <th><fmt:message key="basicinfo.form.accounting.chk_double_inv"/> : <br></th>
    <td>
      <form:select path="accounting.chkDoubleInv">
        <form:option value="true"><fmt:message key="basicinfo.yes"/></form:option>
        <form:option value="false"><fmt:message key="basicinfo.no"/></form:option>
      </form:select>
    </td>
    <th colspan="2">&nbsp;</th>
  </tr>
</tbody>
</table>
</sec:authorize>
</div>
</form:form>
</body>
</html>